let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

// JS
mix.copy([
 'resources/assets/js/tratamientos-paciente/edit-add.js',
 'resources/assets/js/tratamientos-paciente/Pago.js',
 'resources/assets/js/tratamientos-paciente/PlanTratamiento.js',
 'resources/assets/js/tratamientos-paciente/citas.js',
], 'public/js/tratamientos-paciente');

mix.copy([
 'resources/assets/js/programaciones/agenda.js',
 'resources/assets/js/programaciones/calendar.js',
], 'public/js/programaciones');

mix.copy([
 'node_modules/fullcalendar/dist/fullcalendar.min.js',
 'node_modules/fullcalendar/dist/locale/es.js',
], 'public/js/fullcalendar');

mix.copy([
 'node_modules/moment/min/moment.min.js',
], 'public/js/moment');

mix.copy([
  'node_modules/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
], 'public/js/datetimepicker');

mix.copy([
 'node_modules/jspdf/dist/jspdf.min.js',
], 'public/js/jspdf');

// CSS
mix.copy([
  'node_modules/fullcalendar/dist/fullcalendar.min.css',
  'node_modules/fullcalendar/dist/fullcalendar.print.min.css'
], 'public/css/fullcalendar');

mix.copy([
  'node_modules/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
], 'public/css/datetimepicker');

mix.copy([
  'node_modules/@fortawesome/fontawesome-free/css/all.css',
], 'public/css/fontawesome');
