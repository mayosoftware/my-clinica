<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Presupuesto Nº {{ $presupuesto->numero}}</title>
    {{-- <link rel="stylesheet" href="{{ asset('/css/pdf/style.css') }}"> --}}

    {{-- <style media="screen">
    body {
      background: url("/presu_marca_agua.jpg") no-repeat center center fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;

      background-size: 50%;
    }
    </style> --}}

        <style>
      .clearfix:after {
        content: "";
        display: table;
        clear: both;
      }

      a {
        color: #5D6975;
        text-decoration: underline;
      }

      body {
        position: relative;
        width: 18cm;
        height: 29.7cm;
        margin: 0 auto;
        color: #001028;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 12px;
        font-family: Arial;
      }

      header {
        padding: 10px 0;
        margin-bottom: 30px;

      }

      #logo {
        text-align: center;
        margin-bottom: 10px;
      }

      #logo img {
        width: 90px;
      }

      h1 {
        border-top: 1px solid  #5D6975;
        border-bottom: 1px solid  #5D6975;
        color: #5D6975;
        font-size: 2.4em;
        line-height: 1.4em;
        font-weight: normal;
        text-align: center;
        margin: 0 0 20px 0;
        background: url(dimension.png);
      }

      #project {
        float: left;
      }

      #project span {
        color: #5D6975;
        text-align: right;
        width: 52px;
        margin-right: 10px;
        /* display: inline-block; */
        font-size: 1em;
      }

      #company {

        text-align: right;
      }

      #project div,
      #company div {
        white-space: nowrap;
      }

      table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
      }

      table tr:nth-child(2n-1) td {
        background: #F5F5F5;
      }

      table th,
      table td {
        text-align: center;
      }

      table th {
        padding: 5px 20px;
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
      }

      table .service,
      table .desc {
        text-align: left;

      }

      table td {
        padding: 10px;
        text-align: right;
      }

      table td.service,
      table td.desc {
        vertical-align: top;
        width: 50%;
      }

      table td.unit,
      table td.qty,
      table td.total {
        font-size: 1.2em;
      }

      table td.grand {
        border-top: 1px solid #5D6975;;
      }

      #notices .notice {
        color: #5D6975;
        font-size: 1.2em;
      }

      footer {
        color: #5D6975;
        width: 100%;
        height: 30px;
        position: absolute;
        bottom: 0;
        border-top: 1px solid #C1CED9;
        padding: 8px 0;
        text-align: center;

      }

      .pagebreak { page-break-before: always; }

    </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{ Voyager::image( Voyager::setting("admin.clinic_image") )}}">
        <h2>{{ setting('admin.clinic_name') }}</h2>
      </div>
      <h1>Presupuesto Nº {{ $presupuesto->numero}}</h1>
      <div id="company" class="clearfix">
        <div>{{ setting('admin.clinic_name') }}</div>
        <div>CIF: {{ setting('admin.clinic_cif') }}</div>
        <div>{{ setting('admin.clinic_address') }}</div>
        <div>{{ setting('admin.clinic_location') }}</div>
        <div>{{ setting('admin.clinic_phone') }}</div>
      </div>
      <div id="project">
        @if ($presupuesto->cif)
            <div><span>CIF</span> {{ $presupuesto->cif }}</div>
        @endif
        <div><span>DNI</span> {{ $presupuesto->paciente->nif }}</div>
        <div><span>NOMBRE</span> {{ $presupuesto->nombre_paciente }}</div>
        <div><span>DOMICILIO</span> {{ $presupuesto->direccion }}</div>
        <div><span>EMAIL</span> {{ $presupuesto->email }}</div>
        <div><span>FECHA</span> {{ $presupuesto->fechaFormat() }}</div>
      </div>
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th>DIENTE</th>
            <th>UND.</th>
            <th class="desc">TRATAMIENTO</th>
            <th>PRECIO UND.</th>
            <th>DTO %</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($presupuesto->lineas as $linea)
            <tr>
              <td class="qty">{{ $linea->pieza ?? '' }}</td>
              <td class="qty">{{ $linea->cantidad }}</td>
              <td class="desc">{{ $linea->descripcion }}</td>
              <td class="unit">{{ number_format($linea->importe_unitario, 2) }}</td>
              <td class="unit">{{ number_format($linea->porcentaje_descuento, 2) }}</td>
              <td class="total">{{ number_format($linea->total, 2) }}</td>
            </tr>
          @endforeach
          <tr>
            <td colspan="5">SUBTOTAL</td>
            <td class="total">{{ number_format($presupuesto->total + $presupuesto->total_descuento, 2) }}</td>
          </tr>
          <tr>
            <td colspan="5">Importe descuento</td>
            <td class="total">{{ number_format($presupuesto->total_descuento, 2) }}</td>
          </tr>
          <tr>
            <td colspan="5" class="grand total">TOTAL</td>
            <td class="grand total">{{ number_format($presupuesto->total, 2) }}</td>
          </tr>
        </tbody>
      </table>
      <div id="notices">
        <div class="notice">Este presupuesto puede estar sujeto a modificaciones a lo largo del tratamiento según el criterio del odontólogo</div>
        <div class="notice">Los importes estipulados tienen vigencia de 30 días</div>
        <div class="notice">Todos los trabajos tienen garantía</div>

      </div>
    </main>
    <footer>
      Inscrita en el Registro Mercantil de A Coruña en el tomo 344 del Archivo Sección General, al Folio 76 Hoja Nº H-22.848 inscripción 2ª
    </footer>
  </body>
</html>
