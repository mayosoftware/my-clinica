<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Tratamiento</title>
    {{-- <link rel="stylesheet" href="{{ asset('/css/pdf/style.css') }}"> --}}

    <style>
      .clearfix:after {
        content: "";
        display: table;
        clear: both;
      }

      a {
        color: #5D6975;
        text-decoration: underline;
      }

      body {
        position: relative;
        width: 18cm;
        height: 29.7cm;
        margin: 0 auto;
        color: #001028;
        background: #FFFFFF;
        font-family: Arial, sans-serif;
        font-size: 12px;
        font-family: Arial;
      }

      header {
        padding: 10px 0;
        margin-bottom: 30px;

      }

      #logo {
        text-align: center;
        margin-bottom: 10px;
      }

      #logo img {
        width: 90px;
      }

      h1 {
        border-top: 1px solid  #5D6975;
        border-bottom: 1px solid  #5D6975;
        color: #5D6975;
        font-size: 2.4em;
        line-height: 1.4em;
        font-weight: normal;
        text-align: center;
        margin: 0 0 20px 0;
        background: url(dimension.png);
      }

      #project {
        float: left;
      }

      #project span {
        color: #5D6975;
        text-align: right;
        width: 52px;
        margin-right: 10px;
        /* display: inline-block; */
        font-size: 1em;
      }

      #company {

        text-align: right;
      }

      #project div,
      #company div {
        white-space: nowrap;
      }

      table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
      }

      table tr:nth-child(2n-1) td {
        background: #F5F5F5;
      }

      table th,
      table td {
        text-align: center;
      }

      table th {
        padding: 5px 20px;
        color: #5D6975;
        border-bottom: 1px solid #C1CED9;
        white-space: nowrap;
        font-weight: normal;
      }

      table .service,
      table .desc {
        text-align: left;

      }

      table td {
        padding: 10px;
        text-align: right;
      }

      table td.service,
      table td.desc {
        vertical-align: top;
        width: 50%;
      }

      table td.unit,
      table td.qty,
      table td.total {
        font-size: 1.2em;
      }

      table td.grand {
        border-top: 1px solid #5D6975;;
      }

      #notices .notice {
        color: #5D6975;
        font-size: 1.2em;
      }

      footer {
        color: #5D6975;
        width: 100%;
        height: 30px;
        position: absolute;
        bottom: 0;
        border-top: 1px solid #C1CED9;
        padding: 8px 0;
        text-align: center;

      }

      .pagebreak { page-break-before: always; }

    </style>
  </head>
  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="{{ Voyager::image( Voyager::setting("admin.clinic_image") )}}">
        <h2>{{ setting('admin.clinic_name') }}</h2>
      </div>
      <h1>Tratamiento</h1>
      <div id="project">
        <h4>Paciente:</h4>
        @if ($tratamiento->paciente->cif)
            <div><span>CIF</span> {{ $tratamiento->paciente->cif }}</div>
        @endif
        <div><span>CÓDIGO</span> {{ $tratamiento->paciente->codigo }}</div>
        <div><span>NOMBRE</span> {{ $tratamiento->paciente->nombre }}</div>
        <div><span>DOMICILIO</span> {{ $tratamiento->paciente->direccion }}</div>
        <div><span>EMAIL</span> {{ $tratamiento->paciente->email }}</div>
        <div><span>FECHA</span> {{ $tratamiento->created_at }}</div>
      </div>
    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th>DIENTE</th>
            <th>CANTIDAD</th>
            <th class="desc">TRATAMIENTO</th>
            <th>PRECIO UNIDAD €</th>
            <th>DTO %</th>
            <th>TOTAL €</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($tratamiento->lineas as $linea)
            <tr>
              <td class="qty">{{ $linea->pieza ?? '' }}</td>
              <td class="qty">{{ $linea->cantidad }}</td>
              <td class="desc">{{ $linea->descripcion }}</td>
              <td class="unit">{{ $linea->importe_unitario }}</td>
              <td class="unit">{{ $linea->porcentaje_descuento }}</td>
              <td class="total">{{ $linea->total }}</td>
            </tr>
          @endforeach
          <tr>
            <td colspan="5">SUBTOTAL</td>
            <td class="total">{{ $tratamiento->total - $tratamiento->total_iva }}</td>
          </tr>
          <tr>
            <td colspan="5">Importe descuento</td>
            <td class="total">{{ $tratamiento->total_descuento }}</td>
          </tr>
          <tr>
            <td colspan="5" class="grand total">TOTAL</td>
            <td class="grand total">{{ $tratamiento->total }}</td>
          </tr>
        </tbody>
      </table>
      {{-- <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
      </div> --}}
    </main>
    <footer>
      Inscrita en el Registro Mercantil de A Coruña en el tomo 344 del Archivo Sección General, al Folio 76 Hoja Nº H-22.848 inscripción 2ª
    </footer>
  </body>
</html>
