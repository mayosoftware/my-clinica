<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Impresión de etiquetas</title>
    <link rel="stylesheet" href="{{ asset('css/pdf/etiquetas.css') }}">
  </head>
  <body>
    @php
        // echo "DAtos: " . count($data);
        $tablesCount = intval(ceil(count($data) / 16));
    @endphp

    @for ($i=0; $i < $tablesCount; $i++)
      @php
        $max = min(16 + (16 * $i),
          count($data));

        // echo "Tabla: " . $i;
        // echo "max: " . $max;
      @endphp
      <table>
          @for ($key=(16*$i); $key < $max; $key++)
            @php
              $value = $data[$key];
            @endphp
            @if ($value != null)

              @if ($key % 2 == 0)
                <tr>
                  <td>
                      <br> {{ strtoupper($value->nombre) }}
                      <br> {{ strtoupper($value->direccion) }}
                      <br> {{ $value->codigo_postal ?? '' }} {{ $value->poblacion ? strtoupper($value->poblacion) : '' }}
                      <br> {{ $value->provincia ? strtoupper($value->provincia) : '' }}
                    {{-- </div> --}}
                  </td>
                  @if ($key == $max)
                    <td></td>
                  </tr>
                  @endif
              @else
                  <td>
                    <br> {{ strtoupper($value->nombre) }}
                    <br> {{ strtoupper($value->direccion) }}
                    <br> {{ $value->codigo_postal ?? '' }} {{ $value->poblacion ? strtoupper($value->poblacion) : '' }}
                    <br> {{ $value->provincia ? strtoupper($value->provincia) : '' }}
                    {{-- </div> --}}
                  </td>
                </tr>
              @endif
            @endif
          @endfor
      </table>
      @if ($i != $tablesCount - 1)
          <div style="page-break-after: always;"></div>
      @endif

    @endfor
  </body>

</html>
