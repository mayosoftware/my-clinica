<!DOCTYPE html>
<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Movimientos de caja</title>
    <link rel="stylesheet" href="{{ asset('/css/pdf/style.css') }}">
  </head>
  <body>
    <header>

    <h2 style="text-align: center">MOVIMIENTOS DE CAJA</h2>

    <h3 style="text-align: right">Fecha y hora: {{ $date }}</h3>

    <h3 style="text-align: left">{{ $msgRango }}</h3>

    </header>
    <main>
      <table>
        <thead>
          <tr>
            <th>Fecha</th>
            <th class="desc">Concepto</th>
            <th class="desc">Paciente</th>
            <th class="service">Pago</th>
            <th>Entradas</th>
            <th>Salidas</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($data as $key => $value)
            <tr>
              <td class="service">{{ $value->fecha->format('d/m/Y') }}</td>
              <td class="desc">{{ $value->concepto }}</td>
              <td class="desc">{{ $value->tratamiento->paciente->nombre }}</td>
              <td class="service">{{ $value->tipoCobro->nombre }}</td>
              <td class="unit">{{ $value->entrada }} €</td>
              <td class="unit">{{ $value->salida }} €</td>
            </tr>
          @endforeach
          <tr>
            <td colspan="4" class="grand total">TOTAL</td>
            <td class="grand total">{{ $totalEntradas }} €</td>
            <td class="grand total">{{ $totalSalidas }} €</td>
          </tr>
        </tbody>
      </table>
      <div id="notices">
        <table>
          <tr>
            <td class="desc"><strong>Total efectivo:</strong></td>
            <td class="total">{{ $totalEfectivo }} €</td>
            <td class="desc"><strong>Total tarjeta:</strong></td>
            <td class="total">{{ $totalTarjeta }} €</td>
            <td class="desc"><strong>Total financiacion:</strong></td>
            <td class="total">{{ $totalFinanciacion }} €</td>
          </tr>
        </table>
      </div>
    </main>

  </body>
</html>
