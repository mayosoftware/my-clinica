  <!DOCTYPE html>
  <html lang="{{ config('app.locale') }}" @if (config('voyager.multilingual.rtl')) dir="rtl" @endif>
  <head>
      <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}"/>

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

      <!-- Favicon -->
      <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/x-icon">

      <!-- App CSS -->
      <link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">

      @yield('css')
      @if(config('voyager.multilingual.rtl'))
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
          <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
      @endif

      <!-- Few Dynamic Styles -->
      <style type="text/css">
          .voyager .side-menu .navbar-header {
              background:{{ config('voyager.primary_color','#22A7F0') }};
              border-color:{{ config('voyager.primary_color','#22A7F0') }};
          }
          .widget .btn-primary{
              border-color:{{ config('voyager.primary_color','#22A7F0') }};
          }
          .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
              background:{{ config('voyager.primary_color','#22A7F0') }};
          }
          .voyager .breadcrumb a{
              color:{{ config('voyager.primary_color','#22A7F0') }};
          }

          .holiday {
              background: red !important; 
              color: white !important;
          }
      </style>

      @if(!empty(config('voyager.additional_css')))<!-- Additional CSS -->
          @foreach(config('voyager.additional_css') as $css)<link rel="stylesheet" type="text/css" href="{{ asset($css) }}">@endforeach
      @endif

      {{-- Custom CSS --}}
      {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.print.min.css" media="print"> --}}
      {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css"> --}}
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

      <link rel="stylesheet" type="text/css" href="{{asset('css/fullcalendar/fullcalendar.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{asset('css/fullcalendar/fullcalendar.print.min.css') }}" media="print">
      <link rel="stylesheet" type="text/css" href="{{asset('css/datetimepicker/bootstrap-datetimepicker.min.css') }}">

      <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-notifications.min.css') }}">
      {{-- TODO: Ver porque da error: --}}
      {{-- <link rel="stylesheet" type="text/css" href="{{asset('css/fontawesome/all.css') }}" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> --}}
      {{-- Custom styles --}}

      <link rel="stylesheet" type="text/css" href="{{asset('css/style.css') }}">
      {{-- End Custom CSS --}}

      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      @yield('head')
  </head>

  <body class="voyager @if(isset($dataType) && isset($dataType->slug)){{ $dataType->slug }}@endif">

  <div id="voyager-loader">
      <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
      @if($admin_loader_img == '')
          <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
      @else
          <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
      @endif
  </div>

  <?php
  if (starts_with(Auth::user()->avatar, 'http://') || starts_with(Auth::user()->avatar, 'https://')) {
      $user_avatar = Auth::user()->avatar;
  } else {
      $user_avatar = Voyager::image(Auth::user()->avatar);
  }
  ?>

  <div class="app-container">
      <div class="fadetoblack visible-xs"></div>
      <div class="row content-container">
          @include('partials.navbar')
          @include('voyager::dashboard.sidebar')
          <script>
              (function(){
                      var appContainer = document.querySelector('.app-container'),
                          sidebar = appContainer.querySelector('.side-menu'),
                          navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                          loader = document.getElementById('voyager-loader'),
                          hamburgerMenu = document.querySelector('.hamburger'),
                          sidebarTransition = sidebar.style.transition,
                          navbarTransition = navbar.style.transition,
                          containerTransition = appContainer.style.transition;

                      sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                      appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                      navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                      if (window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                          appContainer.className += ' expanded no-animation';
                          loader.style.left = (sidebar.clientWidth/2)+'px';
                          hamburgerMenu.className += ' is-active no-animation';
                      }

                     navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                     sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                     appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
              })();
          </script>
          <!-- Main Content -->
          <div class="container-fluid">
              <div class="side-body padding-top">
                  @yield('page_header')
                  <div id="voyager-notifications"></div>
                  @yield('content')
              </div>
          </div>
      </div>
  </div>
  @include('partials.footer')

  <!-- Javascript Libs -->
  {{-- Se añade el app del proyecto, principalmente para utilizar vue --}}
  {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
  <script type="text/javascript" src="{{ voyager_asset('js/app.js') }}"></script>


  {{-- Custom JS --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script> --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/locale/es.js"></script> --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script> --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/locale/es.js"></script> --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> --}}
  {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script> --}}

  <script type="text/javascript" src="{{ asset('js/moment/moment.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/fullcalendar/fullcalendar.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/fullcalendar/es.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/datetimepicker/bootstrap-datetimepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/jspdf/jspdf.min.js')}}"></script>

  {{-- End Custom JS --}}

  <script>
      @if(Session::has('alerts'))
          let alerts = {!! json_encode(Session::get('alerts')) !!};
          helpers.displayAlerts(alerts, toastr);
      @endif

      @if(Session::has('message'))

      // TODO: change Controllers to use AlertsMessages trait... then remove this
      var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
      var alertMessage = {!! json_encode(Session::get('message')) !!};
      var alerter = toastr[alertType];

      if (alerter) {
          alerter(alertMessage);
      } else {
          toastr.error("toastr alert-type " + alertType + " is unknown");
      }

      @endif
  </script>

  {{-- Suscripción al canal del usuario --}}
  {!! talk_live(['user'=>["id"=>auth()->user()->id, 'callback'=>['msgshow']]]) !!}

  @yield('javascript')

  @if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
      @foreach(config('voyager.additional_js') as $js)<script type="text/javascript" src="{{ asset($js) }}"></script>@endforeach
  @endif

  <script type="text/javascript">
    // Notificaciones en el header
    var notificationsWrapper   = $('.dropdown-notifications');
    var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
    var notificationsCountElem = notificationsToggle.find('i[data-count]');
    var notificationsCount     = parseInt(notificationsCountElem.data('count'));
    var notifications          = notificationsWrapper.find('ul.dropdown-menu');

    if (notificationsCount <= 0) {
      notificationsWrapper.hide();
    }

    // Obtener los mensajes no leidos
    var url = '{{ url('chat/ajax/messages/allNotSeen') }}';
    ajaxRequest(url, null, 'POST', function(json) {
      //console.log(json);
      jQuery.each( json.data.messages, function( i, data ) {
        msgshow(data);
      });
    });

    // Botón "Marcar todos leidos" en nav
    $("#markAsSeen").click(function () {
      var url = '{{ url('chat/ajax/messages/markAllAsSeen') }}';
      ajaxRequest(url, null, 'POST', function(json) {
        // TODO: Quitar todos mensajes de la lista
        console.log(json);
        notifications.empty();
        notificationsCountElem.attr('data-count', 0);
        notificationsWrapper.find('.notif-count').text(0);
        notificationsWrapper.show();
      });
    });

    // Función para mostrar los mensajes no leidos o que acaba de recibir
    var msgshow = function(data) {
      console.log(data);

      // Mostrar nueva notificación en nav
      var existingNotifications = notifications.html();
      var avatar = '{{ Voyager::image('__url') }}'.replace('__url', data.sender.avatar);
      var timestamp = moment(data.created_at, "YYYY-MM-DD HH:mm:ss").format('LLLL');
      var conversationUrl = '{{ url('chat/message/ ')}}' + data.sender.id;

      var newNotificationHtml = `
        <li class="notification active">
            <div class="media">
              <div class="media-left">
                <div class="media-object">
                  <img src="`+ avatar +`" class="img-circle" alt="60x60" style="width: 60px; height: 60px;">
                </div>
              </div>
              <div class="media-body">
                <a href="`+conversationUrl+`" target="_blank">
                  <strong class="notification-title">`+data.sender.name+`</strong>
                  <p class="notification-desc">`+data.message+`</p>
                  <div class="notification-meta">
                    <small class="timestamp">`+timestamp+`</small>
                  </div>
                </a>
              </div>
            </div>
        </li>
      `;
      notifications.html(newNotificationHtml + existingNotifications);

      notificationsCount += 1;
      notificationsCountElem.attr('data-count', notificationsCount);
      notificationsWrapper.find('.notif-count').text(notificationsCount);
      notificationsWrapper.show();

      // TODO: Notificación de escritorio

    }
  </script>
  </body>
  </html>
