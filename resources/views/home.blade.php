@extends('templates.master')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-heading">Todos los usuarios</div>

        <div class="panel-body">
          <table class="table">
            <tbody>

              @foreach($users as $user)

              <tr>
                <td>
                  <img src="{{ Voyager::image($user->avatar)}}" height="60px" width="auto" alt="avatar" class="profile-img" />
                </td>
                <td>
                  {{$user->name}}
                </td>
                <td>
                  <a href="{{route('message.read', ['id'=>$user->id])}}" class="btn btn-success pull-right">Enviar mensaje</a>
                </td>
              </tr>

              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
