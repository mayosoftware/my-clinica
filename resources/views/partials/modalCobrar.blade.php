<div class="modal modal-success fade" id="cobrarModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
 <div class="modal-dialog">
   @php
     $isCollection = is_a($dataTypeContent, 'Illuminate\Database\Eloquent\Collection');
     // Si es un 'Collection' no se completan los atributos
   @endphp
     <!-- Modal content-->
     <div class="modal-content">
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title"><i class="fa fa-euro-sign"></i> Cobrar actuación</h4>
         </div>
         <form id="cobrarForm" action="{{ $isCollection ? '' : route('admin.tratamientoPaciente.generateCobro', ['id_tratamiento' => $dataTypeContent->getKey()]) }}"
               method="GET">

           <!-- CSRF TOKEN -->
           {{ csrf_field() }}

         <div class="modal-body">
           <div class="row">
             <div class="col-md-6">
               <label for="totalCobro">Cobro total €</label>
               <input type="number" class="form-control" name="totalCobro" value="{{ $isCollection ? '' : $dataTypeContent->pendiente }}" required step="any" placeholder="Total cobro">
             </div>
             <div class="col-md-6">
               <label for="tipoCobro">Tipo de cobro</label>
               <select class="form-control" name="tipoCobro">
                 <option value="1">Efectivo</option>
                 <option value="2">Tarjeta</option>
               </select>
             </div>
             <div class="col-md-12">
               <label for="concepto">Concepto</label>
               @php
                if (!$isCollection) {
                  $descLineas = '';
                  foreach ($dataTypeContent->lineas as $linea) {
                     $descLineas = ($descLineas == null ? '' : $descLineas . ', ') . trim($linea->descripcion . ' ' . ($linea->pieza > 0 ? ' en pieza ' . $linea->pieza : ''));
                   }
                 }
               @endphp

               <textarea class="form-control" name="concepto" rows="8" cols="80">{{ $isCollection ? '' : $descLineas }}</textarea>
             </div>
           </div>
         </div>
         <div class="modal-footer">
             <button class="btn btn-info" id="confirmCobro" name="confirmCobro" type="submit">
                 <i class="fa fa-euro"></i> <span>Confirmar cobro</span>
             </button>
             <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
         </div>
       </form>
     </div>
 </div>
</div>
