<div class="modal modal-info fade" id="planesModal" >
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="voyager-categories"></i> Seleccionar plan</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table id="dataTablePlanes" class="table table-hover">
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Total €</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($planes as $plan)
                <tr>
                  <td>{{ $plan->nombre }}</td>
                  <td>{{ number_format($plan->total, 2) }}</td>
                  <td class="bread-actions">
                    <button class="btn-xs btn-success btn-seleccionar-plan" value="{{ $plan->id }}">Seleccionar</button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>

      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
        </div>
      </div>
    </div>
  </div>
</div>

@section('javascript')
  @parent
    <script type="text/javascript">

    $(document).ready( function() {
      var tablePlans = $('#dataTablePlanes').DataTable({!! json_encode(
          array_merge([
              "order" => [],
              "language" => __('voyager::datatable'),
              "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
          ],
          config('voyager.dashboard.data_tables', []))
      , true) !!});
    });
    </script>
@endsection
