<div class="modal modal-info fade" id="pacientesModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="voyager-person"></i> Seleccionar paciente</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table id="dataTablePacientes" class="table table-hover">
            <thead>
              <tr>
                <th>Código</th>
                <th>Nombre</th>
                <th>Email</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pacientes as $paciente)
                <tr>
                  <td>{{ $paciente->codigo }}</td>
                  <td>{{ $paciente->nombre }}</td>
                  <td>{{ $paciente->email }}</td>
                  <td class="bread-actions">
                    <button class="btn-xs btn-success" id="seleccionarPaciente"
                      value="{{ $paciente->id }}">Seleccionar</button>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>

      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button type="reset" data-dismiss="modal" class="btn">Cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
