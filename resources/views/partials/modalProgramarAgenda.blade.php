
<div class="modal modal-success fade large" id="programarModal" tabindex="-1" role="dialog" aria-labelledby="..." aria-hidden="true">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="far fa-calendar-alt"></i> Realizar el tratamiento completo en la misma cita</h4>
      </div>
      <form id="programacionForm" action="{{ route('admin.tratamientoPaciente.programarTratamiento', ['id_tratamiento' => $dataTypeContent->getKey()]) }}"
        method="POST">

        <!-- CSRF TOKEN -->
        {{ csrf_field() }}

        <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-8">
              <div id='agenda'></div>
              <div class="form-group">
                <p class="lead">Haz click sobre un hueco de la agenda para rellenar la fecha y hora</p>
              </div>
            </div>
            <div class="col-md-4">
              <h3>Programar actuación</h3>
              <div class="checkbox">
                <input type="checkbox" name="programacion_conjunta" checked> Programación conjunta
              </div>
              <div class="from-group hidden" name="lineasDiv">
                <label for="linea">Acción del tratamiento</label>
                <select class="form-control" name="linea">
                  @foreach ($dataTypeContent->lineas as $linea)
                    <option value="{{ $linea->id }}">{{ $linea->descripcion . ($linea->pieza > 0 ? ' en pieza ' . $linea->pieza : '') }}</option>
                  @endforeach
                </select>
              </div>
              <div class="from-group">
                <label for="doctor_id">Doctor</label>
                <select class="form-control" name="doctor_id">
                  @foreach ($doctores as $doctor)
                    <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                  @endforeach
                </select>
                <label class="control-label" for="fecha">Fecha actuación</label>
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input name="fecha" id="fecha" class="form-control" />
                    </div>
                </div>
                <label for="hora_inicio" class="control-label">Hora inicio</label>
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                        <input name="hora_inicio" id="hora_inicio" class="form-control" />
                    </div>
                </div>
                <label for="hora_fin" class="control-label">Hora fin</label>
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                        <input name="hora_fin" id="hora_fin" class="form-control" />
                    </div>
                </div>
                <label for="tiempo" class="control-label">Tiempo</label>
                <div class="controls">
                    <div class="input-group">
                        <input type="number" name="tiempo" id="tiempo" class="form-control" />
                        <span class="input-group-addon">mins</span>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-info" id="programacionButton" name="programacionButton" type="submit">
                 <i class="fa fa-euro"></i> <span>Guardar programación</span>
             </button>
          <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
        </div>
      </form>
    </div>
  </div>
</div>

@section('javascript')
@parent
<script type="text/javascript">

  function fillProgramacion(date) {
    $("#fecha").data("DateTimePicker").date(moment(date, "DD/MM/YYYY"));
    $("#hora_inicio").data("DateTimePicker").date(moment(date, "HH:MM:SS"));
    $("#hora_fin").data("DateTimePicker").date(moment(date.add(1, 'hour'), "HH:MM:SS"));
    $("#tiempo").val('60');
  }

  function onEventChangeHours() {
    $('#hora_inicio, #hora_fin').on('dp.change', function (e) {
        if (e.date) {
          var fin = $('#hora_fin').data("DateTimePicker").date();
          var ini = $('#hora_inicio').data("DateTimePicker").date();

          if (fin && ini) {
            var timefin = fin.format('HH:mm');
            var timeini = ini.format('HH:mm');

            var tiempo = moment(timefin, "HH:mm").diff(moment(timeini, "HH:mm"), 'minutes');

            // Se controla que la hora de inicio sea menor que la de fin
            if (tiempo < 0) {
                $('#hora_fin').data("DateTimePicker").date(ini.add(1, 'minutes'));
                toastr.error("La hora de inicio no puede ser anterior");
            } else {
                $('#tiempo').val(tiempo);
            }
          }
        }
    });

    // Controlar cambios en el campo Tiempo cuando se
    $('input[name="tiempo"]').keyup( function () {
      cambiarTiempo();
    });

    // Controlar cambios en el campo Tiempo cuando se utiliza el selector
    $('input[name="tiempo"]').change( function () {
      cambiarTiempo();
    });
  }

  function cambiarTiempo() {
    var tiempo = $('input[name="tiempo"]').val() ? $('input[name="tiempo"]').val() : 0,
      ini = $('input[name="hora_inicio"]').data("DateTimePicker").date(),
      fin = $('input[name="hora_fin"]').data("DateTimePicker").date();

    if (tiempo < 0 || !$.isNumeric(tiempo)) {
      toastr.error("El tiempo debe ser numérico y mayor que 0");
      tiempo = 1;
      $('input[name="tiempo"]').val(1);
    }

    if (ini) {
      $('input[name="hora_fin"]').data("DateTimePicker").date(ini.add(tiempo, 'minutes'));
    } else if (fin) {
      $('input[name="hora_inicio"]').data("DateTimePicker").date(fin.add(-tiempo, 'minutes'));
    }
  }

  function clearModalProgramacion() {
    var $form = $( '#programacionForm' );
    $form.find("input[name='tiempo']").val('');
    $form.find("input[name='fecha']").data("DateTimePicker").clear();
    $form.find("input[name='hora_inicio']").data("DateTimePicker").clear();
    $form.find("input[name='hora_fin']").data("DateTimePicker").clear();
  }

  // Full fullcalendar
  var calendar = $('#agenda').fullCalendar({
    header: {
      left: 'prev, next today',
      center: 'title',
      right: 'agendaWeek, agendaDay'
    },
    validRange: {
      start: moment(new Date).add(-1, 'days')
    },
    minTime: '8:00:00',
    eventAfterRender: function(event, element, view) {
      // Se añaden los iconos
      $(element).find("div.fc-title").empty();

      var alerts = '';

      // Si tiene historial clínico
      if (event.historialClinico > 0) {
        alerts = ' <i class="fa fa-2x fa-notes-medical" aria-hidden="true" style="color:yellow;"></i> ';
      }

      // Si tiene notas
      if (event.notas) {
        alerts += ' <i class="fa fa-2x fa-comment-alt" aria-hidden="true"></i> ';
      }

      if (alerts) {
        $(element).find("div.fc-title").append(alerts);
      }

      $(element).find("div.fc-title").append(event.title);
    },
    defaultDate: moment(),
    height: 500,
    editable: false,
    slotDuration: '00:30:00',
    scrollTime: '08:00:00',
    weekends: true,
    defaultView: 'agendaDay',
    draggable: false,
    locale: 'es',
    buttonIcons: true,
    dayClick: function(date, jsEvent, view) {
      $( '#hora_inicio, #hora_fin' ).off('dp.change');
      fillProgramacion(date);
      onEventChangeHours();
    }
  });

  // Se limpian los datos antes de mostrar el modal
  $('#programarModal').on('show.bs.modal', function() {
    clearModalProgramacion();
  });

  // Se crea el calendar cuando el modal ya se está mostrando
  $('#programarModal').on('shown.bs.modal', function() {
    // Recargar calendar
    $('#agenda').fullCalendar('render');
    // Obtener los datos de la agenda
    var url = '{{ route('admin.programaciones.getProgramacionesDesde', ['fecha' => '__fecha']) }}'.replace('__fecha', moment(new Date).format('YYYY-MM-DD'));
    var posting = $.post( url );

    posting.done(function( data ) {
      console.log(data);
      if (!data.errors) {
        // console.log(data);
        var events = data.events;
        // Añadir evento actualizado
        $('#agenda').fullCalendar('removeEvents');
        $('#agenda').fullCalendar('renderEvents', data.events, true);
      } else {

      }
    });
    posting.fail(function() {
      alert( "error" );
    });
  });

  $(function () {

    $('input[name="programacion_conjunta"]').change(function() {
      if ($( this ).prop('checked')) {
        $('div[name="lineasDiv"]').addClass('hidden');
      } else {
        $('div[name="lineasDiv"]').removeClass('hidden');
      }
    });

    configDatePicker('#fecha');
    configTimePicker('#hora_inicio');
    configTimePicker('#hora_fin');

    // Sumar minutos a la hora de fin al cambiar
    onEventChangeHours();
  });
</script>
@endsection
