<div class="modal modal-info fade" id="facturarModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog">
    @php
      $isCollection = is_a($dataTypeContent, 'Illuminate\Database\Eloquent\Collection');
      // Si es un 'Collection' no se completan los atributos
    @endphp
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-receipt"></i> Generar factura</h4>
      </div>
      <form id="facturarForm" action="{{ $isCollection ? '' : route('admin.tratamientoPaciente.generateFactura', ['id_tratamiento' => $dataTypeContent->getKey()]) }}"
          method="GET">

        <!-- CSRF TOKEN -->
        {{ csrf_field() }}

        <div class="modal-body">
          <div class="form-group col-md-6">
            <label for="ejercicio">Ejercicio</label>
            <select class="form-control" name="ejercicio" required>
              @foreach ($ejercicios as $ejercicio)
                <option value="{{ $ejercicio->id }}">{{ $ejercicio->codigo }}</option>
              @endforeach
             </select>
          </div>

          <div class="form-group col-md-6">
            <label for="numeroFactura">Importe descuento €</label>
            <input type="text" class="form-control" name="descuentoFactura" readonly value="{{ $isCollection ? '' : $dataTypeContent->total_descuento}}">
          </div>
          <div class="form-group col-md-6">
            <label for="numeroFactura">Total €</label>
            <input type="text" class="form-control" name="totalFactura" readonly value="{{ $isCollection ? '' : $dataTypeContent->total}}">
          </div>
          <div class="form-group">
            <label for="numeroFactura">Observaciones</label>
            <textarea class="form-control" name="observacionesFactura" rows="4" cols="80"></textarea>
          </div>

        </div>
        <div class="modal-footer">
          <div class="form-group">
            <button class="btn btn-info" id="confirmCobro" name="confirmCobro" type="submit">
                 <i class="fa fa-euro"></i> <span>Generar factura</span>
             </button>
            <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
