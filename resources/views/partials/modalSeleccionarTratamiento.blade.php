<div class="modal modal-info fade" id="tratamientosModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="voyager-categories"></i> Seleccionar tratamiento</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
          <table id="dataTableTratamientos" class="table table-hover">
            <thead>
              <tr>
                <th>Código</th>
                <th>Descripción</th>
                <th>Importe €</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @foreach ($tratamientos as $tratamiento)
                <tr>
                  <td>{{ $tratamiento->codigo }}</td>
                  <td>{{ $tratamiento->nombre }}</td>
                  <td>{{ number_format($tratamiento->importe, 2) }}</td>
                  <td class="bread-actions">
                    <button class="btn-xs btn-success" id="seleccionarTratamiento" value="{{ $tratamiento->id }}">Seleccionar</button>
                  </td>

                </tr>
              @endforeach
            </tbody>
          </table>
        </div>

      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button class="btn btn-info" id="confirmCobro" name="confirmCobro" type="submit">
                 <i class="fa fa-user"></i> <span> Seleccionar tratamiento</span>
             </button>
          <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
        </div>
      </div>
    </div>
  </div>
</div>


@section('javascript')
  @parent
    <script type="text/javascript">

    $(document).ready( function() {
      var tableTratamientos = $('#dataTableTratamientos').DataTable({!! json_encode(
          array_merge([
              "order" => [],
              "language" => __('voyager::datatable'),
              "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
          ],
          config('voyager.dashboard.data_tables', []))
      , true) !!});
    });

    // Al cerrar el modal se inicializan valores
    $("#tratamientosModal").on("hidden.bs.modal", function () {
      if (($("#programacionModal").data('bs.modal') || {isShown: false}).isShown) {
        console.log('shown');

        $('body').css('overflow', 'hidden');
        $('#programacionModal').css('overflow', 'auto');
      } else {
        console.log('not shown');
      }

    });
    </script>
@endsection
