<div class="modal modal-info fade" id="presupuestoModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-receipt"></i> Generar presupuesto</h4>
      </div>
      <form id="facturarForm" action="{{ route('admin.tratamientoPaciente.generatePresupuesto', ['id_tratamiento' => $dataTypeContent->getKey()]) }}"
        method="GET">

        <!-- CSRF TOKEN -->
        {{ csrf_field() }}

        <div class="modal-body">
          <div class="form-group col-md-6">
            <label for="ejercicio">Ejercicio</label>
            <select class="form-control" name="ejercicio" required>
              @foreach ($ejercicios as $ejercicio)
                <option value="{{ $ejercicio->id }}">{{ $ejercicio->codigo }}</option>
              @endforeach
             </select>
          </div>
          <div class="form-group col-md-6">
            <label for="descuentoPresupuesto">Importe descuento €</label>
            <input type="text" class="form-control" name="descuentoPresupuesto" readonly value="{{ $dataTypeContent->total_descuento}}">
          </div>
          <div class="form-group col-md-6">
            <label for="totalPresupuesto">Total €</label>
            <input type="text" class="form-control" name="totalPresupuesto" readonly value="{{ $dataTypeContent->total}}">
          </div>
          <div class="form-group">
            <label for="observacionesPresupuesto">Observaciones</label>
            <textarea class="form-control" name="observacionesPresupuesto" rows="4" cols="60"></textarea>
          </div>

        </div>
        <div class="modal-footer">
          <div class="form-group">
            <button class="btn btn-info" id="confirmPresupuesto" name="confirmPresupuesto" type="submit">
                 <i class="fa fa-euro"></i> <span>Generar presupuesto</span>
             </button>
            <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
