@extends('templates.master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
        @if(isset($dataType->order_column) && isset($dataType->order_display_column))
            <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
            </a>
        @endif
        @endcan
        @if(isset($printAction) && $printAction)
          <a href="#" class="btn btn-info disabled" value="Imprimir" name="printButton" target="_blank">
            <i class="voyager-documentation"></i> <span>Imprimir</span>
          </a>
        @endif
        @include('voyager::multilingual.language-selector')

        <div class="pull-right">
          @yield('page_header_right')
        </div>
    </div>
@stop

@section('content')
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-bordered">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <form class="form-search inline-form" action="{{ route('voyager.movimientos-caja.index') }}"
                      method="get">
                      <fieldset>
                        <legend>Filtro: </legend>
                          <label class="radio-inline">
                            <input type="radio" name="filter" id="inlineRadio1" value="1"> Hoy
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="filter" id="inlineRadio2" value="2"> Todos
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="filter" id="inlineRadio3" value="3"> Entre fechas
                          </label>
                          <div class="radio-inline">
                            <input name="start" id="start" class="form-control" />
                          </div>
                          <div class="radio-inline">
                            <input name="end" id="end" class="form-control" />
                          </div>

                          <button class="btn btn-success" type="submit" name="submitbutton" value="search">
                            <i class="voyager-search"></i> <span>Buscar</span>
                          </button>

                          <button class="btn btn-info" type="submit" name="submitbutton" value="print">
                            <i class="voyager-documentation"></i> <span>Imprimir movimientos</span>
                          </button>
                        </fieldset>
                    </form>
                  </div>
                </div>

                <div class="table-responsive">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                          <tr>
                            <th>Fecha</th>
                            <th>Paciente</th>
                            <th>Concepto</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                            <th>Tipo cobro</th>
                            <th>Acciones</th>
                          </th>
                        </thead>
                        <tbody>

                          @foreach ($dataTypeContent as $data)
                            <tr>
                              <td>{{ \Carbon\Carbon::parse($data->fecha)->format('d/m/Y') }}</td>
                              <td>{{ $data->tratamiento->paciente->nombre }}</td>
                              <td>{{ $data->concepto }}</td>
                              <td>{{ $data->entrada }}</td>
                              <td>{{ $data->salida }}</td>
                              <td>{{ $data->tipoCobro->nombre }}</td>
                              <td class="no-sort no-click" id="bread-actions">
                                  @foreach(Voyager::actions() as $action)
                                      @include('voyager::bread.partials.actions', ['action' => $action])
                                  @endforeach
                              </td>
                            </tr>
                          @endforeach
                        </tbody>                        
                      </table>
                  </div>
              </div>
              <div class="panel-footer">
                <h4>Totales</h4>
                <div class="table-responsive">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                          <tr>
                            <th>Tipo de cobro</th>
                            <th>Entrada</th>
                            <th>Salida</th>
                          </th>
                        </thead>
                        <tbody>
                          <tr>
                            <td>Efectivo</td>
                            <td>{{ $totalEntradaEfectivo}}</td>
                            <td>{{ $totalSalidaEfectivo}}</td>
                          </tr>
                          <tr>
                            <td>Tarjeta</td>
                            <td>{{ $totalEntradaTarjeta}}</td>
                            <td>{{ $totalSalidaTarjeta}}</td>
                          </tr>
                        
                      </table>
                  </div>
              </div>
              </div>
          </div>
      </div>
  </div>

  {{-- Single delete modal --}}
  <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                              aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
              </div>
              <div class="modal-footer">
                  <form action="#" id="delete_form" method="POST">
                      {{ method_field("DELETE") }}
                      {{ csrf_field() }}
                      <input type="submit" class="btn btn-danger pull-right delete-confirm" value="{{ __('voyager::generic.delete_confirm') }}">
                  </form>
                  <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
              </div>
          </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@section('css')
@if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
<link rel="stylesheet" href="{{ voyager_asset('lib/css/responsive.dataTables.min.css') }}">
@endif
@stop

@section('javascript')
    <!-- DataTables -->
    @if(!$dataType->server_side && config('dashboard.data_tables.responsive'))
        <script src="{{ voyager_asset('lib/js/dataTables.responsive.min.js') }}"></script>
    @endif
    <script>
        $(document).ready(function () {

          @if (!$filter)
            $('#inlineRadio1').prop('checked', true);
          @elseif ($filter == 1)
            $('#inlineRadio1').prop('checked', true);
          @elseif ($filter == 2)
            $('#inlineRadio2').prop('checked', true);
          @elseif ($filter == 3)
            $('#inlineRadio3').prop('checked', true);
          @endif


          configDatePicker('input[name="start"]');
          configDatePicker('input[name="end"]');

          // Desactivar fechas al inicio
          $('input[name="start"]').prop('readonly', true);
          $('input[name="end"]').prop('readonly', true);

          var table = $('#dataTable').DataTable();

          $('input[name="filter"]').change(function(){
            console.log('change');
            var id = $(this).val();
            // Rango de fechas
            if (id == 3) {
              $('input[name="start"]').prop('readonly', false);
              $('input[name="end"]').prop('readonly', false);
            }

            // var url = '{{ route('admin.movimientos-caja.filter') }}';
            //
            // var posting = $.post(url, { filter: id });
            //
            // posting.done(function( data ) {
            //   table.clear().draw();
            //   table.rows.add(data.movimientos);
            //   table.draw();
            // });
          });
        });


        var deleteFormAction;
        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

        // Si se indica que las filas se pueden seleccionar, se controla el radioButton
        $( 'input[name="rowSelected"]' ).change(function() {
          $('a[name="printButton"]').removeClass('disabled');
          var id = $( this ).val();
          var url = '{{ url('admin/'.$dataType->slug.'/print') }}/' + id;
          $('a[name="printButton"]').attr('href', url);
        });
    </script>
@endsection
