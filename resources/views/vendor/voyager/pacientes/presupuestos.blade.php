  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-bordered">
              <div class="panel-body">
                <div class="form-group">
                  <a href="{{ route('voyager.presupuestos.create') }}"
                    class="btn btn-success"><i class="voyager-plus"></i> <span>Añadir nuevo</span>
                  </a>
                </div>

                  <div class="table-responsive">
                      <table id="dataTable_presupuestos" class="table table-hover">
                          <thead>
                              <th>#</th>
                              <th>Identificador</th>
                              <th>Número</th>
                              <th>Fecha</th>
                              <th>Descuento</th>
                              <th>Total</th>
                              <th>Tratamiento</th>
                              <th></th>
                          </thead>
                          <tbody>
                              @foreach($dataTypeContent->presupuestos as $presupuesto)
                                <tr>
                                  <td>{{ $presupuesto->indice }}</td>
                                  <td>{{ $presupuesto->nombre }}</td>
                                  <td>{{ $presupuesto->numero }}</td>
                                  <td>{{ $presupuesto->fechaFormat() }}</td>
                                  <td>{{ $presupuesto->total_descuento }}</td>
                                  <td>{{ $presupuesto->total }}</td>
                                  <td>
                                    @if ($presupuesto->tratamiento)
                                      <span class="label label-info">#{{ $presupuesto->tratamiento->numero }}</span>
                                    @else
                                      <div class="actions">
                                        <a class="btn btn-sm btn-success create" id="delete-{{ $presupuesto->id }}" data-id="{{ $presupuesto->id }}"
                                          role="button">
                                          <i class="voyager-plus"></i>
                                        </a>
                                      </div>
                                    @endif
                                  </td>
                                  <td>
                                    <div class="actions pull-right">
                                      <a class="btn btn-sm btn-info"
                                        href="{{ route('voyager.presupuestos.edit', ['id' => $presupuesto->id]) }}">
                                        Editar
                                      </a>
                                    </div>
                                  </td>
                                </tr>
                              @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
              <div class="panel-body">
                @yield("panel-footer")
              </div>
          </div>
      </div>

      <div class="modal modal-success fade" tabindex="-1" id="crear_tratamiento_modal" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                  aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"><i class="voyager-list-add"></i> Crear tratamiento de presupuesto </h4>
                  </div>
                  <div class="modal-body">
                    <h4 id="body_text">¿Quieres crear un tratamiento con la información del presupuesto?</h4>
                  </div>
                  <div class="modal-footer">
                      <form action="#" id="create_tratamiento_form" method="POST">
                          {{ csrf_field() }}

                          <input type="submit" class="btn btn-success delete-confirm2" value="Si, crear">
                          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                      </form>

                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
  </div>

  @section('javascript')
    @parent
    <script type="text/javascript">

      $(document).ready(function(){

        var table = $('#dataTable_presupuestos').DataTable({!! json_encode(
            array_merge([
                "order" => [ [0, "desc"] ],
                "language" => __('voyager::datatable'),
                "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
            ],
            config('voyager.dashboard.data_tables', []))
        , true) !!});

        $('td').on('click', '.create', function (e) {
            $('#create_tratamiento_form')[0].action = '{{ route('admin.tratamientoPaciente.createFromTratamiento', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#crear_tratamiento_modal').modal('show');
        });

      });
    </script>
  @endsection
