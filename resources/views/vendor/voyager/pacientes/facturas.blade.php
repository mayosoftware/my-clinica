<div class="row">
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <div class="panel-body">
              <div class="form-group">
                <a href="{{ route('voyager.facturas.create') }}"
                  class="btn btn-success"><i class="voyager-plus"></i> <span>Añadir nuevo</span>
                </a>
              </div>

                <div class="table-responsive">
                    <table id="dataTable_facturas" class="table table-hover">
                        <thead>
                            <th>#</th>
                            <th>Identificación</th>
                            <th>Número</th>
                            <th>Fecha</th>
                            <th>Descuento</th>
                            <th>Total</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($dataTypeContent->facturas as $factura)
                              <tr>
                                <td>{{ $factura->indice }}</td>
                                <td>{{ $factura->nombre }}</td>
                                <td>{{ $factura->numero }}</td>
                                <td>{{ $factura->fechaFormat() }}</td>
                                <td>{{ $factura->total_descuento }}</td>
                                <td>{{ $factura->total }}</td>
                                <td>
                                  <div class="actions">
                                    <a class="btn btn-sm btn-info"
                                    href="{{ route('voyager.facturas.edit', ['id' => $factura->id]) }}">Editar</a>
                                  </div>
                                </td>
                              </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="panel-body">
              @yield("panel-footer")
            </div>
        </div>
    </div>
</div>

@section('javascript')
  @parent
  <script type="text/javascript">

    $(document).ready(function(){

      var table = $('#dataTable_facturas').DataTable({!! json_encode(
          array_merge([
              "order" => [ [0, "desc"] ],
              "language" => __('voyager::datatable'),
              "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
          ],
          config('voyager.dashboard.data_tables', []))
      , true) !!});

    });
  </script>
@endsection
