@extends('templates.edit-add')

@section('content')
  <div class="page-content edit-add container-fluid">
    {{-- Crear paciente --}}
    @if(is_null($dataTypeContent->getKey()))
        @include('vendor.voyager.pacientes.info-paciente')

    {{-- Editar paciente --}}
    @else
      <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#tratamientos" aria-controls="tratamientos" role="tab" data-toggle="tab">Tratamientos</a></li>
          <li role="presentation"><a href="#datos" aria-controls="datos" role="tab" data-toggle="tab">Datos paciente</a></li>
          <li role="presentation"><a href="#presupuestos" aria-controls="presupuestos" role="tab" data-toggle="tab">Presupuestos</a></li>
          <li role="presentation"><a href="#facturas" aria-controls="facturas" role="tab" data-toggle="tab">Facturas</a></li>
          <li role="presentation"><a href="#ficheros" aria-controls="ficheros" role="tab" data-toggle="tab">Ficheros</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="tratamientos">
            @include('vendor.voyager.pacientes.tratamientos')
          </div>
          <div role="tabpanel" class="tab-pane" id="datos">
            @include('vendor.voyager.pacientes.info-paciente')
          </div>
          <div role="tabpanel" class="tab-pane" id="presupuestos">
            @include('vendor.voyager.pacientes.presupuestos')
          </div>
          <div role="tabpanel" class="tab-pane" id="facturas">
            @include('vendor.voyager.pacientes.facturas')
          </div>
          <div role="tabpanel" class="tab-pane" id="ficheros">
            @include('vendor.voyager.ficheros')
          </div>
        </div>
      </div>

    @endif
  </div>
@stop

@section('javascript')
    <script>
      // Si es alta se mete el código siguiente
      @if(is_null($dataTypeContent->getKey()))
        $('input[name="codigo"]').val('{{ $dataTypeContent->nextCodigo() }}');
        $('input[name="cuenta_contabilidad"]').val('{{ $dataTypeContent->nextCuentaCont() }}');
      @endif

      console.log('codigos');
      $('input[name="codigo"]').attr('readonly', true);
      $('input[name="cuenta_contabilidad"]').attr('readonly', true);
    </script>
@stop
