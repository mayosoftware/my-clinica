@php
  $tratamientoPacienteType = Voyager::model('DataType')->where('slug', '=', 'tratamientos-paciente')->first();
  $tratamientoPaciente = $dataTypeContent->tratamientosPaciente;
@endphp

  <div class="row">
    <div class="col-sm-12">
      <table class="table table-bordered">
        <thead>
          <th>Nº tratamientos</th>
          <th>Pendiente de cobro</th>
          <th>Total cobrado</th>
          <th>Importe total</th>
          <th>Saldo</th>
        </thead>
        <tbody>
          <td>{{ number_format(count($dataTypeContent->tratamientosPaciente), 0) }}</td>
          <td>{{ number_format($dataTypeContent->tratamientosPaciente->sum('pendiente'), 2) }}</td>
          <td>{{ number_format($dataTypeContent->tratamientosPaciente->sum('cobrado'), 2) }}</td>
          <td>{{ number_format($dataTypeContent->tratamientosPaciente->sum('total'), 2) }}</td>
          <td>{{ number_format($dataTypeContent->saldo, 2) }}</td>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-bordered">
              <div class="panel-body">
                <div class="form-group">
                  <a href="{{ route('admin.tratamiento-paciente.create', ['id' => $dataTypeContent->getKey()]) }}"
                    class="btn btn-success"><i class="voyager-plus"></i> <span>Añadir nuevo</span>

                  <a class="btn btn-danger delete-all" id="delete-all" data-id="{{ $dataTypeContent->id }}">
                    <i class="voyager-trash"></i> Eliminar todos</a>
                  </a>
                </div>

                  <div class="table-responsive">
                      <table id="dataTable" class="table table-hover">
                          <thead>
                              <th>#</th>
                              <th>Plan de tratamiento</th>
                              <th>Presu.</th>
                              <th>Factu.</th>
                              <th>Pendiente trat.</th>
                              <th>Cobrado trat.</th>
                              <th>Actuación</th>
                              <th>Realizada</th>
                              <th>Cita</th>
                              <th>Importe</th>
                              <th></th>
                          </thead>
                          <tbody>
                              @foreach($tratamientoPaciente as $data)
                                @php
                                  $numeroTratamiento = $data->numero;
                                  $nombreTratamiento = $data->nombre;
                                  $pendienteTratamiento = $data->pendiente;
                                  $cobradoTratamiento = $data->cobrado;
                                  $numeroLineas = $data->numero_lineas;
                                  $cobradoTratamiento = $data->cobrado;
                                  $pendienteTratamiento = $data->pendiente;
                                  $totalTratamiento = $data->total;
                                  $financiadoTratamiento = $data->financiado;
                                  $tratamiento_id = $data->id;
                                  $presupuestado = $data->presupuestado;
                                  $facturado = $data->facturado;
                                @endphp

                                {{-- Si el tratamiento no tiene lineas --}}
                                @if ($data->lineas->count() == 0)
                                  <tr>
                                      <td>{{ $numeroTratamiento }}</td>
                                      <td>{{ $nombreTratamiento }}</td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td>{{ $pendienteTratamiento }}</td>
                                      <td>{{ $cobradoTratamiento }}</td>
                                      <td><span class="label label-warning">Sin actuaciones</span></td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td>-</td>
                                      <td>
                                        <a class="btn btn-sm btn-pull-right primary edit" href="{{ route('voyager.tratamientos-paciente.edit', ['id' => $tratamiento_id]) }}">
                                          <i class="voyager-edit"></i> Editar</a>
                                      </td>
                                  </tr>

                                {{-- El tratamiento tiene lineas --}}
                                @else
                                  @foreach ($data->lineas as $linea)
                                    <tr>
                                        <td>{{ $numeroTratamiento }}</td>
                                        <td>{{ $nombreTratamiento }}</td>
                                        <td>
                                          @if ($presupuestado)
                                          <span class="label label-success">SI</span>
                                          @else
                                          <span class="label label-default">NO</span>
                                          @endif
                                        </td>
                                        <td>
                                          @if ($facturado)
                                          <span class="label label-success">SI</span>
                                          @else
                                          <span class="label label-default">NO</span>
                                          @endif
                                        </td>
                                        <td>{{ $pendienteTratamiento }}</td>
                                        <td>{{ $cobradoTratamiento }}</td>
                                        <td>{{ $linea->descripcionComplea() }}</td>
                                        <td>
                                          @if ($linea->realizada)
                                          <span class="label label-success">SI</span>
                                          @else
                                          <span class="label label-default">NO</span>
                                          @endif
                                        </td>
                                        <td>
                                          @if ($linea->realizada)
                                            {{ $linea->fechaRealizacionFormat() }}
                                          @elseif ($linea->programacion)
                                            {{ $linea->fechaInicioCompleta() }}
                                          @else
                                            Sin registro
                                          @endif
                                        </td>
                                        <td>{{ $linea->total }}</td>
                                        <td>
                                          <div class="actions pull-right">
                                            <a class="btn btn-sm btn-danger delete" id="delete-{{ $linea->id }}" data-id="{{ $data->id }}">
                                              <i class="voyager-trash"></i> Eliminar</a>

                                            <a class="btn btn-sm btn-primary edit" href="{{ route('voyager.tratamientos-paciente.edit', ['id' => $tratamiento_id]) }}">
                                              <i class="voyager-edit"></i> Editar</a>

                                          </div>
                                        </td>
                                    </tr>
                                  @endforeach

                                @endif
                              @endforeach
                          </tbody>
                      </table>
                  </div>
              </div>
              <div class="panel-footer">
                @yield("panel-footer")
              </div>
          </div>
      </div>
      {{-- Single delete modal --}}
      <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
          <div class="modal-dialog">
              <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                  aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title"><i class="voyager-trash"></i> Eliminar tratamientos </h4>
                  </div>
                  <div class="modal-body">
                    <h4 id="body_text">¿Quieres eliminar el tratamiento completo?</h4>
                  </div>
                  <div class="modal-footer">
                      <form action="#" id="delete_form" method="POST">
                          {{ method_field("DELETE") }}
                          {{ csrf_field() }}

                          <input type="hidden" name="paciente_id" value="{{ $dataTypeContent->id }}">

                          <input type="submit" class="btn btn-danger delete-confirm2" value="Si, borrar">
                          <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                      </form>

                  </div>
              </div><!-- /.modal-content -->
          </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
  </div>

  @section('javascript')
    @parent
    <script type="text/javascript">

      $(document).ready(function(){

        // TODO: Ordenar con formato dd-mm-yyyy hacer global
        jQuery.extend(jQuery.fn.dataTableExt.oSort, {
         "extract-date-pre": function(value) {
             var date = value; //$(value, 'span')[0].innerHTML;
             date = date.split('/');
             if (date[1]) {
                var yearTime = date[2].split(' ');
                return Date.parse(yearTime[0] + '/' + date[1] + '/' + date[0]);
             } else {
               return null;
             }

         },
         "extract-date-asc": function(a, b) {
             return ((a < b) ? -1 : ((a > b) ? 1 : 0));
         },
         "extract-date-desc": function(a, b) {
             return ((a < b) ? 1 : ((a > b) ? -1 : 0));
         }
        });
        // *******

        var table = $('#dataTable').DataTable({!! json_encode(
            array_merge([
                "order" => [ [8, "desc"], [0, "desc"] ],
                "language" => __('voyager::datatable'),
                "columnDefs" => [['searchable' =>  false, 'targets' => -1 ], ['type' => 'extract-date', 'targets' => [8]]],
            ],
            config('voyager.dashboard.data_tables', []))
        , true) !!});

        $('td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.tratamientos-paciente.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#body_text').text("¿Quieres eliminar el tratamiento completo?");
            $('#delete_modal').modal('show');
        });

        $('.delete-all').click( function (e) {
            $('#delete_form')[0].action = '{{ route('admin.tratamientoPaciente.destroyAll', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#body_text').text("¿Quieres eliminar todos los tratamientos de este paciente?");
            $('#delete_modal').modal('show');
        });

      });
    </script>
  @endsection
