@extends('templates.voyager-edit-add-content')

@include('partials.modalCobrar')
@include('partials.modalFacturar')

@include('partials.modalSeleccionarTratamiento')

@section('javascript')
  @parent
  <script>
      function updateTotal() {
        var cantidad = $('input[name="cantidad"]').val();
        var precio = $('input[name="precio"]').val();
        $('input[name="total"]').val(cantidad * precio);
      }

      $(document).ready(function () {
        // configDatePicker('input[name="fecha_actuacion"');
        configTimePicker('input[name="hora_inicio"');
        configTimePicker('input[name="hora_fin"');

        // Evitar doble submiting
        // $('.panel-footer .save').click(function() {
        //   console.log('click');
        //   $(this).attr('disabled','disabled');
        // });
      });

      $(function () {
        // Si se está creando una actuación
        @if(is_null($dataTypeContent->getKey()))
          $('input[name="cantidad"]').val(1);
          $('input[name="precio"]').val(0);
          $('input[name="total_descuento"]').val(0);
          $('input[name="total"]').val(0);
          $('input[name="cobrado"]').val(0);
          $('input[name="tiempo"]').val(0);

          var idUsuario = '{{ auth()->user()->id }}';
          $('input[name="created_by"]').val(idUsuario);

        @else
          $( "#cobrar_button" ).click(function() {
            @if($dataTypeContent->cobrado > 0)
              toastr.error("Ya se ha cobrado esta actuación");
            @else
              var importe = $('input[name="total"]').val();
              $('input[name="totalCobro"]').val(importe);
              $('#cobrarForm').attr('action', '{{ route('admin.agenda.cobrar', ['id' => $dataTypeContent->getKey()]) }}');
              $('#cobrarModal').modal('show');
            @endif
          });

          $( "#facturar_button" ).click(function() {
            @if($dataTypeContent->cobrado <= 0)
              toastr.error("Se debe cobrar la actuación para facturarla");
            @else
              var cobrado = $('input[name="cobrado"]').val();
              $('input[name="descuentoFactura"]').val(0);
              $('input[name="totalFactura"]').val(cobrado);

              var action = '{{ url('admin/agenda') }}/' + {{ $dataTypeContent->getKey() }} + '/facturar';
              $('#facturarForm').attr('action', action);
              $('#facturarModal').modal('show');
            @endif
          });
        @endif

        $('input[name="total"]').prop('readonly', true);
        $('input[name="cobrado"]').prop('readonly', true);
        $('input[name="tiempo"]').prop('readonly', true);
        $('#facturada').addClass('disabled');

        // Al cambiar precio, se actualiza total
        $('input[name="precio"]').change(function() {
          updateTotal();
        });

        // Al cambiar cantidad, se actualiza total
        $('input[name="cantidad"]').change(function() {
          updateTotal();
        });
      });

      $(function() {
        $('#descripcionDiv').empty();
        $('#descripcionDiv').append(
          '   <label for="descripcion">Descripción</label>' +
          '   <div class="input-group">' +
          '     <input type="text" name="descripcion" placeholder="Introduce una descripción" class="form-control">' +
          '     <span class="input-group-btn">' +
          '       <a class="btn-sm btn-info" data-toggle="modal" data-target="#tratamientosModal">' +
          '         <i class="voyager-double-right"></i>' +
          '       </a>' +
          '     </span>' +
          '   </div>'
        );
      });

      $(function(){
        var tableTratamientos = $('#dataTableTratamientos').DataTable({!! json_encode(
            array_merge([
                "order" => [],
                "language" => __('voyager::datatable'),
                "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
            ],
            config('voyager.dashboard.data_tables', []))
        , true) !!});

        var tratamientos = {!! json_encode($tratamientos) !!};
        $(document).on('click', '#seleccionarTratamiento', function(){
          var id = $(this).val();
          var tratamiento = tratamientos.find(x => x.id == id);
          var tratamientoDesc = tratamiento.nombre;
          var precio = parseFloat(tratamiento.importe);

          $('input[name="descripcion"]').val(tratamientoDesc);
          $('input[name="precio"]').val(precio);
          updateTotal();

          $('#tratamientosModal').modal('hide');
        });
      });

      $(function() {
        $('#hora_inicio, #hora_fin').on('dp.change', function (e) {
          if (e.date) {
            var fin = $('input[name="hora_fin"').data("DateTimePicker").date();
            var ini = $('input[name="hora_inicio"').data("DateTimePicker").date();

            if (fin && ini) {
              var timefin = fin.format('HH:mm');
              var timeini = ini.format('HH:mm');

              var tiempo = moment(timefin, "HH:mm").diff(moment(timeini, "HH:mm"), 'minutes');

              // Se controla que la hora de inicio sea menor que la de fin
              if (tiempo < 0) {
                  $('input[name="hora_fin"]').data("DateTimePicker").date(ini.add(1, 'minutes'));
                  toastr.error("La hora de inicio no puede ser anterior");
              } else {
                  $('input[name="tiempo"]').val(tiempo);
              }
            }
          }
        });
      });

  </script>

@endsection
