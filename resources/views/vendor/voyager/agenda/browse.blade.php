@extends('templates.voyager-browse')

@section('page_header')
  <div class="container-fluid">
      <h1 class="page-title">
          <i class="{{ $dataType->icon }}"></i> Agenda Clínica
      </h1>
      <button class="btn btn-info" data-toggle="modal" data-target="#printModal">
        <i class="voyager-documentation"></i> <span>Imprimir agenda</span>
      </button>
      <button class="btn btn-success" type="button" id="cobrar_button">
        <i class="fa fa-euro-sign"></i> Cobrar actuación
      </button>
      <button class="btn btn-warning" type="button" id="facturar_button">
        <i class="fa fa-receipt"></i> Facturar actuación
      </button>
      @include('voyager::multilingual.language-selector')
  </div>

@stop

@section('panel-body')
  <div class="row">
    <div class="col-md-9">
      <div id='calendar'></div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="panel panel-primary panel-bordered">

            <div class="panel-heading">
                <h3 class="panel-title panel-icon"><i class="voyager-calendar"></i> Selección día</h3>
                <div class="panel-actions">
                    {{-- <a class="panel-action voyager-angle-up" data-toggle="panel-collapse" aria-hidden="true"></a> --}}
                </div>
            </div>

            <div class="panel-body" style="">
              <div id="daypicker"></div>
            </div><!-- .panel-body -->
        </div>

      </div>

      {{-- Comprobar si el rol del usuario tiene permisos para buscar por doctor --}}
      <?php
        $browsUsers = Voyager::can('browse_users');
      ?>
      @if($browsUsers)
        <div class="row">
            <div class="panel panel-primary panel-bordered">

                <div class="panel-heading">
                  <h3 class="panel-title panel-icon"><i class="voyager-file-text"></i> Usuarios</h3>
                  <div class="panel-actions">
                        {{-- <a class="panel-action voyager-angle-up" data-toggle="panel-collapse" aria-hidden="true"></a> --}}
                  </div>
                </div>

                <div class="panel-body" style="">
                  @foreach ($doctores as $doct)
                    <div class="radio">
                      <label><input type="radio" name="radioDoctor" value="{{ $doct->id }}">{{ $doct->name }}</label>
                    </div>
                  @endforeach
                </div><!-- .panel-body -->

                <div class="panel-footer">
                  <a id="deselectRadioGroup" href="#">Quitar seleccionado</a>
                </div>
            </div>
          </div>
        @endif
    </div>
  </div>

  {{-- Modal para crear/editar actuación --}}
  @include('vendor.voyager.agenda.partials.modalActuacion')

  {{-- Modal para imprimir entre Fechas --}}
  @include('vendor.voyager.agenda.partials.modalPrint')

  {{-- Modal para confirmar el cobro de la actuación --}}
  @include('partials.modalCobrar')

  {{-- Modal para generar factura de la actuación --}}
  @include('partials.modalFacturar')

  @include('partials.modalSeleccionarPaciente')
  @include('partials.modalSeleccionarTratamiento')
@stop

@section('panel-footer')
  <div class="row">
    <span class="label" style="color: #ffffff;background-color: #47D27F;">Realizada</span>
    <span class="label" style="color: #ffffff;background-color: #22A7F0;">No Realizada</span>
    <span class="label" style="color: #ffffff;background-color: #F94126;">Cancelada</span>
  </div>

@stop

@section('css')
  <style>
      .bootstrap-datetimepicker-widget table td.event, .bootstrap-datetimepicker-widget table td.event:hover {
          font-weight: bold;
      }
  </style>

@endsection

@section('javascript')
  {{-- <script type="text/javascript" src="{{ asset('agenda/openmodal.js');}}"> --}}

    <script>
        $(document).ready(function () {
            // Full fullcalendar
            $('#calendar').fullCalendar({
              header: {
                  left: 'prev, next today',
                  center: 'title',
                  right: 'month, agendaWeek, agendaDay, listMonth'
              },
              //eventDataTransform: function (json) {
              //    console.log(json);
              //    return json.data;
              //},
              eventAfterRender: function (event, element, view) {
                  // Se añaden los iconos
                  $(element).find("div.fc-title").empty();

                  var alerts = '';

                  // Si tiene historial clínico
                  if (event.historialClinico > 0) {
                    alerts = ' <i class="fa fa-2x fa-notes-medical" aria-hidden="true" style="color:yellow;"></i> ';
                  }

                  // Si tiene notas
                  if (event.notas) {
                    alerts += ' <i class="fa fa-2x fa-comment-alt" aria-hidden="true"></i> ';
                  }

                  if (alerts) {
                    $(element).find("div.fc-title").append(alerts);
                  }

                  $(element).find("div.fc-title").append(event.title);
              },
              defaultDate: moment(),
              height: 600,
              editable: false,
              slotDuration: '00:10:00',
              scrollTime: '08:00:00',
              weekends: true,
              defaultView: 'agendaDay',
              draggable: false,
              locale: 'es',
              buttonIcons: true,
              events: @json($events),
              eventClick: function (calEvent, jsEvent, view) {
                  fillModalForEdit(calEvent);
              },
              dayClick: function(date, jsEvent, view) {
                //alert('Clicked on: ' + date.format());
                //alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                //alert('Current view: ' + view.name);

                console.log(date.format());
                fillModalForCreate(date);
              }
          });

          // Setup datetimepicker
          configDatePickerInline('#daypicker');
          hightlightkDaysInDatePicker();

          $("#daypicker").on("dp.change", function (e) {
              //console.log(e.date);
              hightlightkDaysInDatePicker();
              // Cambiar día del calendario
              $('#calendar').fullCalendar('gotoDate', e.date)
              //$('#dpEnd').data("DateTimePicker").setMinDate(e.date);
          });

          // Actualizar los dias cuando se modifica la vista del datepicker
          $("#daypicker").on("dp.update", function (e) {
              hightlightkDaysInDatePicker(e.viewDate);
          });

          // Fechas modal
          configDatePicker('#fechaActuacion');
          configTimePicker('#horaInicio');
          configTimePicker('#horaFin');

          // Sumar minutos a la hora de fin al cambiar
          $('#horaInicio, #horaFin').on('dp.change', function (e) {
              if (e.date) {
                var fin = $('#horaFin').data("DateTimePicker").date();
                var ini = $('#horaInicio').data("DateTimePicker").date();

                if (fin && ini) {
                  var timefin = fin.format('HH:mm');
                  var timeini = ini.format('HH:mm');

                  var tiempo = moment(timefin, "HH:mm").diff(moment(timeini, "HH:mm"), 'minutes');

                  // Se controla que la hora de inicio sea menor que la de fin
                  if (tiempo < 0) {
                      $('#horaFin').data("DateTimePicker").date(ini.add(1, 'minutes'));
                      toastr.error("La hora de inicio no puede ser anterior");
                  } else {
                      $('#tiempo').val(tiempo);
                  }
                }
              }
          });

          configDatePicker('#fechaRealizacion');
          configDatePicker('#fechaCancelacion');

          // Controlar check de realizada
          $("#realizada").change(function () {
              if ($("#realizada").prop('checked')) {
                  $("#fechaRealizacion").data("DateTimePicker").date(new Date());
              } else {
                  $("#fechaRealizacion").data("DateTimePicker").clear();
              }
          });

          // Controlar check de cancelación
          $("#cancelada").change(function () {
              if ($("#cancelada").prop('checked')) {
                  $("#fechaCancelacion").data("DateTimePicker").date(new Date());
              } else {
                  $("#fechaCancelacion").data("DateTimePicker").clear();
              }
          });

          // Notas richTextBox
          tinymce.init({
            selector:'textarea',
            skin: 'voyager',
            height: 150,
              menubar: false,
              toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
              content_css: [
                '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                '//www.tinymce.com/css/codepen.min.css']
            });

          // Imprimir entre fechas
          // Fechas modal
          configDatePicker('#desde');
          configDatePicker('#hasta');

          $("#printButton").click(function () {
              printBetweenDates();
          });

        });

        function hightlightkDaysInDatePicker(pickerDate = null, jsEvent = null) {
            var dateString;
            var date;

            if (pickerDate == null) {
                dateString = $("#daypicker").data("date");
                date = moment(dateString, "DD/MM/YYYY");
            } else {
                date = pickerDate;
            }

            // Cambiar la clase de los días con eventos
            tableDays = $(".datepicker .datepicker-days .table-condensed");

            var trs = tableDays.find("tbody>tr");

            // Se actualiza un único evento
            if (jsEvent) {
              var evt = jsEvent;
              var eventdate = moment(evt.start).startOf('day');

              var month = eventdate.format('M');
              var year = eventdate.format('YYYY');

              if (eventdate.format('YYYY') == date.format('YYYY') && eventdate.format('M') == date.format('M')) {
                  // Se añade la clase para marcar el evento en el calendario
                  var dayToHighlight = eventdate.format("DD/MM/YYYY");
                  dayWithEvent = trs.find("[data-day='" + dayToHighlight + "']");
                  if (!dayWithEvent.hasClass("active")) { // Si es el día activo no se pinta el día
                      dayWithEvent.addClass("event");
                  }
              }
            } else {
              // Se buscan todos los eventos del calendario en memoria
              var events = $('#calendar').fullCalendar( 'clientEvents');
              if(events) {
                $.each(events, function (index, evt) {
                    var eventdate = moment(evt.start).startOf('day');

                    var month = eventdate.format('M');
                    var year = eventdate.format('YYYY');

                    if (eventdate.format('YYYY') == date.format('YYYY') && eventdate.format('M') == date.format('M')) {
                        // Se añade la clase para marcar el evento en el calendario
                        var dayToHighlight = eventdate.format("DD/MM/YYYY");
                        dayWithEvent = trs.find("[data-day='" + dayToHighlight + "']");
                        if (!dayWithEvent.hasClass("active")) { // Si es el día activo no se pinta el día
                            dayWithEvent.addClass("event");
                        }
                    }
                  });
              }
            }
        }

        function resetForm($form) {
          $form.find( ".modal-footer .alert" ).addClass('hidden');
          $('#idActuacion').val('');
          $('#idCalendarEvent').val('');
          $('#descripcion').val('');
          $("#fechaActuacion").data("DateTimePicker").clear();
          $("#horaInicio").data("DateTimePicker").clear();
          $("#horaFin").data("DateTimePicker").clear();
          $("#tiempo").val('');
          $("#fechaRealizacion").data("DateTimePicker").clear();
          $("#fechaCancelacion").data("DateTimePicker").clear();
          $("#precio").val('');
          $("#cantidad").val('');
          $("#total").val('');
          $("#cobrado").val('');

          tinymce.get('notas').setContent('');
        }

        function fillModalForCreate(date) {
          // Limpiar datos antes de cargarlos
          var $form = $( "#actuacionForm" );

          resetForm($form);

          $("#editModal .modal-header .modal-title").text("Creación de actuación");

          // En la creación se muestra el selector de pacientes
          $( "#pacienteDiv" ).removeClass('hidden');
          $( "#pacienteLinkDiv" ).addClass('hidden');
          $( "#delete_btn" ).addClass('hidden');

          // var pacientesSelect = $('select[name=paciente]');
          // pacientesSelect.empty();
          //
          // @foreach ($pacientes as $paciente)
          //   pacientesSelect.append('<option value="{!! $paciente->id !!}">{!! $paciente->nombre !!}</option>');
          // @endforeach

          var doctoresSelect = $('select[name=doctor]');
          doctoresSelect.empty();

          @foreach ($doctores as $doctor)
            doctoresSelect.append('<option value="{!! $doctor->id !!}">{!! $doctor->name !!}</option>');
          @endforeach

          $("#fechaActuacion").data("DateTimePicker").date(moment(date, "DD/MM/YYYY"));
          $("#horaInicio").data("DateTimePicker").date(moment(date, "HH:MM:SS"));
          $("#horaFin").data("DateTimePicker").date(moment(date.add(1, 'hour'), "HH:MM:SS"));
          $("#tiempo").val('60');

          var tipoActuacionSelect = $('select[name=tipoActuacion]');
          tipoActuacionSelect.empty();
          tipoActuacionSelect.append('<option value="0">-- Seleccionar valor --</option>');

          @foreach ($tiposActuaciones as $tipo)
            tipoActuacionSelect.append('<option value="{!! $tipo->id !!}">{!! $tipo->nombre !!}</option>');
          @endforeach

          var tipoCancelacionSelect = $('select[name=tipoCancelacion]');
          tipoCancelacionSelect.empty();
          tipoCancelacionSelect.append('<option value="0">-- Seleccionar valor --</option>');

          @foreach ($tiposCancelaciones as $tipo)
            tipoCancelacionSelect.append('<option value="{!! $tipo->id !!}">{!! $tipo->nombre !!}</option>');
          @endforeach

          $('#editModal').modal({
            keyboard: true
          }, 'show');

          $form.attr('action', '{{ url('admin/agenda/modalStore') }}');
        }

        function fillModalForEdit(event) {
          // Limpiar datos antes de cargarlos
          var $form = $( "#actuacionForm" );

          resetForm($form);

          $('#idActuacion').val(event.idActuacion);
          $('#idCalendarEvent').val(event._id);

          $("#editModal .modal-header .modal-title").text("Editar actuación paciente: " + event.nombrePaciente);

          $('input[name="saldo"]').val(event.saldo);

          // En la edición se quita el selector de paciente
          $( "#pacienteDiv" ).addClass('hidden');
          $( "#pacienteLinkDiv" ).removeClass('hidden');
          $( "#linkPaciente" ).text(event.nombrePaciente);
          $( "#linkPaciente" ).attr('href', 'pacientes/' + event.idPaciente + '/edit');
          $( "#delete_btn" ).removeClass('hidden');

          var doctoresSelect = $('select[name=doctor]');
          doctoresSelect.empty();
          @foreach ($doctores as $doctor)
            var selected = {!! $doctor->id !!} == event.usuario_id;
            doctoresSelect.append('<option value="{!! $doctor->id !!}"' + (selected ? "selected" : "") + '>{!! $doctor->name !!}</option>');
          @endforeach

          var tipoActuacionSelect = $('select[name=tipoActuacion]');
          tipoActuacionSelect.empty();
          tipoActuacionSelect.append('<option value="0">-- Seleccionar valor --</option>');

          @foreach ($tiposActuaciones as $tipo)
            var selected = {!! $tipo->id !!} == event.idTipoActuacion;
            tipoActuacionSelect.append('<option value="{!! $tipo->id !!}"' + (selected ? "selected" : "") + '>{!! $tipo->nombre !!}</option>');
          @endforeach

          $('#descripcion').val(event.descripcion);
          $("input[name='precio']").val(event.precio);
          $("input[name='cantidad']").val(event.cantidad);
          $("input[name='total']").val(event.total);
          $("input[name='cobrado']").val(event.cobrado);

          $("#fechaActuacion").data("DateTimePicker").date(moment(event.fechaActuacion, "DD/MM/YYYY"));
          $("#horaInicio").data("DateTimePicker").date(moment(event.start, "HH:MM"));
          $("#horaFin").data("DateTimePicker").date(moment(event.end, "HH:MM"));
          $("#tiempo").val(event.tiempo);

          $("#realizada").prop('checked', event.realizada);
          $("#fechaRealizacion").data("DateTimePicker").date(event.fechaRealizacion);

          $("#cancelada").prop('checked', event.cancelada);
          $("#fechaCancelacion").data("DateTimePicker").date(moment(event.fechaCancelacion, "DD/MM/YYYY"));

          var tipoCancelacionSelect = $('select[name=tipoCancelacion]');
          tipoCancelacionSelect.empty();
          tipoCancelacionSelect.append('<option value="0">-- Seleccionar valor --</option>');

          @foreach ($tiposCancelaciones as $tipo)
            var selected = {!! $tipo->id !!} == event.idTipoCancelacion;
            tipoCancelacionSelect.append('<option value="{!! $tipo->id !!}"' + (selected ? "selected" : "") + '>{!! $tipo->nombre !!}</option>');
          @endforeach

          if (event.notas) {
              tinymce.get('notas').setContent(event.notas);
          }

          // Mostrar modal
          $('#editModal').modal({
            keyboard: true
          }, 'show');

          $form.attr('action', '{{ url('admin/agenda/modalUpdate') }}');

          var urlMasOpciones = 'agenda/' + event.idActuacion + '/edit';
          $('#masOpciones').attr('href', urlMasOpciones);
        }

        // $( "#actuacionForm" ).submit(function( event ) {
        $('#actuacionForm').on("click", ".btn-success", function(event) {
          console.log('ajax submitting');
          // Stop form from submitting normally
          // event.preventDefault();

          var id = $('#idActuacion').val();
          var _idEvent = $( '#idCalendarEvent').val();

          // Get some values from elements on the page:
          var $form = $( '#actuacionForm' ),
            tipoActuacion = $form.find( "select[name='tipoActuacion']" ).val(),
            descripcion = $form.find( "input[name='descripcion']" ).val(),
            fechaActuacion = $form.find( "input[name='fechaActuacion']" ).data('date'),
            horaInicio = $form.find( "input[name='horaInicio']" ).val(),
            horaFin = $form.find( "input[name='horaFin']" ).val(),
            tiempo = $form.find( "input[name='tiempo']" ).val(),
            realizada = $form.find( "input[name='realizada']" ).prop('checked'),
            fechaRealizacion = $form.find( "input[name='fechaRealizacion']" ).val(),
            cancelada = $form.find( "input[name='cancelada']" ).prop('checked'),
            fechaCancelacion = $form.find( "input[name='fechaCancelacion']" ).val(),
            tipoCancelacion = $form.find( "select[name='tipoCancelacion']" ).val(),
            paciente = $form.find( "input[name='paciente']" ).val(),
            doctor = $form.find( "select[name='doctor']" ).val(),
            precio = $form.find( "input[name='precio']" ).val(),
            cantidad = $form.find( "input[name='cantidad']" ).val(),
            total = $form.find( "input[name='total']" ).val(),

            url = $form.attr( "action" );

            var notas = tinymce.get('notas').getContent();

          // Si el form tiene un id es porque es edición
          if (id) {
              url = url + '/' + id;
          }

          // Send the data using post
          var posting = $.post( url,
            {
              paciente: paciente,
              doctor: doctor,
              tipoActuacion: tipoActuacion,
              descripcion: descripcion,
              fechaActuacion: fechaActuacion,
              horaInicio: horaInicio,
              horaFin: horaFin,
              tiempo: tiempo,
              realizada: realizada,
              fechaRealizacion: fechaRealizacion,
              cancelada: cancelada,
              fechaCancelacion: fechaCancelacion,
              tipoCancelacion: tipoCancelacion,
              notas: notas,
              precio: precio,
              cantidad: cantidad,
              total: total
            }
          );

          posting.done(function( data ) {
            //console.log(data);
            if (!data.errors) {

              // Se oculta el div de errores
              $form.find( ".modal-footer .alert" ).addClass('hidden');

              // Se muestra mensaje success
              var message = data.message;
              toastr.success(message);

              // Se oculta el modal
              $('#editModal').modal('hide');

              if (_idEvent) {
                // Actualizar el día en el calendario del evento antiguo
                var oldEvent = $('#calendar').fullCalendar( 'getEventSourceById', _idEvent );
                hightlightkDaysInDatePicker(null, oldEvent);
                // Eliminar evento antiguo
                $('#calendar').fullCalendar('removeEvents', _idEvent);
              }

              // Añadir evento actualizado
              $('#calendar').fullCalendar('renderEvent', data.event, true);

              // Actualizar el día en el calendario del nuevo evento
              hightlightkDaysInDatePicker(null, data.event);

            } else {
              $form.find( ".modal-footer .alert" ).empty();
              $form.find( ".modal-footer .alert" ).removeClass('hidden');
              toastr.error("No se ha podido guardar");
              $.each(data.errors, function (index, value) {
                $form.find( ".modal-footer .alert" ).append('<ul><li>' + value + '</li></ul>');
              });
            }
          });
          posting.fail(function() {
            alert( "error" );
          })

          // Prevent submit
          return false;
        });

        // Botón de borrar actuación en el modal
        $('#delete_btn').click(function() {
          var id = $('#idActuacion').val();
          console.log('Borrar id: ' + id);

          $('#delete_form')[0].action = '{{ route('voyager.agenda.destroy', ['id' => '__id']) }}'.replace('__id', id);
          $('#delete_modal').modal('show');
          // $('#voyager-loader').show();
        });

        // Se filtra la agenda por el doctor seleccionado
        $(function () {
          $('.radio input[type=radio]').change(function(){
            var id = $(this).val();
            var url = '{{ url('admin/agenda/actuacionesDoctor') }}' + '/' + id;

            var posting = $.post( url );
            posting.done(function( data ) {
              $('#calendar').fullCalendar( 'removeEvents');
              $('#calendar').fullCalendar( 'addEventSource', data);
            });
          })
        });

        // Se borra la selección del doctor y se carga la agenda completa
        $("#deselectRadioGroup").click(function () {
            $('input:radio').prop('checked', false);
            var url = '{{ url('admin/agenda/actuacionesDoctor') }}' + '/0';

            var posting = $.post( url );
            posting.done(function( data ) {
              $('#calendar').fullCalendar( 'removeEvents');
              $('#calendar').fullCalendar( 'addEventSource', data);
            });
        });

        function printBetweenDates() {

            var events = $('#calendar').fullCalendar( 'clientEvents');

            var desde_print = $("#desde").data('date');
            var hasta_print = $("#hasta").data('date');

            var desde = moment(desde_print, "DD/MM/YYYY");
            var hasta = moment(hasta_print, "DD/MM/YYYY");

            var doc = new jsPDF('p', 'pt', 'letter');

            var y = 20;
            var npage = 1;

            doc.setFontSize(10);
            doc.text("Listado de Agenda", 220, y);
            doc.setFontSize(8);
            doc.text("Fecha de impresión: " + moment(new Date()).format("DD/MM/YYYY HH:mm"), 420, y);

            y = y + 10;

            doc.text("Desde: " + desde.format('DD/MM/YYYY') + " hasta " + hasta.format('DD/MM/YYYY'), 420, y);

            y = y + 20;

            doc.text("F. Actuación | Hora inicio | Actuación", 20, y);
            doc.text("Doctor", 500, y);

            y = y + 10;

            doc.text("Tipo actuación", 20, y);

            y = y + 10;

            doc.line(20, y, 580, y);

            y = y + 10;

            var nevents = 0;
            events.forEach(function (event, i) {
                var eventdate = moment(event.start);
                console.log(eventdate.toDate());
                console.log(hasta);
                if (eventdate.startOf('day').isSameOrAfter(desde) && eventdate.endOf('day').isSameOrBefore(hasta.add(1, 'days'))) {
                    nevents = nevents + 1;

                    doc.setFontSize(8);

                    doc.text(moment(eventdate).format('dddd') + "  " +
                        moment(eventdate).format('DD/MM/YYYY') + "  " +
                        event.horaInicio + "  " +
                        event.descripcion, 20, y);

                    y = y + 10;

                    doc.text(event.tipoActuacion ? event.tipoActuacion : '', 20, y);
                    doc.text(event.nombreDoctor, 520, y);

                    y = y + 10;

                    doc.text("Paciente: " + event.nombrePaciente, 20, y);
                    doc.text("Tipo fichero General", 300, y);
                    doc.text("Código: " + event.codigoPaciente, 480, y);

                    y = y + 10;

                    doc.text("Dirección: " + event.direccion ? event.direccion : '', 20, y);

                    y = y + 10;

                    doc.text("Email: " + event.email ? event.email : '', 20, y);
                    doc.text("Tel: " + event.telefono ? event.telefono : '', 300, y);
                    doc.text("Móvil: " + event.movil ? event.movil : '', 480, y);

                    y = y + 10;

                    if (y > 600) {
                        doc.line(20, 710, 580, 710);
                        doc.text("Número registros: " + nevents, 400, 720);
                        doc.text("Página: " + npage, 500, 720);
                        doc.addPage();
                        y = 20;
                        npages = npage + 1;
                    } else {
                        y = y + 40;
                    }
                }
            });
            doc.save('Test.pdf');
        }

        // Cobro de actuación
        $(function () {
          // Si se está creando una actuación
          var idActuacion = $('#idActuacion').val();

          if (!idActuacion) {
            $('input[name="cantidad"]').val(1);
            $('input[name="precio"]').val(0);
            $('input[name="total"]').val(0);
            $('input[name="cobrado"]').val(0);
          }

          $( "#cobrar_button" ).click(function() {
            var idActuacion = $('#idActuacion').val();
            if (idActuacion) {
              var importe = $('input[name="total"]').val();
              $('input[name="totalCobro"]').val(importe);

              var action = '{{ url('admin/agenda') }}/' + idActuacion + '/cobrar';
              $('#cobrarForm').attr('action', action);

              $('#cobrarModal').modal('show');
            } else {
              toastr.error("Se debe seleccionar una actuación para poder cobrar");
            }
          });

          $( "#facturar_button" ).click(function() {
            var idActuacion = $('#idActuacion').val();
            if (idActuacion) {
              var cobrado = $('input[name="cobrado"]').val();
              $('input[name="descuentoFactura"]').val(0);
              $('input[name="totalFactura"]').val(cobrado);

              var action = '{{ url('admin/agenda') }}/' + idActuacion + '/facturar';
              $('#facturarForm').attr('action', action);

              $('#facturarModal').modal('show');
            } else {
              toastr.error("Se debe seleccionar una actuación para poder facturar");
            }
          });


          $('input[name="total"]').prop('readonly', true);
          $('input[name="cobrado"]').prop('readonly', true);

          // Al cambiar precio, se actualiza total
          $('input[name="precio"]').change(function() {
            var cantidad = $('input[name="cantidad"]').val();
            var precio = $('input[name="precio"]').val();
            $('input[name="total"]').val(cantidad * precio);
          });

          // Al cambiar cantidad, se actualiza total
          $('input[name="cantidad"]').change(function() {
            var cantidad = $('input[name="cantidad"]').val();
            var precio = $('input[name="precio"]').val();
            $('input[name="total"]').val(cantidad * precio);
          });
        });

        $(function(){
          var pacientes = @json($pacientes);
          var tableTratamientos = $('#dataTableTratamientos').DataTable({!! json_encode(
              array_merge([
                  "order" => [],
                  "language" => __('voyager::datatable'),
                  "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
              ],
              config('voyager.dashboard.data_tables', []))
          , true) !!});

          $(document).on('click', '#seleccionarPaciente', function(){
            var id = $(this).val();
            var paciente = pacientes.find(x => x.id == id);
            var nombre = paciente.nombre;
            var codigo = paciente.codigo;
            var saldo = paciente.saldo;

            $('input[name="datosPaciente"]').val(nombre + ' - Código: ' + codigo);
            $('input[name="paciente"]').val(id);
            $('input[name="saldo"]').val(saldo);

            $('#pacientesModal').modal('hide');
          });
        });

        $(function(){
          var tratamientos = {!! json_encode($tratamientos) !!};
          var table = $('#dataTablePacientes').DataTable({!! json_encode(
              array_merge([
                  "order" => [],
                  "language" => __('voyager::datatable'),
                  "columnDefs" => [['searchable' =>  false, 'targets' => -1 ]],
              ],
              config('voyager.dashboard.data_tables', []))
          , true) !!});

          $(document).on('click', '#seleccionarTratamiento', function(){
            var id = $(this).val();
            var tratamiento = tratamientos.find(x => x.id == id);
            var tratamientoDesc = tratamiento.nombre;
            var precio = parseFloat(tratamiento.importe);
            var cantidad = $('input[name="cantidad"]').val() ? $('input[name="cantidad"]').val() : 1;

            $('input[name="descripcion"]').val(tratamientoDesc);
            $('input[name="precio"]').val(precio);
            $('input[name="total"]').val(precio * cantidad);

            $('#tratamientosModal').modal('hide');
          });
        });

    </script>
@stop
