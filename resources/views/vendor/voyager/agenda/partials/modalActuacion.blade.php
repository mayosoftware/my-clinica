<div class="modal modal-info fade large" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <form role="form"
          id="actuacionForm"
          class="form-edit-add"
          action="{{ url('admin/agenda/modalUpdate') }}"
          method="POST" enctype="multipart/form-data">

          <input type="hidden" id="paciente" name="paciente" />

          <!-- CSRF TOKEN -->
          {{ csrf_field() }}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Editar actuación paciente: Nombre paciente</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="idActuacion" name="idActuacion" />
                {{-- Id del evento de fullcalendar. Se utiliza para eliminar el evento antiguo al ser actualizado --}}
                <input type="hidden" id="idCalendarEvent" name="idCalendarEvent" />

                <div class="row">
                  <div class="form-group col-md-4" name="pacienteDiv" id="pacienteDiv">
                    <label class="control-label" for="datosPaciente">Paciente:</label>
                     <div class="input-group">
                       <input class="form-control" type="text" name="datosPaciente" placeholder="Seleccionar paciente" readonly>
                       <span class="input-group-btn">
                         <a class="btn-sm btn-info" data-toggle="modal" data-target="#pacientesModal">
                           <i class="voyager-double-right"></i>
                         </a>
                       </span>
                     </div>
                  </div>
                  <div class="form-group col-md-4 hidden" name="pacienteLinkDiv" id="pacienteLinkDiv">
                    <label class="control-label" for="datosPaciente">Paciente:</label>
                    <div class="form-group">
                      <a href="#" id="linkPaciente" target="_blank"></a>
                    </div>
                  </div>
                  <div class="form-group col-md-2">
                    <label class="control-label" for="saldo">Saldo:</label>
                    <input class="form-control" type="text" name="saldo" readonly>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="control-label" for="doctor">Doctor:</label>
                    <select class="form-control select2" name="doctor" id="doctor">
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label class="control-label" for="state">Tipo de actuación:</label>
                    <select name="tipoActuacion" id="tipoActuacion" class="form-control">
                      <option value="">-- Seleccionar valor --</option>
                    </select>
                  </div>
                </div>
                <hr>
                <div class="row">
                    <div class="form-group col-md-4">
                      <label for="descripcion" class="control-label">Descripción</label>
                       <div class="input-group">
                         <input class="form-control" type="text" id="descripcion" name="descripcion" placeholder="Descripción">
                         <span class="input-group-btn">
                           <a class="btn-sm btn-info" data-toggle="modal" data-target="#tratamientosModal">
                             <i class="voyager-double-right"></i>
                           </a>
                         </span>
                       </div>
                    </div>
                    <div class="form-group col-md-2" id="cantidad">
                        <label for="name">Cantidad</label>
                        <input type="number" class="form-control" name="cantidad" required step="1" value="11">
                    </div>
                    <div class="form-group  col-md-2" id="precio">
                        <label for="name">Precio</label>
                        <input type="number" class="form-control" name="precio" required step="1" value="11">
                    </div>
                    <div class="form-group  col-md-2" id="total">
                        <label for="name">Total</label>
                        <input type="number" class="form-control" name="total" required step="1" value="11">
                    </div>
                    <div class="form-group  col-md-2" id="cobrado">
                        <label for="name">Cobrado</label>
                        <input type="number" class="form-control" name="cobrado" required step="1" value="11">
                    </div>
                </div>

                <div class="row">
                  <div class="col-sm-3">
                      <div class="form-group">
                          <label class="control-label" for="fechaActuacion">Fecha actuación</label>
                          <div class="controls">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  <input name="fechaActuacion" id="fechaActuacion" class="form-control" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="form-group">
                          <label for="horaInicio" class="control-label">Hora inicio</label>
                          <div class="controls">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                  <input name="horaInicio" id="horaInicio" class="form-control" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="form-group">
                          <label for="horaFin" class="control-label">Hora fin</label>
                          <div class="controls">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                                  <input name="horaFin" id="horaFin" class="form-control" />
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-3">
                      <div class="form-group">
                          <label for="tiempo" class="control-label">Tiempo</label>
                          <div class="controls">
                              <div class="input-group">
                                  <input name="tiempo" id="tiempo" class="form-control" readonly />
                                  <span class="input-group-addon">mins</span>
                              </div>
                          </div>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-1">
                      <div class="form-group">
                          <label for="realizada" class="control-label">Realizada</label>
                          <div class="controls">
                              <input type="checkbox" id="realizada" name="realizada" />
                              <i class="fa fa-check" style="color: forestgreen" aria-hidden="true"></i>
                          </div>
                      </div>
                  </div>
                  <div class="col-sm-4">
                      <div class="form-group">
                          <label class="control-label" for="fechaRealizacion">Fecha realización</label>
                          <div class="controls">
                              <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                  <input name="fechaRealizacion" id="fechaRealizacion" class="form-control" />
                              </div>
                          </div>
                      </div>
                  </div>

                <div class="col-sm-1">
                    <div class="form-group">
                        <label for="cancelada" class="control-label">Cancelada</label>
                        <div class="controls">
                          <input type="checkbox" id="cancelada" name="cancelada" />
                          <i class="fa fa-times" style="color: darkred" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="fechaCancelacion">Fecha cancelación</label>
                        <div class="controls">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input name="fechaCancelacion" id="fechaCancelacion" class="form-control" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="form-group">
                        <label class="control-label" for="tipoCancelacion">Tipo</label>
                        <div class="controls">
                            <select id="tipoCancelacion" name="tipoCancelacion" class="form-control"></select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="col-sm-12">
                <div class="form-group">
                  <label class="control-label" for="notas">Notas</label>
                  <textarea id="notas" name="notas"></textarea>
                </div>
              </div>
            </div>


            </div>
            <div class="modal-footer">
                <div class="alert alert-danger pull-left hidden">

                </div>

                <a class="btn-sm btn-default" id="masOpciones">
                  <i class="voyager-dot-3"></i>
                    <span> Más opciones</span>
                </a>
                <button type="button" class="btn btn-success">
                  <i class="voyager-download"></i>
                    <span>Guardar</span>
                </button>
                <a class="btn btn-danger" id="delete_btn">
                  <i class="voyager-trash"></i>
                  <span>Eliminar</span>
                </a>
                <button type="reset" data-dismiss="modal" class="btn btn-default">
                  Cancelar
                </button>
            </div>

        </form>
      </div>
  </div>
</div>
