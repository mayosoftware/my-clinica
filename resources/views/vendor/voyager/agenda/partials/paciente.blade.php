@if ($dataTypeContent->getKey())
    <div class="container-fluid">
        <div class="alert alert-info">
            <strong>Actuación del paciente:</strong>
            <p>Código: {{ $dataTypeContent->paciente->codigo . ' - ' . $dataTypeContent->paciente->nombre }} 
              <small><code><a target="_blank" href="{{ route('voyager.pacientes.edit', ['paciente' => $dataTypeContent->paciente_id ]) }}">Ficha del paciente</a></code></small>
            </p>
        </div>
    </div>
@endif
