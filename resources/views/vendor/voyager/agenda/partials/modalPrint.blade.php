<div class="modal fade" id="printModal" role="dialog">
 <div class="modal-dialog">

     <!-- Modal content-->
     <div class="modal-content">
         <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal">&times;</button>
             <h4 class="modal-title">Opciones imprimir</h4>
         </div>
         <div class="modal-body">
             <h4 class="modal-title">Rango de fechas</h2>
             <div class="row">
                 <div class="col-sm-6">
                     <div class="form-group">
                         <label class="control-label">Desde</label>
                         <div class="controls">
                             <div class='input-group date' id='datetimepicker1'>
                                 <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                 <input id="desde" name="desde" class="form-control" />
                             </div>
                         </div>
                     </div>
                 </div><!--/col-sm-6-->
                 <div class="col-sm-6">
                     <div class="form-group">
                         <label class="control-label">Hasta</label>
                         <div class="controls">
                             <div class='input-group date' id='datetimepicker1'>
                                 <span class="input-group-addon">
                                     <span class="glyphicon glyphicon-calendar"></span>
                                 </span>
                                 <input id="hasta" name="hasta" class="form-control" />
                             </div>
                         </div>
                     </div>
                 </div><!--/col-sm-6-->
             </div>
         </div>
         <div class="modal-footer">
             <button class="btn btn-info" id="printButton" name="printButton">
                 <i class="voyager-documentation"></i> <span>Imprimir agenda</span>
             </button>
             <button type="reset" data-dismiss="modal" class="btn">Cancelar</button>
         </div>
     </div>
 </div>
</div>
