@extends('templates.master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_title', __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ __('voyager::generic.'.(!is_null($dataTypeContent->getKey()) ? 'edit' : 'add')).' '.$dataType->display_name_singular }}
    </h1>
    @if (!is_null($dataTypeContent->getKey()))

      <button class="btn btn-success" type="button" id="cobrar_button">
        <i class="fa fa-euro-sign"></i> Cobrar actuación
      </button>

      <button class="btn btn-warning" type="button" id="facturar_button">
        <i class="fa fa-receipt"></i> Facturar actuación
      </button>
    @endif

    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    @include('vendor.voyager.agenda.partials.paciente')

  <div class="page-content edit-add container-fluid">
    {{-- Crear paciente --}}
    @if(is_null($dataTypeContent->getKey()))
        @include('vendor.voyager.agenda.info-actuacion')

    {{-- Editar paciente --}}
    @else
      <div class="row">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#datos" aria-controls="datos" role="tab" data-toggle="tab">Datos actuación</a></li>
          <li role="presentation"><a href="#ficheros" aria-controls="ficheros" role="tab" data-toggle="tab">Ficheros</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="datos">
            @include('vendor.voyager.agenda.info-actuacion')
          </div>
          <div role="tabpanel" class="tab-pane" id="ficheros">
            @include('vendor.voyager.ficheros')
          </div>
        </div>
      </div>

    @endif
  </div>
@stop

@section('javascript')
    @parent
    <script>
        var params = {}
        var $image

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs
            $('.form-group input[type=date]').each(function (idx, elt) {
                if (elt.type != 'date' || elt.hasAttribute('data-datepicker')) {
                    elt.type = 'text';
                    $(elt).datetimepicker($(elt).data('datepicker'));
                }
            });

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', function (e) {
                e.preventDefault();
                $image = $(this).siblings('img');

                params = {
                    slug:   '{{ $dataType->slug }}',
                    image:  $image.data('image'),
                    id:     $image.data('id'),
                    field:  $image.parent().data('field-name'),
                    _token: '{{ csrf_token() }}'
                }

                $('.confirm_delete_name').text($image.data('image'));
                $('#confirm_delete_modal').modal('show');
            });

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $image.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing image.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection
