@extends('templates.master')

@section('page_title', __('voyager::generic.viewing').' '.$dataType->display_name_plural)

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ $dataType->display_name_plural }}
        </h1>
        @can('add',app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan
        @can('delete',app($dataType->model_name))
            @include('voyager::partials.bulk-delete')
        @endcan
        @can('edit',app($dataType->model_name))
        @if(isset($dataType->order_column) && isset($dataType->order_display_column))
            <a href="{{ route('voyager.'.$dataType->slug.'.order') }}" class="btn btn-primary">
                <i class="voyager-list"></i> <span>{{ __('voyager::bread.order') }}</span>
            </a>
        @endif
        @endcan
        @if(isset($printAction) && $printAction)
          <a href="#" class="btn btn-info disabled" value="Imprimir" name="printButton" target="_blank">
            <i class="voyager-documentation"></i> <span>Imprimir</span>
          </a>
        @endif
        @include('voyager::multilingual.language-selector')

        <div class="pull-right">
          @yield('page_header_right')
        </div>
    </div>
@stop

@section('content')
  <div class="row">
      <div class="col-md-12">
          <div class="panel panel-bordered">
              <div class="panel-body">
                <div class="row">
                  <div class="col-md-12">
                    <form class="form-search form-inline" action="{{ route('admin.etiquetas.index') }}" method="get">
                      <div class="form-group">
                        <fieldset>
                          <legend>Pacientes última cita entre: </legend>
                            <div class="form-inline">
                              <div class="form-group">
                                <input name="start" id="start" class="form-control" value="{{ $start }}" />
                              </div>
                              <div class="form-group">
                                <input name="end" id="end" class="form-control" value="{{ $end }}" />
                              </div>
                            </div>
                        </fieldset>
                      </div>
                      <div class="form-group">
                        <fieldset>
                          <legend>Pacientes fecha nacimiento entre: </legend>
                              <div class="form-inline">
                                  <input name="birthStart" id="birthStart" class="form-control" value="{{ $birthStart }}" />
                                  <input name="birthEnd" id="birthEnd" class="form-control" value="{{ $birthEnd }}" />

                                <button class="btn btn-success" type="submit" name="submitbutton" value="search">
                                  <i class="voyager-search"></i> <span>Buscar</span>
                                </button>

                                <button class="btn btn-info" type="submit" name="submitbutton" value="print">
                                  <i class="voyager-documentation"></i> <span>Imprimir etiquetas</span>
                                </button>
                              </div>
                        </fieldset>
                      </div>
                    </form>
                  </div>
                </div>

                <div class="table-responsive">
                    <table id="dataTable" class="table table-hover">
                        <thead>
                          <tr>
                            <th>Paciente</th>
                            <th>Fecha de nacimiento</th>
                            <th>Email</th>
                            <th>Población</th>
                            <th>Código postal</th>
                            <th>Saldo</th>
                          </th>
                        </thead>
                        <tbody>
                          @foreach ($dataTypeContent as $data)
                            @if ($data != null)
                            <tr>
                              <td>{{ $data->nombre }}</td>
                              <td>{{ $data->fecha_nacimiento ? \Carbon\Carbon::parse($data->fecha_nacimiento)->format('d/m/Y') : '' }}</td>
                              <td>{{ $data->email }}</td>
                              <td>{{ $data->poblacion ? $data->poblacion : '' }}</td>
                              <td>{{ $data->codigo_postal }}</td>
                              <td>{{ $data->saldo }}</td>
                            </tr>
                            @endif
                          @endforeach
                        </tbody>
                      </table>
                  </div>
              </div>
              <div class="panel-footer">

              </div>
          </div>
      </div>
  </div>
@endsection

@section('javascript')
  <script type="text/javascript">

    $(document).ready(function () {

      configDatePicker('input[name="start"]');
      configDatePicker('input[name="end"]');
      configDatePicker('input[name="birthStart"]');
      configDatePicker('input[name="birthEnd"]');

      var table = $('#dataTable').DataTable();

    });
  </script>
@endsection
