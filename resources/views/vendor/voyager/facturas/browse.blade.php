@extends('templates.browse')

@section('filterbar')
  <div class="row">
    <div class="col-md-12">
      <form class="form-search form-inline" action="{{ route('admin.facturas.printBetweenDates') }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
          <fieldset>
            <legend>Facturas entre: </legend>
              <div class="form-inline">
                <div class="form-group">
                  <input name="start" id="start" class="form-control" required />
                </div>
                <div class="form-group">
                  <input name="end" id="end" class="form-control" required />
                </div>
                <button class="btn btn-info" type="submit">
                  <i class="voyager-documentation"></i> <span>Imprimir todas</span>
                </button>
              </div>
          </fieldset>
        </div>



      </form>
    </div>
  </div>

@endsection

@section('javascript')
  @parent
  <script type="text/javascript">

    $(document).ready(function () {

      configDatePicker('input[name="start"]');
      configDatePicker('input[name="end"]');

    });
  </script>
@endsection
