@extends('templates.edit-add')

@section('page_header_right')
  @if (!is_null($dataTypeContent->getKey()))

    <a class="btn btn-default pull-right" href="{{ route('admin.facturas.print', ['id' => $dataTypeContent->getKey() ]) }}" target="_blank">
      <i class="fa fa-print"> Imprimir</i>
    </a>

  @endif
@endsection

@section('content')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  @include('partials.modalSeleccionarTratamiento')
  @include('partials.modalSeleccionarPlanPredefinido')

    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">

                  <!-- form start -->
                  <form role="form"
                          id="factura-form"
                          class="form-edit-add factura-form"
                          action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                          method="POST" enctype="multipart/form-data">

                      <div class="panel-body">

                        <!-- PUT Method if we are editing -->
                        @if(!is_null($dataTypeContent->getKey()))
                            {{ method_field("PUT") }}
                        @endif

                        <!-- CSRF TOKEN -->
                        {{ csrf_field() }}

                        <input type="hidden" name="ejercicio"
                          value="@if (!is_null($dataTypeContent->getKey())){{ $dataTypeContent->secuencia->ejercicio->id }}@else{{ $ejercicio }}@endif">

                        <input type="hidden" name="paciente_id"
                          value="@if (!is_null($dataTypeContent->getKey())) {{ $dataTypeContent->paciente->id }} @endif">

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                          @if (!is_null($dataTypeContent->getKey()))
                            <div class="form-group col-lg-3">
                               <label for="codigo_paciente">Paciente</label>
                               <input class="form-control" type="text" name="nombre_paciente" value="{{ $paciente->codigo . ' - ' . $paciente->nombre}}" readonly>
                            </div>

                           @else
                             <div class="form-group col-lg-3" name="pacienteDiv">
                                <label class="control-label" for="q">Paciente:</label>
                                <div class="input-group">
                                  <input type="text" class="form-control" name="q" placeholder="Introduce el nombre, código o email de un paciente"
                                    required/>
                                  <span class="input-group-btn">
                                    <a class="btn-sm btn-danger disabled clear">
                                      <i class="fas fa-trash-alt"></i>
                                    </a>
                                  </span>
                                </div>
                             </div>
                           @endif

                        <div class="form-group col-lg-3">
                          <label for="numero">Nº de factura</label>
                          <input class="form-control" type="text" name="numero"
                            value="@if(!is_null($dataTypeContent->getKey())){{ $dataTypeContent->numero}}@else{{ $siguiente_numero_factura }}@endif"
                            required>
                       </div>

                       <div class="form-group col-lg-3">
                         <label for="fecha_factura">Fecha</label>
                         <input class="form-control" type="text" name="fecha_factura"
                          value="{{ $dataTypeContent->fecha_factura }}"
                          required>
                      </div>

                      <div class="form-group col-lg-3">
                        <label for="fecha_vencimiento">Fecha vencimiento</label>
                        <input class="form-control" type="text" name="fecha_vencimiento"
                          value="{{ $dataTypeContent->fecha_vencimiento }}"
                          readonly>
                     </div>

                     @if (!is_null($dataTypeContent->getKey()))
                       <div class="form-group col-lg-2">
                         <label for="indice">Índice</label>
                         <input class="form-control" type="text" name="indice"
                           value="#{{ $dataTypeContent->indice }}" readonly>
                      </div>
                     @endif
                     <div class="form-group col-lg-4">
                       <label for="nombre">Nombre identificativo</label>
                       <input class="form-control" type="text" name="nombre"
                         value="@if(is_null($dataTypeContent->getKey()))Factura {{ $siguiente_numero_factura }} @else {{ $dataTypeContent->nombre }} @endif">
                    </div>

                     @include('vendor.voyager.tratamientos-paciente.partials.tablePlanTratamiento',
                       ['data' => $dataTypeContent, 'key' => $dataTypeContent->getKey(), 'version' => 'invoice',
                       'mode' => is_null($dataTypeContent->getKey()) ? 'add' : 'edit' ])

                      </div><!-- panel-body -->

                      <div class="panel-footer">
                        <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                      </div>

                    </form>
                  </div>
              </div>
          </div>
      </div>

@stop

@section('javascript')

  @parent
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/PlanTratamiento.js')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  <script type="text/javascript">
  $( document ).ready(function() {
    var idTratamiento = null;
    var mode = 'add';
    var version = 'invoice';

    @if ( !is_null($dataTypeContent->getKey()) )
      idTratamiento = {{ $dataTypeContent->getKey() }};
      mode = 'edit';
    @endif

    var routeFindPlan = '{{ url('admin/planesPredefinidos/find') }}';
    setupPlan({{ $dataTypeContent->lineas->count() ?? 0 }},
      {!! json_encode($tratamientos) !!},
      version,
      mode,
      {!! $dataTypeContent->financiado ? 1 : 0 !!},
      {!! json_encode($piezas) !!},
      routeFindPlan
    );

    configDatePicker('input[name="fecha_factura"]');
    configDatePicker('input[name="fecha_vencimiento"]');
    $('input[name="fecha_factura"]').on("dp.change", calcularVencimiento);

    @if ( is_null($dataTypeContent->getKey()) )
      $('input[name="fecha_factura"]').data("DateTimePicker").date(moment());
      $('input[name="fecha_vencimiento"]').data("DateTimePicker").date(moment().add('days', 30));
    @else
      $('input[name="fecha_factura"]').data("DateTimePicker").date(moment('{{ $dataTypeContent->fecha_factura }}'));
      $('input[name="fecha_vencimiento"]').data("DateTimePicker").date(moment('{{ $dataTypeContent->fecha_vencimiento }}'));
    @endif

    var $form = $( '.factura-form' );

    $form.find('input[name="q"]').autocomplete({
      source: "{{ route('searchPaciente') }}",
      minLength: 2,
      select: function(event, ui) {
        var id = ui.item.id;
        var saldo = ui.item.saldo;
        if (id) {
          $form.find('input[name="q"]').prop('readonly', 'true');
          $form.find('.clear').removeClass('disabled');
          $form.find('input[name="paciente_id"]').val(id);
          $form.find('input[name="saldo"]').val(saldo);

        } else {
          return false;
        }
      }
    });

    $form.on('click', '.clear', function(){
      $form.find('input[name="q"]').removeAttr('readonly');
      $form.find('input[name="q"]').val('');
      $form.find('input[name="paciente_id"]').val('');
      $form.find('.clear').addClass('disabled');
    });

    // Al cambiar la fecha, actualizar fecha de vencimiento
    $form.find
  });

  function calcularVencimiento(e) {
    var vencimiento = e.date.add('days', 30);
    $('input[name="fecha_vencimiento"]').data("DateTimePicker").date(vencimiento);
  }

  </script>


@endsection
