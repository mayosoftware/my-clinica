@extends('templates.edit-add')

@section('content')
  <div class="page-content edit-add container-fluid">
      <div class="row">
          <div class="col-md-12">
              <div class="panel panel-bordered">

                <!-- form start -->
                <form role="form"
                        class="form-edit-add plan-predefinido-form"
                        action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                        method="POST" enctype="multipart/form-data">

                    <div class="panel-body">

                      <!-- PUT Method if we are editing -->
                      @if(!is_null($dataTypeContent->getKey()))
                          {{ method_field("PUT") }}
                      @endif

                      <!-- CSRF TOKEN -->
                      {{ csrf_field() }}

                      <div class="form-group">
                        <label for="nombre">Nombre</label>
                        @if (!is_null($dataTypeContent->getKey()))
                          <input type="text" class="form-control" name="nombre" value="{{ $dataTypeContent->nombre }}" required>
                        @else
                          <input type="text" class="form-control" name="nombre" required>
                        @endif

                        <label for="observaciones">Observaciones</label>
                        @if (!is_null($dataTypeContent->getKey()))
                          <textarea name="observaciones" class="form-control" rows="4" cols="60" maxlength="255">{{ $dataTypeContent->observaciones }}</textarea>
                        @else
                          <textarea name="observaciones" class="form-control" rows="4" cols="60" maxlength="255"></textarea>
                        @endif
                      </div>

                      <div class="planTratamiento form-group col-md-12">
                          <h4>Plan de tratamiento</h4>
                          <div class="alert alert-danger print-error-msg" style="display:none">
                            <ul></ul>
                          </div>

                          <div class="alert alert-success print-success-msg" style="display:none">
                            <ul></ul>
                          </div>

                          <div class="table-responsive">
                            <table class="table table-bordered" id="dynamic_field">
                              <thead>
                                <th>#</th>
                                <th>Descripcion</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Total</th>
                                <th></th>
                              </thead>
                              <tfoot>
                                <td colspan=4>TOTAL</td>
                                <td>
                                  @if (!is_null($dataTypeContent->getKey()))
                                    <input type="number" name="total" class="form-control" value={{ $dataTypeContent->total }} readonly>
                                  @else
                                    <input type="number" name="total" class="form-control" value=0 readonly>
                                  @endif
                                </td>
                                <td><a class="btn-sm btn-success btn_add" role="button"><i class="fas fa-plus"></i></a></td>
                              </tfoot>
                              <tbody>
                                @if (!is_null($dataTypeContent->getKey()))
                                  @foreach ($dataTypeContent->lineas as $linea)
                                    <tr id="row{{ $linea->linea }}" class="dynamic-added">
                                      <input type="hidden" name="linea_id[{{ $linea->linea }}]" value="{{ $linea->id }}">
                                      <td>{{ $linea->linea }}</td>
                                      <td>
                                         <div class="col-lg-12">
                                          <div class="input-group">
                                            <input type="text" id="{{ $linea->id }}" name="descripcion_linea[{{ $linea->linea }}]"
                                              placeholder="Introduce una descripción" class="form-control descripcion_linea"
                                              value="{{ $linea->descripcion }}" required>
                                            <span class="input-group-btn">
                                              <a class="btn-sm btn-info" name="descripcion_linea_button[{{ $linea->linea }}]"
                                                data-target="#tratamientosModal" onclick="setDescriptionIndex({{ $linea->linea }});">
                                                <i class="voyager-double-right"></i>
                                              </a>
                                            </span>
                                          </div>
                                        </div>
                                      </td>
                                      <td>
                                        <input type="number" name="cantidad_linea[{{ $linea->linea }}]" class="form-control"
                                          value={{ $linea->cantidad }} required min=1
                                          onchange="updateTotals({{ $linea->linea }})">
                                      </td>
                                      <td>
                                        <input type="number" name="precio_linea[{{ $linea->linea }}]" class="form-control"
                                          value={{ $linea->precio }} required onchange="updateTotals({{ $linea->linea }})" min=0>
                                      </td>
                                      <td>
                                        <input type="number" name="total_linea[{{ $linea->linea }}]" class="form-control"
                                          value={{ $linea->total }} required
                                          onchange="updateTotals({{ $linea->linea }})" min=0>
                                      </td>
                                      <td>
                                        <a name="{{ $linea->linea }}" value="{{ $linea->id }}"
                                          class="btn-sm btn-danger btn_remove" role="button">
                                          <i class="fas fa-trash"></i>
                                        </a>
                                      </td>
                                    </tr>
                                  @endforeach
                                @endif
                              </tbody>
                            </table>
                          </div>
                        </div>
                    </div><!-- panel-body -->

                    <div class="panel-footer">
                      <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                    </div>

                  </form>
                </div>
            </div>
        </div>
    </div>

    @include('partials.modalSeleccionarTratamiento')
@endsection

@section('javascript')
  <script type="text/javascript">

    var rows = 0;
    @if (!is_null($dataTypeContent->getKey()))
      rows = {{ $dataTypeContent->lineas->count() }};
    @endif

    var tratamientos = {!! json_encode($tratamientos) !!};

    // Indicador de la descripción seleccionada
    var descIndex = 0;
    function setDescriptionIndex(index) {
        descIndex = index;
        //$('#tratamientosModal').show();
        $('#tratamientosModal').modal({
          keyboard: true
        }, 'show');
    }

    // Calcular valores
    function updateRow(index) {
      var precio = parseFloat($('input[name="precio_linea[' + index + ']"]').val() ? $('input[name="precio_linea[' + index + ']"]').val() : 0);
      var cantidad = parseFloat($('input[name="cantidad_linea[' + index + ']"]').val() ? $('input[name="cantidad_linea[' + index + ']"]').val() : 0);
      var totalAntes = parseFloat($('input[name="total_linea[' + index + ']"]').val() ? $('input[name="total_linea[' + index + ']"]').val() : 0);

      var total = parseFloat(precio * cantidad);
      total = parseFloat(total).toFixed(2);

      $('input[name="total_linea[' + index + ']"]').val(total);

    }

    function updateTotals(index) {
      if (index) {
        updateRow(index);
      }

      var totalTratamiento = 0.0;

      // Actualizar totales
      for (var i = 1; i <= rows; i++) {
        var total = $('input[name="total_linea[' + i + ']"]').val();
        var precio = $('input[name="precio_linea[' + i + ']"]').val();
        var cantidad = $('input[name="cantidad_linea[' + i + ']"]').val();

        if (total) {
            totalTratamiento += parseFloat(total);
        }
      }

      $('input[name="total"]').val(parseFloat(totalTratamiento).toFixed(2));
    }

    function addLinea() {
      $('#dynamic_field tbody').append(
        '   <tr id="row' + rows + '" class="dynamic-added"> ' +
        '     <td>' + rows + '</td> ' +
        '     <td> ' +
        '        <div class="col-lg-12"> ' +
        '         <div class="input-group"> ' +
        '           <input type="text" name="descripcion_linea[' + rows +  ']" ' +
        '             placeholder="Introduce una descripción" class="form-control descripcion_linea" ' +
        '             required> ' +
        '           <span class="input-group-btn"> ' +
        '             <a class="btn-sm btn-info" name="descripcion_linea_button[' + rows +  ']" ' +
        '               data-target="#tratamientosModal" onclick="setDescriptionIndex(' + rows +  ');"> ' +
        '               <i class="voyager-double-right"></i> ' +
        '             </a> ' +
        '           </span> ' +
        '         </div> ' +
        '       </div> ' +
        '     </td> ' +
        '     <td> ' +
        '       <input type="number" name="cantidad_linea[' + rows +  ']" class="form-control" ' +
        '         value=1 required min=1 ' +
        '         onchange="updateTotals(' + rows +  ')"> ' +
        '     </td> ' +
        '     <td> ' +
        '       <input type="number" name="precio_linea[' + rows +  ']" class="form-control" ' +
        '         required onchange="updateTotals(' + rows +  ')" min=0> ' +
        '     </td> ' +
        '     <td> ' +
        '       <input type="number" name="total_linea[' + rows +  ']" class="form-control" ' +
        '         required ' +
        '         onchange="updateTotals(' + rows +  ')" min=0> ' +
        '     </td> ' +
        '    <td> ' +
        '      <a name="[' + rows + ']" value="[' + rows + ']" ' +
        '        class="btn-sm btn-danger btn_remove" role="button"> ' +
        '        <i class="fas fa-trash"></i> ' +
        '      </a> ' +
        '    </td>' +
        '   </tr> '
      );
    }

    // 1. Añadir nueva linea
    $(document).on('click', '.btn_add', function(){
      rows++;
      addLinea();
    });

    // 2. Eliminar linea
    $(document).on('click', '.btn_remove', function(){
      var row = $(this).attr('name');
      $('#row' + row + '').remove();
      updateTotals();
    });

    $(document).on('click', '#seleccionarTratamiento', function(){
      var id = $(this).val();
      var tratamiento = tratamientos.find(x => x.id == id);
      var tratamientoDesc = tratamiento.nombre;
      var precio = parseFloat(tratamiento.importe);

      $('input[name="descripcion_linea[' + descIndex + ']"]').val(tratamientoDesc);
      $('input[name="precio_linea[' + descIndex + ']"]').val(precio);

      updateTotals(descIndex);

      $('#tratamientosModal').modal('hide');
    });
  </script>
@endsection
