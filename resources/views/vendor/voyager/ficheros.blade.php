
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">

                <div id="filemanager">

                    <div id="toolbar">
                        <div class="btn-group offset-right">
                            <button type="button" class="btn btn-primary" id="upload"><i class="voyager-upload"></i>
                                {{ __('voyager::generic.upload') }}
                            </button>
                            <button type="button" class="btn btn-primary" id="new_folder"
                                    onclick="jQuery('#new_folder_modal').modal('show');"><i class="voyager-folder"></i>
                                {{ __('voyager::generic.add_folder') }}
                            </button>
                        </div>
                        <button type="button" class="btn btn-default" id="refresh"><i class="voyager-refresh"></i>
                        </button>
                        <div class="btn-group offset-right">
                            <button type="button" class="btn btn-default" id="delete"><i class="voyager-trash"></i>
                                {{ __('voyager::generic.delete') }}
                            </button>
                        </div>
                    </div>

                    <div id="uploadPreview" style="display:none;"></div>

                    <div id="uploadProgress" class="progress active progress-striped">
                        <div class="progress-bar progress-bar-success" style="width: 0"></div>
                    </div>

                    <div id="content">


                        <div class="breadcrumb-container">
                            <ol class="breadcrumb filemanager">
                                <li class="media_breadcrumb" data-folder="/" data-index="0"><span class="arrow"></span><strong>{{ __('voyager::media.library') }}</strong></li>
                                <template v-for="(folder, index) in folders">
                                    <li v-bind:data-folder="folder" v-bind:data-index="index+1"
									v-bind:class="{media_breadcrumb: index !== folders.length - 1}"><span
                                                class="arrow"></span>@{{ folder }}</li>

                                </template>
                            </ol>

                            <div class="toggle"><span>{{ __('voyager::generic.close') }}</span><i class="voyager-double-right"></i></div>
                        </div>
                        <div class="flex">

                            <div id="left">

                                <ul id="files">

                                    <li v-for="(file,index) in files.items">
                                        <div class="file_link" :data-folder="file.name" :data-index="index">
                                            <div class="link_icon">
                                                <template v-if="file.type.includes('image')">
                                                    <div class="img_icon" :style="imgIcon(file.path)"></div>
                                                </template>
                                                <template v-if="file.type.includes('video')">
                                                    <i class="icon voyager-video"></i>
                                                </template>
                                                <template v-if="file.type.includes('audio')">
                                                    <i class="icon voyager-music"></i>
                                                </template>
												<template v-if="file.type.includes('zip')">
                                                    <i class="icon voyager-archive"></i>
                                                </template>
                                                <template v-if="file.type == 'folder'">
                                                    <i class="icon voyager-folder"></i>
                                                </template>
                                                <template
                                                        v-if="file.type != 'folder' && !file.type.includes('image') && !file.type.includes('video') && !file.type.includes('audio') && !file.type.includes('zip')">
                                                    <i class="icon voyager-file-text"></i>
                                                </template>

                                            </div>
                                            <div class="details" :data-type="file.type">
                                                <div :class="file.type">
                                                    <h4>@{{ file.name }}</h4>
                                                    <small>
                                                        <template v-if="file.type == 'folder'">
                                                        <!--span class="num_items">@{{ file.items }} file(s)</span-->
                                                        </template>
                                                        <template v-else>
                                                            <span class="file_size">@{{ file.size }}</span>
                                                        </template>
                                                    </small>
                                                </div>
                                            </div>
                                        </div>
                                    </li>

                                </ul>

                                <div id="file_loader">
                                    <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
                                    @if($admin_loader_img == '')
                                        <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
                                    @else
                                        <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
                                    @endif
                                    <p>{{ __('voyager::media.loading') }}</p>
                                </div>

                                <div id="no_files">
                                    <h3><i class="voyager-meh"></i> {{ __('voyager::media.no_files_in_folder') }}</h3>
                                </div>

                            </div>

                            <div id="right">
                                <div class="right_none_selected">
                                    <i class="voyager-cursor"></i>
                                    <p>{{ __('voyager::media.nothing_selected') }}</p>
                                </div>
                                <div class="right_details">
                                    <div class="detail_img">
                                        <div :class="selected_file.type">
                                            <template v-if="selectedFileIs('image')">
                                                <img :src="selected_file.path"/>
                                            </template>
                                            <template v-if="selectedFileIs('video')">
                                                <video width="100%" height="auto" controls>
                                                    <source :src="selected_file.path" type="video/mp4">
                                                    <source :src="selected_file.path" type="video/ogg">
                                                    <source :src="selected_file.path" type="video/webm">
                                                    {{ __('voyager::media.browser_video_support') }}
                                                </video>
                                            </template>
                                            <template v-if="selectedFileIs('audio')">
												<i class="voyager-music"></i>
                                                <audio controls style="width:100%; margin-top:5px;">
                                                    <source :src="selected_file.path" type="audio/ogg">
                                                    <source :src="selected_file.path" type="audio/mpeg">
                                                    {{ __('voyager::media.browser_audio_support') }}
                                                </audio>
                                            </template>
											<template v-if="selectedFileIs('zip')">
                                                <i class="voyager-archive"></i>
                                            </template>
                                            <template v-if="selected_file.type == 'folder'">
                                                <i class="voyager-folder"></i>
                                            </template>
                                            <!--template
                                                    v-if="selected_file.type != 'folder' && !selectedFileIs('audio') && !selectedFileIs('video') && !selectedFileIs('image')">
                                                <i class="voyager-file-text-o"></i>
                                            </template>-->
                                        </div>

                                    </div>
                                    <div class="detail_info">
                                        <div :class="selected_file.type">
                                            <span><h4>{{ __('voyager::media.title') }}:</h4>
    							            <p>@{{selected_file.name}}</p></span>
                                            <span><h4>{{ __('voyager::media.type') }}:</h4>
    							            <p>@{{selected_file.type}}</p></span>

                                            <template v-if="selected_file.type != 'folder'">
    								            <span><h4>{{ __('voyager::media.size') }}:</h4>
    								            <p><span class="selected_file_count">@{{ selected_file.items }} item(s)</span><span
                                                    class="selected_file_size">@{{selected_file.size}}</span></p></span>
                                                <span><h4>{{ __('voyager::media.public_url') }}:</h4>
    								            <p><a :href="selected_file.path" target="_blank">Click Here</a></p></span>
                                                <span><h4>{{ __('voyager::media.last_modified') }}:</h4>
    								            <p>@{{ dateFilter(selected_file.last_modified) }}</p></span>
                                            </template>
                                        </div>
                                    </div>
                                </div>

                            </div><!-- #right -->

                        </div>

                        <div class="nothingfound">
                            <div class="nofiles"></div>
                            <span>{{ __('voyager::media.no_files_here') }}</span>
                        </div>

                    </div>

                    <!-- Move File Modal -->
                    <div class="modal fade modal-warning" id="move_file_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><i class="voyager-move"></i> {{ __('voyager::media.move_file_folder') }}</h4>
                                </div>

                                <div class="modal-body">
                                    <h4>{{ __('voyager::media.destination_folder') }}</h4>
                                    <select id="move_folder_dropdown">
                                        <template v-if="folders.length">
                                            <option value="/../">../</option>
                                        </template>
                                        <template v-for="dir in directories">
                                            <option :value="dir">@{{ dir }}</option>
                                        </template>
                                    </select>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                                    <button type="button" class="btn btn-warning" id="move_btn">{{ __('voyager::generic.move') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Move File Modal -->

                    <!-- Rename File Modal -->
                    <div class="modal fade modal-warning" id="rename_file_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><i class="voyager-character"></i> {{ __('voyager::media.rename_file_folder') }}</h4>
                                </div>

                                <div class="modal-body">
                                    <h4>{{ __('voyager::media.new_file_folder') }}</h4>
                                    <input id="new_filename" class="form-control" type="text"
                                           :value="selected_file.name">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                                    <button type="button" class="btn btn-warning" id="rename_btn">{{ __('voyager::generic.rename') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Move File Modal -->

					<!-- Image Modal -->
					<div class="modal fade" id="imagemodal">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
								<div class="modal-body">
									<img :src="selected_file.path" class="img img-responsive" style="margin: 0 auto;">
								</div>

								<div class="modal-footer text-left">
									<small class="image-title">@{{ selected_file.name }}</small>
								</div>

							</div>
						</div>
					</div>
					<!-- End Image Modal -->

					<!-- Crop Image Modal -->
                    <div class="modal fade modal-warning" id="confirm_crop_modal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::media.crop_image') }}</h4>
                                </div>

                                <div class="modal-body">
                                    <div class="crop-container">
                                        <img v-if="selectedFileIs('image')" id="cropping-image" class="img img-responsive" :src="selected_file.path + '?' + selected_file.last_modified"/>
                                    </div>
                                    <div class="new-image-info">
                                        {{ __('voyager::media.width') }} <span id="new-image-width"></span>, {{ __('voyager::media.height') }}<span id="new-image-height"></span>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                                    <button type="button" class="btn btn-warning" id="crop_btn" data-confirm="{{ __('voyager::media.crop_override_confirm') }}">{{ __('voyager::media.crop') }}</button>
                                    <button type="button" class="btn btn-warning" id="crop_and_create_btn">{{ __('voyager::media.crop_and_create') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Crop Image Modal -->


                </div><!-- #filemanager -->

                <!-- New Folder Modal -->
                <div class="modal fade modal-info" id="new_folder_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="voyager-folder"></i> {{ __('voyager::media.add_new_folder') }}</h4>
                            </div>

                            <div class="modal-body">
                                <input name="new_folder_name" id="new_folder_name" placeholder="{{ __('voyager::media.new_folder_name') }}"
                                       class="form-control" value=""/>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                                <button type="button" class="btn btn-info" id="new_folder_submit">{{ __('voyager::media.create_new_folder') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End New Folder Modal -->

                <!-- Delete File Modal -->
                <div class="modal fade modal-danger" id="confirm_file_delete_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                            </div>

                            <div class="modal-body">
                                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_file_delete_name"></span>'</h4>
                                <h5 class="folder_warning"><i class="voyager-warning"></i> {{ __('voyager::media.delete_folder_question') }}</h5>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                                <button type="button" class="btn btn-danger" id="confirm_file_delete">{{ __('voyager::generic.delete_confirm') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Delete File Modal -->

                <div id="dropzone"></div>
                <!-- Delete File Modal -->
                <div class="modal fade" id="upload_files_modal">
                    <div class="modal-dialog">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;</button>
                                <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::media.drag_drop_info') }}</h4>
                            </div>

                            <div class="modal-body">

                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-success" data-dismiss="modal">{{ __('voyager::generic.all_done') }}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Delete File Modal -->


            </div><!-- .row -->
        </div><!-- .col-md-12 -->
    </div><!-- .page-content container-fluid -->

    <input type="hidden" id="storage_path" value="{{ storage_path() }}">
    <input type="hidden" id="base_url" value="{{ route('voyager.dashboard') }}">
    <input type="hidden" id="paciente_id" value="{{ $dataTypeContent->getKey() }}">
    <input type="hidden" id="files_path" value="{{ $filesDirectory }}">


@section('javascript')
    @parent
    <script>
        // Se quita el componente de la definición de Voyager para poder controlarlo
        // TODO: Ver como crear/modificar el componene en vez de sobreescribirlo
        window.MediaManager = '';
        //MediaManager();
    </script>
    <script type="text/javascript">
    $( document ).ready(function() {
      if(document.getElementById('filemanager')){
        var manager = new Vue({
          el: '#filemanager',
          data: {
              files: '',
              folders: [],
              selected_file: '',
              directories: [],
          },
          methods: {
            selectedFileIs: function(val){
              if(typeof(this.selected_file.type) != 'undefined' && this.selected_file.type.includes(val)){
                return true;
              }
              return false;
            },
            imgIcon: function(path){
              return 'background-size: cover; background-image: url("' + path + '"); background-repeat:no-repeat; background-position:center center;display:inline-block; width:100%; height:100%;';
            },
            dateFilter: function(date){
              if(!date){
                return null;
              }
              var date = new Date(date * 1000);

              var month = "0" + (date.getMonth() + 1);
              var minutes = "0" + date.getMinutes();
              var seconds = "0" + date.getSeconds();

              var dateForamted = date.getFullYear() + '-' + month.substr(-2) + '-' + date.getDate() + ' ' + date.getHours() + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

              return dateForamted;
            }
          }
        });


        CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        var VoyagerMedia = function(o){
          var files = $('#files');
          var defaults = {
            baseUrl: "/admin"
          };
          var options = $.extend(true, defaults, o);
          this.init = function(){
            $("#upload").dropzone({
              url: options.baseUrl+"/media/upload_file",
              previewsContainer: "#uploadPreview",
              totaluploadprogress: function(uploadProgress, totalBytes, totalBytesSent){
                $('#uploadProgress .progress-bar').css('width', uploadProgress + '%');
                if(uploadProgress == 100){
                  $('#uploadProgress').delay(1500).slideUp(function(){
                    $('#uploadProgress .progress-bar').css('width', '0%');
                  });

                }
              },
              processing: function(){
                $('#uploadProgress').fadeIn();
              },
              sending: function(file, xhr, formData) {
                console.log(file.name);
                formData.append("_token", CSRF_TOKEN);
                formData.append("upload_path", manager.files.path);
                // Se le pasa la clase y el id
                formData.append("className", '{{ class_basename($dataTypeContent) }}');
                formData.append("id", '{{ $dataTypeContent->getKey() }}');
                formData.append("filename", file.name);
              },
              success: function(e, res){
                if(res.success){
                  toastr.success(res.message, "!Subida correcta!");
                } else {
                  toastr.error(res.message, "Algo ha fallado...");
                }
              },
              error: function(e, res, xhr){
                toastr.error(res, "Algo ha fallado...");
              },
              queuecomplete: function(){
                getFiles(manager.folders);
              }
            });

            // Obtener la carpeta de ficheros correspondiente
            console.log("cargar files");
            getFiles("/");

            files.on("dblclick", "li .file_link", function(){
              var type = manager.selected_file.type;

              var imageMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];

              if (imageMimeTypes.indexOf(type) > -1) {
                $('#imagemodal').modal('show');
                return false;
              }

              if (type !== "folder") {
                console.log("Folder");
                return false;
              }

              manager.folders.push(manager.selected_file.name);
              getFiles(manager.folders);
            });

            files.on("click", "li", function(e){
              var clicked = e.target;
              if(!$(clicked).hasClass('file_link')){
                clicked = $(e.target).closest('.file_link');
              }
              setCurrentSelected(clicked);
            });

            $('.breadcrumb').on("click", "li.media_breadcrumb", function(){
              var index = $(this).data('index');

              manager.folders = manager.folders.splice(0, index);
              getFiles(manager.folders);
            });

            $('.breadcrumb-container .toggle').click(function(){
              $('.flex #right').toggle();
              var toggle_text = $('.breadcrumb-container .toggle span').text();
              $('.breadcrumb-container .toggle span').text(toggle_text == "Close" ? "Open" : "Close");
              $('.breadcrumb-container .toggle .icon').toggleClass('fa-toggle-right').toggleClass('fa-toggle-left');
            });


            //********** Add Keypress Functionality **********//
            var isBrowsingFiles = null,
            fileBrowserActive = function(el){
              el = el instanceof jQuery ? el : $(el);
              if ($.contains(files.parent()[0], el[0])) {
                return true;
              } else {
                //$(document).off('click');
                return false;
              }
            },
            handleFileBrowserStatus = function (target) {
              isBrowsingFiles = fileBrowserActive(target);
            };

            files.on('click', function (event) {
              if (! isBrowsingFiles) {
                files.on('click', function (e) {
                  handleFileBrowserStatus(e.target);
                });
              } else {
                handleFileBrowserStatus(event.target);
              }
            });

            $(document).keydown(function(e) {
              if(!$('.modal').hasClass('in')){
                var isKeyControl = e.which >= 37 && e.which <= 40;
                if (! isBrowsingFiles && isKeyControl) {
                  return false;
                } else if (isKeyControl && isBrowsingFiles) {
                  e.preventDefault();
                }
                var curSelected = $('#files li .selected').data('index');
                // left key
                if( (e.which == 37 || e.which == 38) && parseInt(curSelected)) {
                  newSelected = parseInt(curSelected)-1;
                  setCurrentSelected( $('*[data-index="'+ newSelected + '"]') );
                }
                // right key
                if( (e.which == 39 || e.which == 40) && parseInt(curSelected) < manager.files.items.length-1 ) {
                  newSelected = parseInt(curSelected)+1;
                  setCurrentSelected( $('*[data-index="'+ newSelected + '"]') );
                }
                // enter key
                if(e.which == 13) {
                  if (!$('#new_folder_modal').is(':visible') && !$('#move_file_modal').is(':visible') && !$('#confirm_file_delete_modal').is(':visible') ) {
                    var type = manager.selected_file.type;

                    var imageMimeTypes = ['image/jpeg', 'image/png', 'image/gif'];

                    if (imageMimeTypes.indexOf(type) > -1) {
                      $('#imagemodal').modal('show');
                      return false;
                    }

                    if (type !== "folder") {
                      return false;
                    }

                    manager.folders.push(manager.selected_file.name);
                    getFiles(manager.folders);
                  }
                  if($('#confirm_file_delete_modal').is(':visible')){
                    $('#confirm_file_delete').trigger('click');
                  }
                }
                // backspace key
                if(e.which == 8) {
                  var index =  manager.folders.length - 1;
                  manager.folders = manager.folders.splice(0, index);
                  getFiles(manager.folders);
                }
              }
            });

            //********** End Keypress Functionality **********//


            /********** TOOLBAR BUTTONS **********/
            $('#refresh').click(function(){
              getFiles(manager.folders);
            });

            $('#new_folder_modal').on('shown.bs.modal', function() {
              $("#new_folder_name").focus();
            });

            $('#new_folder_name').keydown(function(e) {
              if(e.which == 13) {
                $('#new_folder_submit').trigger('click');
              }
            });

            $('#move_file_modal').on('hidden.bs.modal', function () {
              $("#s2id_move_folder_dropdown").select2("close");
            });

            $('#new_folder_submit').click(function(){
              new_folder_path = manager.files.path + '/' + $('#new_folder_name').val();
              $.post(options.baseUrl+'/media/new_folder', { new_folder: new_folder_path, _token: CSRF_TOKEN }, function(data){
                if(data.success == true){
                  toastr.success('successfully created ' + $('#new_folder_name').val(), "Sweet Success!");
                  getFiles(manager.folders);
                } else {
                  toastr.error(data.error, "Whoops!");
                }
                $('#new_folder_name').val('');
                $('#new_folder_modal').modal('hide');
              });
            });

            $('#delete').click(function(){
              if(manager.selected_file.type == 'directory'){
                $('.folder_warning').show();
              } else {
                $('.folder_warning').hide();
              }
              $('.confirm_file_delete_name').text(manager.selected_file.name);
              $('#confirm_file_delete_modal').modal('show');
            });

            $('#confirm_file_delete').click(function(){

              $.post(options.baseUrl+'/media/delete_file',
                {
                  folder_location: manager.folders,
                  file_folder: manager.selected_file.name,
                  type: manager.selected_file.type,
                  id: {{ $dataTypeContent->getKey() }},
                  className: '{{ class_basename($dataTypeContent) }}',
                  _token: CSRF_TOKEN,
                  directory: '{{ $filesDirectory }}'
                }, function(data){
                if(data.success == true){
                  toastr.success('successfully deleted ' + manager.selected_file.name, "Sweet Success!");
                  getFiles(manager.folders);
                  $('#confirm_file_delete_modal').modal('hide');
                } else {
                  toastr.error(data.error, "Whoops!");
                }
              });
            });

            $('#move').click(function(){
              $('#move_file_modal').modal('show');
            });

            $('#rename').click(function(){
              if(typeof(manager.selected_file) !== 'undefined'){
                $('#rename_file').val(manager.selected_file.name);
              }
              $('#rename_file_modal').modal('show');
            });

            $('#move_folder_dropdown').keydown(function(e) {
              if(e.which == 13) {
                $('#move_btn').trigger('click');
              }
            });

            $('#move_btn').click(function(){
              source = manager.selected_file.name;
              destination = $('#move_folder_dropdown').val() + '/' + manager.selected_file.name;
              $('#move_file_modal').modal('hide');
              $.post(options.baseUrl+'/media/move_file', { folder_location: manager.folders, source: source, destination: destination, _token: CSRF_TOKEN }, function(data){
                if(data.success == true){
                  toastr.success('Successfully moved file/folder', "Sweet Success!");
                  getFiles(manager.folders);
                } else {
                  toastr.error(data.error, "Whoops!");
                }
              });
            });

            $('#rename_btn').click(function(){
              source = manager.selected_file.path;
              filename = manager.selected_file.name;
              new_filename = $('#new_filename').val();
              $('#rename_file_modal').modal('hide');
              $.post(options.baseUrl+'/media/rename_file', { folder_location: manager.folders, filename: filename, new_filename: new_filename, _token: CSRF_TOKEN }, function(data){
                if(data.success == true){
                  toastr.success('Successfully renamed file/folder', "Sweet Success!");
                  getFiles(manager.folders);
                } else {
                  toastr.error(data.error, "Whoops!");
                }
              });
            });

            // Crop Image
            $('#crop').click(function(){
              // Cleanup the previous cropper
              if (typeof cropper !== 'undefined' && cropper instanceof Cropper) {
                cropper.destroy();
              }
              $('#confirm_crop_modal').modal('show');
            });

            // Cropper must init after the modal shown
            $('#confirm_crop_modal').on('shown.bs.modal', function (e) {
              var croppingImage = document.getElementById('cropping-image');
              cropper = new Cropper(croppingImage, {
                crop: function(e) {
                  document.getElementById('new-image-width').innerText = Math.round(e.detail.width) + 'px';
                  document.getElementById('new-image-height').innerText = Math.round(e.detail.height) + 'px';
                  croppedData = {
                    x: Math.round(e.detail.x),
                    y: Math.round(e.detail.y),
                    height: Math.round(e.detail.height),
                    width: Math.round(e.detail.width)
                  };
                }
              });
            });

            $('#crop_btn').click(function(){
              if (window.confirm($(this).data('confirm'))) {
                cropImage(false);
              }
            });

            $('#crop_and_create_btn').click(function(){
              cropImage(true);
            });

            // $('#upload').click(function(){
            // 	$('#upload_files_modal').modal('show');
            // });
            /********** END TOOLBAR BUTTONS **********/

            manager.$watch('files', function (newVal, oldVal) {
              setCurrentSelected( $('*[data-index="0"]') );
              $('#filemanager #content #files').hide();
              $('#filemanager #content #files').fadeIn('fast');
              $('#filemanager .loader').fadeOut(function(){

                $('#filemanager #content').fadeIn();
              });

              if(newVal.items.length < 1){
                $('#no_files').show();
              } else {
                $('#no_files').hide();
              }
            });

            manager.$watch('directories', function (newVal, oldVal) {
              if($("#move_folder_dropdown").select2()){
                $("#move_folder_dropdown").select2('destroy');
              }
              $("#move_folder_dropdown").select2();
            });

            manager.$watch('selected_file', function (newVal, oldVal) {
              if(typeof(newVal) == 'undefined'){
                $('.right_details').hide();
                $('.right_none_selected').show();
                $('#move').attr('disabled', true);
                $('#delete').attr('disabled', true);
              } else {
                $('.right_details').show();
                $('.right_none_selected').hide();
                $('#move').removeAttr("disabled");
                $('#delete').removeAttr("disabled");
              }
            });

            function getFiles(folders){
              console.log("Función getFiles");
              console.log(folders);

              // Se indica que el directorio inicial es el del paciente
              if(folders != '/'){
                var folder_location = '{{ $filesDirectory }}/' + folders.join('/');
              } else {
                var folder_location = '{{ $filesDirectory }}';
              }

              $('#file_loader').fadeIn();
              $.post(options.baseUrl+'/media/get_files',
                {
                  folder: folder_location,
                  id: {{ $dataTypeContent->getKey() }},
                  className: '{{ class_basename($dataTypeContent) }}',
                  _token: CSRF_TOKEN,
                  _token: CSRF_TOKEN
                }, function(data) {
                $('#file_loader').hide();
                manager.files = data;
                files.trigger('click');
                for(var i=0; i < manager.files.items.length; i++){
                  if(typeof(manager.files.items[i].size) != undefined){
                    manager.files.items[i].size = bytesToSize(manager.files.items[i].size);
                  }
                }
              });

              // Add the latest files to the folder dropdown
              var all_folders = '';
              $.post(options.baseUrl+'/media/directories', { folder_location:manager.folders, _token: CSRF_TOKEN }, function(data){
                //console.log(data);
                manager.directories = data;
              });

            }

            function setCurrentSelected(cur){
              $('#files li .selected').removeClass('selected');
              $(cur).addClass('selected');
              manager.selected_file = manager.files.items[$(cur).data('index')];
            }

            function bytesToSize(bytes) {
              var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
              if (bytes == 0) return '0 Bytes';
              var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
              return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
            }

            function cropImage(createMode) {
              croppedData.originImageName = manager.selected_file.name
              croppedData.upload_path = '/' + manager.folders.join('/')
              croppedData.createMode = createMode

              var postData = Object.assign(croppedData, {_token: CSRF_TOKEN})
              $.post(options.baseUrl+'/media/crop', postData, function(data){
                console.log(data)
                if(data.success == true){
                  toastr.success(data.message)
                  getFiles(manager.folders);
                  $('#confirm_crop_modal').modal('hide');
                } else {
                  toastr.error(data.error, "Whoops!");
                }
              });
            }


          }
        };

        var media = new VoyagerMedia({
            baseUrl: document.getElementById('base_url').value
        });
        $(function () {
            media.init();
        });

      }
    });

    </script>

@endsection
