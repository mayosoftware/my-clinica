@extends('templates.edit-add')

@section('page_header_right')
  @if (!is_null($dataTypeContent->getKey()))
    <a class="btn btn-default" id="send_mail">
      <i class="fa fa-envelope"></i>
    </button>
    <a class="btn btn-default" href="{{ route('admin.tratamientoPaciente.print', ['id_tratamiento' => $dataTypeContent->getKey() ]) }}" target="_blank">
      <i class="fa fa-print"></i>
    </a>
  @endif
  <a href="{{ route('voyager.pacientes.edit', ['paciente' => $paciente->id]) }}" class="btn btn-default" value="Volver">
    <i class="voyager-double-left"></i> <span>Volver</span>
  </a>
@endsection

@section('content')
    @include('partials.modalPresupuesto')
    @include('partials.modalFacturar')
    {{-- @include('partials.modalCobrar') --}}
    {{-- @include('partials.modalProgramarAgenda') --}}
    @include('partials.modalSeleccionarTratamiento')
    @include('partials.modalSeleccionarPlanPredefinido')
    @include('partials.modalDelete')

    <div class="page-content edit-add container-fluid">
      {{-- Comprobar rol del usuario --}}
      @if (Auth::user()->role_id != 2)  {{-- Admin --}}
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active tratamiento-tab"><a href="#tratamiento" aria-controls="tratamiento" role="tab" data-toggle="tab">Tratamiento</a></li>
          <li role="presentation" class="pagos-tab"><a href="#pagos" aria-controls="pagos" role="tab" data-toggle="tab">Pagos</a></li>
          <li role="presentation" class="citas-tab"><a href="#citas" aria-controls="citas" role="tab" data-toggle="tab">Citas</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane active" id="tratamiento">
            @include('vendor.voyager.tratamientos-paciente.partials.tratamiento')
          </div>
          <div role="tabpanel" class="tab-pane" id="pagos">
            @include('vendor.voyager.tratamientos-paciente.partials.pagos')
          </div>
          <div role="tabpanel" class="tab-pane" id="citas">
            @include('vendor.voyager.tratamientos-paciente.partials.citas')
          </div>
        </div>
      @else
        @include('vendor.voyager.tratamientos-paciente.partials.tratamiento')
      @endif

    </div>
@stop

@section('javascript')

  @parent
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/edit-add.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/Pago.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/PlanTratamiento.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/citas.js')}}"></script>

  <script type="text/javascript">
    $( document ).ready(function() {
      var idTratamiento = null;
      var mode = 'add';

      @if ( !is_null($dataTypeContent->getKey()) )
        idTratamiento = {{ $dataTypeContent->getKey() }};
        mode = 'edit';
      @endif

      setupTratamiento(idTratamiento,
        '{{ route('admin.tratamientoPaciente.programarTratamiento', ['id_tratamiento' => $dataTypeContent->getKey()]) }}',
        '{{ route('admin.programaciones.destroy', ['id' => $dataTypeContent->getKey()]) }}');

      var routeFindPlan = '{{ url('admin/planesPredefinidos/find') }}';
      setupPlan({{ $dataTypeContent->lineas->count() ?? 0 }},
        {!! json_encode($tratamientos) !!},
        'full',
        mode,
        {!! $dataTypeContent->financiado ? 1 : 0 !!},
        {!! json_encode($piezas) !!},
        routeFindPlan
      );

      var routeMovimientos = '{{ route('admin.movimientos-caja.tratamiento', ['id' => $dataTypeContent->getKey()]) }}';
      var routeCobro = '{{ route('admin.tratamientoPaciente.ajaxGenerateCobro', ['id_tratamiento' => $dataTypeContent->getKey()]) }}';
      var routeDeleteMovimiento = '{{ url('admin/movimientos-caja') }}';

      setupPagos(routeMovimientos, routeCobro, routeDeleteMovimiento);

      setupCitas('{{ url('admin/programaciones/ajaxCrearProgramacionLinea') }}',
        '{{ url('admin/programaciones/ajaxEliminarProgramacionLinea') }}',
        '{{ url('admin/programaciones/__fecha/getProgramaciones') }}',
        '{{ route('sendPusher.agenda') }}');

    });

  </script>
@endsection
