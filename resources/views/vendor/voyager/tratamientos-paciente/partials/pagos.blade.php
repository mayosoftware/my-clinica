<div class="row">
  <div class="col-md-12">
    <div class="panel panel-bordered">
      <form role="form" class="form-registrar-cobro"
        action=""
        method="POST">

        <div class="panel-body">
          {{-- Creación de tratamiento --}}
          <div class="form-group alert-pagos-group
          @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente && !is_null($dataTypeContent->getKey()))
            hidden
          @endif
          ">
            <div class="alert alert-warning" role="alert">Debes guardar el tratamiento antes de poder registrar cobros</div>
          </div>

          {{-- Edición de tratamiento --}}
          <div class="form-group registrar-pago-group
          @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente && is_null($dataTypeContent->getKey()))
            hidden
          @endif
          ">
            <!-- CSRF TOKEN -->
            {{ csrf_field() }}

            <input type="hidden" name="tratamiento_id">

            <h3>Registrar cobro</h3>
            <div class="col-md-4">
              <label for="totalCobro">Importe</label>
              <input class="form-control" type="number" name="totalCobro" value="0" step="0.01">
              <label for="tipoCobro">Tipo de cobro</label>
              <select class="form-control" name="tipoCobro">
                  <option value="1">Efectivo</option>
                  <option value="2">Tarjeta</option>
                </select>
            </div>
            <div class="col-md-8">
              <label for="concepto">Concepto</label>
              <textarea class="form-control" name="concepto" rows="5"></textarea>
            </div>
            <button class="btn btn-xs btn-success pull-right" type="submit">
              <i class="fa fa-euro-sign"></i> Registrar
            </button>

            <div class="form-group col-md-12 movimientos-caja-group">
              <label>Movimientos de caja</label>              
              <div class="table-responsive table-movimientos-caja">
                <table class="table table-bordered">
                  <thead>
                    <th>Fecha</th>
                    <th>Concepto</th>
                    <th>Entrada</th>
                    <th>Salida</th>
                    <th>Acciones</th>
                  </thead>
                  <tbody>
                    @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente)
                      @foreach ($dataTypeContent->movimientosCaja as $movimiento)
                      <tr id="mov-{{$movimiento->id}}">
                        <td>{{ $movimiento->fechaCobroFormat() }}</td>
                        <td>{{ $movimiento->concepto }}</td>
                        <td>{{ $movimiento->entrada }}</td>
                        <td>{{ $movimiento->salida }}</td>
                        <td>
                          <a class="btn-sm btn-danger borrar-movimiento" name="borrar-movimiento" role="button"
                            data-id="1">
                            <i class="fas fa-trash"></i>
                          </a>
                        </td>                        
                      </tr>
                      @endforeach
                    @endif
                  </tbody>
                </table>
              </div>
              {{-- table-responsive --}}
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
