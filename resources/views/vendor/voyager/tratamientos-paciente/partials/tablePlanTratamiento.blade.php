<div class="planTratamiento form-group col-md-12">
    <div class="row form-group">
      <h4 class="pull-left">Plan de tratamiento</h4>
      <button type="button" class="btn btn-primary pull-right"
        data-toggle="modal" data-target="#planesModal">
        Cargar Plan Predefinido
      </button>
    </div>

    <div class="alert alert-danger print-error-msg" style="display:none">
      <ul></ul>
    </div>

    <div class="alert alert-success print-success-msg" style="display:none">
      <ul></ul>
    </div>

    <div class="row form-group table-responsive">
      <table class="table table-bordered" id="dynamic_field">
        <thead>

          <th class="th-hecho
            @if (!is_null($key))
              hidden
            @endif
          "></th>

          <th width="10%">Pieza</th>
          <th width="30%">Descripción</th>
          <th width="10%">Und.</th>
          <th>Precio</th>
          <th width="10%">Dto.%</th>
          {{-- @if ($version == 'full')
            <th>Neto</th>
          @endif --}}
          <th>Total</th>
          @if ($version != 'invoice')
            <th>Cobro</th>
          @endif
          <th></th>

        </thead>
        <tfoot>
          {{-- Actualizar tratamiento --}}
          @if (!is_null($key))
            <tr>
              @if ($version != 'invoice')
                <td colspan="2"></td>
                <td colspan="3">TOTALES</td>
              @else
                <td colspan="4">TOTALES</td>
              @endif
              <td><input type="number" name="total_descuento" class="form-control" value="{{ $data->total_descuento }}" readonly></td>
              {{-- @if ($version == 'full')
                <td><input type="number" name="total_neto" class="form-control" value="{{ $data->total }}" readonly></td>
              @endif --}}
              <td><input type="number" name="total" class="form-control" value="{{ $data->total }}" readonly></td>

              @if ($version != 'invoice')
                  <td></td>
              @endif

              <td><a class="btn-sm btn-success btn_add" role="button"><i class="fas fa-plus"></i></a></td>

            </tr>
          @else
            <tr>
              @if ($version != 'invoice')
                <td></td>
              @else
                <td></td>
              @endif
              <td colspan="3">TOTALES</td>

              <td><input type="number" name="total_descuento" class="form-control" value=0 readonly></td>
              {{-- @if ($version == 'full')
                <td><input type="number" name="total_neto" class="form-control" value=0 readonly></td>
              @endif --}}
              <td><input type="number" name="total" class="form-control" value=0 readonly></td>

              @if ($version != 'invoice')
                <td></td>
              @endif
              <td><a class="btn-sm btn-success btn_add" role="button"><i class="fas fa-plus"></i></a></td>

            </tr>
          @endif
        </tfoot>
        <tbody>
          {{-- Actualizar tratamiento --}}
          @if (!is_null($key))
            @foreach ($data->lineas as $linea)
              <tr id="row{{ $linea->linea }}" class="dynamic-added">
                  <input type="hidden" name="linea_id[{{ $linea->linea }}]" value="{{ $linea->id }}">
                  <input type="hidden" name="realizada_programacion[{{ $linea->linea }}]" value="{{ $linea->realizada_programacion_id }}">
                  <input type="hidden" name="fecha_realizacion_linea[{{ $linea->linea }}]" value="{{ $linea->fecha_realizacion_linea }}">

                  <input type="hidden" name="cobrada[{{ $linea->linea }}]" value="{{ $linea->cobrada }}">
                  <input type="hidden" name="fecha_cobro[{{ $linea->linea }}]" value="{{ $linea->fecha_cobro }}">

                  @if ($version != 'invoice')
                    <td><input class="change_state" type="checkbox" id="{{ $linea->linea }}" name="lineaRealizada[{{ $linea->linea }}]" {{ $linea->realizada ? "checked" : "" }}></td>
                  @endif
                  <td>
                    <select class="form-control select_pieza" name="pieza_linea[{{ $linea->linea }}]" id="{{ $linea->id }}">
                      @foreach ($piezas as $pieza)
                        <option value="{{ $pieza }}" {{ $pieza == ($linea->pieza ? $linea->pieza : 0) ? 'selected' : '' }}>{{ $pieza }}</option>
                      @endforeach
                    </select>
                  </td>
                  <td>
                     <div class="col-lg-12">
                      <div class="input-group">
                        <input type="text" id="{{ $linea->id }}" name="descripcion_linea[{{ $linea->linea }}]" placeholder="Introduce una descripción" class="form-control descripcion_linea"
                          value="{{ $linea->descripcion }}" required
                          @if ($linea->cobrada)
                            readonly="true"
                          @endif
                          >
                        <span class="input-group-btn">
                          <a class="btn-sm btn-info @if ($linea->cobrada) disabled @endif" name="descripcion_linea_button[{{ $linea->linea }}]"
                            data-toggle="modal" data-target="#tratamientosModal" onclick="setDescriptionIndex({{ $linea->linea }});">
                            <i class="voyager-double-right"></i>
                          </a>
                        </span>
                      </div>
                    </div>
                  </td>
                  <td><input type="number" name="cantidad_linea[{{ $linea->linea }}]" class="form-control" value={{ $linea->cantidad }} required onchange="updateTotals({{ $linea->linea }})" min=0
                      @if ($linea->cobrada)
                        readonly="true"
                      @endif
                    >
                  </td>
                  <td><input type="number" name="precio_linea[{{ $linea->linea }}]" class="form-control" value={{ $linea->importe_unitario }} required onchange="updateTotals({{ $linea->linea }})" min=0
                      @if ($linea->cobrada)
                        readonly="true"
                      @endif
                    >
                  </td>
                  <td><input type="number" name="descuento_linea[{{ $linea->linea }}]" class="form-control" value={{ $linea->porcentaje_descuento }} required onchange="updateTotals({{ $linea->linea }})" min=0 max=100
                      @if ($linea->cobrada)
                        readonly="true"
                      @endif
                    >
                  </td>
                  {{-- @if ($version == 'full')
                    <td><input type="text" name="neto_linea[{{ $linea->linea }}]" class="form-control" value={{ $linea->total }} required readonly></td>
                  @endif --}}
                  <td><input type="text" name="total_linea[{{ $linea->linea }}]" class="form-control" value={{ $linea->total }} readonly min=0></td>
                  @if ($version != 'invoice')
                    <td>
                      <a href="#" role="button" class="cobrar_linea" id="{{ $linea->linea }}" name="cobrar_linea[{{ $linea->linea }}]">
                        <i class="fas fa-2x fa-euro-sign"
                          @if ($linea->cobrada)
                            style="color: #5CB85D"
                          @else
                            style="color: #9E9C9A"
                          @endif
                        >
                        </i>
                      </a>
                    </td>
                  @endif
                  <td><a name="{{ $linea->linea }}" value="{{ $linea->id }}" class="btn-sm btn-danger btn_remove" role="button"><i class="fas fa-trash"></i></a></td>

              </tr>
            @endforeach
          @endif
        </tbody>
      </table>
    </div>
  </div>
