<style >
  .dimmed {
    position: relative;
    padding: 10px;
  }

  .dimmed:after {
    content: " ";
    z-index: 10;
    display: block;
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(170, 172, 176, 0.5);
  }

</style>
    <div class="panel panel-bordered">
      <div class="panel-body" style="overflow: visible;">

        {{-- Creación de tratamiento --}}
        <div class="form-group alert-citas-group
        @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente && !is_null($dataTypeContent->getKey()))
          hidden
        @endif
        ">
          <div class="alert alert-warning" role="alert">Debes guardar el tratamiento antes de poder dar citas</div>
        </div>

        {{-- Edición de tratamiento --}}
        <div class="citas-tratamiento-group
        @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente && is_null($dataTypeContent->getKey()))
          hidden
        @endif
        ">
          <div class="table-responsive col-md-6">
            <h4>Citas del tratamiento</h4>
            <table class="table table-bordered" id="citasTable">
              <thead>
                <th>Descripción</th>
                <th>Total</th>
                <th>Realizada</th>
                <th>Cita</th>
              </thead>

              <tbody>
                @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente)

                  @foreach ($dataTypeContent->lineas as $key => $linea)
                  <tr>
                    <td><span name="descripcion[{{ $linea->id }}]">{{ $linea->descripcionComplea() }}</span></td>
                    <td>{{ $linea->total }}</td>
                    <td>
                      @if ($linea->realizada)
                      <span class="label label-success">SI</span>
                      @else
                      <span class="label label-default">NO</span>
                      @endif
                    </td>
                    <td>
                      <div class="actions" name="actions[{{ $linea->id }}]">
                        @if ($linea->realizada)
                          {{ $linea->fechaRealizacionFormat() }}
                        @elseif ($linea->programacion)
                          <span class="fecha-cita" name="fecha-cita[{{ $linea->id }}]">{{ $linea->fechaInicioCompleta() }}</span>
                          <a class="btn-sm btn-danger borrar-cita" name="borrar-cita[{{ $linea->id }}]" role="button"
                            data-id="{{ $linea->programacion->id }}"><i class="fas fa-trash"></i></a>
                        @else
                          <a class="btn-sm btn-info crear-cita" name="crear-cita[{{ $linea->id }}]"
                            data-id="{{ $linea->id }}"><i class="fa fa-calendar-alt"></i></a>
                        @endif
                      </div>
                    </td>
                  </tr>
                  @endforeach
                @endif
              </tbody>
            </table>
          </div>

          <div class="form-group col-md-6">

            <div class="alert alert-warning alert-tratamiento" role="alert">
              Seleccione un tratamiento para crear o modificar una cita
            </div>

            <div class="row col-lg-12 programacion-cita dimmed">
              <form class="programar-cita-form"
                @if ($dataTypeContent instanceof \App\Models\TratamientoPaciente)
                  action="{{ route('admin.tratamientoPaciente.programarTratamiento', ['id_tratamiento' => $dataTypeContent->getKey()]) }}"
                @endif
                method="POST">
                <!-- CSRF TOKEN -->
                {{ csrf_field() }}


                <div class="form-group">
                  <div id='agenda'></div>
                </div>

                <div class="from-group">
                  <div class="col-lg-6">
                    <label for="doctor_id">Doctor</label>
                    <select class="form-control" name="doctor_id">
                        @foreach ($doctores as $doctor)
                          <option value="{{ $doctor->id }}">{{ $doctor->name }}</option>
                        @endforeach
                      </select>
                  </div>
                  <div class="col-lg-6">
                    <label class="control-label" for="fecha">Fecha actuación</label>
                    <div class="controls">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input name="fecha" id="fecha" class="form-control" required />
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-lg-4">
                    <label for="hora_inicio" class="control-label">Hora inicio</label>
                    <div class="controls">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                        <input name="hora_inicio" id="hora_inicio" class="form-control" required />
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <label for="hora_fin" class="control-label">Hora fin</label>
                    <div class="controls">
                      <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                        <input name="hora_fin" id="hora_fin" class="form-control" required />
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    <label for="tiempo" class="control-label">Tiempo</label>
                    <div class="controls">
                      <div class="input-group">
                        <input type="number" name="tiempo" id="tiempo" class="form-control" required />
                        <span class="input-group-addon">mins</span>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <button class="btn btn-info pull-right" id="programacionButton" name="programacionButton" type="submit">
                       <i class="fa fa-euro"></i> <span>Guardar programación</span>
                   </button>

                </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
