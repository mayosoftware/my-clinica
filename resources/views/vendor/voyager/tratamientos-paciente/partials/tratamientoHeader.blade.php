<div class="form-group">
  <div class="col-md-2">
    <label for="numero">Número</label>
    @if (is_null($dataTypeContent->getKey()))
      <div class="input-group">
        <span class="input-group-addon">#</span>
        <input class="form-control disabled" type="text" name="numero" value="{{ $paciente->nextNumeroTratamiento() }}" readonly>
      </div>

    @else
      <div class="input-group">
        <span class="input-group-addon">#</span>
        <input class="form-control disabled" type="text" name="numero" value="{{ $dataTypeContent->numero }}" readonly>
      </div>
    @endif

  </div>
  <div class="col-md-4">
    <label for="nombre">Nombre tratamiento</label>
    <input class="form-control" type="text" name="nombre" value="{{ $dataTypeContent->nombre }}" required>
  </div>
  @if (!is_null($dataTypeContent->getKey()))
    <div class="col-md-4">
      <label for="paciente">Paciente</label>
      <div class="form-group">
        <a href="{{ route('voyager.pacientes.edit', ['paciente' => $dataTypeContent->paciente]) }}" target="_blank">
          {{ $dataTypeContent->paciente->nombre }}
        </a>
      </div>
    </div>
    <div class="col-md-2">
      <label for="saldo">Saldo</label>
      <input class="form-control disabled" type="text" name="saldo" value="{{ $dataTypeContent->paciente->saldo }}" readonly>
    </div>
  @endif
</div>
