<div class="row">
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <!-- form start -->
            <form role="form"
                    class="form-edit-add form-tratamiento"
                    action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
                    method="POST" enctype="multipart/form-data">
                <!-- PUT Method if we are editing -->
                @if(!is_null($dataTypeContent->getKey()))
                    {{ method_field("PUT") }}
                @endif

                <!-- CSRF TOKEN -->
                {{ csrf_field() }}

                <div class="panel-body">
                  @if (count($errors) > 0)
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif

                  <input type="hidden" name="paciente_id" value="{{ $paciente->id }}">
                  @if (!is_null($dataTypeContent->getKey()))
                    <input type="hidden" name="programacion_conjunta" value="{{ $dataTypeContent->programacion_conjunta ? 1 : 0 }}">
                    <input type="hidden" name="presupuestado" value="{{ $dataTypeContent->presupuestado ? 1 : 0 }}">
                    <input type="hidden" name="presupuesto_id" value="{{ $dataTypeContent->presupuesto_id }}">
                    <input type="hidden" name="facturado" value="{{ $dataTypeContent->facturado ? 1 : 0 }}">
                    <input type="hidden" name="factura_id" value="{{ $dataTypeContent->factura_id }}">
                  @endif

                  @include('vendor.voyager.tratamientos-paciente.partials.tratamientoHeader')

                  <div class="form-group">
                    @if (!is_null($dataTypeContent->getKey()))
                      <div class="col-md-3">
                        <label for="presupuesto">Presupuesto asociado:</label>
                          @if($dataTypeContent->presupuestado)
                            <div class="form-group">
                              <a class="btn btn-link" role="button" href="{{ route('admin.presupuestos.print', ['id' => $dataTypeContent->presupuesto_id]) }}"
                                target="_blank">Presupuesto {{ $dataTypeContent->presupuesto->numero }}
                              </a>
                              <a class="btn-xs btn-info" role="button" href="{{ route('voyager.presupuestos.edit', ['id' => $dataTypeContent->presupuesto_id]) }}"
                                target="_blank">Editar
                              </a>
                            </div>
                          @else
                            <div class="form-group">
                              <a class="btn-sm btn-info crear-presupuesto" role="button">
                                <i class="fa fa-receipt"></i> <span>Crear</span>
                              </a>
                            </div>
                          @endif
                      </div>
                      <div class="col-md-3">
                        <label for="factura">Factura asociada:</label>
                          @if($dataTypeContent->facturado)
                            <div class="form-group">
                              <a class="btn btn-link" role="button" href="{{ route('admin.facturas.print', ['id' => $dataTypeContent->factura_id]) }}"
                                target="_blank">Factura {{ $dataTypeContent->factura->numero }}
                              </a>
                              <a class="btn-xs btn-info" role="button" href="{{ route('voyager.facturas.edit', ['id' => $dataTypeContent->factura_id]) }}"
                                target="_blank">Editar
                              </a>
                            </div>
                          @else
                            <div class="form-group">
                              <a class="btn-sm btn-info crear-factura" role="button">
                                <i class="fa fa-receipt"></i> <span>Crear</span>
                              </a>
                            </div>
                          @endif
                      </div>
                    @endif
                  </div>

                  @include('vendor.voyager.tratamientos-paciente.partials.tablePlanTratamiento',
                    ['data' => $dataTypeContent, 'key' => $dataTypeContent->getKey(), 'version' => 'full'])

                    <div class="form-group">
                      <div class="col-md-3">
                          <div class="checkbox">
                           <label>
                             <input class="checkbox_financiado" type="checkbox" name="financiado"
                              {{ $dataTypeContent->financiado ? 'checked' : ''}} {{ $dataTypeContent->financiado ? 'readonly' : ''}}> Financiado
                           </label>
                         </div>

                         <div class="cobro_financiacion {{ $dataTypeContent->financiado ? '' : 'hidden' }}">
                           <label for="total_financiacion">Total financiación</label>
                           <input class="form-control" type="text" name="total_financiacion"
                            value="{{ !is_null($dataTypeContent->getKey()) ? $dataTypeContent->total_financiacion : 0 }}" {{ $dataTypeContent->financiado ? 'readonly' : '' }}>

                           <label for="consumido_financiacion">Consumido</label>
                           <input class="form-control" type="text" name="consumido_financiacion"
                            value="{{ !is_null($dataTypeContent->getKey()) ? $dataTypeContent->consumido_financiacion : 0 }}" readonly>

                         </div>

                         <div class="cobro_regular {{ $dataTypeContent->financiado ? 'hidden' : '' }}">
                           <label for="cobrado">Cobrado</label>
                           <input class="form-control" type="text" name="cobrado" value="{{ !is_null($dataTypeContent->getKey()) ? $dataTypeContent->cobrado : 0 }}" readonly>
                           <label for="pendiente">Pendiente</label>
                           <input class="form-control" type="text" name="pendiente" value="{{ !is_null($dataTypeContent->getKey()) ? $dataTypeContent->pendiente : 0 }}" readonly>
                        </div>
                      </div>
                      <div class="col-md-9">
                        <label for="observaciones">Observaciones</label>
                        <textarea class="form-control" name="observaciones" rows="8" cols="80">{{ $dataTypeContent->observaciones }}</textarea>
                      </div>
                    </div>


                </div><!-- panel-body -->
                <div class="panel-footer">
                  <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                </div>
            </form>
      </div>
  </div>
</div>
