@extends('templates.voyager-browse')

@section('page_header_right')
  <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#printModal">
    <i class="voyager-documentation"></i> <span>Imprimir agenda</span>
  </button>
@endsection

@section('panel-body')
  @include('partials.modalSeleccionarPlanPredefinido')
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

  <form role="form" class="buscar-paciente">
    <div class="form-group row col-md-4">
      <label class="control-label" for="q">Buscar siguiente tratamiento de paciente:</label>
       <div class="input-group">
         <input type="text" class="form-control" name="q" placeholder="Introduce el nombre, código o email de un paciente" />
         <span class="input-group-btn">
           <a class="btn-sm btn-danger disabled clear">
             <i class="fas fa-trash-alt"></i>
           </a>
         </span>
       </div>
    </div>
  </form>
  <div class="row">
    <div class="col-md-9">
      {{-- Se utiliza para saber quién actualiza la agenda y no refrescar el tratamiento en el caso del que ha hecho el cambio --}}
      <input type="hidden" class="socketId" value="">
      <div id='calendar'></div>
    </div>
    <div class="col-md-3">
      <div class="row">
        <div class="panel panel-primary panel-bordered">

            <div class="panel-heading">
                <h3 class="panel-title panel-icon"><i class="voyager-calendar"></i> Selección día</h3>
                <div class="panel-actions">
                    {{-- <a class="panel-action voyager-angle-up" data-toggle="panel-collapse" aria-hidden="true"></a> --}}
                </div>
            </div>

            <div class="panel-body" style="">
              <div id="daypicker"></div>
            </div><!-- .panel-body -->
        </div>

      </div>

      {{-- Comprobar si el rol del usuario tiene permisos para buscar por doctor --}}
      @php
        $browsUsers = Voyager::can('browse_users');
      @endphp

      @if($browsUsers)
        <div class="row">
            <div class="panel panel-primary panel-bordered">

                <div class="panel-heading">
                  <h3 class="panel-title panel-icon"><i class="voyager-file-text"></i> Doctores</h3>
                  <div class="panel-actions">
                        {{-- <a class="panel-action voyager-angle-up" data-toggle="panel-collapse" aria-hidden="true"></a> --}}
                  </div>
                </div>

                <div class="panel-body" style="">
                  @foreach ($doctores as $doct)
                    <div class="radio">
                      <label><input type="radio" name="radioDoctor" value="{{ $doct->id }}">{{ $doct->name }}</label>
                    </div>
                  @endforeach
                </div><!-- .panel-body -->

                <div class="panel-footer">
                  <a id="deselectRadioGroup" href="#">Quitar seleccionado</a>
                </div>
            </div>
          </div>
        @endif
    </div>
  </div>

  {{-- Modal para crear/editar actuación --}}
  @include('vendor.voyager.programaciones.partials.modalProgramacion')

  {{-- Modal para imprimir entre Fechas --}}
  @include('vendor.voyager.programaciones.partials.modalPrint')

  {{-- @include('partials.modalSeleccionarPaciente') --}}
  @include('partials.modalSeleccionarTratamiento')
@stop

@section('panel-footer')
  <div class="row">
    <span class="label" style="color: #ffffff;background-color: #47D27F;">Realizada</span>
    <span class="label" style="color: #ffffff;background-color: #22A7F0;">No Realizada</span>
    <span class="label" style="color: #ffffff;background-color: #F94126;">Cancelada</span>
  </div>

@stop

@section('css')
  <style>
      .bootstrap-datetimepicker-widget table td.event, .bootstrap-datetimepicker-widget table td.event:hover {
          font-weight: bold;
      }
  </style>

@endsection

@section('javascript')

  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/PlanTratamiento.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/programaciones/agenda.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/programaciones/calendar.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/Pago.js')}}"></script>
  <script type="text/javascript" src="{{ asset('js/tratamientos-paciente/citas.js')}}"></script>

  <script>

    $(document).ready(function () {
      var routeSearchPaciente = '{{ route('searchPaciente') }}';
      var routeNextCita = '{{ route('admin.programaciones.getSiguienteProgramacionPaciente', ['id' => '__id']) }}';
      var routeEventsByMonth = '{{ route('admin.programaciones.eventsByMonth', ['month' => '__month']) }}';
      var routeBuscarPotencial = '{{ route('admin.paciente.ajaxBuscarPacientePotencial')}}';
      var routeGuardarPotencial = '{{ route('admin.paciente.ajaxGuardarPacientePotencial')}}';
      var routeFiltroDoctor = '{{ route('admin.programaciones.ajaxProgramacionesDoctor', ['id' => '__id']) }}';
      var routePrintBetweenDates = '{{ route('admin.programaciones.ajaxProgramacionEntreFechas') }}';
      var routeProgramacion = '{{ route('admin.programaciones.ajaxProgramacion', ['id' => '__id']) }}';
      var routeFindPlan = '{{ url('admin/planesPredefinidos/find') }}';

      console.log(@json($events));

      setupCalendar(@json($events), {{ Auth::user()->id }}, @json($festivos));

      setupAgenda(routeSearchPaciente, routeNextCita, routeEventsByMonth,
        routeBuscarPotencial, routeGuardarPotencial, routeFiltroDoctor, routePrintBetweenDates,
        routeProgramacion, @json($doctores));

      setupCitas('{{ url('admin/programaciones/ajaxCrearProgramacionLinea') }}',
        '{{ url('admin/programaciones/ajaxEliminarProgramacionLinea') }}',
        '{{ url('admin/programaciones/__fecha/getProgramaciones') }}',
        '{{ route('sendPusher.agenda') }}');

    });

    function fillModalForCreate(date) {
      setupPlan(0,
        {!! json_encode($tratamientos) !!},
        'light',
        'add',
        0,
        {!! json_encode($piezas) !!},
        routeFindPlan
      );

      cambiosEnImportes = false;

      // Limpiar datos antes de cargarlos
      var $form = $( "#programacionForm" );

      resetProgramacionForm();

      $form.find(".modal-header .modal-title").text("Creación de actuación");

      // En la creación se muestra el selector de pacientes
      $form.find( 'div[name="pacienteDiv"]' ).removeClass('hidden');
      $form.find( 'div[name="pacienteLinkDiv"]' ).addClass('hidden');
      $form.find( ".delete" ).addClass('hidden');

      $form.find(".cancelacion").addClass('hidden');

      $form.find('.div_checkbox_potencial').removeClass('hidden');

      $form.find('.cobro_financiacion').addClass('hidden');
      $form.find('.cobro_regular').addClass('hidden');

      $form.find('.div_saldo').addClass('hidden');
      $form.find('.div_mutua').addClass('hidden');
      $form.find('.div_poliza').addClass('hidden');
      $form.find('select[name="tipo_paciente"]').addClass('hidden');

      $form.find('#masOpciones').addClass('hidden');

      var doctoresSelect = $form.find('select[name=doctor_id]');
      doctoresSelect.empty();

      @foreach ($doctores as $doctor)
        doctoresSelect.append('<option value="{!! $doctor->id !!}">{!! $doctor->name !!}</option>');
      @endforeach

      rows = 0;
      rows++;
      addLinea();

      // $('#tratamiento_' + rows).removeAttr("autocomplete").attr("autocomplete", "on");
      // $('#tratamiento_' + rows).attr('autocomplete','on');

      updateTotals();

      $form.find('input[name="fecha"]').data("DateTimePicker").date(moment(date, "DD/MM/YYYY"));
      $form.find('input[name="hora_inicio"]').data("DateTimePicker").date(moment(date, "HH:MM:SS"));
      $form.find('input[name="hora_fin"]').data("DateTimePicker").date(moment(date.add(1, 'hour'), "HH:MM:SS"));
      $form.find('input[name="tiempo"]').val('60');

      var tipoCancelacionSelect = $form.find('select[name=tipo_cancelacion]');
      tipoCancelacionSelect.empty();
      tipoCancelacionSelect.append('<option value="0">-- Seleccionar valor --</option>');

      @foreach ($tiposCancelaciones as $tipo)
        tipoCancelacionSelect.append('<option value="{!! $tipo->id !!}">{!! $tipo->nombre !!}</option>');
      @endforeach

      $('#programacionModal').modal({
        keyboard: true
      }, 'show');

      $form.attr('action', '{{ route('admin.programaciones.modalStore') }}');

      desactivarCitas();
      desactivarPagos();
    }

    function fillModalForEdit(event, _id = null) {
      // Obtener el id de la programacion
      var programacion_id = event.programacion_id;

      // Consultar esa programación
      var url = '{{ route('admin.programaciones.ajaxLoadProgramacion', ['id' => '__id']) }}'.replace('__id', programacion_id);
      ajaxRequest(url, null, 'GET', function(json) {
        console.log(json);
        if (!json.error) {
          var programacion = json.data.event,
            paciente = json.data.event.paciente,
            tratamiento = json.data.event.tratamiento,
            lineas = json.data.event.lineas,
            $form = $( "#programacionForm" );

          // Limpiar datos antes de cargarlos
          resetProgramacionForm();

          if (_id) {
            $form.find('input[name="idCalendarEvent"]').val(_id);  
          } else {
            $form.find('input[name="idCalendarEvent"]').val(event._id); 
          }
          $form.find('input[name="programacion_id"]').val(programacion.id);

          $("#programacionModal .modal-header .modal-title").text("Editar actuación paciente: " + paciente.nombre);

          // En la edición se quita el selector de paciente
          $form.find('div[name="pacienteDiv"]').addClass('hidden');
          $form.find('div[name="pacienteLinkDiv"]').removeClass('hidden');
          $form.find('.div_checkbox_potencial').addClass('hidden');
          $form.find('#linkPaciente').text(paciente.nombre);
          $form.find('#linkPaciente').attr('href', 'pacientes/' + paciente.id + '/edit');
          $form.find('.delete').removeClass('hidden');

          $form.find('.div_saldo').removeClass('hidden');
          $form.find('.div_mutua').removeClass('hidden');
          $form.find('.div_poliza').removeClass('hidden');
          $form.find('input[name="mutua"]').val(paciente.mutua);
          $form.find('input[name="numero_poliza"]').val(paciente.numero_poliza);
          // Marcar tipo de paciente
          $form.find('select[name="tipo_paciente"]').val(paciente.tipo_paciente_id).change();

          $form.find('input[name="nombre"]').val(tratamiento.nombre);
          $form.find(".cancelacion").removeClass('hidden');
          $form.find('#masOpciones').removeClass('hidden');
          $form.find('input[name="saldo"]').val(paciente.saldo);

          if (paciente.saldo < 0) {
            $form.find('.div_saldo').addClass('has-error');
          } else {
            $form.find('.div_saldo').removeClass('has-error');
          }

          // Cargar plan tratamiento
          setupPlan(lineas.lenght,
            {!! json_encode($tratamientos) !!},
            'light',
            'edit',
            tratamiento.financiado ? 1 : 0,
            {!! json_encode($piezas) !!},
            routeFindPlan
          );

          loadPlanFromLineas(lineas, programacion);

          // Marcar doctor del evento
          $form.find('select[name="doctor_id"]').val(programacion.doctor_id).change();

          var routeMovimientos = '{{ route('admin.movimientos-caja.tratamiento', ['id' => '__id']) }}'.replace('__id', tratamiento.id);
          var routeCobro = '{{ route('admin.tratamientoPaciente.ajaxGenerateCobro', ['id_tratamiento' => '__id']) }}'.replace('__id', tratamiento.id);
          var routeDeleteMovimiento = '{{ url('admin/movimientos-caja') }}';
          setupPagos(routeMovimientos, routeCobro, routeDeleteMovimiento);

          bucarMovimientos();

          resetCitas();
          loadCitasFromLineas(lineas);

          if (tratamiento.financiado) {
            $form.find('input[name="financiado"]').prop('checked', true);
            $form.find('.cobro_financiacion').removeClass('hidden');
            $form.find('.cobro_financiacion').show();
            $form.find('.cobro_regular').addClass('hidden');
            $form.find('.cobro_regular').hide();

            $form.find('input[name="total_financiacion"]').val(tratamiento.total_financiacion);
            $form.find('input[name="consumido_financiacion"]').val(tratamiento.consumido_financiacion);
            $form.find('input[name="cobrado"]').val(0);
            $form.find('input[name="pendiente"]').val(0);

          } else {
            $form.find('input[name="financiado"]').prop('checked', false);
            $form.find('.cobro_financiacion').addClass('hidden');
            $form.find('.cobro_financiacion').hide();
            $form.find('.cobro_regular').removeClass('hidden');
            $form.find('.cobro_regular').show();

            $form.find('input[name="total_financiacion"]').val(0);
            $form.find('input[name="consumido_financiacion"]').val(0);
            $form.find('input[name="cobrado"]').val(tratamiento.cobrado);
            $form.find('input[name="pendiente"]').val(tratamiento.pendiente);
          }

          $form.find('input[name="fecha"]').data("DateTimePicker").date(moment(programacion.fecha));

          $form.find('input[name="hora_inicio"]').data("DateTimePicker").date(moment(programacion.hora_inicio, "hh:mm:ss"));
          $form.find('input[name="hora_fin"]').data("DateTimePicker").date(moment(programacion.hora_fin, "hh:mm:ss"));
          $form.find('input[name="tiempo"]').val(programacion.tiempo);

          $form.find('input[name="realizada"]').prop('checked', programacion.realizada);
          $form.find('input[name="fecha_realizacion"]').data("DateTimePicker").date(moment(programacion.fecha_realizacion, "YYYY-MM-DD"));

          @if (Auth::user()->role_id != 2)  {{-- Admin --}}
            $form.find('input[name="cancelada"]').prop('checked', programacion.cancelada);
            $form.find('input[name="fecha_cancelacion"]').data("DateTimePicker").date(moment(programacion.fecha_cancelacion, "YYYY-MM-DD"));
            var tipoCancelacionSelect = $form.find('select[name=tipo_cancelacion]');
            tipoCancelacionSelect.empty();
            tipoCancelacionSelect.append('<option value="">-- Seleccionar valor --</option>');

            @foreach ($tiposCancelaciones as $tipo)
              var selected = {!! $tipo->id !!} == programacion.tipo_cancelacion_id;
              tipoCancelacionSelect.append('<option value="{!! $tipo->id !!}"' + (selected ? "selected" : "") + '>{!! $tipo->nombre !!}</option>');
            @endforeach
          @endif

          if (tratamiento.observaciones) {
              $form.find('#observaciones').val(tratamiento.observaciones);
          }

          // Completar datos del cobro en la pestaña de contabilidad
          fillCobrarForm(tratamiento.id, tratamiento.pendiente, event.title);

          var url = '{{ route('admin.programaciones.modalUpdate', ['id' => '__id']) }}'.replace('__id', programacion.id);
          $form.attr('action', url);

          var urlMasOpciones = '{{ route('voyager.tratamientos-paciente.edit', ['id' => '__id']) }}'.replace('__id', tratamiento.id);
          $('#masOpciones').attr('href', urlMasOpciones);

          cambiosEnImportes = false;

          // Mostrar modal si no está ya en pantalla
          if ( !($("#programacionModal").data('bs.modal') || {}).isShown) {
              $('#programacionModal').modal({ keyboard: true }, 'show');
          }

          activarCitas();
          activarPagos();
        }
      });
    }

    // TODO: Lo hago así porque no me deja importar Echo
    var agendaChanel = pusher.subscribe('agenda');
    var socketId = pusher.connection.socket_id;
    console.log(socketId);
    $('.socketId').val(socketId);

    // Al recibir un evento de actualización de agenda, se actualiza
    agendaChanel.bind('agenda.updated', function(data) {
      if (data) {
        var programacion_id = data.programacion_id;
        var updated_at = data.user_id;
        var tratamiento_id = data.tratamiento_id;

        // if (updated_at == {{ Auth::user()->id }}) {
        //   console.log('mismo usuario');
        //   return
        // }

        var date = $('#calendar').fullCalendar( 'getDate');
        var _month = date.format('MM-YYYY');

        // Se busca la programación que se ha actualizado
        actualizarEventoEnAgenda(_month, programacion_id, tratamiento_id, updated_at);
      }
    });

    // Al recibir un evento de eliminación de cita
    agendaChanel.bind('agenda.deleted', function(data) {
      console.log('deleted');

      if (data) {
        var programacion_id = data.programacion_id;
        var updated_at = data.user_id;

        // Si es el mismo usuario no se hace nada
        // if (updated_at == {{ Auth::user()->id }}) {
        //   return
        // }

        var calendarEvents = filtrarCalendario('programacion_id', programacion_id);

        if (calendarEvents && calendarEvents.length > 0) {
          // Solamente puede haber una programación igual en la agenda
          var calendarEvent = calendarEvents[0];
          var event_id = calendarEvent._id;

          $('#calendar').fullCalendar( 'removeEvents', event_id );
        }
      }

    });

  </script>
@endsection
