<form role="form"
  id="programacionForm"
  class="form-edit-add form-tratamiento"
  action="{{ url('admin/agenda/modalUpdate') }}"
  method="POST" enctype="multipart/form-data">

  <div class="modal-body">
    <input type="hidden" name="paciente_id" />
    <input type="hidden" name="programacion_id" />

    <!-- CSRF TOKEN -->
    {{ csrf_field() }}
      <div class="row">
        {{-- Id del evento de fullcalendar. Se utiliza para eliminar el evento antiguo al ser actualizado --}}
        <input type="hidden" id="idCalendarEvent" name="idCalendarEvent" />

        <div class="row form-group">
          <div class="col-md-2 div_checkbox_potencial">
            <div class="checkbox">
             <label>
               <input class="checkbox_potencial" type="checkbox" name="paciente_potencial" id="paciente_potencial"> Paciente potencial
             </label>
            </div>
          </div>
          <div class="form-group col-md-3" name="pacienteDiv">
            <label class="control-label" for="q">Paciente:</label>
             <div class="input-group">
               <input type="text" class="form-control" name="q" placeholder="Introduce el nombre, código o email de un paciente" />
               <span class="input-group-btn">
                 <a class="btn-sm btn-danger disabled clear">
                   <i class="fas fa-trash-alt"></i>
                 </a>
               </span>
             </div>
          </div>
          <div class="col-md-3 hidden" name="pacienteLinkDiv">
            <label class="control-label" for="datosPaciente">Paciente:</label>
            <div class="form-group">
              <a href="#" id="linkPaciente" target="_blank"></a>
            </div>
          </div>
          <div class="col-md-6 hidden" name="pacientePotencialDiv">
            <div class="form-group col-md-6">
              <label class="control-label" for="nombre_paciente_potencial">Nombre paciente:</label>
              <input type="text" class="form-control" name="nombre_paciente_potencial">
            </div>
            <div class="col-md-6">
              <label class="control-label" for="telefono_paciente_potencial">Teléfono:</label>
              <input type="text" class="form-control" name="telefono_paciente_potencial">
            </div>
          </div>
          @if (Auth::user()->role_id != 2)  {{-- Admin --}}
            <div class="col-md-2 div_saldo">
              <label class="control-label" for="saldo">Saldo:</label>
              <input class="form-control" type="text" name="saldo" readonly>
            </div>
            <div class="col-md-2 div_mutua">
              <label for="mutua">Mútua</label>
              <input class="form-control" type="text" name="mutua" value="" required>
            </div>
            <div class="col-md-2 div_poliza">
              <label for="numero_poliza">Nº póliza</label>
              <input class="form-control" type="text" name="numero_poliza" value="" required>
            </div>
            <div class="col-md-3">
              <label class="control-label" for="tipo_paciente">Tipo paciente:</label>
              <select class="form-control select2" name="tipo_paciente">
                <option value="1">General</option>
                <option value="2">Mútua</option>
                <option value="3">Potencial</option>
              </select>
            </div>

          @endif
        </div>

        <div class="row form-group">
          <div class="col-md-3">
            <label class="control-label" for="doctor_id">Doctor:</label>
            <select class="form-control select2" name="doctor_id">
            </select>
          </div>
          <div class="form-group col-md-2">
            <label for="nombre">Nombre tratamiento</label>
            <input class="form-control" type="text" name="nombre" value="" required>
          </div>
        </div>

        @include('vendor.voyager.tratamientos-paciente.partials.tablePlanTratamiento',
        ['data' => null, 'key' => null, 'version' => 'light'])

        <div class="form-group">
          <input class="checkbox_financiado hidden" type="checkbox" name="financiado">
          <div class="col-md-2">
            <div class="cobro_financiacion">
              <label for="total_financiacion">Total financiación</label>
              <input class="form-control" type="text" name="total_financiacion" readonly>

              <label for="consumido_financiacion">Consumido</label>
              <input class="form-control" type="text" name="consumido_financiacion" readonly>

            </div>

            <div class="cobro_regular">
              <label for="cobrado">Cobrado</label>
              <input class="form-control" type="text" name="cobrado" readonly>
              <label for="pendiente">Pendiente</label>
              <input class="form-control" type="text" name="pendiente" readonly>
           </div>
         </div>
         <div class="col-md-10">
           <label for="observaciones">Observaciones</label>
           <textarea class="form-control" id="observaciones" name="observaciones" rows="6"></textarea>
         </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label" for="fecha">Fecha</label>
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input name="fecha" class="form-control" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="hora_inicio" class="control-label">Hora inicio</label>
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                        <input name="hora_inicio" class="form-control" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="hora_fin" class="control-label">Hora fin</label>
                <div class="controls">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-clock"></i></span>
                        <input name="hora_fin" class="form-control" />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="tiempo" class="control-label">Tiempo</label>
                <div class="controls">
                    <div class="input-group">
                        <input type="number" name="tiempo" id="tiempo" class="form-control" />
                        <span class="input-group-addon">mins</span>
                    </div>
                </div>
            </div>
        </div>
          <div class="col-md-1">
              <div class="form-group">
                  <label for="realizada" class="control-label">Realizada</label>
                  <div class="controls">
                    <input type="checkbox" id="realizada" name="realizada" />
                    <i class="fas fa-check" style="color: green" aria-hidden="true"></i>
                  </div>
              </div>
          </div>
          <div class="col-md-4">
              <div class="form-group">
                  <label class="control-label" for="fecha_realizacion">Fecha realización</label>
                  <div class="controls">
                      <div class="input-group">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input name="fecha_realizacion" id="fecha_realizacion" class="form-control" />
                      </div>
                  </div>
              </div>
          </div>
        @if (Auth::user()->role_id != 2)  {{-- Admin --}}
          <div class="cancelacion">
            <div class="col-md-1">
                <div class="form-group">
                    <label for="cancelada" class="control-label">Cancelada</label>
                    <div class="controls">
                      <input type="checkbox" id="cancelada" name="cancelada" />
                      <i class="fa fa-times" style="color: darkred" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label class="control-label" for="fecha_cancelacion">Fecha cancelación</label>
                    <div class="controls">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            <input name="fecha_cancelacion" id="fecha_cancelacion" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label class="control-label" for="tipo_cancelacion">Tipo</label>
                    <div class="controls">
                        <select id="tipo_cancelacion" name="tipo_cancelacion" class="form-control"></select>
                    </div>
                </div>
            </div>
          </div>
        @endif
      </div>
    </div>
    <div class="modal-footer">
        <div class="alert alert-danger validations-alert pull-left hidden">
        </div>

        <a class="btn-sm btn-default" id="masOpciones">
          <i class="voyager-dot-3"></i>
            <span> Más opciones</span>
        </a>
        <button type="submit" class="btn btn-success guardar-tratamiento">
          <i class="voyager-download"></i>
            <span>Guardar</span>
        </button>
        <a class="btn btn-danger delete">
          <i class="voyager-trash"></i>
          <span>Eliminar</span>
        </a>
        <button type="reset" data-dismiss="modal" class="btn btn-default">
          Cancelar
        </button>

        <div class="form-group confirm-delete hidden">
          <div class="alert alert-warning pull-right">
            ¿Confirmas el borrado de esta cita?
            <div class="form-group">
                <a class="btn-xs btn-danger aceptar-borrado" role="button">Aceptar</a>
                <a class="btn-xs btn-default cancelar-borrado" role="button">Cancelar</a>
            </div>
          </div>
        </div>

    </div>
</form>

@section('javascript')
  @parent

  <script type="text/javascript">
    // Botón de borrar actuación en el modal
    $('.delete').click(function() {
      $('.confirm-delete').removeClass('hidden');
    });

    $('.cancelar-borrado').click(function() {
      $('.confirm-delete').addClass('hidden');
    });

    $('.aceptar-borrado').click(function() {
      var programacion_id = $('input[name="programacion_id"]').val();
      var url = '{{ route('admin.programaciones.ajaxDestroy', ['id' => '__id']) }}'.replace('__id', programacion_id);
      ajaxRequest(url, null, 'DELETE', function(json) {
        if (!json.error) {
          var _idEvent = $form.find('input[name="idCalendarEvent"]').val();
          $('#calendar').fullCalendar( 'removeEvents', _idEvent );
          $('#programacionModal').modal('hide');
        }
      });
    });


  </script>
@endsection
