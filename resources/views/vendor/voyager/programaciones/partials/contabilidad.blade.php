<form role="form"
  class="form-registrar-cobro"
  action="#"
  method="POST">

  <div class="modal-body">
    <div class="row">
      <!-- CSRF TOKEN -->
      {{ csrf_field() }}

      <input type="hidden" name="tratamiento_id">
      <fieldset>
        <legend>Registrar cobro</legend>
        <div class="col-md-4">
          <label for="totalCobro">Importe</label>
          <input class="form-control" type="number" name="totalCobro" value="0">
          <label for="tipoCobro">Tipo de cobro</label>
          <select class="form-control" name="tipoCobro">
            <option value="1">Efectivo</option>
            <option value="2">Tarjeta</option>
          </select>
        </div>
        <div class="col-md-8">
          <label for="concepto">Concepto</label>
          <textarea class="form-control" name="concepto" rows="5"></textarea>
        </div>
        <button class="btn btn-xs btn-success pull-right" type="submit">
          <i class="fa fa-euro-sign"></i> Registrar
      </fieldset>

      <div class="form-group col-md-12">
        <label>Movimientos de caja</label>
        <div class="table-responsive table-movimientos-caja">
          <table class="table table-bordered">
            <thead>
              <th>Fecha</th>
              <th>Concepto</th>
              <th>Entrada</th>
              <th>Salida</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        {{-- table-responsive --}}
      </div>
    </div>
  </div>
</form>

@section('javascript')
  @parent

  <script type="text/javascript">
    $( ".form-registrar-cobro" ).submit(function( event ) {
      event.preventDefault();

      if (cambiosEnImportes) {
        alert("Antes de registrar un cobro, tienes que guardar los cambios en la pestaña 'Tratamientos'");
        return;
      }

      var $form = $( '.form-registrar-cobro' ),
        url = $form.attr( "action" );

      ajaxRequest(url, $form.serialize(), 'POST', function(json) {
        if (!json.error) {
          var movimiento = json.data.movimiento,
            entrada = parseFloat(movimiento.entrada ? movimiento.entrada : 0),
            salida = parseFloat(movimiento.entrada ? movimiento.entrada : 0),
            cobrado = parseFloat(json.data.cobrado ? json.data.cobrado : 0),
            pendiente = parseFloat(json.data.pendiente ? json.data.pendiente : 0),
            saldo = parseFloat(json.data.saldo ? json.data.saldo : 0);

          $('#programacionForm').find('input[name="cobrado"]').val(cobrado);
          $('#programacionForm').find('input[name="pendiente"]').val(pendiente);

          // Actualizar saldo del paciente
          $('#programacionForm').find('input[name="saldo"]').val(saldo);

          // Añadir registro a la tabla de movimientos
          console.log(movimiento);
          $('.table-movimientos-caja tbody').append(
            ' <tr> ' +
            '   <td>' + moment().format('DD/MM/YYYY') + '</td>' +
            '   <td>' + movimiento.concepto + '</td>' +
            '   <td>' + entrada + '</td>' +
            '   <td>' + salida + '</td>' +
            ' </tr> '
          );

          // Actualizar el evento del calendario si estamos en la agenda
          var $form = $( '#programacionForm' );
          if ($form) {
            var _idEvent = $form.find('input[name="idCalendarEvent"]').val();
            var events = $("#calendar").fullCalendar('clientEvents', _idEvent);
            if (events.length > 0 ) {
              console.log('actualizar evento');
              var event = events[0];
              event.cobrado = cobrado;
              event.pendiente = pendiente;
              event.saldo = saldo;

              $('#calendar').fullCalendar('updateEvent', event);
            }
          }
        }
      });
    });
  </script>

@endsection
