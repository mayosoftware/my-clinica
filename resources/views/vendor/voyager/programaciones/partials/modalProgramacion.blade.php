<style>
.modal-agenda {
    width: 90% !important;
}
</style>
<div class="modal modal-info fade large" id="programacionModal" tabindex="-1" role="dialog" aria-labelledby="editModal" aria-hidden="true">
   <div class="modal-dialog modal-lg modal-agenda">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Editar actuación paciente: Nombre paciente</h4>
        </div>

        {{-- Comprobar rol del usuario --}}
        @if (Auth::user()->role_id != 2)  {{-- Admin --}}
          <ul class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active tratamiento-tab"><a href="#tratamiento" aria-controls="tratamiento" role="tab" data-toggle="tab">Tratamiento</a></li>
            <li role="presentation" class="pagos-tab"><a href="#pagos" aria-controls="pagos" role="tab" data-toggle="tab">Pagos</a></li>
            <li role="presentation" class="citas-tab"><a href="#citas" aria-controls="citas" role="tab" data-toggle="tab">Citas</a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="tratamiento">
              @include('vendor.voyager.programaciones.partials.info-programacion')
            </div>
            <div role="tabpanel" class="tab-pane" id="pagos">
              @include('vendor.voyager.tratamientos-paciente.partials.pagos')
            </div>
            <div role="tabpanel" class="tab-pane" id="citas">
              @include('vendor.voyager.tratamientos-paciente.partials.citas')
            </div>
          </div>
        @else
          @include('vendor.voyager.programaciones.partials.info-programacion')
        @endif
      </div>
  </div>
</div>

@section('javascript')
  @parent
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script type="text/javascript">

    var $form = $( '#programacionForm' );

    $form.find('input[name="q"]').autocomplete({
      source: "{{ route('searchPaciente') }}",
      minLength: 2,
      appendTo: $('#programacionForm'),
      select: function(event, ui) {
        var id = ui.item.id;
        var saldo = ui.item.saldo;
        if (id) {
          $form.find('input[name="q"]').prop('readonly', 'true');
          $form.find('.clear').removeClass('disabled');
          $form.find('input[name="paciente_id"]').val(id);
          $form.find('input[name="saldo"]').val(saldo);

        } else {
          return false;
        }
      }
    });

    $form.on('click', '.clear', clearPacienteSeleccionado);

    // Seleccionar paciente potencial
    $(document).on('change', '.checkbox_potencial', function(event){
      var potencial = $( this ).prop('checked');
      setupPaciente(potencial);
    });

    function clearPacienteSeleccionado() {
      $form.find('input[name="q"]').removeAttr('readonly');
      $form.find('input[name="q"]').val('');
      $form.find('input[name="paciente_id"]').val('');
      $form.find('.clear').addClass('disabled');
    }

    function setupPaciente(potencial) {
      if (potencial) {
        $('div[name="pacienteDiv"]').addClass('hidden');
        $('div[name="pacienteLinkDiv"]').addClass('hidden');
        $('.div_saldo').addClass('hidden');
        $('.div_mutua').addClass('hidden');
        $('.div_poliza').addClass('hidden');
        $('div[name="pacientePotencialDiv"]').removeClass('hidden');
      } else {
        $('div[name="pacienteDiv"]').removeClass('hidden');
        $('div[name="pacienteLinkDiv"]').addClass('hidden');
        $('.div_saldo').removeClass('hidden');
        $('.div_mutua').removeClass('hidden');
        $('.div_poliza').removeClass('hidden');
        $('div[name="pacientePotencialDiv"]').addClass('hidden');
      }
    }

    $(function () {
      $form.find('input[name="total"]').prop('readonly', true);
      $form.find('input[name="cobrado"]').prop('readonly', true);

      // Al cambiar precio, se actualiza total
      $form.find('input[name="importe_unitario"]').change(function() {
        var cantidad = $form.find('input[name="cantidad"]').val();
        var importe_unitario = $form.find('input[name="importe_unitario"]').val();
        $form.find('input[name="total"]').val(cantidad * importe_unitario);
      });

      // Al cambiar cantidad, se actualiza total
      $form.find('input[name="cantidad"]').change(function() {
        var cantidad = $form.find('input[name="cantidad"]').val();
        var importe_unitario = $form.find('input[name="importe_unitario"]').val();
        $form.find('input[name="total"]').val(cantidad * importe_unitario);
      });

      // Controlar check de realización
      $form.find('input[name="realizada"]').change(function () {
        if ($form.find('input[name="realizada"]').prop('checked')) {
            $form.find('input[name="fecha_realizacion"]').data("DateTimePicker").date(new Date());
        } else {
            $form.find('input[name="fecha_realizacion"]').data("DateTimePicker").clear();
        }
      });

      // Controlar check de cancelación
      $form.find('input[name="cancelada"]').change(function () {
        if ($form.find('input[name="cancelada"]').prop('checked')) {
            $form.find('input[name="fecha_cancelacion"]').data("DateTimePicker").date(new Date());
        } else {
            $form.find('input[name="fecha_cancelacion"]').data("DateTimePicker").clear();
        }
      });

      // Sumar minutos a la hora de fin al cambiar
      $form.find('input[name="hora_inicio"], input[name="hora_fin"]').on('dp.change', function (e) {
        if (e.date) {
          var fin = $form.find('input[name="hora_fin"]').data("DateTimePicker").date();
          var ini = $form.find('input[name="hora_inicio"]').data("DateTimePicker").date();

          if (fin && ini) {
            var timefin = fin.format('HH:mm');
            var timeini = ini.format('HH:mm');

            var tiempo = moment(timefin, "HH:mm").diff(moment(timeini, "HH:mm"), 'minutes');

            // Se controla que la hora de inicio sea menor que la de fin
            if (tiempo < 0) {
                $form.find('input[name="hora_fin"]').data("DateTimePicker").date(ini.add(1, 'minutes'));
                toastr.error("La hora de inicio no puede ser anterior");
            } else {
                $form.find('input[name="tiempo"]').val(tiempo);
            }
          }
        }
      });

      // Controlar cambios en el campo Tiempo cuando se
      $form.find('input[name="tiempo"]').keyup(cambiarTiempo);

      // Controlar cambios en el campo Tiempo cuando se utiliza el selector
      $form.find('input[name="tiempo"]').change(cambiarTiempo);

      function cambiarTiempo() {
        var tiempo = $(this).val() ? $(this).val() : 0,
          ini = $form.find('input[name="hora_inicio"]').data("DateTimePicker").date(),
          fin = $form.find('input[name="hora_fin"]').data("DateTimePicker").date();

        if (tiempo < 0 || !$.isNumeric(tiempo)) {
          toastr.error("El tiempo debe ser numérico y mayor que 0");
          tiempo = 1;
          $(this).val(1);
        }

        if (ini) {
          $form.find('input[name="hora_fin"]').data("DateTimePicker").date(ini.add(tiempo, 'minutes'));
        } else if (fin) {
          $form.find('input[name="hora_inicio"]').data("DateTimePicker").date(fin.add(-tiempo, 'minutes'));
        }
      }

    });

    function resetProgramacionForm() {
      var $form = $( '#programacionForm' );
      setupPaciente(false);
      clearPacienteSeleccionado();
      $('.confirm-delete').addClass('hidden');
      $form.find('#paciente_potencial').prop('checked', false);
      $form.find( ".modal-footer .validations-alert" ).addClass('hidden');
      $form.find('input[name="idCalendarEvent"]').val('');
      $form.find('input[name="descripcion"]').val('');
      if ($form.find('input[name="fecha"]').data("DateTimePicker")) {
        $form.find('input[name="fecha"]').data("DateTimePicker").clear();
      }
      if ($form.find('input[name="hora_inicio"]').data("DateTimePicker")) {
        $form.find('input[name="hora_inicio"]').data("DateTimePicker").clear();
      }
      if ($form.find('input[name="hora_fin"]').data("DateTimePicker")) {
        $form.find('input[name="hora_fin"]').data("DateTimePicker").clear();
      }
      $form.find('input[name="tiempo"]').val('');
      if ($form.find('input[name="fecha_cancelacion"]').data("DateTimePicker")) {
        $form.find('input[name="fecha_cancelacion"]').data("DateTimePicker").clear();
      }

      $form.find('input[name="cantidad"]').val(1);
      $form.find('input[name="importe_unitario"]').val(0);
      $form.find('input[name="total"]').val(0);
      $form.find('input[name="cobrado"]').val(0);
      $form.find('input[name="total_financiacion"]').val(0);
      $form.find('input[name="consumido_financiacion"]').val(0);
      $form.find('input[name="cobrado"]').val(0);
      $form.find('input[name="pendiente"]').val(0);

      if ($form.find('input[name="realizada"]')) {
        $form.find('input[name="realizada"]').prop('checked', false);
      }
      if ($form.find('input[name="fecha_realizacion"]').data("DateTimePicker")) {
        $form.find('input[name="fecha_realizacion"]').data("DateTimePicker").clear;
      }
      if ($form.find('input[name="cancelada"]')) {
        $form.find('input[name="cancelada"]').prop('checked', false);
      }
      if ($form.find('input[name="fecha_cancelacion"]').data("DateTimePicker")) {
        $form.find('input[name="fecha_cancelacion"]').data("DateTimePicker").clear;
      }

      $form.find('#dynamic_field tbody').empty();
      rows = 0;

      $form.find("#observaciones").val('');
    }

    // // Controlar el cambio de tabs
    // var cargarMovimientos = true;
    // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    //   var target = $(e.target).attr("href") // activated tab
    //   if (target == '#pagos' && cargarMovimientos) {
    //     // Cargar pendiente en total cobro
    //     var pendiente = $( '#programacionForm' ).find('input[name="pendiente"]').val();
    //     $('.form-registrar-cobro').find('input[name="totalCobro"]').val(pendiente ? pendiente : 0);
    //
    //     // Cargar movimientos de caja
    //     var id = $('.form-registrar-cobro').find('input[name="tratamiento_id"]').val();
    //     var url = '{{ route('admin.movimientos-caja.tratamiento', ['id' => '__id']) }}'.replace('__id', id);
    //     ajaxRequest(url, null, 'GET', function(json) {
    //       if (!json.error) {
    //         var movimientos = json.data.movimientos;
    //         $.each(movimientos, function( index, movimiento ) {
    //           $('.table-movimientos-caja tbody').append(
    //             ' <tr> ' +
    //             '   <td>' + moment(movimiento.created_at).format('DD/MM/YYYY') + '</td>' +
    //             '   <td>' + movimiento.concepto + '</td>' +
    //             '   <td>' + (movimiento.entrada ? movimiento.entrada : 0) + '</td>' +
    //             '   <td>' + (movimiento.salida ? movimiento.salida : 0) + '</td>' +
    //             ' </tr> '
    //           );
    //         });
    //       }
    //     });
    //     cargarMovimientos = false;
    //   }
    // });

    // Al cerrar el modal se inicializan valores
    $("#programacionModal").on("hidden.bs.modal", function () {
      // Limpiar tabla de movimientos
      $('.table-movimientos-caja tbody').empty();
      $('#citasTable tbody').empty();
      $('.form-registrar-cobro').find('input[name="tratamiento_id"]').val('');

      // Poner activa la primera pestaña
      $('.nav-tabs a:first').tab('show');

      $('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
      $('#programacionModal .modal-backdrop').remove();//eliminamos el backdrop del modal

      // TODO: Reiniciar resto de valores y quitarlo de la página browse

      // Scroll al body de la página
      //$('body').css('overflow', 'auto');
    });

    $("#programacionModal").on("show.bs.modal", function () {
      var id = $('.form-registrar-cobro').find('input[name="tratamiento_id"]').val();
      var financiado = $('#programacionForm').find('input[name="financiado"]').prop('checked');

      //Si está creando tratamiento se deshabilita pestaña contabilidad
      if (!id) {
        // Pagos
        $('.alert-pagos-group').removeClass('hidden');
        $('.registrar-pago-group').addClass('hidden');
        $('.movimientos-caja-group').addClass('hidden');

        // Citas
        $('.alert-citas-group').removeClass('hidden');
        $('.citas-tratamiento-group').addClass('hidden');

      } else if (financiado) {
        $('.alert-pagos-group').addClass('hidden');
        $('.registrar-pago-group').addClass('hidden');
        $('.movimientos-caja-group').removeClass('hidden');

        $('.alert-citas-group').addClass('hidden');
        $('.citas-tratamiento-group').removeClass('hidden');
      } else {
        $('.alert-pagos-group').addClass('hidden');
        $('.registrar-pago-group').removeClass('hidden');
        $('.movimientos-caja-group').removeClass('hidden');

        $('.alert-citas-group').addClass('hidden');
        $('.citas-tratamiento-group').removeClass('hidden');
      }
    });

  </script>

@endsection
