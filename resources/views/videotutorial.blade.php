@extends('templates.master')

@section('content')
  <div class="page-content browse container-fluid">

      <div class="row">
          <div class="col-md-12">
              <div class="panel panel-bordered">
                  <div class="panel-body">
                    <div class="form-group">
                      <h3>Registro de paciente</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273286970" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Gestión ficheros de paciente</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273286986" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Gestión de la agenda</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273287002" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Creación de actuación</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273287019" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Cobrar y facturar actuaciones</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273287049" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Gestión de presupuestos</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273287063" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Movimientos de caja</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273287071" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>

                    <div class="form-group">
                      <h3>Gestión de facturas</h3>
                      <iframe width="640" height="564" src="https://player.vimeo.com/video/273287092" frameborder="0" allowFullScreen mozallowfullscreen webkitAllowFullScreen></iframe>
                    </div>
                  </div>
              </div>
          </div>
      </div>
  </div>

@stop
