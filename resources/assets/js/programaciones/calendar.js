var events;
var user_id;
var festivos;

function setupCalendar(_events, _user_id, _festivos) {

  events = _events;
  user_id = _user_id;
  festivos = _festivos;

  // Full fullcalendar
  $('#calendar').fullCalendar({
    header: {
        left: 'prev, next today',
        center: 'title',
        right: 'month, agendaWeek, agendaDay, listMonth'
    },
    //eventDataTransform: function (json) {
    //    console.log(json);
    //    return json.data;
    //},
    eventAfterRender: function (event, element, view) {
        // Se añaden los iconos
        $(element).find("div.fc-title").empty();

        var alerts = '';

        // Si tiene historial clínico
        if (event.historialClinico > 0) {
          alerts = ' <i class="fa fa-2x fa-notes-medical" aria-hidden="true" style="color:yellow;"></i> ';
        }

        // Si tiene observaciones
        if (event.observaciones) {
          alerts += ' <i class="fa fa-2x fa-comment-alt" aria-hidden="true"></i> ';
        }

        // Si tiene mutua
        if (event.mutua) {
          alerts += ' <i class="fa fa-2x" aria-hidden="true" style="color:black;">M</i> ';
        }

        // Si es potencial
        console.log(event.pacientePotencial);
        if (event.pacientePotencial == true) {
          alerts += ' <i class="fa fa-2x" aria-hidden="true" style="color:red;">P</i> ';
        }

        if (alerts) {
          $(element).find("div.fc-title").append(alerts);
        }

        $(element).find("div.fc-title").append(event.title);
    },
    defaultDate: moment(),
    height: 600,
    editable: false,
    slotDuration: '00:10:00',
    scrollTime: '08:00:00',
    weekends: true,
    defaultView: 'agendaDay',
    draggable: false,
    locale: 'es',
    buttonIcons: true,
    events: events,
    eventClick: function (calEvent, jsEvent, view) {
      fillModalForEdit(calEvent);
    },
    dayClick: function(date, jsEvent, view) {
      fillModalForCreate(date);
    },
    viewRender: function (element) {
      var month = element.start.format('MM-YYYY');
      console.log('Mes seleccionado: ' + month);
      if (jQuery.inArray(month, selectedMonths) == -1) {
        $('#voyager-loader').show();
        // Consultar eventos del mes
        loadEventsByMonth(month);
        selectedMonths.push(month);
      }
    }
  });
}

function hightlightkDaysInDatePicker(pickerDate = null, jsEvent = null) {
    var dateString;
    var date;

    if (pickerDate == null) {
      dateString = $("#daypicker").data("date");
      date = moment(dateString, "DD/MM/YYYY");
    } else {
      date = pickerDate;
    }

    // Cambiar la clase de los días con eventos
    tableDays = $(".datepicker .datepicker-days .table-condensed");

    var trs = tableDays.find("tbody>tr");
    
    // Pintar festivos    
    for (var i = 0; i < festivos.length; i++) {
      festivo = festivos[i];
      var festivoMoment = moment(festivo.dia, 'YYYY-MM-DD');
      var dayToHighlight = festivoMoment.format("DD/MM/YYYY");
      dayWithEvent = trs.find("[data-day='" + dayToHighlight + "']");
      // if (!dayWithEvent.hasClass("active")) { // Si es el día activo no se pinta el día
        dayWithEvent.addClass("holiday");
      // }
    }   

    // Se actualiza un único evento
    if (jsEvent) {
      var evt = jsEvent;
      var eventdate = moment(evt.start).startOf('day');

      var month = eventdate.format('M');
      var year = eventdate.format('YYYY');

      if (eventdate.format('YYYY') == date.format('YYYY') && eventdate.format('M') == date.format('M')) {
          // Se añade la clase para marcar el evento en el calendario
          var dayToHighlight = eventdate.format("DD/MM/YYYY");
          dayWithEvent = trs.find("[data-day='" + dayToHighlight + "']");
          if (!dayWithEvent.hasClass("active")) { // Si es el día activo no se pinta el día
              dayWithEvent.addClass("event");
          }
      }
    } else {
      // Se buscan todos los eventos del calendario en memoria
      var events = $('#calendar').fullCalendar( 'clientEvents');
      if(events) {
        $.each(events, function (index, evt) {
            var eventdate = moment(evt.start).startOf('day');

            var month = eventdate.format('M');
            var year = eventdate.format('YYYY');

            if (eventdate.format('YYYY') == date.format('YYYY') && eventdate.format('M') == date.format('M')) {
                // Se añade la clase para marcar el evento en el calendario
                var dayToHighlight = eventdate.format("DD/MM/YYYY");
                dayWithEvent = trs.find("[data-day='" + dayToHighlight + "']");
                if (!dayWithEvent.hasClass("active")) { // Si es el día activo no se pinta el día
                    dayWithEvent.addClass("event");
                }
            }
          });
      }
    }
}

/**
 * Se actualiza un evento en el calendario
 * @param  {[type]} _idEvent [description]
 * @return {[type]}          [description]
 */
function guardarEventoCalendario(_idEvent, eventUpdated) {
  if (_idEvent) {
    var events = $("#calendar").fullCalendar('clientEvents', _idEvent);
    // Si existe el evento se actualiza
    if (events.length > 0) {
      var event = events[0];
      // Recorrer elementos del evento
      $.each(eventUpdated, function( key, value ) {
        event[key] = value;
      });
      event['idCalendarEvent'] = _idEvent;
      $('#calendar').fullCalendar('updateEvent', event);

    // Si no existe se crea
    } else {
      // Añadir evento actualizado
      $('#calendar').fullCalendar('renderEvent', eventUpdated, true);
    }
    hightlightkDaysInDatePicker(null, event);

  // Es un evento nuevo
  } else {
    $('#calendar').fullCalendar('renderEvent', eventUpdated, true);
  }
}

function actualizarEventoEnAgenda(_month, programacion_id, tratamiento_id, updated_at) {
  var url = routeProgramacion.replace('__id', programacion_id);
  ajaxRequest(url, { month : _month}, 'POST', function(json) {
    if (json.data) {
      var events = json.data.events;
      $.each(events, function (index, eventUpdated) {
        // Se buscan los eventos con la programación que se ha actualizado
        var calendarEvents = filtrarCalendario('programacion_id', programacion_id);

        // Evento actualizado
        if (calendarEvents && calendarEvents.length > 0) {
          // Solamente puede haber una programación igual en la agenda
          var calendarEvent = calendarEvents[0];
          var event_id = calendarEvent._id;

          guardarEventoCalendario(event_id, eventUpdated);

          // Si hay otras programaciones del mismo tratamiento en la agenda, se atualizan
          var tratamientoEvents = filtrarCalendario('tratamiento_id', tratamiento_id);
          if (tratamientoEvents && tratamientoEvents.length > 0) {
            guardarOtrasProgramacionesTratamiento(tratamientoEvents, _month);
          }

          // Si el modal está abierto con la programación, se actualiza
          var programacion_abierta_id = $( "#programacionForm" ).find('input[name="programacion_id"]').val();
          if (programacion_abierta_id == programacion_id &&
            ($("#programacionModal").data('bs.modal') || {}).isShown &&
            updated_at != user_id ) {
            var eventAgenda = filtrarCalendarioPorId(event_id);
            fillModalForEdit(eventAgenda);
          }

        // Nuevo evento
        } else {
          guardarEventoCalendario(null, eventUpdated);
        }
      });
    }
  });
}

/**
 * Guardar otras programaciones del tratamiento modificado
 * @param  {[type]} events [description]
 * @param  {[type]} _month [description]
 * @return {[type]}        [description]
 */
function guardarOtrasProgramacionesTratamiento(events, _month) {
  $.each(events, function (index, event) {
    var programacion_id = event.programacion_id;

    var url = routeProgramacion.replace('__id', programacion_id);
    ajaxRequest(url, { month : _month}, 'POST', function(json) {
      if (json.data) {
        var events = json.data.events;
        $.each(events, function (index, eventUpdated) {
          // Se buscan los eventos con la programación que se ha actualizado
          var calendarEvents = filtrarCalendario('programacion_id', programacion_id);

          // Evento actualizado
          if (calendarEvents && calendarEvents.length > 0) {
            // Solamente puede haber una programación igual en la agenda
            var calendarEvent = calendarEvents[0];
            var event_id = calendarEvent._id;

            guardarEventoCalendario(event_id, eventUpdated);
          }
        });
      }
    });
  });
}

/**
 * Realiza una búsqueda por la clave indicada, con el valor indicado
 * @param  {[type]} key   [description]
 * @param  {[type]} value [description]
 * @return {[type]}       [description]
 */
function filtrarCalendario(key, value) {
  var events = $('#calendar').fullCalendar('clientEvents', function(evt) {
    return evt[key] == value;
  });
  return events;
}

function filtrarCalendarioEntreFechas(start, end) {
  var events = $('#calendar').fullCalendar('clientEvents', function(evt) {
    console.log(evt['fecha']);

    var fecha = moment(evt['fecha'], "DD/MM/YYYY");
    return fecha.isBetween(start, end);
  });
  return events;
}

/**
 * Realiza una búsqueda por el id del evento
 * @param  {[type]} _idEvent [description]
 * @return {[type]}          [description]
 */
function filtrarCalendarioPorId(_idEvent) {
  var eventAgenda = null;
  var events = $("#calendar").fullCalendar('clientEvents', _idEvent);
  // Si existe el evento se actualiza
  if (events.length > 0) {
    eventAgenda = events[0];
  }
  return eventAgenda;
}
