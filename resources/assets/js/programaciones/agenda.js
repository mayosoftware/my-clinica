var routeNextCita = null;
var routeSearchPaciente = null;
var routeEventsByMonth = null;
var routeBuscarPotencial = null;
var routeGuardarPotencial = null;
var routeFiltroDoctor = null;
var routePrintBetweenDates = null;
var routeProgramacion = null;
var doctores = null;
// Se obtiene el mes actual para controlar cuando se cambia de año en la agenda
var selectedMonths = [moment().format('MM-YYYY')];

function setupAgenda(_routeSearchPaciente, _routeNextCita, _routeEventsByMonth, _routeBuscarPotencial,
  _routeGuardarPotencial, _routeFiltroDoctor, _routePrintBetweenDates, _routeProgramacion, _doctores) {
  routeSearchPaciente = _routeSearchPaciente;
  routeNextCita = _routeNextCita;
  routeEventsByMonth = _routeEventsByMonth;
  routeBuscarPotencial= _routeBuscarPotencial;
  routeGuardarPotencial = _routeGuardarPotencial;
  routeFiltroDoctor = _routeFiltroDoctor;
  routePrintBetweenDates = _routePrintBetweenDates;
  routeProgramacion = _routeProgramacion;
  doctores = _doctores;

  setupBuscadorCitas();

  // Setup datetimepicker
  configDatePickerInline('#daypicker');
  hightlightkDaysInDatePicker();

  // Imprimir entre fechas
  configDatePicker('#desde');
  configDatePicker('#hasta');

  var $form = $( "#programacionForm" ),
    doctoresSelect = $form.find('select[name="doctor_id"]');

  // Configurar fechas del modal
  configDatePicker($form.find('input[name="fecha"]'));
  configTimePicker($form.find('input[name="hora_inicio"]'));
  configTimePicker($form.find('input[name="hora_fin"]'));
  configDatePicker($form.find('input[name="fecha_realizacion"]'));
  configDatePicker($form.find('input[name="fecha_cancelacion"]'));

  $('.radio input[type=radio]').change(filtroPorDoctor);
  $("#deselectRadioGroup").click(clearFiltroDoctor);
  $("#printButton").click(printBetweenDates);

  $.each(doctores, function (index, value) {
    doctoresSelect.append('<option value="'+ value.id +'">'+ value.name +'</option>');
  });
}

function loadEventsByMonth(month) {
  var url = routeEventsByMonth.replace('__month', month);
  ajaxRequest(url, null, 'POST', function(json) {
    $('#voyager-loader').hide();
    if (json.data.events) {
        $('#calendar').fullCalendar( 'addEventSource', json.data.events);
    }
  });
}

function setupBuscadorCitas() {
  var $form = $( '.buscar-paciente' );

  $form.find('input[name="q"]').autocomplete({
    source: routeSearchPaciente,
    minLength: 2,
    appendTo: $('.buscar-paciente'),
    select: function(event, ui) {
      var id = ui.item.id;
      if (id) {
        var url = routeNextCita.replace('__id', id);
        ajaxRequest(url, null, 'GET', function(json) {
          // Si hay siguiente actuación se navega a ella
          if (json.data && json.data.fecha) {
              let fecha = json.data.fecha,
                hora = json.data.hora,
                date = moment(fecha, "YYYY-MM-DD"),
                time = moment(hora, "HH:mm:ss");

                console.log(time);

            // Navegar a la fecha del evento
            $('#calendar').fullCalendar('gotoDate', date);

            // TODO: Navegar a la hora
            $('#calendar').fullCalendar( 'incrementDate', {
              hours: 3
            })
          }
        });

      } else {
        return false;
      }
    }
  });
}

$("#daypicker").on("dp.change", function (e) {
  console.log('cambio daypicker');
    //console.log(e.date);
    hightlightkDaysInDatePicker();
    // Cambiar día del calendario
    $('#calendar').fullCalendar('gotoDate', e.date)
    //$('#dpEnd').data("DateTimePicker").setMinDate(e.date);
});

// Actualizar los dias cuando se modifica la vista del datepicker
$("#daypicker").on("dp.update", function (e) {
    hightlightkDaysInDatePicker(e.viewDate);
});

function showErrors(data) {
  if (data.errors) {
    $form.find( ".modal-footer .validations-alert" ).empty();
    $form.find( ".modal-footer .validations-alert" ).addClass('alert-danger');
    $form.find( ".modal-footer .validations-alert" ).removeClass('alert-info');
    $form.find( ".modal-footer .validations-alert" ).removeClass('hidden');
    toastr.error("No se ha podido guardar");
    $.each(data.errors, function (index, value) {
      $form.find( ".modal-footer .validations-alert" ).append('<ul><li>' + value + '</li></ul>');
    });
  } else if (data.paciente_en_sistema) {
    $form.find( ".modal-footer .validations-alert" ).empty();
    $form.find( ".modal-footer .validations-alert" ).removeClass('alert-danger');
    $form.find( ".modal-footer .validations-alert" ).addClass('alert-info');
    $form.find( ".modal-footer .validations-alert" ).removeClass('hidden');
    $form.find( ".modal-footer .validations-alert" ).append('<ul><li>' + data.paciente_en_sistema + '</li></ul>');
    $form.find( ".modal-footer .validations-alert" ).append('<a class="btn-sm btn-success confirm_paciente_potencial" role="button">Haz click para confirmar la creación</a>');
    // eventConfirmarPaciente();

  }
}

function guardarTratamiento() {
  var $form = $( '#programacionForm' ),
    _idEvent = $form.find('input[name="idCalendarEvent"]').val(),
    url = $form.attr( "action" );
  // Guardar tratamiento
  ajaxRequest(url, $form.serialize(), 'POST', function(json) {
    if (json.error == true) {
      showErrors(json.data);
    } else {
      // Se oculta el div de errores
      $form.find( ".modal-footer .validations-alert" ).addClass('hidden');

      // Se muestra mensaje success
      var message = json.data.message;
      toastr.success(message);

      cambiosEnImportes = false;

      guardarEventoCalendario(_idEvent, json.data.event);

      // Se oculta el modal cuando se está creando un evento
      if (!_idEvent) {
        $('#programacionModal').modal('hide');
      } else {
        fillModalForEdit(json.data.event, _idEvent);
      }
    }
  });
}

function guardarPacientePotencial() {
    var $form = $( '#programacionForm' );
    var nombre_paciente_potencial = $form.find('input[name="nombre_paciente_potencial"]').val(),
      telefono_paciente_potencial = $form.find('input[name="telefono_paciente_potencial"]').val();

    var data = {
      nombre_paciente_potencial: nombre_paciente_potencial,
      telefono_paciente_potencial: telefono_paciente_potencial
    }
    ajaxRequest(routeGuardarPotencial, data, 'POST', function(json) {
      if (json.data.error == true) {
        showErrors(json.data);
      } else {
        // Añadir paciente_id
        $form.find('input[name="paciente_id"]').val(json.data.paciente_id);
        // Se ha guardado bien el paciente, se guarda el tratamiento
        guardarTratamiento();
      }
    });
}

// $(document).ready(function () {
  // Submit del formulario
  //$( "#programacionForm" ).submit(function( event ) {
  $('#programacionForm').on("click", ".guardar-tratamiento, .confirm_paciente_potencial", function(event) {
    // Stop form from submitting normally
    event.preventDefault();

    var $form = $( '#programacionForm' );

    // Comprobar si se está introduciendo un paciente potencial
    var potencial = $form.find('#paciente_potencial').prop('checked');
    var crear_paciente_duplicado = $(this).hasClass('confirm_paciente_potencial') ? 1 : 0;

    // 1. Se crea el paciente potencial y tratamiento
    if (crear_paciente_duplicado == 1) {
      guardarPacientePotencial();

    // 2. Comprobar si el paciente potencial existe. Sino existe, se guarda el tratamiento
    } else if (potencial == true) {
      var nombre_paciente_potencial = $form.find('input[name="nombre_paciente_potencial"]').val(),
        telefono_paciente_potencial = $form.find('input[name="telefono_paciente_potencial"]').val();

      var data = {
        nombre_paciente_potencial: nombre_paciente_potencial,
        telefono_paciente_potencial: telefono_paciente_potencial
      }

      ajaxRequest(routeBuscarPotencial, data, 'POST', function(json) {
        if (json.error == true) {
          showErrors(json.data);
          // Prevent submit
        } else {
          guardarPacientePotencial();
        }
      });

    // PAciente registrado, se guarda el tratamiento
    } else {
      guardarTratamiento();
    }

    return false;
  });
// });

function filtroPorDoctor() {
  var id = $(this).val();
  var date = $('#calendar').fullCalendar( 'getDate');
  var _month = date.format('MM-YYYY');

  var url = routeFiltroDoctor.replace('__id', id);

  var posting = $.post( url, { month : _month} );
  posting.done(function( data ) {
    $('#calendar').fullCalendar( 'removeEvents');
    $('#calendar').fullCalendar( 'addEventSource', data);
  });
}

// Se borra la selección del doctor y se carga la agenda completa
function clearFiltroDoctor() {
  event.preventDefault();
  $('input:radio').prop('checked', false);
  var date = $('#calendar').fullCalendar( 'getDate');
  var _month = date.format('MM-YYYY');
  var url = routeFiltroDoctor.replace('__id', 0);

  var posting = $.post( url, { month : _month} );
  posting.done(function( data ) {
    $('#calendar').fullCalendar( 'removeEvents');
    $('#calendar').fullCalendar( 'addEventSource', data);
  });
}

function printBetweenDates() {

    var events = $('#calendar').fullCalendar( 'clientEvents');

    var desde_print = $("#desde").data('date');
    var hasta_print = $("#hasta").data('date');

    var desde = moment(desde_print, "DD/MM/YYYY");
    var hasta = moment(hasta_print, "DD/MM/YYYY");

    var doc = new jsPDF('p', 'pt', 'letter');

    var y = 20;
    var npage = 1;

    doc.setFontSize(10);
    doc.text("Listado de Agenda", 220, y);
    doc.setFontSize(8);
    doc.text("Fecha de impresión: " + moment(new Date()).format("DD/MM/YYYY HH:mm"), 420, y);

    y = y + 10;

    doc.text("Desde: " + desde.format('DD/MM/YYYY') + " hasta " + hasta.format('DD/MM/YYYY'), 420, y);

    y = y + 20;

    doc.text("F. Actuación | Hora inicio | Actuación", 20, y);
    doc.text("Doctor", 500, y);

    y = y + 10;

    doc.text("Tipo actuación", 20, y);

    y = y + 10;

    doc.line(20, y, 580, y);

    y = y + 10;

    var nevents = 0;
    console.log('Desde: ' + desde);
    console.log('Hasta: ' + hasta);
    var url = routePrintBetweenDates,
      data = {
        start_date: desde_print,
        end_date: hasta_print
      };

    ajaxRequest(url, data, 'POST', function(json) {
      if (json.data) {
        var events = json.data.events;
        console.log(events);
        events.forEach(function (event, i) {
            var eventdate = moment(event.fecha);
            if (eventdate.startOf('day').isSameOrAfter(desde) && eventdate.endOf('day').isSameOrBefore(hasta.add(1, 'days'))) {
                nevents = nevents + 1;

                doc.setFontSize(8);

                doc.text(moment(eventdate).format('dddd') + "  " +
                    moment(eventdate).format('DD/MM/YYYY') + "  " +
                    event.hora_inicio + "  " +
                    event.descripcion, 20, y);

                y = y + 10;

                //doc.text(event.tipoActuacion ? event.tipoActuacion : '', 20, y);
                doc.text(event.doctor.name, 420, y);

                y = y + 10;

                doc.text("Paciente: " + event.paciente.nombre, 20, y);
                doc.text("Tipo fichero General", 300, y);
                doc.text("Código: " + event.paciente.codigo, 480, y);

                y = y + 10;

                doc.text("Dirección: " + (event.paciente.direccion ? event.paciente.direccion : ''), 20, y);

                y = y + 10;

                doc.text("Email: " + (event.paciente.email ? event.paciente.email : ''), 20, y);
                doc.text("Tel: " + (event.paciente.telefono ? event.paciente.telefono : ''), 300, y);
                doc.text("Móvil: " + (event.paciente.movil ? event.paciente.movil : ''), 480, y);

                y = y + 10;

                if (y > 600) {
                    doc.line(20, 710, 580, 710);
                    doc.text("Número registros: " + nevents, 400, 720);
                    doc.text("Página: " + npage, 500, 720);
                    doc.addPage();
                    y = 20;
                    npages = npage + 1;
                } else {
                    y = y + 40;
                }
            }
        });
        doc.save('Agenda_' + desde_print + ' - ' + hasta_print + '.pdf');
      }
    });
}
