var rows = 0;
var cambiosEnImportes = false;
var tratamientos = null;// {!! json_encode($tratamientos) !!};
var version = 'light'; // full / ligth
var mode = 'add'; // add / edit
var tratamientoFinanciado = false;
var piezas = null;
var tratamientos = null;
var routeFindPlan = null;

function setupPlan(_rows, _tratamientos, _version, _mode, _financiado, _piezas, _routeFindPlan) {
  rows = _rows;
  tratamientos = _tratamientos;
  version = _version;
  mode = _mode;
  tratamientoFinanciado = _financiado;
  piezas = _piezas;
  routeFindPlan = _routeFindPlan;

  var pendiente = parseFloat($('input[name="pendiente"]').val());
  if (pendiente > 0) {
    $('input[name="totalCobro"]').val(pendiente);
  }

  // Si estamos editando se añade cabecera de la tabla
  if(mode == 'edit' && version != 'invoice') {
    $('#dynamic_field .th-hecho').removeClass('hidden');
  } else {
    $('#dynamic_field .th-hecho').addClass('hidden');
  }

  if (version == 'light') {
    setTableFooter();
  }
}

function addLinea() {
    var newRow =
   ' <tr id="row' + rows + '" class="dynamic-added"> ' +
   '  <input type="hidden" name="realizada_programacion[' + rows + ']" value=""> ' +
   '  <input type="hidden" name="fecha_realizacion_linea[' + rows + ']" value=""> ' +
   '  <input type="hidden" name="cobrada['+ rows +']" value="0"> ' +
   '  <input type="hidden" name="fecha_cobro['+ rows +']" value=""> ';

   if (mode == 'edit' && version != 'invoice') {
      newRow = newRow + '  <td><input class="change_state" id="' + rows + '" type="checkbox" name="lineaRealizada[' + rows + ']"></td>';
   }

   var piezasOptions;

   piezas.forEach(function(pieza, index) {
    piezasOptions = piezasOptions +  '<option value="' + pieza + '">' + pieza + '</option>';
   });

   newRow = newRow +
   '    <td>' +
   '      <select class="form-control select_pieza" name="pieza_linea[' + rows + ']">' +
            piezasOptions +
   '      </select>' +
   '    </td>';


   newRow = newRow +
   '    <td>' +
   '      <div class="col-lg-12">' +
   '       <div class="input-group">' +
   '         <input type="text" name="descripcion_linea[' + rows + ']" placeholder="Introduce una descripción" class="form-control descripcion_linea" required>' +
   '         <span class="input-group-btn">' +
   '           <a class="btn-sm btn-info" name="descripcion_linea_button[' + rows + ']" data-toggle="modal" data-target="#tratamientosModal" onclick="setDescriptionIndex(' + rows + ');">' +
   '             <i class="voyager-double-right"></i>' +
   '           </a>' +
   '         </span>' +
   '       </div>' +
   '      </div>' +
   '    </td>' +
   '    <td><input type="number" name="cantidad_linea[' + rows + ']" class="form-control" value=1 required onchange="updateTotals(' + rows + ')" min=0></td>' +
   '    <td><input type="number" name="precio_linea[' + rows + ']" class="form-control" value=0 required onchange="updateTotals(' + rows + ')" min=0></td>' +
   '    <td><input type="number" name="descuento_linea[' + rows + ']" class="form-control" value=0 required onchange="updateTotals(' + rows + ')" min=0></td>';

   // if (version == 'full') {
   //    newRow = newRow + '    <td><input type="number" name="neto_linea[' + rows + ']" class="form-control" value=0 readonly></td>';
   // }

   newRow = newRow +
   '    <td><input type="number" name="total_linea[' + rows + ']" class="form-control" value=0 readonly min=0></td>';

   if (version != 'invoice') {
     newRow = newRow +
     ' <td>' +
     '    <a href="#" role="button" class="cobrar_linea" id="' + rows + '" name="cobrar_linea[' + rows + ']">' +
     '      <i class="fas fa-2x fa-euro-sign" style="color: #9E9C9A"></i>' +
     '    </a>' +
     ' </td>';
   }

   newRow = newRow +
   ' <td><a name="' + rows + '" class="btn-sm btn-danger btn_remove" role="button"><i class="fas fa-trash"></i></a></td> ' +
   ' </tr>';

   $('#dynamic_field tbody').append(newRow);
}

function setTableFooter() {
  $('#dynamic_field tfoot').empty();

  var footer = ' <tr>';

  if (mode == 'edit') {
    footer = footer + '   <td colspan="2"></td>';
  } else {
    footer = footer + '   <td></td>';
  }

  footer = footer +
  '   <td colspan="3">TOTALES</td>' +
  '   <td><input type="number" name="total_descuento" class="form-control" value=0 readonly></td>';


  if(version == 'full') {
      footer = footer + ' <td><input type="number" name="total_neto" class="form-control" value=0 readonly></td>';
  }

  footer = footer + '   <td><input type="number" name="total" class="form-control" value=0 readonly></td>';

  if (mode == 'edit') {
    footer = footer + '   <td></td>';
  }

  footer = footer +
  '   <td><a class="btn-sm btn-success btn_add" role="button"><i class="fas fa-plus"></i></a></td>' +
  ' </tr>';

  $('#dynamic_field tfoot').append(footer);

}

// Indicador de la descripción seleccionada
var descIndex = 0;
function setDescriptionIndex(index) {
    descIndex = index;
}

// Calcular valores
function updateRow(index) {
  var descuento = parseFloat($('input[name="descuento_linea[' + index + ']"]').val() ? $('input[name="descuento_linea[' + index + ']"]').val() : 0);
  var precio = parseFloat($('input[name="precio_linea[' + index + ']"]').val() ? $('input[name="precio_linea[' + index + ']"]').val() : 0);
  var cantidad = parseFloat($('input[name="cantidad_linea[' + index + ']"]').val() ? $('input[name="cantidad_linea[' + index + ']"]').val() : 0);
  var totalAntes = parseFloat($('input[name="total_linea[' + index + ']"]').val() ? $('input[name="total_linea[' + index + ']"]').val() : 0);
  var lineaRealizada = $('input[name="lineaRealizada[' + index + ']"]').val();

  if (version == "full") {
    var neto = parseFloat( precio * cantidad ).toFixed(2);
    neto = neto - (neto * (descuento / 100));
    neto = parseFloat(neto).toFixed(2);
    $('input[name="neto_linea[' + index + ']"]').val(neto);
  }

  var total = parseFloat(precio * cantidad);
  total = total - (total * (descuento / 100));
  total = parseFloat(total).toFixed(2);

  $('input[name="total_linea[' + index + ']"]').val(total);

  // TODO: Deprecated
  // Si cambia el total y la linea está marcada como hecha. Hay que actualizar el pendienteç
  // console.log(lineaRealizada);
  // if (totalAntes != total && lineaRealizada) {
  //   actualizarImportesTratamiento(total - totalAntes, true);
  // }
}

function updateTotals(index) {
  if (index) {
    updateRow(index);
  }

  var totalDescuento = 0.0;
  var totalTratamiento = 0.0;

  if (version == "full") {
    var totalNeto = 0.0;
  }

  // Actualizar totales
  for (var i = 1; i <= rows; i++) {
    var total = $('input[name="total_linea[' + i + ']"]').val();
    var precio = $('input[name="precio_linea[' + i + ']"]').val();
    var cantidad = $('input[name="cantidad_linea[' + i + ']"]').val();
    var descuento = (precio * cantidad) - total;

    if (version == "full") {
      var neto = $('input[name="neto_linea[' + i + ']"]').val();
    }

    if (total) {
        totalTratamiento += parseFloat(total);
    }
    if (descuento) {
        totalDescuento += parseFloat(descuento);
    }

    if (version == "full") {
      if (neto) {
          totalNeto += parseFloat(neto);
      }
    }
  }

  if (version == 'full') {
    $('input[name="total_neto"]').val(parseFloat(totalNeto).toFixed(2));
  }

  $('input[name="total_descuento"]').val(parseFloat(totalDescuento).toFixed(2));
  $('input[name="total"]').val(parseFloat(totalTratamiento).toFixed(2));

  cambiosEnImportes = true;
  console.log('update totals');
}

function actualizarImportesTratamiento(total, cobrada) {
  var pendiente = parseFloat($('input[name="pendiente"]').val());
  var consumido = parseFloat($('input[name="consumido_financiacion"]').val());
  var financiado = $('input[name="financiado"]' ).prop('checked');

  // ACTUALIZAR SALDO PACIENTE SI EXISTE
  var saldo = parseFloat($('input[name="saldo"]').val() ? $('input[name="saldo"]').val() : 0);

  // Si se ha marcado como cobrada, se suma a pendiente/consumido
  if (cobrada) {
    if (financiado) {
      $('input[name="consumido_financiacion"]').val(consumido + total);
      $('input[name="saldo"]').val(saldo - total);
    } else {
      $('input[name="pendiente"]').val(pendiente + parseFloat(total));
      $('input[name="saldo"]').val(saldo - total);
    }
  // Si se desmarca se resta a pendiente
  } else {
    if (financiado) {
      $('input[name="consumido_financiacion"]').val(consumido - total);
      $('input[name="saldo"]').val(saldo + total);
    } else {
      $('input[name="pendiente"]').val(pendiente - total);
      $('input[name="saldo"]').val(saldo + total);
    }
  }

  if (parseFloat($('input[name="saldo"]').val()) < 0) {
    $('.form-tratamiento').find('.div_saldo').addClass('has-error');
  } else {
    $('.form-tratamiento').find('.div_saldo').removeClass('has-error');
  }

  pendiente = parseFloat($('input[name="pendiente"]').val());

  // Se mete en el importe del cobro el valor de pendiente
  if (pendiente > 0) {
    $('input[name="totalCobro"]').val(pendiente);
  }

  cambiosEnImportes = true;
  console.log('actualizar importes tratamiento');
}

/**
 * Se llama cuando una linea se marca como pagada. Se bloquean los importes para que no se puedan editar.
 */
function bloquearImportesLinea(row) {
  $('input[name="cantidad_linea[' + row + ']"]').attr('readonly', true);
  $('input[name="precio_linea[' + row + ']"]').attr('readonly', true);
  $('input[name="descuento_linea[' + row + ']"]').attr('readonly', true);
  $('a[name="descripcion_linea_button[' + row + ']"').addClass('disabled');
  $('a[name="descripcion_linea_button[' + row + ']"').removeAttr('data-toggle');
  $('input[name="descripcion_linea[' + row + ']"').attr('readonly', true);

  
}

function desbloquearImportesLinea(row) {
  $('input[name="cantidad_linea[' + row + ']"]').attr('readonly', false);
  $('input[name="precio_linea[' + row + ']"]').attr('readonly', false);
  $('input[name="descuento_linea[' + row + ']"]').attr('readonly', false);
  console.log($('#descripcion_linea_button[' + row + ']'));
  $('a[name="descripcion_linea_button[' + row + ']"').removeClass('disabled');
  $('a[name="descripcion_linea_button[' + row + ']"').attr('data-toggle', 'modal');
  $('input[name="descripcion_linea[' + row + ']"').attr('readonly', false);
}

function gestionarCobro(row) {
  var cobrada = $('input[name="cobrada[' + row + ']"]').val();
  var total_linea = parseFloat($('input[name="total_linea[' + row + ']"]') ? $('input[name="total_linea[' + row + ']"]').val() : 0);
  var buttonCobro = $('a[name="cobrar_linea['+ row +']"')
  if (cobrada && cobrada == 1) {
    // Se pasa a no cobrada
    $('input[name="cobrada[' + row + ']"]').val(0);
    $('input[name="fecha_cobro[' + row + ']"]').val("");
    buttonCobro.find('i').css('color', '#9E9C9A');

    actualizarImportesTratamiento(total_linea, false);
    desbloquearImportesLinea(row);

  } else {
    // Se pasa a cobrada
    $('input[name="cobrada[' + row + ']"]').val(1);
    $('input[name="fecha_cobro[' + row + ']"]').val(moment().format('YYYY-MM-DD HH:mm:ss'));
    buttonCobro.find('i').css('color', '#5CB85D');

    actualizarImportesTratamiento(total_linea, true);
    bloquearImportesLinea(row);
  }
}

function actualizarBotonCobro(row, cobrada, fecha_cobro) {
  var buttonCobro = $('a[name="cobrar_linea['+ row +']"')
  if (cobrada && cobrada == 1) {
    // Se marca cobrada
    $('input[name="cobrada[' + row + ']"]').val(1);
    $('input[name="fecha_cobro[' + row + ']"]').val(
      moment(fecha_cobro).format('YYYY-MM-DD HH:mm:ssz'));
    buttonCobro.find('i').css('color', '#5CB85D');
    bloquearImportesLinea(row);
  } else {
    // Se marca no cobrada
    $('input[name="cobrada[' + row + ']"]').val(0);
    $('input[name="fecha_cobro[' + row + ']"]').val("");
    buttonCobro.find('i').css('color', '#9E9C9A');
    desbloquearImportesLinea(row);
  }
}

function loadPlanFromLineas(lineas, programacion) {
  rows = 0;
  $.each(lineas, function( index, value ) {
    rows++;
    addLinea();
    // ' <tr id="row' + rows + '" class="dynamic-added"> ' +
    $('#row'+rows).append('<input type="hidden" name="linea_id['+ rows +']" value="'+ value.id +'">');
    $('input[name="lineaRealizada[' + rows + ']"]' ).prop('checked', value.realizada ? value.realizada : false);
    $('select[name="pieza_linea[' + rows + ']"').val(value.pieza ? value.pieza : 0);

    $('input[name="descripcion_linea[' + rows + ']"').val(value.descripcion);
    $('input[name="cantidad_linea[' + rows + ']"').val(value.cantidad);
    $('input[name="precio_linea[' + rows + ']"').val(value.importe_unitario);
    $('input[name="descuento_linea[' + rows + ']"').val(value.porcentaje_descuento);
    $('input[name="total_linea[' + rows + ']"').val(value.total);
    $('input[name="realizada_programacion[' + rows + ']"').val(value.realizada_programacion_id);
    if(value.fecha_realizacion) {
      $('input[name="fecha_realizacion_linea[' + rows + ']"').val(
        moment(value.fecha_realizacion).format('YYYY-MM-DD HH:mm:ssz'));
    }

    actualizarBotonCobro(rows, value.cobrada == true ? 1 : 0, value.fecha_cobro);

    // Linea programada para esta cita pero todavía no realizada
    if (!value.realizada && programacion.id == value.programacion_id) {
      $('#row' + rows).addClass('warning');

      // Linea realizada en esta cita
    } else if (value.realizada && programacion.id == value.realizada_programacion_id) {
      $('#row' + rows).addClass('info');
    }
  });

  // Actualizar totales en el plan de tratamiento
  updateTotals();
}

function loadPlanFromPredefinido(lineas, plan) {
  $.each(lineas, function( index, value ) {
    rows++;
    addLinea();
    $('#row' + rows).append('<input type="hidden" name="linea_id['+ rows +']">');
    $('input[name="lineaRealizada[' + rows + ']"]' ).prop('checked', false);
    $('select[name="pieza_linea[' + rows + ']"').val(0);

    $('input[name="descripcion_linea[' + rows + ']"').val(value.descripcion);
    $('input[name="cantidad_linea[' + rows + ']"').val(value.cantidad);
    $('input[name="precio_linea[' + rows + ']"').val(value.precio);
    $('input[name="descuento_linea[' + rows + ']"').val(0);
    $('input[name="total_linea[' + rows + ']"').val(value.total);

  });

  updateTotals();
  $('input[name="nombre"]').val(plan.nombre);
}

$(function(){
  // 1. Añadir nueva linea
  $(document).on('click', '.btn_add', function(){
    rows++;
    addLinea();
  });

  // 2. Eliminar linea
  $(document).on('click', '.btn_remove', function(){
    var row = $(this).attr('name');

    var cobrada = $('input[name="cobrada[' + row + ']"]').val();
    if (cobrada == 1) {
      alert('No se puede eliminar un tratamiento cobrado. Avise al administrador para más detalles');
      return;
    }

    // Si la linea estaba realizada se resta de pendiente
    var realizada = $('input[name="lineaRealizada[' + row + ']"]' ).prop('checked');
    var financiado = $('input[name="financiado"]' ).prop('checked');

    if (realizada) {
      var total = parseFloat($('input[name="total_linea[' + row + ']"]').val());

      if (financiado) {
        var consumido = parseFloat($('input[name="consumido_financiacion"]').val());
        $('input[name="consumido_financiacion"]').val(consumido - total);
      } else {
        var pendiente = parseFloat($('input[name="pendiente"]').val());
        $('input[name="pendiente"]').val(pendiente - total);
      }
    }
    $('#row' + row + '').remove();
    updateTotals();
  });

  // 3. Marcar linea como realizada
  $(document).on('change', '.change_state', function(event){
    var row = $( this ).attr('id');
    var total = parseFloat($('input[name="total_linea[' + row + ']"]').val());
    var realizada = $( this ).prop('checked');

    // Actualizar datos realización linea
    if (realizada) {
      // Marcar cita como realizada
      $('input[name="realizada"]').prop('checked', true);
      // Añadir fecha de realización si existe en la página
      if ($('input[name="fecha_realizacion"]').data("DateTimePicker")) {
          $('input[name="fecha_realizacion"]').data("DateTimePicker").date(new Date());
      }

      // Marcar en que programación se ha realizado la linea
      var programacion_id = $('input[name="programacion_id"]').val();
      $('input[name="realizada_programacion[' + row + ']"]').val(programacion_id);
      $('input[name="fecha_realizacion_linea[' + row + ']"]').val(moment().format('YYYY-MM-DD HH:mm:ss'));

    } else {
      $('input[name="realizada_programacion[' + row + ']"]').val('');
      $('input[name="fecha_realizacion_linea[' + row + ']"]').val('');
    }

    // TODO: Deprecated
    // actualizarImportesTratamiento(total, realizada);
  });

  // 4. Marcar financiación
  $(document).on('change', '.checkbox_financiado', function(event){
    var financiado = $( this ).prop('checked');
    if (financiado) {
      $('.cobro_financiacion').removeClass('hidden');
      $('.cobro_regular').addClass('hidden');
    } else {
      $('.cobro_financiacion').addClass('hidden');
      $('.cobro_regular').removeClass('hidden');
    }
  });

  if (mode == 'edit') {
      $('input[name="financiado"]').click(function(){
        return false;
      });
  }

  // 5. Seleccionar tratamiento
  $(document).on('click', '#seleccionarTratamiento', function(){
    var id = $(this).val();
    var tratamiento = tratamientos.find(x => x.id == id);
    var tratamientoDesc = tratamiento.nombre;
    var precio = parseFloat(tratamiento.importe);

    $('input[name="descripcion_linea[' + descIndex + ']"]').val(tratamientoDesc);
    $('input[name="precio_linea[' + descIndex + ']"]').val(precio);

    updateTotals(descIndex);

    $('#tratamientosModal').modal('hide');
  });

  // 6. Marcar linea como cobrada
  $(document).on('click', '.cobrar_linea', function(e){
    e.preventDefault();
    var row = $( this ).attr('id');

    gestionarCobro(row);
  });

  $('.btn-seleccionar-plan').click(function(e){
    var id = $(this).val();

    var url = routeFindPlan + '/' + id;

    ajaxRequest(url, null, 'GET', function(json) {
      if (json.error == false) {
        loadPlanFromPredefinido(json.data.event.lineas, json.data.event);
      }
    })

  });
});
