var routeMovimientos = null;
var routePago = null;
var routeDeleteMovimiento = null;

function setupPagos(_routeMovimientos, _routePago, _routeDeleteMovimiento) {
  routeMovimientos = _routeMovimientos;
  routePago = _routePago;
  routeDeleteMovimiento = _routeDeleteMovimiento;
}

function bucarMovimientos() {
  $('.table-movimientos-caja tbody').empty();
  // Cargar movimientos de caja
  ajaxRequest(routeMovimientos, null, 'GET', function(json) {
    if (!json.error) {
      var movimientos = json.data.movimientos;
      $.each(movimientos, function( index, movimiento ) {
        $('.table-movimientos-caja tbody').append(
          ' <tr id="mov-' + movimiento.id + '"> ' +
          '   <td>' + moment(movimiento.created_at).format('DD/MM/YYYY') + '</td>' +
          '   <td>' + movimiento.concepto + '</td>' +
          '   <td>' + (movimiento.entrada ? movimiento.entrada : 0) + '</td>' +
          '   <td>' + (movimiento.salida ? movimiento.salida : 0) + '</td>' +
          '   <td> ' +
          '     <a class="btn-sm btn-danger borrar-movimiento" name="borrar-movimiento[' + movimiento + ']" role="button" ' +
          '       data-id="' + movimiento.id + '"> ' +
          '       <i class="fas fa-trash"></i> ' +
          '     </a> ' +
          '   </td> ' +
          ' </tr> '
        );
      });
      $('.borrar-movimiento').unbind().click(borrarMovimiento);
    }
  });
}

function activarPagos() {
  $('.alert-pagos-group').addClass('hidden');
  $('.registrar-pago-group').removeClass('hidden');
}

function desactivarPagos() {
  $('.alert-pagos-group').removeClass('hidden');
  $('.registrar-pago-group').addClass('hidden');
}

// Cobro de actuación
function fillCobrarForm(id, total, concepto) {
  var $form = $( '.form-registrar-cobro' );
  $form.find('input[name="totalCobro"]').val(total > 0 ? total : 0);
  $form.find('textarea[name="concepto"]').text(concepto);
  $form.find('input[name="tratamiento_id"]').val(id);
}

function borrarMovimiento() {
  var id = $(this).attr('data-id');

  var url = routeDeleteMovimiento + "/" + id;

  var data = {
    _method: 'DELETE'
  };

  ajaxRequest(url, data, 'POST', function (json) {
    if (json.data) {
      $('#mov-' + id).remove(); 

      var importe = parseFloat(json.data.importe);

      var cobrado = parseFloat($('.form-tratamiento').find('input[name="cobrado"]').val()) - importe;
      var pendiente = parseFloat($('.form-tratamiento').find('input[name="pendiente"]').val()) + importe;
      var saldo = parseFloat($('.form-tratamiento').find('input[name="saldo"]').val()) - importe;

      $('.form-tratamiento').find('input[name="cobrado"]').val(cobrado);
      $('.form-tratamiento').find('input[name="pendiente"]').val(pendiente);
      $('.form-tratamiento').find('input[name="saldo"]').val(saldo);
        
      if (saldo < 0) {
        $('.form-tratamiento').find('.div_saldo').addClass('has-error');
      } else {
        $('.form-tratamiento').find('.div_saldo').removeClass('has-error');
      }

    }
  });
}

// Submit del formulario
$( ".form-registrar-cobro" ).submit(function( event ) {
  event.preventDefault();

  if (cambiosEnImportes) {
    alert("Antes de registrar un cobro, tienes que guardar los cambios en la pestaña 'Tratamientos'");
    return;
  }

  var url = routePago,
    data = $(this).serialize();

  ajaxRequest(url, data, 'POST', function(json) {
    if (!json.error) {
      var movimiento = json.data.movimiento,
        entrada = parseFloat(movimiento.entrada ? movimiento.entrada : 0),
        salida = parseFloat(movimiento.entrada ? movimiento.entrada : 0),
        cobrado = parseFloat(json.data.cobrado ? json.data.cobrado : 0),
        pendiente = parseFloat(json.data.pendiente ? json.data.pendiente : 0),
        saldo = parseFloat(json.data.saldo ? json.data.saldo : 0);

      $( ".form-registrar-cobro" ).find('input[name="totalCobro"]').val(0);
      $('.form-tratamiento').find('input[name="cobrado"]').val(cobrado);
      $('.form-tratamiento').find('input[name="pendiente"]').val(pendiente);
      $('.form-tratamiento').find('input[name="saldo"]').val(saldo);

      if (saldo < 0) {
        $('.form-tratamiento').find('.div_saldo').addClass('has-error');
      } else {
        $('.form-tratamiento').find('.div_saldo').removeClass('has-error');
      }

      $('.table-movimientos-caja tbody').append(
        ' <tr id="mov-' + movimiento.id + '"> ' +
        '   <td>' + moment().format('DD/MM/YYYY HH:MI') + '</td>' +
        '   <td>' + movimiento.concepto + '</td>' +
        '   <td>' + entrada + '</td>' +
        '   <td>' + salida + '</td>' +
        '   <td> ' +
        '     <a class="btn-sm btn-danger borrar-movimiento" name="borrar-movimiento[' + movimiento + ']" role="button" ' +
        '       data-id="' + movimiento.id + '"> ' +
        '       <i class="fas fa-trash"></i> ' +
        '     </a> ' +
        '   </td> ' +
        ' </tr> '
      );
      $('.borrar-movimiento').unbind().click(borrarMovimiento);

      // Actualizar el evento del calendario si estamos en la agenda
      var _idEvent = $('.form-tratamiento').find('input[name="idCalendarEvent"]').val();
      if (_idEvent) {
        var events = $("#calendar").fullCalendar('clientEvents', _idEvent);
        if (events.length > 0 ) {
          console.log('actualizar evento');
          var event = events[0];
          event.cobrado = cobrado;
          event.pendiente = pendiente;
          event.saldo = saldo;

          $('#calendar').fullCalendar('updateEvent', event);
        }
      }
    }
  });
});
