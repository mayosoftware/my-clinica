<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AgendaUpdated implements ShouldBroadcast // Sync Queue
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tratamiento_id;
    public $event_id;

    public function __construct($tratamiento_id, $event_id)
    {
        $this->tratamiento_id = $tratamiento_id;
        $this->event_id = $event_id;
    }

    public function broadcastAs()
    {
        return 'agenda.updated';
    }
    public function broadcastOn()
    {
        return new PrivateChannel('agenda');
    }

    public function broadcastWith()
    {
        return [
            'tratamiento_id'  => $this->tratamiento_id,
            'event_id'        => $this->event_id
        ];
    }
}
