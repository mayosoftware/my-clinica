<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActuacionPaciente extends Model
{
    protected $table = 'actuaciones_paciente';

    use SoftDeletes;
    protected $dates = ['fecha_actuacion', 'fecha_realizacion', 'fecha_cancelacion', 'deleted_at'];

    protected $dateFormat = 'Y-m-d H:i:sO';

    public function paciente()
    {
        return $this->belongsTo('App\Models\Paciente');
    }

    public function tipoActuacion()
    {
        return $this->belongsTo('App\Models\TipoActuacion');
    }

    public function tipoCancelacion()
    {
        return $this->belongsTo('App\Models\TipoCancelacionActuacion');
    }

    public function usuario()
    {
        return $this->belongsTo('App\User');
    }

    public function movimientos()
    {
        return $this->hasMany('App\Models\MovimientosCaja');
    }
}
