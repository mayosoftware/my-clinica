<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoActuacion extends Model
{
    protected $table = 'tipos_actuacion';
    protected $dates = ['created_at', 'deleted_at'];
    protected $dateFormat = 'Y-m-d H:i:sO';
}
