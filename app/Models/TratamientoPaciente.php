<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TratamientoPaciente extends Model
{
  protected $table = 'tratamientos_paciente';
  protected $dates = ['created_at', 'updated_at'];
  protected $dateFormat = 'Y-m-d H:i:sO';

  public function lineas()
  {
      return $this->hasMany('App\Models\TratamientoPacienteLinea')->orderBy('linea');
  }

  public function ficheros()
  {
      return $this->hasMany('App\Models\FicheroTratamientoPaciente');
  }

  public function movimientosCaja()
  {
      return $this->hasMany('App\Models\MovimientosCaja');
  }

  public function factura()
  {
      return $this->belongsTo('App\Models\Contabilidad\Factura');
  }

  public function presupuesto()
  {
      return $this->belongsTo('App\Models\Contabilidad\Presupuesto');
  }

  public function paciente()
  {
      return $this->belongsTo('App\Models\Paciente');
  }

  public function programacion()
  {
      return $this->belongsTo('App\Models\TratamientoPacienteProgramacion', 'programacion_id');
  }

  public function fechaUltimaCita() {
    if ($this->programacion && $this->programacion->fecha <= Carbon::now()) {
      $this->programacion->fecha;

    } else if ($this->lineas) {

      // Excluir citas posteriores a hoy
      $result = $this->lineas->filter(function ($linea, $key) {
        if ($linea->programacion && $linea->programacion->fecha <= Carbon::now()) {
          return $linea->programacion->fecha;
        } else {
          return false;
        }
      })->sortByDesc(function ($linea, $key) {
          if ($linea->programacion) {
            return $linea->programacion->fecha;
          } else {
            return null;
          }
      });

      $linea = $result->values()->firstWhere('programacion_id', '!=', null);
      if ($linea) {
        $fecha = new Carbon($linea->programacion->fecha);
        return $fecha->format('d/m/Y');
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  public function fechaProximaCita() {
    if ($this->programacion && $this->programacion->fecha > Carbon::now()) {
      $this->programacion->fecha;
    } else if ($this->lineas) {

      // Excluir citas posteriores a hoy
      $result = $this->lineas->filter(function ($linea, $key) {
        if ($linea->programacion && $linea->programacion->fecha > Carbon::now()) {
          return $linea->programacion->fecha;
        } else {
          return false;
        }
      })->sortByDesc(function ($linea, $key) {
          if ($linea->programacion) {
            return $linea->programacion->fecha;
          } else {
            return null;
          }
      });

      $linea = $result->values()->firstWhere('programacion_id', '!=', null);
      if ($linea) {
        $fecha = new Carbon($linea->programacion->fecha);
        return $fecha->format('d/m/Y');
      } else {
        return null;
      }
    } else {
      return null;
    }
  }
}
