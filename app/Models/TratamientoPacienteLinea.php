<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class TratamientoPacienteLinea extends Model
{
  protected $table = 'tratamientos_paciente_lineas';
  protected $dates = ['fecha_realizacion', 'created_at', 'updated_at'];
  protected $dateFormat = 'Y-m-d H:i:sO';

  public function tipoIva()
  {
      return $this->belongsTo('App\Models\Contabilidad\TipoIva');
  }

  public function tratamientoPaciente()
  {
      return $this->belongsTo('App\Models\TratamientoPaciente');
  }

  public function programacion()
  {
      return $this->belongsTo('App\Models\TratamientoPacienteProgramacion', 'programacion_id');
  }

  public function fechaInicioCompleta() {
     if ($this->programacion) {
        return $this->programacion->fecha->format('d/m/Y') . ' ' . substr($this->programacion->hora_inicio, 0, 5);
     } else {
       return '';
     }
  }

  public function fechaRealizacionFormat() {
     if ($this->realizada && $this->fecha_realizacion) {
        return $this->fecha_realizacion->format('d/m/Y');
     } else {
       return '';
     }
  }

  public function descripcionComplea() {
    return $this->descripcion . ($this->pieza > 0 ? ' en pieza '.$this->pieza : '');
  }
}
