<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tratamiento extends Model
{
  protected $table = "tratamientos";
  protected $dates = ['created_at', 'updated_at'];
  protected $dateFormat = 'Y-m-d H:i:sO';
}
