<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class HistorialClinicoPaciente extends Model
{
  protected $table = "historial_clinico_paciente";
  
  use SoftDeletes;
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';
}
