<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class _r_Paciente extends Model
{
    protected $table = 'audit._r_pacientes';
    protected $fillable = ['id', 'paciente_id', 'user_id', 'read_at'];
    public $timestamps = false;
}
