<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

use App\Models\Contabilidad\Factura;
use App\Models\Contabilidad\Presupuesto;

class Paciente extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $dateFormat = 'Y-m-d H:i:sO';

    public function tipoPaciente()
    {
        return $this->belongsTo('App\Models\TipoPaciente');
    }

    public function poblacion()
    {
        return $this->belongsTo('App\Models\Poblacion');
    }

    public function tratamientosPaciente()
    {
        return $this->hasMany('App\Models\TratamientoPaciente')->orderBy('created_at');
    }

    public function presupuestos()
    {
        return $this->hasMany('App\Models\Contabilidad\Presupuesto')->orderBy('numero', 'desc');
    }

    public function facturas()
    {
        return $this->hasMany('App\Models\Contabilidad\Factura')->orderBy('numero', 'desc');
    }

    public function historialClinico()
    {
        return $this->belongsToMany('App\Models\HistorialClinico');
    }

    public function nextCodigo() {
        $query = "CAST(codigo AS BIGINT) DESC";
        $lastPaciente = DB::table('pacientes')->whereNotNull('codigo')->orderByRaw($query)->first();
        return $lastPaciente ? $lastPaciente->codigo + 1 : 1;
    }

    public function nextCuentaCont() {
        $lastPaciente = DB::table('pacientes')->latest('id')->first();
        return $lastPaciente ? $lastPaciente->cuenta_contabilidad + 1 : 4300000000;
    }

    public function nextNumeroTratamiento() {
      $tratamiento = TratamientoPaciente::where('paciente_id', '=', $this->id)
      ->orderBy('numero', 'desc')->first();

      if($tratamiento) {
        return $tratamiento->numero + 1;
      } else {
        return 1;
      }
    }

    public function nextIndicePresupuesto() {
      $presupuesto = Presupuesto::where('paciente_id', '=', $this->id)
      ->orderBy('indice', 'desc')->first();

      if($presupuesto) {
        return $presupuesto->indice + 1;
      } else {
        return 1;
      }
    }

    public function nextIndiceFactura() {
      $factura = Factura::where('paciente_id', '=', $this->id)
      ->orderBy('indice', 'desc')->first();

      if($factura) {
        return $factura->indice + 1;
      } else {
        return 1;
      }
    }
}
