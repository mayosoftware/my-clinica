<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class PlanPredefinido extends Model
{
    protected $table = "commons.planes_predefinidos";
    protected $dateFormat = 'Y-m-d H:i:sO';

    public function lineas()
    {
        return $this->hasMany('App\Models\Commons\PlanPredefinidoLinea')->orderBy('linea');
    }
}
