<?php

namespace App\Models\Commons;

use Illuminate\Database\Eloquent\Model;

class PlanPredefinidoLinea extends Model
{
  protected $table = "commons.planes_predefinidos_lineas";
  protected $dateFormat = 'Y-m-d H:i:sO';

  public function planPredefinido()
  {
      return $this->belongsTo('App\Models\Commons\PlanPredefinido');
  }
}
