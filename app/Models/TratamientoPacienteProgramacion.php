<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TratamientoPacienteProgramacion extends Model
{
  protected $table = 'tratamientos_paciente_programaciones';
  protected $dates = ['created_at', 'updated_at', 'fecha', 'fecha_realizacion', 'fecha_cancelacion'];
  protected $dateFormat = 'Y-m-d H:i:sO';

  public function tratamientosPacienteLineas()
  {
      return $this->hasMany('App\Models\TratamientoPacienteLinea', 'programacion_id');
  }

  public function tratamientosPaciente()
  {
      return $this->hasMany('App\Models\TratamientoPaciente', 'programacion_id');
  }

  public function tipoCancelacion()
  {
      return $this->belongsTo('App\Models\TipoCancelacion');
  }

  public function usuario()
  {
      return $this->belongsTo('App\User', 'user_id');
  }

  public function doctor()
  {
      return $this->belongsTo('App\User', 'doctor_id');
  }

  public function paciente() {
    if ($this->tratamientosPacienteLineas->isNotEmpty()) {
      return $this->tratamientosPacienteLineas->first()->tratamientoPaciente->paciente;
    } else if ($this->tratamientosPaciente->isNotEmpty()) {
      return $this->tratamientosPaciente->paciente;
    }
    return null;
  }

  public function descripcion() {
    if ($this->tratamientosPacienteLineas->isNotEmpty()) {
      //dd($this->tratamientosPacienteLineas->count());
      return $this->tratamientosPacienteLineas->first()->descripcion;
    } else if ($this->tratamientosPaciente->isNotEmpty() &&
      $this->tratamientosPaciente->lineas->isNotEmpty()) {
      return $this->tratamientosPaciente->lineas->first()->descripcion;
    }
    return '';
  }

  public function lineas() {
    if ($this->tratamientosPacienteLineas->isNotEmpty()) {
      return $this->tratamientosPacienteLineas->first()->tratamientoPaciente->lineas->sortBy('id');
    } else if ($this->tratamientosPaciente->isNotEmpty() &&
      $this->tratamientosPaciente->lineas->isNotEmpty()) {
      return $this->tratamientosPaciente->lineas->sortBy('id');
    }
    return null;
  }

  public function tratamiento() {
    if ($this->tratamientosPacienteLineas->isNotEmpty()) {
      return $this->tratamientosPacienteLineas->first()->tratamientoPaciente;
    } else if ($this->tratamientosPaciente->isNotEmpty()) {
      return $this->tratamientosPaciente;
    }
    return null;
  }
}
