<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoPaciente extends Model
{
    protected $table = 'tipos_paciente';

    use SoftDeletes;
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    protected $dateFormat = 'Y-m-d H:i:sO';
}
