<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    protected $dateFormat = 'Y-m-d H:i:sO';

    public function comunidadAutonoma()
    {
        return $this->belongsTo('App\Models\ComunidadAutonoma');
    }

    public function poblaciones()
    {
        return $this->hasMany('App\Models\Poblacion');
    }
}
