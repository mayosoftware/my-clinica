<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ComunidadAutonoma extends Model
{
    protected $table = 'comunidades_autonomas';

    protected $dates = ['created_at', 'updated_at'];

    protected $dateFormat = 'Y-m-d H:i:sO';

    public function pais()
    {
        return $this->belongsTo('App\Models\Pais');
    }

    public function provincias()
    {
        return $this->hasMany('App\Models\Provincia');
    }
}
