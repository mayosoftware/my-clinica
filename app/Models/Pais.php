<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Pais extends Model
{
  protected $table = 'paises';

  protected $dates = ['created_at', 'updated_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';

  public function comunidadesAutonomas()
  {
      return $this->hasMany('App\Models\ComunidadAutonoma');
  }
}
