<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class TiposCobro extends Model
{
    protected $table = "tipos_cobro";
    protected $dates = ['created_at', 'updated_at'];
    protected $dateFormat = 'Y-m-d H:i:sO';
}
