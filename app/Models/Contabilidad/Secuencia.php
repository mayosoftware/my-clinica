<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Secuencia extends Model
{
  protected $table = "contabilidad.secuencias";

  use SoftDeletes;
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';

  public function ejercicio()
  {
      return $this->belongsTo('App\Models\Contabilidad\Ejercicio');
  }
}
