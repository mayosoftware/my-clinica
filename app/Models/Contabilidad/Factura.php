<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factura extends Model
{
  protected $table = "contabilidad.facturas";

  use SoftDeletes;
  protected $dates = ['fecha_factura', 'fecha_vencimiento', 'created_at', 'updated_at', 'deleted_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';

  public function paciente()
  {
      return $this->belongsTo('App\Models\Paciente');
  }

  public function lineas()
  {
      return $this->hasMany('App\Models\Contabilidad\LineaFactura');
  }

  public function secuencia()
  {
      return $this->belongsTo('App\Models\Contabilidad\Secuencia');
  }

  public function fechaFormat() {
     if ($this->fecha_factura) {
        return $this->fecha_factura->format('d/m/Y');
     } else {
       return '';
     }
  }
}
