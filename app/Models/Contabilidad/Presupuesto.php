<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\TratamientoPaciente;

class Presupuesto extends Model
{
  protected $table = "contabilidad.presupuestos";

  use SoftDeletes;
  protected $dates = ['fecha_presupuesto', 'fecha_vencimiento', 'created_at', 'updated_at', 'deleted_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';

  // function __construct(TratamientoPaciente $tratamiento){
  //   $this->model = $request->product;
  //   $this->create = new CreateRepository($this->model);
  // }

  public function paciente()
  {
      return $this->belongsTo('App\Models\Paciente');
  }

  public function lineas()
  {
      return $this->hasMany('App\Models\Contabilidad\LineaPresupuesto');
  }

  public function secuencia()
  {
      return $this->belongsTo('App\Models\Contabilidad\Secuencia');
  }

  public function tratamiento()
  {
      return $this->hasOne('App\Models\TratamientoPaciente');
  }

  public function fechaFormat() {
     if ($this->fecha_presupuesto) {
        return $this->fecha_presupuesto->format('d/m/Y');
     } else {
       return '';
     }
  }
}
