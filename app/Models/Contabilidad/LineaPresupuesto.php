<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class LineaPresupuesto extends Model
{
  protected $table = "contabilidad.lineas_presupuesto";

  use SoftDeletes;
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';

  public function presupuesto()
  {
      return $this->belongsTo('App\Models\Contabilidad\Presupuesto');
  }

}
