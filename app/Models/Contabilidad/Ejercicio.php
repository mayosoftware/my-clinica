<?php

namespace App\Models\Contabilidad;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ejercicio extends Model
{
  protected $table = "contabilidad.ejercicios";

  use SoftDeletes;
  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $dateFormat = 'Y-m-d H:i:sO';

  public function secuencias()
  {
      return $this->hasMany('App\Models\Contabilidad\Secuencia');
  }
}
