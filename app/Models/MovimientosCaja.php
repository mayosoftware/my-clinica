<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class MovimientosCaja extends Model
{
    protected $table = "movimientos_caja";
    protected $dates = ['fecha', 'created_at', 'updated_at'];
    protected $dateFormat = 'Y-m-d H:i:sO';

    public function tratamiento()
    {
        return $this->belongsTo('App\Models\TratamientoPaciente', 'tratamiento_paciente_id');
    }

    public function tipoCobro()
    {
        return $this->belongsTo('App\Models\TiposCobro');
    }

    public function fechaCobroFormat() {
       if ($this->fecha) {
          return $this->fecha->format('d/m/Y H:i');
       } else {
         return '';
       }
    }
}
