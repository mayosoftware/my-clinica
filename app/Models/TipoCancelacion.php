<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoCancelacion extends Model
{
    protected $table = 'tipos_cancelacion';
    protected $dates = ['created_at', 'updated_at'];
    protected $dateFormat = 'Y-m-d H:i:sO';
}
