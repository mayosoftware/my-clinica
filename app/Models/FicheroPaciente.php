<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FicheroPaciente extends Model
{
    use SoftDeletes;
    protected $table = 'ficheros_pacientes';

    protected $dates = ['deleted_at'];
    protected $dateFormat = 'Y-m-d H:i:sO';
}
