<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Models\Paciente;
use App\Models\Tratamiento;
use App\Models\TratamientoPaciente;
use App\Models\TratamientoPacienteLinea;
use App\Models\TratamientoPacienteProgramacion;
use App\Models\TipoCancelacion;
use App\Models\Commons\PlanPredefinido;
use App\Models\Festivo;
use App\Models\MovimientosCaja;

use App\Models\Contabilidad\TipoIva;
use App\Models\Contabilidad\Ejercicio;

use App\Http\Controllers\Pusher\PusherController;

use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

use Carbon\Carbon;
use DateTime;
use DateInterval;

class TratamientosPacienteProgramacionesController extends VoyagerBaseController
{

  private $pusherController;

  private $tratamientoPacienteController;
  private $tratamientoPacienteLineaController;

  /**
   * Constructor
   */
  public function __construct()
  {
      $this->pusherController = new PusherController();
      $this->tratamientoPacienteController = new TratamientosPacienteController();
      $this->tratamientoPacienteLineaController = new TratamientosPacienteLineasController();
  }

  private function findEventsCalendar($month, $doctor = null, $programacion_id = null) {
    $start = Carbon::createFromFormat('d-m-Y', '01-' . $month)->startOfMonth();
    $end = Carbon::createFromFormat('d-m-Y', '01-' . $month)->endOfMonth();

    $citas = TratamientoPacienteProgramacion::whereBetween('fecha', [$start, $end]);

    if ($doctor) {
      $citas->where('doctor_id', '=', $doctor);
    }

    if ($programacion_id) {
      $citas->where('id', '=', $programacion_id);
    }

    $events = $citas->get()->map(function ($item) {
      return $this->makeEvent($item);
    });

    return $events;
  }

  /**
   * Crear el evento que se utiliza en el calendario
   * @param  [type] $item [description]
   * @return [type]       [description]
   */
  private function makeEvent($item) {
    if ($item && $item->paciente()) {
      $title = $item->paciente()->nombre . ' - ' . $item->descripcion();
    } else {
      $title = 'Desc' . ' - ' . $item->descripcion();
    }
    $start = $this->addTimeToDate($item->fecha, $item->hora_inicio);
    $end = $this->addTimeToDate($item->fecha, $item->hora_fin);
    $color = $this->getColor($item->realizada, $item->cancelada);
    if ($historialClinico = $item->paciente()) {
      $historialClinico = $item->paciente()->historialClinico->count();
      $mutua = $item->paciente()->mutua;
    } else {
      $historialClinico = null;
      $mutua = null;
    }
    if ($item->paciente() && $item->paciente()->tipoPaciente->nombre == 'Potencial') {
      $pacientePotencial = true;
    } else {
      $pacientePotencial = false;
    }

    $observaciones = $item->observaciones;

    return [
        'title'             => $title,
        'start'             => $start,
        'end'               => $end,
        'color'             => $color,
        'programacion_id'   => $item->id,
        'historialClinico'  => $historialClinico,
        'observaciones'     => $observaciones,
        'mutua'             => $mutua,
        'pacientePotencial'      => $pacientePotencial
      ];
  }

  private function makeEventForShow($cita) {
    $event = $cita->toArray();
    // Añadir información al evento
    $event['tratamiento'] = $cita->tratamiento();
    $event['paciente'] = $cita->paciente();
    $event['descripcion'] = $cita->descripcion();
    $event['doctor'] = $cita->doctor;

    $lineas = $cita->lineas();
    
    // Añadir programación de las lineas que tengan
    if ($lineas) {
      $event['lineas'] = $lineas->map(function ($linea) {
        $linea['fecha_realizacion_format'] = $linea->fechaRealizacionFormat();
        if ($linea->programado) {
          $linea['fecha_inicio_completa'] = $linea->fechaInicioCompleta();
        }
        return $linea;
      })->sort(function ($a, $b) {
          if (!$a->fecha_realizacion) {
            return 1;
          } else if (!$b || ($a->fecha_realizacion && !$b->fecha_realizacion)) {
            return -1;
          }

          return $a->fecha_realizacion < $b->fecha_realizacion ? 0 : 1;
      })->values()->all();
    }    
    return $event;
  }

  /**
   * Función que se consulta por ajax para cargar la info de una programación
   * cuando se pulsa un evento sobre el calendario
   * @param  [type] $id [description]
   * @return [type]     [description]
   */
  public function ajaxLoadProgramacion($id) {
    $cita = TratamientoPacienteProgramacion::find($id);

    if (!$cita->tratamiento()) {
      return response()->json([
        'event' => null,
        'message' => 'Esta cita no está asociada a ningún tratamiento',
        'alert-type' => 'error',
      ]);
    }

    $event = $this->makeEventForShow($cita);

    return response()->json([
      'event' => $event,
      'alert-type' => 'success',
    ]);
  }

  public function ajaxGetEventsByMonth($month) {
    if (auth()->user()->role_id == 2) {
      $events = $this->findEventsCalendar($month, auth()->user()->id);
    } else {
      $events = $this->findEventsCalendar($month, null);
    }

    return response()->json([
      'events' => $events,
      'alert-type' => 'success',
    ]);
  }

  public function ajaxGetProgramacionesDoctor(Request $request, $id) {
    $month = $request->month;
    $events = $this->findEventsCalendar($month, $id);

    return response()->json([
      'events' => $events,
      'alert-type' => 'success',
    ]);
  }

  // Se utiliza al recibir un evento de actualización de agenda
  public function ajaxGetProgramacion(Request $request, $id) {
    $month = $request->month;
    $events = $this->findEventsCalendar($month, null, $id);

    return response()->json([
      'events' => $events,
      'alert-type' => 'success',
    ]);
  }

  public function ajaxGetProgramacionesEntreFechas(Request $request) {
    $start = Carbon::createFromFormat('d/m/Y', $request->start_date);
    $end = Carbon::createFromFormat('d/m/Y', $request->end_date);

    $citas = TratamientoPacienteProgramacion::whereBetween('fecha', [$start, $end])->get();

    if ($citas->isEmpty()) {
      return response()->json([
        'events' => null,
        'message' => 'No hay citas entre las fechas seleccionadas',
        'alert-type' => 'error',
      ]);
    }

    $events = $citas->map(function ($item) {
      return $this->makeEventForShow($item);
    });

    return response()->json([
      'events' => $events,
      'alert-type' => 'success',
    ]);
  }

  public function getProgramacionesDesde($fecha) {
    $citas = TratamientoPacienteProgramacion::whereDate('fecha', '>=', new Carbon($fecha))->get();

    $events = $citas->map(function ($item) {
      return $this->makeEvent($item);
    });

    return response()->json([
      'alert-type' => 'success',
      'events' => $events
    ]);
  }

  public function getProgramaciones($fecha) {
    $citas = TratamientoPacienteProgramacion::whereDate('fecha', '=', new Carbon($fecha))->get();

    $events = $citas->map(function ($item) {
      return $this->makeEvent($item);
    });

    return response()->json([
      'alert-type' => 'success',
      'events' => $events
    ]);
  }

  public function getSiguienteProgramacionPaciente($id) {
      // TODO: Mejorar esto
      $tratamiento = DB::select(
        'SELECT
        	p.*
        FROM tratamientos_paciente as tp
        JOIN tratamientos_paciente_lineas as l
        on tp.id = l.tratamiento_paciente_id
        JOIN tratamientos_paciente_programaciones as p
        ON (tp.programacion_conjunta AND tp.programacion_id = p.id)
        	OR
            (NOT tp.programacion_conjunta AND l.programacion_id = p.id)
        where tp.paciente_id = ?
        AND p.fecha >= ?
        ORDER BY p.fecha, p.hora_inicio, p.hora_fin',
        [
          $id,
          Carbon::now()
        ]
      );

      if (count($tratamiento) > 0) {
        return response()->json([
          'alert-type' => 'success',
          'fecha'     => $tratamiento[0]->fecha,
          'hora'      => $tratamiento[0]->hora_inicio
        ]);
      } else {
        return response()->json([
          'message'    => "No tiene siguientes tratamientos programados",
          'alert-type' => 'error',
        ]);
      }
  }

  public function index(Request $request) {

    // Programaciones del mes actual
    $month = Carbon::now()->format('m-Y');

    // Tipos iva
    $tiposIva = TipoIva::all();
    // Tratamientos
    $tratamientos = Tratamiento::all();
    // Ejercicios abiertos
    $ejercicios = Ejercicio::where('cerrado', '=', false)->get();
    // Doctores
    $doctores = \App\User::where('role_id', '=', 2) // Doctores
           ->orderBy('name', 'desc')
           ->get();

    // Tipos cancelaciones
    $tiposCancelaciones = TipoCancelacion::all();

    if (auth()->user()->role_id == 2) {
      $events = $this->findEventsCalendar($month, auth()->user()->id);
    } else {
      $events = $this->findEventsCalendar($month);
    }

    $piezas = $this->piezas;

    $planes = PlanPredefinido::all();

    $festivos = Festivo::all();

    $data = compact(
      'tiposIva',
      'tratamientos',
      'ejercicios',
      'doctores',
      'events',
      'tiposCancelaciones',
      'piezas',
      'planes',
      'festivos'
    );

    return $this->commonIndex($request, $data, null);
  }

  public function update(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Compatibility with Model binding.
      $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

      $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

      // Check permission
      $this->authorize('edit', $data);

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->ajax()) {
          // $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
          // TODO: Intentar hacer que se devuelva desde el html el formato correcto
          $data->fecha = $this->stringToDatetime($request->fecha);
          $data->hora_inicio = $request->hora_inicio;
          $data->hora_fin = $request->hora_fin;
          $data->tiempo = $request->tiempo;
          $data->doctor_id = $request->doctor_id;

          $data->save();

          $tratamiento = $data->tratamiento();

          return redirect()
              ->route("voyager.tratamientos-paciente.edit", ["tratamiento" => $tratamiento->id])
              ->with([
                  'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                  'alert-type' => 'success',
              ]);
      }
  }

  public function destroy(Request $request, $id) {
    $programacion = TratamientoPacienteProgramacion::find($id);

    $tratamiento = $programacion->tratamiento();
    $descripcion = $programacion->descripcion();
    $paciente = $programacion->paciente();
    $lineas = $programacion->tratamientosPacienteLineas;

    if ($lineas == null) {
      $tratamiento->programacion_conjunta = false;
      $tratamiento->programacion_id = null;
      $tratamiento->save();

    } else {
      $linea = $lineas->first();
      $linea->programado = false;
      $linea->programacion_id = null;
      $linea->save();
    }

    $programacion::destroy($id);

    return redirect()
        ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $tratamiento->id])
        ->with([
            'message'    => "Programación eliminada",
            'alert-type' => 'success',
        ]);
  }

  public function storeFromModal(Request $request)
  {
      \Log::debug($request);
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      $tratamientosType = Voyager::model('DataType')->where('slug', '=', 'tratamientos-paciente')->first();

      $this->authorize('add', app($tratamientosType->model_name));

      $paciente_id = $request->paciente_id;
      $paciente = Paciente::find($paciente_id);

      $numero_tratamiento = $paciente->nextNumeroTratamiento();

      $tratamiento = new TratamientoPaciente();
      $tratamiento->paciente_id = $paciente_id;
      $tratamiento->programacion_conjunta = false;
      $tratamiento->observaciones = $request->observaciones;
      $tratamiento->total = $request->total;
      // En el modal que tiene menos opciones se pone iva del 0% por defecto
      $tratamiento->total_iva = 0;
      // En el modal no hay descuento
      $tratamiento->total_descuento = 0;
      $tratamiento->numero_lineas = 1;
      $tratamiento->numero = $numero_tratamiento;

      // Validar tratamiento
      $val = $this->validateBread($tratamiento->getAttributes(), $tratamientosType->addRows);

      if ($val->fails()) {
          \Log::debug($val->messages());
          \Log::debug($request);
          return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->has('_validate')) {
        $tratamiento->save();
      }

      $programacion = new TratamientoPacienteProgramacion();

      // TODO: Ver como tratar el formato de las fechas igual en todas partes
      // Lo ideal es separar entre formato 'display' e 'internal'
      $programacion->fecha = $this->stringToDatetime($request->fecha);
      $programacion->hora_inicio = $request->hora_inicio;
      $programacion->hora_fin = $request->hora_fin;
      $programacion->tiempo = $request->tiempo;
      $programacion->observaciones = $request->observaciones;
      $programacion->doctor_id = $request->doctor_id;
      $programacion->user_id = auth()->user()->id;

      // Validar programación
      $val = $this->validateBread($programacion->getAttributes(), $dataType->addRows);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->has('_validate')) {
        $programacion->save();
      }

      $numero_linea = 0;
      foreach ($request->descripcion_linea as $key => $value) {
        $numero_linea++;
        $linea = new TratamientoPacienteLinea();
        $linea->tratamiento_paciente_id = $tratamiento->id;
        $linea->linea = $numero_linea;
        $linea->descripcion = $value;
        $linea->pieza = $request->pieza_linea[$key];
        $linea->cantidad = $request->cantidad_linea[$key];
        $linea->importe_unitario = $request->precio_linea[$key] ?? 0;
        $linea->total = $request->total_linea[$key];
        $linea->total_iva = 0;
        $linea->porcentaje_descuento = $request->descuento_linea[$key];
        $linea->programacion_id = $programacion->id;
        $linea->programado = true;
        if (!$request->has('_validate')) {
          $linea->save();
        }
      }

      // Crear evento
      $event = $this->makeEvent($programacion);

      $this->pusherController->enviarPusherAgenda('agenda.updated', $programacion->id, $request->socketId);

      // Actualización correcta
      return response()->json([
        'message'    => "Tratamiento " .__('voyager::generic.successfully_added_new'),
        'alert-type' => 'success',
        'event'      => $event,
        'success' => true,
        'fecha_calendar' => $programacion->fecha
      ]);
  }

  public function updateFromModal(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      $programacion = TratamientoPacienteProgramacion::find($id);

      // TODO: Ver como tratar el formato de las fechas igual en todas partes
      // Lo ideal es separar entre formato 'display' e 'internal'
      if($request->has('fecha')) {
        $programacion->fecha = $this->stringToDatetime($request->fecha);
      }
      $programacion->hora_inicio = $request->hora_inicio;
      $programacion->hora_fin = $request->hora_fin;
      $programacion->tiempo = $request->tiempo;
      $programacion->observaciones = $request->observaciones;
      $programacion->realizada = $request->realizada ?? false;
      $programacion->fecha_realizacion = $this->stringToDatetime($request->fecha_realizacion);

      // Usuario admin
      if (auth()->user()->role_id != 2) {
        $programacion->doctor_id = $request->doctor_id;
        $programacion->cancelada = $request->cancelada ?? false;
        $programacion->fecha_cancelacion = $this->stringToDatetime($request->fecha_cancelacion);
        $programacion->tipo_cancelacion_id = $request->tipo_cancelacion;
      }

      $programacion->user_id = auth()->user()->id;

      // Validar programación
      $val = $this->validateBread($programacion->getAttributes(), $dataType->addRows);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->has('_validate')) {
        $programacion->save();

        $tratamiento = $programacion->tratamiento();

        $tratamiento->total_financiacion = $request->total_financiacion;
        $tratamiento->consumido_financiacion = $request->consumido_financiacion;
        $tratamiento->cobrado = $request->cobrado;
        $tratamiento->pendiente = $request->pendiente;
        $tratamiento->observaciones = $request->observaciones;

        $tratamiento->save();       

        // Actualizar saldo paciente
        $tratamiento->paciente->saldo = $request->saldo ? $request->saldo : 0;
        $tratamiento->paciente->mutua = $request->mutua;
        $tratamiento->paciente->numero_poliza = $request->numero_poliza;
        $tratamiento->paciente->tipo_paciente_id = $request->tipo_paciente ?? 1;
        $tratamiento->paciente->save();

        $this->actualizarLineas($request, $tratamiento);

        $this->pusherController->enviarPusherAgenda('agenda.updated', $programacion->id, $request->socketId, $tratamiento->id);
      }

      // Crear evento
      $event = $this->makeEvent($programacion);

      // Actualización correcta
      return response()->json([
        'message'    => "Tratamiento " . __('voyager::generic.successfully_updated'),
        'alert-type' => 'success',
        'event'      => $event,
        'fecha_calendar' => $programacion->fecha
      ]);
  }

  private function actualizarLineas(Request $request, $tratamiento) {
    // Ver si hay lineas que borrar
    $lineasGuardar = $request->linea_id;
    $lineasBorrar = $tratamiento->lineas->filter(function($item) use ($lineasGuardar) {
        return !in_array($item->id, $lineasGuardar);
    });
    
    foreach ($lineasBorrar as $linea) {
      // Comprobar si la linea está cobrada
      if ($linea->cobrada) {
        // // Crear un movimiento de caja de salida para dejar registro
        // $movimiento = new MovimientosCaja();
        // $movimiento->tratamiento_paciente_id = $tratamiento->id;
      
        // $movimiento->salida = abs($linea->total);
        // $movimiento->tipo_cobro_id = 1;
        // $movimiento->concepto = 'Devolución línea: ' . ($linea->pieza ? 'Pieza '. $linea->pieza .' - ' : ''). $linea->descripcion;
        // $movimiento->save();

        // $tratamiento->cobrado -= $linea->total;
        // $tratamiento->save();
        // // // Actualizar saldo paciente
        // // $paciente = Paciente::find($tratamiento->paciente->id);
        // // $paciente->saldo += $linea->total;
        // // $paciente->save();
      } else {
        // Se quita de pendiente
        $tratamiento->pendiente -= $linea->total;
        $tratamiento->save();
      }
      TratamientoPacienteLinea::destroy($linea->id);
    }

    $numero_linea = 0;
    foreach ($request->descripcion_linea as $key => $value) {
        $numero_linea++;
        // Si existe la linea se modifica
        if ( array_key_exists($key, $request->linea_id) ) {
          $linea = TratamientoPacienteLinea::find($request->linea_id[$key]);

        // Si no existe se crea
        } else {
          $linea = new TratamientoPacienteLinea();
          $linea->tratamiento_paciente_id = $tratamiento->id;
        }

        $linea->linea = $numero_linea;
        $linea->descripcion = $value;
        $linea->pieza = $request->pieza_linea[$key];
        $linea->zona = 0;
        // Versión light del plan de tratamiento
        // $linea->tipo_iva_id = $request->tipo_iva_linea[$key];
        // // $linea->total_iva = $request->total_linea[$key] - $request->neto_linea[$key];
        $linea->cantidad = $request->cantidad_linea[$key];
        $linea->importe_unitario = $request->precio_linea[$key] ?? 0;
        $linea->total = $request->total_linea[$key];
        $linea->porcentaje_descuento = $request->descuento_linea[$key];
        $linea->realizada = $request->lineaRealizada[$key] ?? false;

        $linea->realizada_programacion_id = $request->realizada_programacion[$key] ?? null;
        $linea->fecha_realizacion = $request->fecha_realizacion_linea[$key] ?
          $this->stringToDatetime($request->fecha_realizacion_linea[$key], 'Y-m-d H:i:s') : null;

        $linea->cobrada = $request->cobrada[$key] ?? false;
        $linea->fecha_cobro = $request->fecha_cobro[$key] ?
          $this->stringToDatetime($request->fecha_cobro[$key], 'Y-m-d H:i:s') : null;

        $linea->save();
    }
    $tratamiento->numero_lineas = $numero_linea;
    $tratamiento->save();
  }

  public function ajaxDestroy($id) {
    $programacion = TratamientoPacienteProgramacion::find($id);

    $tratamiento = $programacion->tratamiento();
    $descripcion = $programacion->descripcion();
    $paciente = $programacion->paciente();
    $lineas = $programacion->tratamientosPacienteLineas;

    if ($lineas == null) {
      $tratamiento->programacion_conjunta = false;
      $tratamiento->programacion_id = null;
      $tratamiento->save();

    } else {
      $linea = $lineas->first();
      $linea->programado = false;
      $linea->programacion_id = null;
      $linea->save();
    }

    $programacion::destroy($id);

    $this->pusherController->enviarPusherAgenda('agenda.deleted', $programacion->id, null);

    return response()->json([
      'message'    => "Cita eliminada",
      'alert-type' => 'success'
    ]);
  }

  public function buscarTratamientos(Request $request)
  {
      $term = $request->term;

      $results = array();

      $queries = Tratamiento::where('nombre', 'ILIKE', '%'.$term.'%')
      ->orWhere('descripcion', 'ILIKE', '%'.$term.'%')
      ->take(10)->get();

      foreach ($queries as $query) {
          $results[] = [ 'id' => $query->id, 'value' => $query->descripcion, 'importe' => $query->importe ];
      }

      if (!$results) {
        $results[] = [ 'value' => 'No hay resultados'];
        return response()->json($results);
      } else {
        return response()->json($results);
      }
  }

  public function ajaxCrearProgramacionLinea(Request $request, $id) {

    $programacion = new TratamientoPacienteProgramacion();
    $programacion->doctor_id = $request->doctor_id;
    $programacion->fecha = $this->stringToDatetime($request->fecha);
    $programacion->hora_inicio = $request->hora_inicio;
    $programacion->hora_fin = $request->hora_fin;
    $programacion->tiempo = $request->tiempo;
    $programacion->user_id = auth()->user()->id;

    $programacion->save();

    $linea = TratamientoPacienteLinea::find($id);
    $linea->programacion_id = $programacion->id;
    $linea->programado = true;

    $linea->save();

    return response()->json([
      'message'    => "Cita registrada",
      'alert-type' => 'success',
      'linea_id'  => $linea->id,
      'programacion_id' => $programacion->id,
      'fecha_inicio'    => $linea->fechaInicioCompleta()
    ]);
  }

  public function ajaxEliminarProgramacionLinea($id) {
    // Buscar linea
    $linea = $this->tratamientoPacienteLineaController->buscarLineaPorProgramacion($id);
    if ($linea) {
      $programacion_deleted = $linea->programacion_id;

      $linea->programacion_id = null;
      $linea->programado = false;

      $linea->save();
    }

    $programacion = TratamientoPacienteProgramacion::find($id);
    if ($programacion) {
        $programacion->delete();
    }

    return response()->json([
      'message'    => "Cita eliminada",
      'alert-type' => 'success',
      'linea_id'  => $linea ? $linea->id : null,
      'programacion_deleted' => $programacion_deleted ?? 0
    ]);
  }
}
