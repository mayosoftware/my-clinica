<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use TCG\Voyager\Facades\Voyager;

use App\Models\MovimientosCaja;

use Carbon\Carbon;

class MovimientosCajaController extends VoyagerBaseController
{

    public function index(Request $request) {

      switch($request->submitbutton) {
        case 'print':
          return $this->imprimirMovimientos($request);
          break;

        default:
          $slug = 'movimientos-caja';
          $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

          // Check permission
          $this->authorize('browse', app($dataType->model_name));

          \Log::debug($request);
          $dataTypeContent = $this->applyFilters($request);

          if (!empty($dataTypeContent)) {
            $totalEntradaEfectivo = $dataTypeContent->reduce(function ($carry, $item) {
              return ($carry ?? 0) + ($item->tipo_cobro_id == 1 ? $item->entrada : 0);
            });
            $totalEntradaTarjeta = $dataTypeContent->reduce(function ($carry, $item) {
              return ($carry ?? 0) + ($item->tipo_cobro_id == 2 ? $item->entrada : 0);
            });
            $totalSalidaEfectivo = $dataTypeContent->reduce(function ($carry, $item) {
              return ($carry ?? 0) + ($item->tipo_cobro_id == 1 ? $item->salida : 0);
            });
            $totalSalidaTarjeta = $dataTypeContent->reduce(function ($carry, $item) {
              return ($carry ?? 0) + ($item->tipo_cobro_id == 2 ? $item->salida : 0);
            });  
          } else {
            $totalEntradaEfectivo = 0;
            $totalEntradaTarjeta = 0;
            $totalSalidaEfectivo = 0;
            $totalSalidaTarjeta = 0;
          }

          // $dataTypeContent = MovimientosCaja::whereDate('fecha', '=', date('Y-m-d'))
          //   ->get();

          $isServerSide = false;
          $isModelTranslatable = null;
          $search = null;
          $orderBy = null;
          $sortOrder = null;
          $searchable = null;
          $filter = $request->filter;

          // No mostrar los botones de acción
          $showActions = true;

          $view = 'voyager::movimientos-caja.browse';

          return Voyager::view($view, compact(
              'dataType',
              'dataTypeContent',
              'isModelTranslatable',
              'search',
              'orderBy',
              'sortOrder',
              'searchable',
              'isServerSide',
              'showActions',
              'filter',
              'totalEntradaEfectivo',
              'totalEntradaTarjeta',
              'totalSalidaEfectivo',
              'totalSalidaTarjeta'
          ));
          break;
      }
    }

    public function destroy(Request $request, $id) {
      $movimiento = MovimientosCaja::find($id);
      $paciente = $movimiento->tratamiento->paciente;

      // Actualizar saldo del paciente
      if ($movimiento->entrada > 0) {
        $paciente->saldo -= $movimiento->entrada;
      } else if ($movimiento->salida > 0) {
        $paciente->saldo += $movimiento->salida;
      }
      $paciente->save();

      // Actualizar tratamiento
      $tratamiento = $movimiento->tratamiento;
      $tratamiento->pendiente += $movimiento->entrada;
      $tratamiento->cobrado -= $movimiento->entrada;

      $tratamiento->save();

      if ($request->ajax()) {
        $m = MovimientosCaja::find($id);
        $importe = $m->entrada ? $m->entrada : $m->salida;
        $m->delete();

        return response()->json([
            'message'    => "Operación correcta",
            'alert-type' => 'success',
            'importe'  => $importe
        ]);
      }

      return parent::destroy($request, $id);
    }

    public function storeFromTratamiento($tratamientoId,
      $tipoCobro, $totalCobro, $concepto) {

      $movimiento = new MovimientosCaja();
      $movimiento->tratamiento_paciente_id = $tratamientoId;

      if ($totalCobro > 0) {
        $movimiento->entrada = abs($totalCobro);
      } else {
        $movimiento->salida = abs($totalCobro);
      }
      $movimiento->tipo_cobro_id = $tipoCobro;
      $movimiento->concepto = $concepto;

      $movimiento->save();

      return $movimiento;
    }

    public function ajaxMovimientosCajaTratamiento($id) {
        $movimientos = MovimientosCaja::where('tratamiento_paciente_id', '=', $id)->get();
        return response()->json([
          'alert-type'    => 'success',
          'movimientos'   => $movimientos
        ]);
    }

    protected function makeDataSet($value) {
      return array(
        '',
        $value->fecha->format('d/m/Y'),
        $value->tratamiento->paciente->nombre,
        $value->concepto,
        $value->entrada,
        $value->salida,
        $value->tipoCobro->nombre,
        ''
      );
    }

    protected function applyFilters(Request $request) {
      $filter = $request->filter;
      $start = $request->start;
      $end = $request->end;

      $startDate = $start ? Carbon::createFromFormat('d/m/Y',  $start)->startOfDay() : null;
      $endDate = $end ? Carbon::createFromFormat('d/m/Y',  $end)->endOfDay() : null;

      $movimientos = [];
      // 1. Movimientos de Hoy
      if ($filter == 1) {
        $movimientos = MovimientosCaja::whereDate('fecha', '=', date('Y-m-d'))
          ->get();
      // 2. Todos los movimientos
      } else if ($filter == 2) {
        $movimientos = MovimientosCaja::all();
      // 3. Movimientos entre fechas
    } else if ($filter == 3 && $startDate && $endDate) {
        $movimientos = MovimientosCaja::whereBetween('fecha', [$startDate, $endDate])
          ->get();
      }

      return $movimientos;
    }

    public function filter(Request $request) {
      \Log::debug($request);
      $movimientos = $this->applyFilters($request);

      // Crear dataSet
      $events = [];
      if ($movimientos && $movimientos->count()) {
          foreach ($movimientos as $key => $value) {
              $events[] = $this->makeDataSet($value);
          }
      }

      $data = ['movimientos'   => $events];

      return $this->commonIndex($request, $data, null);
    }

    public function imprimirMovimientos(Request $request) {
      $data = $this->applyFilters($request);

      $date = date('d-m-Y H:i:s');

      $filter = $request->filter;

      $msgRango = "";
      if ($filter == 1) {
        $msgRango = "Movimientos de hoy: " . date('d-m-Y');
      } else if ($filter == 2) {
        $msgRango = "Todos los movimientos";
      } else if ($filter == 3) {
        $msgRango = "Movimientos entre " . $request->start . " y " . $request->end;
      }

      // Totales de entrada y salida
      $totalEntradas = $data->sum('entrada');
      $totalSalidas = $data->sum('salida');

      $totalEfectivo = $data->where('tipo_cobro_id', '=', '1')->sum('entrada');

      $totalTarjeta = $data->where('tipo_cobro_id', '=', '2')->sum('entrada');

      $totalFinanciacion = $data->where('tipo_cobro_id', '=', '3')->sum('entrada');

      $view =  \View::make('templates.pdf.movimientos-caja',
        compact('data',
        'date',
        'msgRango',
        'totalEntradas',
        'totalSalidas',
        'totalEfectivo',
        'totalTarjeta',
        'totalFinanciacion'
        ))
        ->render();

      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('invoice');
    }
}
