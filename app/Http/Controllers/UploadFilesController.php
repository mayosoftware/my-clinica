<?php

namespace App\Http\Controllers;

use App\Models\FicheroPaciente;
use App\Models\FicheroActuacion;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use League\Flysystem\Plugin\ListWith;
use TCG\Voyager\Facades\Voyager;

class UploadFilesController extends VoyagerMediaController
{
    /** @var string */
    private $filesystem;

    /** @var string */
    private $directory = '';

    public function __construct()
    {
        $this->filesystem = config('voyager.storage.disk');
    }

    private function createFolder($new_folder)
    {
        if (Storage::disk($this->filesystem)->exists($new_folder)) {
            return true;
        } elseif (Storage::disk($this->filesystem)->makeDirectory($new_folder)) {
            return true;
        } else {
            return false;
        }
    }

    public function files(Request $request)
    {
        $folder = $request->folder;

        if ($folder == '/') {
            $folder = '';
        }

        $dir = $this->directory.$folder;

        $items = $this->getFiles($dir);

        // Si la carpeta no existe, se crea
        if (count($items) === 0) {
            $result = $this->createFolder($folder);
        }

        return response()->json([
          'name'          => 'files',
          'type'          => 'folder',
          'path'          => $dir,
          'folder'        => $folder,
          'items'         => $items,
          'last_modified' => 'asdf',
      ]);
    }

    private function getFiles($dir)
    {
        $files = [];
        $storage = Storage::disk($this->filesystem)->addPlugin(new ListWith());
        $storageItems = $storage->listWith(['mimetype'], $dir);

        foreach ($storageItems as $item) {
            if ($item['type'] == 'dir') {
                $files[] = [
                  'name'          => $item['basename'],
                  'type'          => 'folder',
                  'path'          => Storage::disk($this->filesystem)->url($item['path']),
                  'items'         => '',
                  'last_modified' => '',
              ];
            } else {
                if (empty(pathinfo($item['path'], PATHINFO_FILENAME)) && !config('voyager.hidden_files')) {
                    continue;
                }

                // Se obtiene una url temporal en s3
                $temporaryUrl = Storage::disk($this->filesystem)->temporaryUrl(
                $item['path'],
                now()->addMinutes(5)
              );

                $files[] = [
                  'name'          => $item['basename'],
                  'type'          => isset($item['mimetype']) ? $item['mimetype'] : 'file',
                  'path'          => $temporaryUrl,
                  'size'          => $item['size'],
                  'last_modified' => $item['timestamp'],
              ];
            }
        }

        return $files;
    }

    public function upload(Request $request)
    {
        try {
            $realPath = Storage::disk($this->filesystem)->getDriver()->getAdapter()->getPathPrefix();

            $allowedImageMimeTypes = [
              'image/jpeg',
              'image/png',
              'image/gif',
              'image/bmp',
              'image/svg+xml',
          ];
            $file = $request->file->store($request->upload_path, $this->filesystem);

            // Guardar fichero en BD
            $className = $request->className;
            $id = $request->id;
            $filename = $request->filename;

            if ($className === 'Paciente') {
                $fichero = new FicheroPaciente();
                $fichero->paciente_id = $id;
                $fichero->ruta = $file;
                $fichero->nombre = $filename;

                $fichero->save();
            } else {
                $fichero = new FicheroActuacion();
                $fichero->actuacion_paciente_id = $id;
                $fichero->ruta = $file;
                $fichero->nombre = $filename;

                $fichero->save();
            }

            $success = true;
            $message = __('voyager::media.success_uploaded_file');
            $path = preg_replace('/^public\//', '', $file);
        } catch (Exception $e) {
            \Log::error($e);
            $success = false;
            $message = $e->getMessage();
            $path = '';
        }

        return response()->json(compact('success', 'message', 'path'));
    }

    public function delete(Request $request)
    {
        $folderLocation = $request->folder_location;
        $fileFolder = $request->file_folder;
        $type = $request->type;
        $success = true;
        $error = '';

        $className = $request->className;
        $id = $request->id;

        $directory = $request->directory;

        if (is_array($folderLocation)) {
            $folderLocation = rtrim(implode('/', $folderLocation), '/');
        }

        // $location = "{$this->directory}/{$folderLocation}";
        // $fileFolder = "{$location}/{$fileFolder}";

        $fileFolder = $directory . '/' . $fileFolder;

        \Log::debug($fileFolder);
        \Log::debug($className);

        if ($type == 'folder') {
            if (!Storage::disk($this->filesystem)->deleteDirectory($fileFolder)) {
                $error = __('voyager::media.error_deleting_folder');
                $success = false;
            }
        } elseif (!Storage::disk($this->filesystem)->delete($fileFolder)) {
            $error = __('voyager::media.error_deleting_file');
            $success = false;
        } else {
          if ($className === 'Paciente') {
              FicheroPaciente::where('ruta', 'ilike', $fileFolder)
              ->delete();
          } elseif ($className === 'ActuacionPaciente') {
              FicheroActuacion::where('ruta', 'ilike', $fileFolder)
              ->delete();
          }
        }

        return compact('success', 'error');
    }
}
