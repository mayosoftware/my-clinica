<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;

use App\Models\Paciente;
use App\Models\ActuacionPaciente;
use App\Models\TratamientoPacienteProgramacion;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class EtiquetasController extends VoyagerBaseController
{
    public function index(Request $request) {

      switch($request->submitbutton) {
        case 'print':
          return $this->print($request);
          break;

        default:
          // dd('etiquetas');
          $slug = 'pacientes';

          // GET THE DataType based on the slug
          $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

          // Check permission
          $this->authorize('browse', app($dataType->model_name));

          // Buscar
          $dataTypeContent = $this->applyFilters($request);

          $isServerSide = false;
          $isModelTranslatable = null;
          $search = null;
          $orderBy = null;
          $sortOrder = null;
          $searchable = null;

          $start = $request->start;
          $end = $request->end;

          $birthStart = $request->birthStart;
          $birthEnd = $request->birthEnd;

          // No mostrar los botones de acción
          $showActions = true;

          $view = 'voyager::etiquetas.browse';

          return Voyager::view($view, compact(
              'dataType',
              'dataTypeContent',
              'isModelTranslatable',
              'search',
              'orderBy',
              'sortOrder',
              'searchable',
              'isServerSide',
              'start',
              'end',
              'birthStart',
              'birthEnd',
              'showActions'
          ));
        }
    }

    protected function makeDataSet($value) {
      return array(
        '',
        $value->id,
        $value->nombre,
        $value->tipoPaciente->nombre,
        $value->nombre,
        $value->fecha_nacimiento,
        $value->poblacion ? $value->poblacion->nombre : '',
        $value->email,
        $value->creation_at,
        $value->saldo,
        $value->domicilio,
        ''
      );
    }

    protected function applyFilters(Request $request) {
      $filter = $request->filter;
      $start = $request->start;
      $end = $request->end;
      $birthStart = $request->birthStart;
      $birthEnd = $request->birthEnd;

      $startDate = $start ? Carbon::createFromFormat('d/m/Y',  $start)->startOfDay() : null;
      $endDate = $end ? Carbon::createFromFormat('d/m/Y',  $end)->endOfDay() : null;
      $birthStartDate = $birthStart ? Carbon::createFromFormat('d/m/Y',  $birthStart)->startOfDay() : null;
      $birthEndDate = $birthEnd ? Carbon::createFromFormat('d/m/Y',  $birthEnd)->endOfDay() : null;

      $pacientes = [];
      if ($startDate || $endDate || $birthStartDate || $birthEndDate) {
        $pacientes = $this->findPacientesEntre($startDate, $endDate, $birthStartDate, $birthEndDate);
      }

      return $pacientes;
    }

    /**
     * Buscar pacientes que tengan citas entre las fechas
     * [findPacientesTratamientosEntre description]
     * @param  [type] $startDate [description]
     * @param  [type] $endDate   [description]
     * @return [type]            [description]
     */
    private function findPacientesEntre($startDate, $endDate, $birthStartDate, $birthEndDate) {
      $citas = TratamientoPacienteProgramacion::whereBetween('fecha', [$startDate, $endDate])->get();

      // Buscar pacientes cuya última cita esté entre las fechas seleccionadas
      $pacientes = DB::select('SELECT
                                id,
                                tipo_paciente_id,
                                mutua,
                                nif,
                                nombre,
                                codigo,
                                telefono,
                                movil,
                                fax,
                                email,
                                direccion,
                                poblacion,
                                provincia,
                                fecha_nacimiento,
                                notas,
                                motivo_consulta,
                                sintomas,
                                created_at,
                                updated_at,
                                deleted_at,
                                created_by,
                                updated_by,
                                saldo,
                                cuenta_contabilidad,
                                numero_poliza,
                                codigo_postal
                            FROM pacientes_ultima_cita_entre(:_start, :_end, :_birthStartDate, :_birthEndDate)',
          [
            '_start'            => $startDate,
            '_end'              => $endDate,
            '_birthStartDate'   => $birthStartDate,
            '_birthEndDate'     => $birthEndDate
          ]
      );

      return $pacientes;
    }

    public function filter(Request $request) {

      $pacientes = $this->applyFilters($request);

      // Crear dataSet
      $events = [];
      if ($pacientes && $pacientes->count()) {
          foreach ($pacientes as $key => $value) {
              $events[] = $this->makeDataSet($value);
          }
      }

      return response()->json([
        'etiquetas'   => $events
      ]);
    }

    public function print(Request $request) {
      $data = $this->applyFilters($request);

      $date = date('d-m-Y H:i:s');

      $filter = $request->filter;

      $view =  \View::make('templates.pdf.etiquetas',
        compact('data'))
        ->render();

      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('invoice');
    }

}
