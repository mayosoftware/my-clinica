<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\ActuacionPaciente;
use App\Models\TipoActuacion;
use App\Models\TipoCancelacionActuacion;
use App\Models\Paciente;
use App\Models\Tratamiento;
use App\Models\MovimientosCaja;
use App\Models\Contabilidad\Factura;
use App\Models\Contabilidad\LineaFactura;
use App\Models\Contabilidad\Ejercicio;
use App\Models\Contabilidad\Secuencia;
use App\Models\Contabilidad\TipoIva;

use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

use Carbon\Carbon;
use DateTime;
use DateInterval;

class ActuacionesPacienteController extends VoyagerBaseController
{

    protected function makeCalendarEvent($value) {
      return array(
        'title' => ($value->tipoActuacion ? $value->tipoActuacion->nombre . ' - ' : ""). $value->paciente->nombre. ' '. $value->paciente->apellido.' - '.$value->descripcion,
        'start' => $this->addTimeToDate($value->fecha_actuacion, $value->hora_inicio),
        'end' => $this->addTimeToDate($value->fecha_actuacion, $value->hora_fin),
        'color' => $this->getColor($value->realizada, $value->cancelada),
        'descripcion' => $value->descripcion,
        'nombrePaciente' => $value->paciente->nombre,
        'idPaciente' => $value->paciente->id,
        'saldo' => $value->paciente->saldo ?? 0,
        'fechaActuacion' => $value->fecha_actuacion->format('d/m/Y'),
        'horaInicio' => $value->hora_inicio,
        'horaFin' => $value->hora_fin,
        'tiempo' => $value->tiempo,
        'idActuacion' => $value->id,
        'realizada' => $value->realizada,
        'fechaRealizacion' => $value->fecha_realizacion ? $value->fecha_realizacion->format('d/m/Y') : '',
        'cancelada' => $value->cancelada,
        'fechaCancelacion' => $value->fecha_cancelacion ? $value->fecha_cancelacion->format('d/m/Y') : '',
        'notas' => $value->notas,
        'idTipoActuacion' => $value->tipoActuacion ? $value->tipoActuacion->id : "",
        'tipActuacion' => $value->tipoActuacion ? $value->tipoActuacion->nombre : "",
        'idTipoCancelacion' => $value->tipoCancelacion ? $value->tipoCancelacion->id : "",
        'tipoPaciente' => $value->paciente->tipoPaciente->nombre ?? "",
        'direccion' => $value->paciente->direccion ?? "",
        'telefono' => $value->paciente->telefono ?? "",
        'movil' => $value->paciente->movil ?? "",
        'email' => $value->paciente->email ?? "",
        'codigoPaciente' => $value->paciente->codigo,
        'nombreDoctor' => $value->usuario->name,
        'usuario_id' => $value->usuario_id,
        'precio' => $value->precio,
        'cantidad' => $value->cantidad,
        'total' => $value->total,
        'cobrado' => $value->cobrado,
        'historialClinico' => count($value->paciente->historialClinico)
      );
    }

    public function index(Request $request)
    {
      // TODO: Ver como extender del Controller Base y llamar a la función super

        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);

        // GET THE DataType based on the slug
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('browse', app($dataType->model_name));

        // Se obtienen las actuaciones y se parsean con el formato del calendario
        $events = [];

        // Comprobar si el rol del usuario tiene permisos para buscar otros Doctores
        $browseUsers = Voyager::can('browse_users');

        if (!$browseUsers) {
            // Buscar solamente sus actuaciones
            $id = auth()->user()->id;
            $data = ActuacionPaciente::where('usuario_id', $id)
              ->orderBy('fecha_actuacion')
              ->get();

            $doctores = [auth()->user()];
        } else {
            // Buscar todas las actuaciones
            $data = ActuacionPaciente::all();
            // Lista de doctores
            $doctores = \App\User::where('role_id', '!=', 3) // Doctores
                   ->orderBy('name', 'desc')
                   ->get();
        }

        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = $this->makeCalendarEvent($value);
            }
        }

        $getter = $dataType->server_side ? 'paginate' : 'get';

        $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
        $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
        $orderBy = $request->get('order_by');
        $sortOrder = $request->get('sort_order', null);

        // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);
            $query = $model::select('*');

            $relationships = $this->getRelationships($dataType);

            // If a column has a relationship associated with it, we do not want to show that field
            $this->removeRelationshipField($dataType, 'browse');

            if ($search->value && $search->key && $search->filter) {
                $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
                $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
                $query->where($search->key, $search_filter, $search_value);
            }

            if ($orderBy && in_array($orderBy, $dataType->fields())) {
                $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';
                $dataTypeContent = call_user_func([
                    $query->with($relationships)->orderBy($orderBy, $querySortOrder),
                    $getter,
                ]);
            } elseif ($model->timestamps) {
                $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
            } else {
                $dataTypeContent = call_user_func([$query->with($relationships)->orderBy($model->getKeyName(), 'DESC'), $getter]);
            }

            // Replace relationships' keys for labels and create READ links if a slug is provided.
            $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
        } else {
            // If Model doesn't exist, get data from table name
            $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
            $model = false;
        }

        // Check if BREAD is Translatable
        if (($isModelTranslatable = is_bread_translatable($model))) {
            $dataTypeContent->load('translations');
        }

        // Check if server side pagination is enabled
        $isServerSide = isset($dataType->server_side) && $dataType->server_side;

        $view = 'voyager::bread.browse';

        if (view()->exists("voyager::$slug.browse")) {
            $view = "voyager::$slug.browse";
        }

        // Tipos de actuaciones
        $tiposActuaciones = TipoActuacion::all();

        // Tipos cancelaciones
        $tiposCancelaciones = TipoCancelacionActuacion::all();

        // Tipos iva
        $tiposIva = TipoIva::all();

        // Ejercicios abiertos
        $ejercicios = Ejercicio::where('cerrado', '=', false)->get();

        // Calcular nuevo número de factura
        $ejercicioActual = $ejercicios->first();

        // Secuencia activa del tipo factura
        $secuencia = $ejercicioActual->secuencias->where('tipo_secuencia_id', '=', 2)
        ->first();

        $numeroFactura = $this->calcularNumeroFactura($ejercicioActual, $secuencia);

        // Pacientes
        $pacientes = Paciente::all();

        // Tratamientos
        $tratamientos = Tratamiento::all();

        return Voyager::view($view, compact(
            'dataType',
            'dataTypeContent',
            'isModelTranslatable',
            'search',
            'orderBy',
            'sortOrder',
            'searchable',
            'isServerSide',
            'events',
            'tiposActuaciones',
            'tiposCancelaciones',
            'doctores',
            'pacientes',
            'tiposIva',
            'ejercicios',
            'numeroFactura',
            'tratamientos'
        ));
    }

    public function getActuacionesDoctor($id)
    {
        // Check permission
        $this->authorize('browse', app('App\Models\ActuacionPaciente'));
        if ($id == 0) {
          $actuaciones = ActuacionPaciente::all();
        } else {
          $actuaciones = ActuacionPaciente::where('usuario_id', $id)
            ->orderBy('fecha_actuacion')
            ->get();
        }

        $events = [];
        if ($actuaciones->count()) {
            foreach ($actuaciones as $value) {
                $events[] = $this->makeCalendarEvent($value);
            }
        }

        return response()->json($events);
    }

    public function cobrarActuacion(Request $request) {
      $id = $request->id;
      $tipoCobro = $request->tipoCobro;

      $actuacion = ActuacionPaciente::find($id);

      if ($actuacion->cobrado > 0) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "La actuación ya estaba cobrada",
                'alert-type' => 'error',
            ]);
      }

      // Si la actuación no tiene cobro, se continua
      $totalCobro = $request->totalCobro;

      $movimiento = new MovimientosCaja();
      $movimiento->actuacion_paciente_id = $id;
      if ($totalCobro > 0) {
        $movimiento->entrada = abs($totalCobro);
      } else {
        $movimiento->salida = abs($totalCobro);
      }
      $movimiento->tipo_cobro_id = $tipoCobro;
      $movimiento->concepto = $actuacion->descripcion;

      $movimiento->save();

      $actuacion->cobrado = $totalCobro;
      $actuacion->save();

      return redirect()
          ->route("voyager.agenda.index")
          ->with([
              'message'    => "Cobro registrado",
              'alert-type' => 'success',
          ]);

      // return response()->json([
      //   'message'    => __('voyager::generic.successfully_created')." cobro de actuación",
      //   'alert-type' => 'success'
      // ]);
    }

    public function facturarActuacion(Request $request) {
      $id = $request->id;
      $tipoCobro = $request->tipoCobro;

      $actuacion = ActuacionPaciente::find($id);

      if (!$actuacion->cobrado || $actuacion->cobrado == 0) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "La actuación todavía no se ha cobrado",
                'alert-type' => 'error',
            ]);
      }

      if ($actuacion->facturada) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "La actuación ya ha sido facturada",
                'alert-type' => 'error',
            ]);
      }

      // Obtener el ejercicio seleccionado y comprobar que no está cerrado
      $ejercicio = Ejercicio::find($request->ejercicio);

      if ($ejercicio->cerrado) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "El ejercicio " . $ejercicio->codigo . " está marcado como cerrado",
                'alert-type' => 'error',
            ]);
      }


      $tipoIva = TipoIva::find($request->tipoIva);

      $secuencia = Secuencia::where([
        ['ejercicio_id', '=', $ejercicio->id],
        ['tipo_secuencia_id', '=', 2] // Secuencia de las facturas
      ])->first();

      // Calcular número de factura
      $numero_factura = $this->calcularNumeroFactura($ejercicio, $secuencia);

      $total = $actuacion->cobrado;

      $factura = new Factura();
      $factura->numero = $numero_factura;
      $factura->secuencia_id = $secuencia->id;
      $factura->codigo_paciente = $actuacion->paciente->codigo;
      $factura->paciente_id = $actuacion->paciente->id;
      $factura->nombre_paciente = $actuacion->paciente->nombre;
      $factura->cif = $actuacion->paciente->cif;
      $factura->direccion = $actuacion->paciente->direccion;
      $factura->ciudad = $actuacion->paciente->poblacion ? $actuacion->paciente->poblacion->nombre : null;
      $factura->provincia = $actuacion->paciente->poblacion ? $actuacion->paciente->poblacion->provincia->nombre : null;
      $factura->pais = $actuacion->paciente->poblacion ? $actuacion->paciente->poblacion->provincia->comunidadAutonoma->pais->nombre : null;
      // $factura->codigo_postal = $actuacion->paciente()->;
      $factura->telefono = $actuacion->paciente->telefono;
      $factura->email = $actuacion->paciente->email;
      $factura->fecha_factura = Carbon::now();
      $factura->fecha_vencimiento = Carbon::now()->addMonths(1);
      $factura->pagada = true;
      $factura->anulada = false;
      $factura->observaciones = $request->observaciones;
      $factura->total = $actuacion->cobrado;
      $factura->total_iva = $actuacion->cobrado - ($actuacion->cobrado / 1.21);
      // $factura->total_irpf = ;
      $factura->total_descuento = $actuacion->total_descuento;
      $factura->user_id = $id = auth()->user()->id;

      $factura->save();

      $lineaFactura = new LineaFactura();
      $lineaFactura->factura_id = $factura->id;
      $lineaFactura->linea = 1;
      $lineaFactura->descripcion = $actuacion->descripcion;
      // TODO: Dejar elegir tipo de iva. Ahora se pone 21%
      $lineaFactura->tipo_iva_id = $tipoIva->id;
      $lineaFactura->cantidad = $actuacion->cantidad;
      $lineaFactura->importe_unitario = $actuacion->precio;
      $lineaFactura->total = $actuacion->cobrado;
      $lineaFactura->total_iva = $this->calcularImporteIva($tipoIva, $total);
      $lineaFactura->total_irpf = 0;
      $lineaFactura->total_descuento = 0;

      $lineaFactura->save();

      $secuencia->valor_siguiente += 1;
      $secuencia->save();

      $actuacion->facturada = true;
      $actuacion->save();

      return redirect()
          ->route("voyager.agenda.index")
          ->with([
              'message'    => "Factura registrada",
              'alert-type' => 'success',
          ]);
    }

    public function storeFromModal(Request $request)
    {
        $slug = "agenda"; //$this->getSlug($request);

        // Comprobar permisos
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('add', app($dataType->model_name));
        $actuacion = new ActuacionPaciente();

        $paciente = $request->paciente;
        $doctor = $request->doctor;
        $tipoActuacion = $request->tipoActuacion;
        $descripcion = $request->descripcion;
        $fechaActuacion = $request->fechaActuacion;
        // Se añaden los segundos para facilitar la conversión
        $horaInicio = $request->horaInicio . ':00';
        $horaFin = $request->horaFin . ':00';
        $tiempo = $request->tiempo;
        $realizada = $request->realizada == "true";
        $fechaRealizacion = $request->fechaRealizacion;
        $cancelada = $request->cancelada == "true";
        $fechaCancelacion = $request->fechaCancelacion;
        $tipoCancelacion = $request->tipoCancelacion;
        $notas = $request->notas;

        $actuacion->paciente_id = $paciente;
        $actuacion->tipo_actuacion_id = empty($tipoActuacion) ? null : $tipoActuacion;
        $actuacion->descripcion = $descripcion;
        $actuacion->fecha_actuacion = $this->stringToDatetime($fechaActuacion);
        $actuacion->hora_inicio = $horaInicio;
        $actuacion->hora_fin = $horaFin;
        $actuacion->tiempo = $tiempo;
        $actuacion->realizada = $realizada;
        if ($realizada) {
            $actuacion->fecha_realizacion = $this->stringToDatetime($fechaRealizacion);
        } else {
          $actuacion->fecha_realizacion = null;
        }
        $actuacion->cancelada = $cancelada;
        if ($cancelada) {
            $actuacion->fecha_cancelacion = $this->stringToDatetime($fechaCancelacion);
            $actuacion->tipo_cancelacion_id = empty($tipoCancelacion) ? null : $tipoCancelacion;
        } else {
          $actuacion->fecha_cancelacion = null;
          $actuacion->tipo_cancelacion_id = null;
        }
        $actuacion->notas = empty($notas) ? null : $notas;

        $actuacion->precio = $request->precio;
        $actuacion->cantidad = $request->cantidad;
        $actuacion->total = $request->total;

        $actuacion->usuario_id = $doctor;

        // Validate fields with ajax
        $val = $this->validateBread($actuacion->getAttributes(), $dataType->editRows, $slug);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        $actuacion->save();

        // Crear evento
        $event = $this->makeCalendarEvent($actuacion);

        // Actualización correcta
        return response()->json([
          'message'    => __('voyager::generic.successfully_created')." {$dataType->display_name_singular}",
          'alert-type' => 'success',
          'event'      => $event
        ]);
    }

    public function updateFromModal(Request $request, $id)
    {
        $slug = "agenda"; //$this->getSlug($request);

        \Log::debug('update actuación');
        \Log::debug($request);
        // Comprobar permisos
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('edit', app($dataType->model_name));

        // Buscar actuación para proceder a la edición
        $actuacion = ActuacionPaciente::find($id);

        $doctor = $request->doctor;
        $tipoActuacion = $request->tipoActuacion;
        $descripcion = $request->descripcion;
        $fechaActuacion = $request->fechaActuacion;
        // Se añaden los segundos para facilitar la conversión
        $horaInicio = $request->horaInicio . ':00';
        $horaFin = $request->horaFin . ':00';
        $tiempo = $request->tiempo;
        $realizada = $request->realizada == "true";
        $fechaRealizacion = $request->fechaRealizacion;
        $cancelada = $request->cancelada == "true";
        $fechaCancelacion = $request->fechaCancelacion;
        $tipoCancelacion = $request->tipoCancelacion;
        $notas = $request->notas;

        $actuacion->tipo_actuacion_id = empty($tipoActuacion) ? null : $tipoActuacion;
        $actuacion->descripcion = $descripcion;
        $actuacion->fecha_actuacion = $this->stringToDatetime($fechaActuacion);
        $actuacion->hora_inicio = $horaInicio;
        $actuacion->hora_fin = $horaFin;
        $actuacion->tiempo = $tiempo;
        $actuacion->realizada = $realizada;
        if ($realizada) {
            $actuacion->fecha_realizacion = $this->stringToDatetime($fechaRealizacion);
        } else {
          $actuacion->fecha_realizacion = null;
        }
        $actuacion->cancelada = $cancelada;
        if ($cancelada) {
            $actuacion->fecha_cancelacion = $this->stringToDatetime($fechaCancelacion);
            $actuacion->tipo_cancelacion_id = empty($tipoCancelacion) ? null : $tipoCancelacion;
        } else {
          $actuacion->fecha_cancelacion = null;
          $actuacion->tipo_cancelacion_id = null;
        }
        $actuacion->notas = empty($notas) ? null : $notas;
        $actuacion->usuario_id = $doctor;

        $actuacion->precio = $request->precio;
        $actuacion->cantidad = $request->cantidad;
        $actuacion->total = $request->total;

        // Validate fields with ajax
        $val = $this->validateBread($actuacion->getAttributes(), $dataType->editRows, $slug, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        $actuacion->save();

        // Crear evento
        $event = $this->makeCalendarEvent($actuacion);

        // Actualización correcta
        return response()->json([
          'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
          'alert-type' => 'success',
          'event'      => $event
        ]);
    }

    // Cargar pantalla de edición de la actuación
    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $relationships = $this->getRelationships($dataType);

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->with($relationships)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details = json_decode($row->details);
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        $paciente_id = $dataTypeContent->paciente_id;
        // Ficheros
        // TODO: Crear definiciones globales
        $filesDirectory = 'pacientes/' . $paciente_id . '/actuaciones/' . $id . '/ficheros';

        $title = "Ficheros de la actuacion: " . $dataTypeContent->nombre . ' - ' . $dataTypeContent->paciente->nombre;
        // Tipos iva
        $tiposIva = TipoIva::all();

        // Ejercicios abiertos
        $ejercicios = Ejercicio::where('cerrado', '=', false)->get();

        // Calcular nuevo número de factura
        $ejercicioActual = $ejercicios->first();

        // Secuencia activa del tipo factura
        $secuencia = $ejercicioActual->secuencias->where('tipo_secuencia_id', '=', 2)
        ->first();

        $numeroFactura = $this->calcularNumeroFactura($ejercicioActual, $secuencia);

        // Pacientes
        $pacientes = Paciente::all();

        // Tratamientos
        $tratamientos = Tratamiento::all();

        return Voyager::view($view, compact('dataType',
          'dataTypeContent',
          'isModelTranslatable',
          'title',
          'filesDirectory',
          'tiposIva',
          'ejercicios',
          'numeroFactura',
          'tratamientos'
        ));
    }
}
