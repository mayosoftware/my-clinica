<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Paciente;

class DashboardController extends VoyagerController
{
    public function searchPacienteFreeText(Request $request)
    {
        $term = $request->term;

        $results = array();

        $queries = Paciente::where('nombre', 'ILIKE', '%'.$term.'%')
        ->orWhere('email', 'ILIKE', '%'.$term.'%')
        ->orWhere('codigo', 'ILIKE', '%'.$term.'%')
        ->take(10)->get();

        foreach ($queries as $query) {
            $results[] = [ 'id' => $query->id, 'value' => 'Código: ' . $query->codigo . ' - ' . $query->nombre, 'saldo' => $query->saldo ];
        }

        if (!$results) {
          $results[] = [ 'value' => 'No hay resultados'];
          return response()->json($results);
        } else {
          return response()->json($results);
        }
    }
}
