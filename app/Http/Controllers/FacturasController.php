<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Carbon\Carbon;
use mikehaertl\wkhtmlto\Pdf;

use App\Models\Contabilidad\Factura;
use App\Models\Paciente;
use App\Models\Tratamiento;
use App\Models\Contabilidad\Secuencia;
use App\Models\Contabilidad\TipoIva;
use App\Models\Contabilidad\Ejercicio;
use App\Models\Contabilidad\LineaFactura;
use App\Models\Commons\PlanPredefinido;

class FacturasController extends VoyagerBaseController
{

  private $lineasFacturaController;

  /**
   * Constructor
   */
  public function __construct()
  {
      $this->lineasFacturaController = new LineasFacturaController();
  }

  public function index(Request $request) {
    $data = compact(
      'showActions',
      'printAction'
    );

    return $this->commonIndex($request, $data, null);
  }

  public function create(Request $request)
  {
      // Tipos iva
      $tiposIva = TipoIva::all();

      // Ejercicio
      $year = Carbon::now()->year;
      $ejercicio = Ejercicio::where([
        ['codigo', '=', $year],
        ['cerrado', false]
      ])->first();

      // TODO: Sino existe ejercicio para el año, se crea

      if ($ejercicio) {
        $secuencia = Secuencia::where([
          ['ejercicio_id', '=', $ejercicio->id],
          ['tipo_secuencia_id', '=', 2] // Secuencia de los facturas
        ])->first();
      }

      // Calcular número de factura
      $siguiente_numero_factura = $this->calcularNumeroFactura($ejercicio, $secuencia);

      // Tratamientos
      $tratamientos = Tratamiento::all();

      // Planes predefinidos
      $planes = PlanPredefinido::all();

      $data = [
        'tiposIva' => $tiposIva,
        'tratamientos' => $tratamientos,
        'planes'  => $planes,
        'piezas' => $this->piezas,
        'ejercicio' => $ejercicio->id,
        'siguiente_numero_factura' => $siguiente_numero_factura
      ];

      return $this->commonCreate($request, $data, null);
  }

  public function edit(Request $request, $id)
  {
      // Paciente
      $factura = Factura::find($id);
      // Tipos iva
      $tiposIva = TipoIva::all();
      // Tratamientos
      $tratamientos = Tratamiento::all();
      // Ejercicios abiertos
      $ejercicios = Ejercicio::where('cerrado', '=', false)->get();

      $pacienteNombreHeader = $factura->paciente->nombre;
      $pacienteCodigoHeader = $factura->paciente->codigo;

      // Planes predefinidos
      $planes = PlanPredefinido::all();

      $data = [
        'tiposIva' => $tiposIva,
        'paciente' => $factura->paciente,
        'tratamientos' => $tratamientos,
        'planes'  => $planes,
        'pacienteNombreHeader' => $pacienteNombreHeader,
        'pacienteCodigoHeader' => $pacienteCodigoHeader,
        'piezas' => $this->piezas,
        'ejercicios' => $ejercicios
      ];

      return $this->commonEdit($request, $id, $data);
  }


  public function store(Request $request)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->addRows);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      $facturaDuplicated = Factura::where('numero', '=', $request->numero)->get();

      if ($facturaDuplicated->count() > 0) {
        $message = [
                'message'    => "El número de factura ya existe",
                'alert-type' => 'error',
            ];

        return redirect()->route("voyager.{$dataType->slug}.create")->with($message);
      }

      // Si no hay lineas, se muestra un error
      if (!$request->descripcion_linea) {
        $message = [
                'message'    => "Se debe indicar al menos un tratamiento",
                'alert-type' => 'error',
            ];
        return redirect()->route("voyager.{$dataType->slug}.create")->with($message);
      }

      if (!$request->has('_validate')) {
          $this->makeFactura($request);

          if ($request->ajax()) {
              return response()->json(['success' => true, 'data' => $data]);
          }

          return redirect()
              ->route("voyager.{$dataType->slug}.index")
              ->with([
                      'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                      'alert-type' => 'success',
                  ]);
      }
  }

  public function update(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Compatibility with Model binding.
      $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

      $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

      // Check permission
      $this->authorize('edit', $data);

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      $facturaDuplicated = Factura::where([
            ['numero', '=', $request->numero],
            ['id', '!=', $id]
        ])->get();

      if ($facturaDuplicated->count() > 0) {
        $message = [
                'message'    => "El número de factura ya existe",
                'alert-type' => 'error',
            ];
        // dd('duplicado');
        return redirect()->route("voyager.facturas.edit", ['factura' => $id])->with($message);
      }

      // Si no hay lineas, se muestra un error
      if (!$request->descripcion_linea) {
        $message = [
                'message'    => "Se debe indicar al menos un tratamiento",
                'alert-type' => 'error',
            ];

        return redirect()->route("voyager.facturas.edit", ['factura' => $id])->with($message);
      }

      if (!$request->ajax()) {
          $this->makeFactura($request, $id);

          return redirect()
              ->route("voyager.facturas.edit", ['factura' => $id])
              ->with([
                  'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                  'alert-type' => 'success',
              ]);
      }
  }

  /**
   * Función que parsea los datos para llamar a la función que almacena la factura
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  private function makeFactura(Request $request, $id = null) {
    // Obtener el ejercicio seleccionado y comprobar que no está cerrado
    $ejercicio = Ejercicio::find($request->ejercicio);

    $tipoIva = 1; //TipoIva::find($request->tipoIva);

    $paciente = Paciente::find($request->paciente_id);    

    $observaciones = $request->observaciones;
    $total = $request->total;
    $totalIva = 0; //$request->totalIvaFactura;
    $totalDescuento = $request->total_descuento;
    $numero_factura = $request->numero;
    $nombre = $request->nombre;

    return $this->storeFactura($request, $ejercicio, $tipoIva, $paciente, $observaciones,
      $total, $totalIva, $totalDescuento, $numero_factura, $nombre, $id);
  }

  public function storeFactura($request, $ejercicio, $tipoIva, $paciente, $observaciones,
    $total, $totalIva, $totalDescuento, $numero_factura = null, $nombre, $id) {
      // Obtener el ejercicio seleccionado y comprobar que no está cerrado
      if ($ejercicio->cerrado) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "El ejercicio " . $ejercicio->codigo . " está marcado como cerrado",
                'alert-type' => 'error',
            ]);
      }

      $secuencia = Secuencia::where([
        ['ejercicio_id', '=', $ejercicio->id],
        ['tipo_secuencia_id', '=', 2] // Secuencia de los facturas
      ])->first();

      // Calcular número de factura
      \Log::debug('numero: ' . $numero_factura);
      if ($numero_factura == null) {
        $numero_factura = $this->calcularNumeroFactura($ejercicio, $secuencia, true);
      }

      if ($id == null) {
        $factura = new Factura();
        $factura->indice = $paciente->nextIndiceFactura();
      } else {
        $factura = Factura::find($id);
      }

      $factura->nombre = $nombre;
      $factura->numero = $numero_factura;
      $factura->secuencia_id = $secuencia->id;
      $factura->codigo_paciente = $paciente->codigo;
      $factura->paciente_id = $paciente->id;
      $factura->nombre_paciente = $paciente->nombre;
      $factura->cif = $paciente->cif;
      $factura->direccion = $paciente->direccion;
      $factura->ciudad = $paciente->poblacion ? $paciente->poblacion->nombre : null;
      $factura->provincia = $paciente->poblacion ? $paciente->poblacion->provincia->nombre : null;
      $factura->pais = $paciente->poblacion ? $paciente->poblacion->provincia->comunidadAutonoma->pais->nombre : null;
      // $factura->codigo_postal = $actuacion->paciente()->;
      $factura->telefono = $paciente->telefono;
      $factura->email = $paciente->email;
      $factura->fecha_factura = Carbon::now();
      $factura->fecha_vencimiento = Carbon::now()->addMonths(1);
      $factura->anulada = false;
      $factura->observaciones = $observaciones;
      $factura->total = $total;
      $factura->total_iva = $totalIva;
      // $factura->total_irpf = ;
      $factura->total_descuento = $totalDescuento;
      $factura->user_id = auth()->user()->id;

      $factura->save();

      // Se guardan las lineas
      if ($id == null) {
        $this->lineasFacturaController->guardarLineas($request, $factura);
        $secuencia->valor_siguiente += 1;
        $secuencia->save();
      } else {
        $this->lineasFacturaController->actualizarLineas($request, $factura);
      }

      return $factura;
  }

  public function crearFactura(Request $request, $tratamiento) {
    // Obtener el ejercicio seleccionado y comprobar que no está cerrado
    $ejercicio = Ejercicio::find($request->ejercicio);

    if ($ejercicio->cerrado) {
      return redirect()
          ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $id])
          ->with([
              'message'    => "El ejercicio " . $ejercicio->codigo . " está marcado como cerrado",
              'alert-type' => 'error',
          ]);
    }

    $tipoIva = TipoIva::find($request->tipoIva);

    $secuencia = Secuencia::where([
      ['ejercicio_id', '=', $ejercicio->id],
      ['tipo_secuencia_id', '=', 2] // Secuencia de las facturas
    ])->first();

    // Calcular número de factura
    $numero_factura = $this->calcularNumeroFactura($ejercicio, $secuencia);

    $total = $tratamiento->total;

    $factura = new Factura();
    $factura->numero = $numero_factura;
    $factura->secuencia_id = $secuencia->id;
    $factura->codigo_paciente = $tratamiento->paciente->codigo;
    $factura->paciente_id = $tratamiento->paciente->id;
    $factura->nombre_paciente = $tratamiento->paciente->nombre;
    $factura->cif = $tratamiento->paciente->cif;
    $factura->direccion = $tratamiento->paciente->direccion;
    $factura->ciudad = $tratamiento->paciente->poblacion ? $tratamiento->paciente->poblacion->nombre : null;
    $factura->provincia = $tratamiento->paciente->poblacion ? $tratamiento->paciente->poblacion->provincia->nombre : null;
    $factura->pais = $tratamiento->paciente->poblacion ? $tratamiento->paciente->poblacion->provincia->comunidadAutonoma->pais->nombre : null;
    // $factura->codigo_postal = $tratamiento->paciente()->;
    $factura->telefono = $tratamiento->paciente->telefono;
    $factura->email = $tratamiento->paciente->email;
    $factura->fecha_factura = Carbon::now();
    $factura->fecha_vencimiento = Carbon::now()->addMonths(1);
    $factura->pagada = true;
    $factura->anulada = false;
    $factura->observaciones = $request->observaciones;
    $factura->total = $tratamiento->total;
    $factura->total_iva = $tratamiento->total_iva;
    // $factura->total_irpf = ;
    $factura->total_descuento = $tratamiento->total_descuento;
    $factura->user_id = auth()->user()->id;

    $factura->save();

    // Se recorren las líneas
    foreach ($tratamiento->lineas as $linea) {
      $lineaFactura = new LineaFactura();

      $lineaFactura->factura_id = $factura->id;
      $lineaFactura->linea = $linea->linea;
      $lineaFactura->descripcion = $linea->descripcion;
      $lineaFactura->cantidad = $linea->cantidad;
      $lineaFactura->importe_unitario = $linea->importe_unitario;
      $lineaFactura->porcentaje_descuento = $linea->porcentaje_descuento;
      $lineaFactura->total_iva = $linea->total_iva;
      $lineaFactura->tipo_iva_id = $linea->tipo_iva_id;
      $lineaFactura->total = $linea->total;
      $lineaFactura->pieza = $linea->pieza;

      $lineaFactura->save();
    }

    $secuencia->valor_siguiente += 1;
    $secuencia->save();

    return $factura;
  }

  /**
   * Función que parsea los datos para llamar a la función que almacena la factura
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function makeFacturaFromTratamiento($tratamiento, $ejercicio, $observaciones, $total, $totalDescuento, $nombre) {
    $tipoIva = 1; //TipoIva::find($request->tipoIva);

    $paciente = Paciente::find($tratamiento->paciente_id);

    $totalIva = 0; //$request->totalIvaPresupuesto;

    return $this->storeFacturaFromTratamiento($tratamiento, $ejercicio, $tipoIva, $paciente, $observaciones,
      $total, $totalIva, $totalDescuento, $nombre);
  }

  public function storeFacturaFromTratamiento($tratamiento, $ejercicio, $tipoIva, $paciente, $observaciones,
    $total, $totalIva, $totalDescuento, $nombre) {
      // Obtener el ejercicio seleccionado y comprobar que no está cerrado
      if ($ejercicio->cerrado) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "El ejercicio " . $ejercicio->codigo . " está marcado como cerrado",
                'alert-type' => 'error',
            ]);
      }

      $secuencia = Secuencia::where([
        ['ejercicio_id', '=', $ejercicio->id],
        ['tipo_secuencia_id', '=', 2] // Secuencia de los facturas
      ])->first();

      $numero_factura = $this->calcularNumeroFactura($ejercicio, $secuencia, true);

      $factura = new Factura();

      $factura->numero = $numero_factura;
      $factura->secuencia_id = $secuencia->id;
      $factura->codigo_paciente = $paciente->codigo;
      $factura->paciente_id = $paciente->id;
      $factura->nombre_paciente = $paciente->nombre;
      $factura->cif = $paciente->cif;
      $factura->direccion = $paciente->direccion;
      $factura->ciudad = $paciente->poblacion ? $paciente->poblacion->nombre : null;
      $factura->provincia = $paciente->poblacion ? $paciente->poblacion->provincia->nombre : null;
      $factura->pais = $paciente->poblacion ? $paciente->poblacion->provincia->comunidadAutonoma->pais->nombre : null;
      // $factura->codigo_postal = $actuacion->paciente()->;
      $factura->telefono = $paciente->telefono;
      $factura->email = $paciente->email;
      $factura->fecha_factura = Carbon::now();
      $factura->fecha_vencimiento = Carbon::now()->addMonths(1);
      $factura->anulada = false;
      $factura->observaciones = $observaciones;
      $factura->total = $total;
      $factura->total_iva = $totalIva;
      // $factura->total_irpf = ;
      $factura->total_descuento = $totalDescuento;
      $factura->user_id = auth()->user()->id;

      $factura->nombre = $nombre;
      $factura->indice = $paciente->nextIndiceFactura();
      $factura->save();

      $this->lineasFacturaController->guardarLineasFromTratamiento($tratamiento, $factura);
      $secuencia->valor_siguiente += 1;
      $secuencia->save();

      return $factura;
  }

  /**
   * Imprimir con dompdf
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function print(Request $request) {
    $id = $request->id;

    $factura = Factura::find($id);

    $date = date('d-m-Y H:i:s');

    $view =  \View::make('templates.pdf.factura',
      compact('factura',
      'date'
      ))
      ->render();

    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadHTML($view);
    return $pdf->stream('invoice');
  }

  /**
   * Imprimir facturas entre fechas utilizando wkhtmltopdf
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function printBetweenDates(Request $request) {
    if ($request->start && $request->end) {
      // TODO: Ver como tratar el formato de las fechas igual en todas partes
      // Lo ideal es separar entre formato 'display' e 'internal'
      $start = $this->stringToDatetime($request->start);
      $end = $this->stringToDatetime($request->end);

      $facturas = Factura::whereBetween('fecha_factura', [$start, $end])->get();

      $pdf = new Pdf(\View::make('templates.pdf.facturas',
        compact('facturas',
        'date'
        ))->render());

      if (!$pdf->send('relacion_de_facturas.pdf')) {
          echo $pdf->getError();
      }
    } else {
      return response()->json([
        'message'    => "Se deben introducir fecha inicio y fin",
        'alert-type' => 'error'
      ]);
    }
  }
}
