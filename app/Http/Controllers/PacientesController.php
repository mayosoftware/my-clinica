<?php

namespace App\Http\Controllers;

use App\Models\Paciente;
use Illuminate\Http\Request;

use TCG\Voyager\Facades\Voyager;

class PacientesController extends VoyagerBaseController
{
    public function index(Request $request)
    {
        return $this->commonIndex($request, null, null);
    }

    public function edit(Request $request, $id)
    {
        // TODO: Estoy haciendo una consulta de más
        $paciente = Paciente::find($id);

        // Ficheros
        // TODO: Crear definiciones globales
        $filesDirectory = '/pacientes/' . $id . '/ficheros';

        $title = "Ficheros del paciente: " . $paciente->nombre;

        $data = [
        'title' => $title,
        'filesDirectory' => $filesDirectory,
        'pacienteNombreHeader' => $paciente->nombre,
        'pacienteCodigoHeader' => $paciente->codigo
      ];

        return $this->commonEdit($request, $id, $data);
    }

    public function create(Request $request)
    {
        return $this->commonCreate($request, null, null);
    }

    public function store(Request $request)
    {
        return $this->commonStore($request);
    }

    public function indexFicheros(Paciente $paciente)
    {
        // Check permission
        Voyager::canOrFail('browse_media');

        // TODO: Crear definiciones globales
        $filesDirectory = '/pacientes/' . $paciente->id . '/ficheros/';

        $title = "Ficheros del paciente: " . $paciente->nombre;

        return Voyager::view('vendor.voyager.pacientes.ficheros', compact(
        'title',
        'paciente',
        'filesDirectory'
      ));
    }

    public function update(Request $request, $id) {
        return parent::update($request, $id);
    }

    public function actualizarSaldo($id, $saldo) {
      $paciente = Paciente::find($id);

      $paciente->saldo = $saldo;
      $paciente->save();
    }

    public function ajaxGuardarPacientePotencial(Request $request) {
      $pacientePotencial = new Paciente();
      $pacientePotencial->nombre = $request->nombre_paciente_potencial;
      $pacientePotencial->telefono = $request->telefono_paciente_potencial;
      $pacientePotencial->tipo_paciente_id = 3; // Potencial
      $pacientePotencial->codigo = $pacientePotencial->nextCodigo();

      $pacientePotencialType = Voyager::model('DataType')->where('slug', '=', 'pacientes')->first();
      $valPotencial = $this->validateBread($pacientePotencial->getAttributes(), $pacientePotencialType->addRows);

      if ($valPotencial->fails()) {
          return response()->json(['errors' => $valPotencial->messages()]);
      }

      if (!$request->has('_validate')) {
        $pacientePotencial->save();
      }

      return response()->json([
        'paciente_id' => $pacientePotencial->id,
        'alert-type' => 'success'
      ]);
    }

    public function ajaxBuscarPacientePotencial(Request $request) {
      \Log::debug($request);
      // Ver si es un paciente potencial
      $pacientePotencial = new Paciente();
      if (!$request->nombre_paciente_potencial) {
        return response()->json(['errors' => ['Debes indicar el nombre del paciente potencial']]);
      }
      if (!$request->telefono_paciente_potencial) {
        return response()->json(['errors' => ['Debes indicar el teléfono del paciente potencial']]);
      }
      $pacientePotencial->nombre = $request->nombre_paciente_potencial;
      $pacientePotencial->telefono = $request->telefono_paciente_potencial;

      // Sino se ha confirmado la creación del paciente duplicado, se busca
      // if (!$request->crear_paciente_duplicado) {
        // Comprobar si existe un paciente con mismo nombre o teléfono
        $pacienteSistema = Paciente::where('nombre', $pacientePotencial->nombre)
          ->orWhere('telefono', $pacientePotencial->telefono)
          ->first();

        if ($pacienteSistema) {
          return response()->json([
              'paciente_en_sistema' => 'Existe un paciente que coincide con los datos: ' . $pacienteSistema->nombre,
              'alert-type' => 'error'
          ]);
        } else {
          return response()->json([
              'alert-type'  => 'success'
          ]);
        }
      // }
    }
}
