<?php

namespace App\Http\Controllers;

use App\Models\Provincia;
use Illuminate\Http\Request;

class ProvinciasController extends VoyagerBaseController
{
    public function getPoblaciones(Provincia $provincia)
    {
        return $provincia->poblaciones()->select('id', 'nombre')->get();
    }
}
