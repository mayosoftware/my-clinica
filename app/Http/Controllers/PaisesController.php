<?php

namespace App\Http\Controllers;

use App\Models\Pais;

use Illuminate\Http\Request;

class PaisesController extends VoyagerBaseController
{
    public function getComunidadesAutonomas(Pais $pais)
    {
        return $pais->comunidadesAutonomas()->select('id', 'nombre')->get();
    }
}
