<?php

namespace App\Http\Controllers;

use App\Models\ComunidadAutonoma;
use Illuminate\Http\Request;

class ComunidadesAutonomasController extends VoyagerBaseController
{
    public function getProvincias(ComunidadAutonoma $comunidadAutonoma)
    {
        return $comunidadAutonoma->provincias()->select('id', 'nombre')->get();
    }
}
