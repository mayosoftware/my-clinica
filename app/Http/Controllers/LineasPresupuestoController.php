<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contabilidad\LineaPresupuesto;

use App\Models\TratamientoPaciente;

class LineasPresupuestoController extends Controller
{
  /**
   * Constructor
   */
  public function __construct()
  {

  }

  public function guardarLineas(Request $request, $presupuesto)
  {
      $numero_linea = 0;
      // Se recorren las líneas
      foreach ($request->descripcion_linea as $key => $value) {
          $numero_linea++;
          $linea = new LineaPresupuesto();

          $linea->presupuesto_id = $presupuesto->id;
          $linea->linea = $numero_linea;
          $linea->descripcion = $value;
          $linea->pieza = $request->pieza_linea[$key];
          $linea->tipo_iva_id = 1;
          $linea->cantidad = $request->cantidad_linea[$key];
          $linea->importe_unitario = $request->precio_linea[$key];
          $linea->total = $request->total_linea[$key];
          $linea->total_iva = 0;
          $linea->porcentaje_descuento = $request->descuento_linea[$key];
          $linea->save();
      }
      //$presupuesto->numero_lineas = $numero_linea;
      $presupuesto->save();
  }

  public function actualizarLineas(Request $request, $presupuesto) {
    // Ver si hay lineas que borrar
    $lineasGuardar = $request->linea_id;
    $lineasBorrar = $presupuesto->lineas->filter(function($item) use ($lineasGuardar) {
        return !in_array($item->id, $lineasGuardar);
    });

    foreach ($lineasBorrar as $value) {
      LineaPresupuesto::destroy($value->id);
    }

    $numero_linea = 0;
    foreach ($request->descripcion_linea as $key => $value) {
        $numero_linea++;
        // Si existe la linea se modifica
        if ( array_key_exists($key, $request->linea_id) &&
          !empty($request->linea_id[$key])) {
          $linea = LineaPresupuesto::find($request->linea_id[$key]);

        // Si no existe se crea
        } else {
          $linea = new LineaPresupuesto();
          $linea->presupuesto_id = $presupuesto->id;
        }

        $linea->linea = $numero_linea;
        $linea->descripcion = $value;
        $linea->pieza = $request->pieza_linea[$key];
        $linea->tipo_iva_id = 1;
        $linea->cantidad = $request->cantidad_linea[$key];
        $linea->importe_unitario = $request->precio_linea[$key];
        $linea->total = $request->total_linea[$key];
        $linea->total_iva = 0;
        $linea->porcentaje_descuento = $request->descuento_linea[$key];

        $linea->save();
    }
    $presupuesto->save();
  }

  public function guardarLineasFromTratamiento($tratamiento, $presupuesto)
  {
      $numero_linea = 0;
      // Se recorren las líneas
      foreach ($tratamiento->lineas as $linea) {
          $numero_linea++;
          $lineaPresupuesto = new LineaPresupuesto();

          $lineaPresupuesto->presupuesto_id = $presupuesto->id;
          $lineaPresupuesto->linea = $numero_linea;
          $lineaPresupuesto->descripcion = $linea->descripcion;
          $lineaPresupuesto->pieza = $linea->pieza;
          $lineaPresupuesto->tipo_iva_id = 1;
          $lineaPresupuesto->cantidad = $linea->cantidad;
          $lineaPresupuesto->importe_unitario = $linea->importe_unitario;
          $lineaPresupuesto->total = $linea->total;
          $lineaPresupuesto->total_iva = 0;
          $lineaPresupuesto->porcentaje_descuento = $linea->porcentaje_descuento;
          $lineaPresupuesto->save();
      }
      //$presupuesto->numero_lineas = $numero_linea;
      $presupuesto->save();
  }
}
