<?php

namespace App\Http\Controllers\Chat;

use App\User;
use Illuminate\Http\Request;
use Nahid\Talk\Facades\Talk;
use Nahid\Talk\Facades\Message;
use Auth;
use View;
use App\Http\Controllers\Controller;

class MessageController extends Controller
{
    protected $authUser;
    public function __construct()
    {
        // $this->middleware('auth');
        $this->middleware(function ($request, $next) { Talk::setAuthUserId(Auth::user()->id); return $next($request); });
        //dd(auth()->user());
        //Talk::setAuthUserId(Auth::user()->id);
        // Talk::setAuthUserId(Auth::user()->id);

        View::composer('chat.partials.peoplelist', function($view) {
            $threads = Talk::threads();
            $view->with(compact('threads'));
        });
    }

    /**
     * Historial de mensajes con otro usuario
     * @param  [type] $id Identificador del otro usuario
     * @return [type]     [description]
     */
    public function chatHistory($id)
    {
        $conversations = Talk::getMessagesByUserId($id, 0, 100);
        // dd($conversations);
        $user = '';
        $messages = [];
        if(!$conversations) {
            $user = User::find($id);
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;

            // Marcar como leidos, los no leidos
            foreach ($messages as $message)
            {
                if (auth()->id() != $message->user_id && !$message->isSeen) {
                    Talk::user(auth()->id())->makeSeen($message->id);
                }
            }
        }

        if (count($messages) > 0) {
            $messages = $messages->sortBy('id');
        }

        return view('chat.messages.conversations', compact('messages', 'user'));
    }

    /**
     * Función que obtiene los mensajes no leidos de un usuario
     * @return [type]     [description]
     */
    public function ajaxMessagesNotSeen() {
      // Otener las conversaciones del usuario logueado (No hace falta pasar el id)
      $inboxes = Talk::getInbox('desc');

      $messagesNotSeen = [];
      foreach ($inboxes as $inbox) {
        $lastMessage = $inbox->thread;
        $user = $inbox->withUser;
        if (!$lastMessage->is_seen) {
          $messagesNotSeen[] = array(
            'id'              => $lastMessage->id,
            'message'         => $lastMessage->message,
            'created_at'      => (string) $lastMessage->created_at,
            'sender'          => array('name' => $user->name, 'avatar' => $user->avatar, 'id' => $user->id)
          );
        }
      }
      return response()->json(['status'=>'success',
        'messages'=>$messagesNotSeen], 200);
    }

    /**
     * [markAllMessagesAsSeen description]
     * @return [type] [description]
     */
    public function ajaxMarkAllMessagesAsSeen() {
      $inboxes = Talk::getInbox('desc');

      foreach ($inboxes as $inbox) {
        $lastMessage = $inbox->thread;
        if (!$lastMessage->is_seen) {
          Talk::user(auth()->id())->makeSeen($lastMessage->id);
        }
      }

      \Log::debug('mark seen');

      return response()->json(['status'=>'success'], 200);
    }

    public function ajaxSendMessage(Request $request)
    {
        if ($request->ajax()) {
            $rules = [
                'message-data'=>'required',
                '_id'=>'required'
            ];

            $this->validate($request, $rules);

            $body = $request->input('message-data');
            $userId = $request->input('_id');

            if ($message = Talk::sendMessageByUserId($userId, $body)) {
                $html = view('chat.ajax.newMessageHtml', compact('message'))->render();
                return response()->json(['status'=>'success', 'html'=>$html], 200);
            }
        }
    }

    public function ajaxDeleteMessage(Request $request, $id)
    {
        if ($request->ajax()) {
            if(Talk::deleteMessage($id)) {
                return response()->json(['status'=>'success'], 200);
            }

            return response()->json(['status'=>'errors', 'msg'=>'something went wrong'], 401);
        }
    }

    public function tests()
    {
        dd(Talk::channel());
    }
}
