<?php

namespace App\Http\Controllers;

use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;

use Carbon\Carbon;
use DateTime;
use DateInterval;

class VoyagerBaseController extends BaseVoyagerBaseController
{
  public $piezas = [0, 18, 17, 16, 15, 14, 13, 12, 11, 21, 22, 23, 24, 25, 26, 27, 28,
    55, 54, 54, 52, 51, 61, 62, 63, 64, 65,
    48, 47, 46, 45, 44, 43, 42, 41, 31, 32, 33, 34, 35, 36, 37, 38,
    85, 84, 83, 82, 81, 71, 72, 73, 74, 75
  ];

  // TODO: Crear controlador para las operaciones entre fechas con Carbon
  public function addTimeToDate($date, $time)
  {
      // list($hours, $minutes, $seconds) = explode(":", $time);
      // $interval = new DateInterval("PT" . $hours . "H" . $minutes . "M" . $seconds . "S");
      // return $date->add($interval);

      return $date->toDateString() . ' ' . $time;
  }

  public function stringToDatetime($stringDate, $format = 'd/m/Y')
  {
      return empty($stringDate) ? null : Carbon::createFromFormat($format, $stringDate);
  }

  // --------------------------------------------------------------------------

  public function getColor($realizada, $cancelada)
  {
      $bgcolors = [ "#22A7F0", "#47D27F", "#F94126" ]; // No Realizada, Realizada, Cancelada
      if ($cancelada == true) {
          return $bgcolors[2];
      } elseif ($realizada == true) {
          return $bgcolors[1];
      } else {
          return $bgcolors[0];
      }
  }

  public function calcularNumeroFactura($ejercicio, $secuencia, $presu = false) {
    // Calcular número de factura
    return ($presu ? 'P-' : '') . $ejercicio->codigo . '-' . str_pad($secuencia->valor_siguiente, 4, '0', STR_PAD_LEFT);
  }

  public function calcularImporteIva($tipoIva, $total) {
    return $total - ($total / (1 + $tipoIva->valor));
  }

  //***************************************
  //               ____
  //              |  _ \
  //              | |_) |
  //              |  _ <
  //              | |_) |
  //              |____/
  //
  //      Browse our Data Type (B)READ
  //
  //****************************************

  public function commonIndex(Request $request, $data, $slugName)
  {
      if ($slugName) {
        $slug = $slugName;
      } else {
        // GET THE SLUG, ex. 'posts', 'pages', etc.
        $slug = $this->getSlug($request);
      }

      // GET THE DataType based on the slug
      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('browse', app($dataType->model_name));

      $getter = $dataType->server_side ? 'paginate' : 'get';

      $search = (object) ['value' => $request->get('s'), 'key' => $request->get('key'), 'filter' => $request->get('filter')];
      $searchable = $dataType->server_side ? array_keys(SchemaManager::describeTable(app($dataType->model_name)->getTable())->toArray()) : '';
      $orderBy = $request->get('order_by');
      $sortOrder = $request->get('sort_order', null);

      // Next Get or Paginate the actual content from the MODEL that corresponds to the slug DataType
      if (strlen($dataType->model_name) != 0) {
          $relationships = $this->getRelationships($dataType);

          $model = app($dataType->model_name);
          $query = $model::select('*')->with($relationships);

          // If a column has a relationship associated with it, we do not want to show that field
          $this->removeRelationshipField($dataType, 'browse');

          if ($search->value && $search->key && $search->filter) {
              $search_filter = ($search->filter == 'equals') ? '=' : 'LIKE';
              $search_value = ($search->filter == 'equals') ? $search->value : '%'.$search->value.'%';
              $query->where($search->key, $search_filter, $search_value);
          }

          if ($orderBy && in_array($orderBy, $dataType->fields())) {
              $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'DESC';
              $dataTypeContent = call_user_func([
                  $query->orderBy($orderBy, $querySortOrder),
                  $getter,
              ]);
          } elseif ($model->timestamps) {
              $dataTypeContent = call_user_func([$query->latest($model::CREATED_AT), $getter]);
          } else {
              $dataTypeContent = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), $getter]);
          }

          // Replace relationships' keys for labels and create READ links if a slug is provided.
          $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType);
      } else {
          // If Model doesn't exist, get data from table name
          $dataTypeContent = call_user_func([DB::table($dataType->name), $getter]);
          $model = false;
      }

      // Check if BREAD is Translatable
      if (($isModelTranslatable = is_bread_translatable($model))) {
          $dataTypeContent->load('translations');
      }

      // Check if server side pagination is enabled
      $isServerSide = isset($dataType->server_side) && $dataType->server_side;

      $view = 'voyager::bread.browse';

      if (view()->exists("voyager::$slug.browse")) {
          $view = "voyager::$slug.browse";
      }

      $commonData = compact(
          'dataType',
          'dataTypeContent',
          'isModelTranslatable',
          'search',
          'orderBy',
          'sortOrder',
          'searchable',
          'isServerSide'
      );

      if ($data) {
        $finalData = array_merge($commonData, $data);
      } else {
        $finalData = $commonData;
      }

      return Voyager::view($view, $finalData);
  }

  //***************************************
  //                ______
  //               |  ____|
  //               | |__
  //               |  __|
  //               | |____
  //               |______|
  //
  //  Edit an item of our Data Type BR(E)AD
  //
  //****************************************
  public function commonEdit(Request $request, $id, $data)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      $relationships = $this->getRelationships($dataType);

      $dataTypeContent = (strlen($dataType->model_name) != 0)
          ? app($dataType->model_name)->with($relationships)->findOrFail($id)
          : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

      foreach ($dataType->editRows as $key => $row) {
          $details = json_decode($row->details);
          $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
      }

      // If a column has a relationship associated with it, we do not want to show that field
      $this->removeRelationshipField($dataType, 'edit');

      // Check permission
      $this->authorize('edit', $dataTypeContent);

      // Check if BREAD is Translatable
      $isModelTranslatable = is_bread_translatable($dataTypeContent);

      $view = 'voyager::bread.edit-add';

      if (view()->exists("voyager::$slug.edit-add")) {
          $view = "voyager::$slug.edit-add";
      }

      $commonData = compact('dataType', 'dataTypeContent', 'isModelTranslatable');

      if ($data) {
        $finalData = array_merge($commonData, $data);
      } else {
        $finalData = $commonData;
      }

      return Voyager::view($view, $finalData);
  }

  //***************************************
  //
  //                   /\
  //                  /  \
  //                 / /\ \
  //                / ____ \
  //               /_/    \_\
  //
  //
  // Add a new item of our Data Type BRE(A)D
  //
  //****************************************

  public function commonCreate(Request $request, $data, $slugName)
  {
      \Log::debug('create');
      if ($slugName) {
        $slug = $slugName;
      } else {
        $slug = $this->getSlug($request);
      }

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      $dataTypeContent = (strlen($dataType->model_name) != 0)
                          ? new $dataType->model_name()
                          : false;

      foreach ($dataType->addRows as $key => $row) {
          $details = json_decode($row->details);
          $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
      }

      // If a column has a relationship associated with it, we do not want to show that field
      $this->removeRelationshipField($dataType, 'add');

      // Check if BREAD is Translatable
      $isModelTranslatable = is_bread_translatable($dataTypeContent);

      $view = 'voyager::bread.edit-add';

      if (view()->exists("voyager::$slug.edit-add")) {
          $view = "voyager::$slug.edit-add";
      }

      $commonData = compact('dataType', 'dataTypeContent', 'isModelTranslatable');

      if ($data) {
        $finalData = array_merge($commonData, $data);
      } else {
        $finalData = $commonData;
      }

      return Voyager::view($view, $finalData);
  }


  /**
   * POST BRE(A)D - Store data.
   *
   * @param \Illuminate\Http\Request $request
   *
   * @return \Illuminate\Http\RedirectResponse
   */
  public function commonStore(Request $request)
  {
      \Log::debug('store');
      \Log::debug($request->ajax());
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->addRows);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      if (!$request->has('_validate')) {
        // TODO: Añado el echo para que funcione la redirección
        // Al incluir el input tipo_paciente_id hace el submit pero no redirige a la vista y no tengo ni idea de por que
        // echo "1";
          $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

          event(new BreadDataAdded($dataType, $data));

          if ($request->ajax()) {
              return response()->json(['success' => true, 'data' => $data]);
          }

          return redirect()
              ->route("voyager.{$dataType->slug}.index")
              ->with([
                      'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                      'alert-type' => 'success',
                  ]);
      }
  }
}
