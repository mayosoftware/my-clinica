<?php

namespace App\Http\Controllers;

use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataAdded;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Paciente;
use App\Models\Tratamiento;
use App\Models\TratamientoPaciente;
use App\Models\TratamientoPacienteLinea;
use App\Models\TratamientoPacienteProgramacion;
use App\Models\Contabilidad\TipoIva;
use App\Models\Contabilidad\Ejercicio;
use App\Models\Contabilidad\Secuencia;
use App\Models\Contabilidad\Presupuesto;
use App\Models\Contabilidad\LineaPresupuesto;
use App\Models\Contabilidad\Factura;
use App\Models\Contabilidad\LineaFactura;
use App\Models\MovimientosCaja;
use App\Models\Commons\PlanPredefinido;

use DateTime;
use DateInterval;

use Debugbar;

class TratamientosPacienteController extends VoyagerBaseController
{
    private $lineasController;
    private $presupuestosController;
    private $facturasController;
    private $pcientesController;
    private $movimientosCajaController;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->lineasController = new TratamientosPacienteLineasController();
        $this->presupuestosController = new PresupuestosController();
        $this->facturasController = new FacturasController();
        $this->pacientesController = new PacientesController();
        $this->movimientosCajaController = new MovimientosCajaController();
    }

    public function createWithPaciente(Request $request, $idPaciente)
    {
        // Paciente
        $paciente = Paciente::find($idPaciente);
        // Tratamientos
        $tratamientos = Tratamiento::all();
        // Ejercicios abiertos
        $ejercicios = Ejercicio::where('cerrado', '=', false)->get();

        $pacienteNombreHeader = $paciente->nombre;
        $pacienteCodigoHeader = $paciente->codigo;

        // Doctores
        $doctores = \App\User::where('role_id', '!=', 3) // Doctores
               ->orderBy('name', 'desc')
               ->get();

         // Planes predefinidos
         $planes = PlanPredefinido::all();

        $data = [
          'paciente' => $paciente,
          'tratamientos' => $tratamientos,
          'planes'  => $planes,
          'pacienteNombreHeader' => $pacienteNombreHeader,
          'pacienteCodigoHeader' => $pacienteCodigoHeader,
          'piezas' => $this->piezas,
          'ejercicios' => $ejercicios,
          'doctores' => $doctores
        ];

        $slug = "tratamientos-paciente";

        return $this->commonCreate($request, $data, $slug);
    }

    public function edit(Request $request, $id)
    {
        // Paciente
        $tratamiento = TratamientoPaciente::find($id);
        // Tratamientos
        $tratamientos = Tratamiento::all();
        // Ejercicios abiertos
        $ejercicios = Ejercicio::where('cerrado', '=', false)->get();
        // Doctores
        $doctores = \App\User::where('role_id', '!=', 3) // Doctores
               ->orderBy('name', 'desc')
               ->get();

        $pacienteNombreHeader = $tratamiento->paciente->nombre;
        $pacienteCodigoHeader = $tratamiento->paciente->codigo;

        // Planes predefinidos
        $planes = PlanPredefinido::all();

        $data = [
          'paciente' => $tratamiento->paciente,
          'tratamientos' => $tratamientos,
          'planes' => $planes,
          'pacienteNombreHeader' => $pacienteNombreHeader,
          'pacienteCodigoHeader' => $pacienteCodigoHeader,
          'piezas' => $this->piezas,
          'ejercicios' => $ejercicios,
          'doctores' => $doctores
        ];

        return $this->commonEdit($request, $id, $data);
    }

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->descripcion_linea) {
          return response()->json([
              'success' => false,
              'message'    => 'Debes introducir al menos un tratamiento'
          ]);
        }

        if (!$request->has('_validate')) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            event(new BreadDataAdded($dataType, $data));

            $this->lineasController->guardarLineas($request, $data);

            // Si se marca como financiado se guarda movimiento de caja
            if ($data->financiado) {
              // Guardar movimiento de caja
              $movimientoFinanciacion = new MovimientosCaja();
              $movimientoFinanciacion->fecha = Carbon::now();
              $movimientoFinanciacion->concepto = 'Financiación tratamiento';
              $movimientoFinanciacion->entrada = $request->total_financiacion;
              $movimientoFinanciacion->salida = 0;
              $movimientoFinanciacion->tipo_cobro_id = 3; // Financiación
              $movimientoFinanciacion->tratamiento_paciente_id = $data->id;
              $movimientoFinanciacion->financiacion = true;
              $movimientoFinanciacion->save();
            }

            if ($request->ajax()) {
                return response()->json(['success' => true, 'data' => $data]);
            }

            return redirect()
                ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $data->id])
                ->with([
                        'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                        'alert-type' => 'success',
                    ]);
        }
    }

    // POST BR(E)AD
    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
          \Log::debug($dataType->editRows);
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);
            $this->lineasController->actualizarLineas($request, $data);

            // TODO: Corregir para que no sea necesario esto:
            $data->presupuestado = $request->presupuestado;
            $data->facturado = $request->facturado;
            $data->programacion_conjunta = $request->programacion_conjunta;
            $data->nombre = $request->nombre;
            $data->save();

            $this->pacientesController->actualizarSaldo($request->paciente_id, $request->saldo);

            // Si se marca como financiado se guarda movimiento de caja, si no existe ya
            if ($data->financiado) {
              $movimiento = MovimientosCaja::where('tratamiento_paciente_id', '=', $data->id)
              ->where('financiacion', 'true')
              ->get();

              if ($movimiento->count() == 0) {
                // Guardar movimiento de caja
                $movimientoFinanciacion = new MovimientosCaja();
                $movimientoFinanciacion->fecha = Carbon::now();
                $movimientoFinanciacion->concepto = 'Financiación tratamiento';
                $movimientoFinanciacion->entrada = $request->total_financiacion;
                $movimientoFinanciacion->salida = 0;
                $movimientoFinanciacion->tipo_cobro_id = 3; // Financiación
                $movimientoFinanciacion->tratamiento_paciente_id = $data->id;
                $movimientoFinanciacion->financiacion = true;
                $movimientoFinanciacion->save();
              }
            }

            return redirect()
                ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $id])
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    public function destroy(Request $request, $id) {
        $tratamiento = TratamientoPaciente::find($id);

        if ($tratamiento) {
            $this->borrarTratamiento($tratamiento);
        }

        return redirect()
            ->route("voyager.pacientes.edit", ["paciente" => $request->paciente_id])
            ->with([
                    'message'    => "Tratamiento eliminado correctamente",
                    'alert-type' => 'success',
                ]);
    }

    public function destroyAll(Request $request, $id) {
        $paciente = Paciente::find($id);

        if ($paciente) {
            foreach ($paciente->tratamientosPaciente as $tratamiento) {
                $this->borrarTratamiento($tratamiento);
            }
        }

        return redirect()
            ->route("voyager.pacientes.edit", ["paciente" => $request->paciente_id])
            ->with([
                    'message'    => "Tratamientos eliminados correctamente",
                    'alert-type' => 'success',
                ]);
    }


   private function borrarTratamiento($tratamiento) {

     // Borrar lineas
     foreach ($tratamiento->lineas as $linea) {
       $this->lineasController->eliminarLinea($linea->id);
     }

     // Borrar ficheros
     foreach ($tratamiento->ficheros as $fichero) {
       $fichero->delete();
     }

     // Borrrar movimientos de caja
     foreach ($tratamiento->movimientosCaja as $movimiento) {

      // Actualizar el saldo del usuario
      if ($movimiento->entrada)
        // Actualizar saldo del paciente
        if ($movimiento->entrada > 0) {
          $tratamiento->paciente->saldo -= $movimiento->entrada;
        } else if ($movimiento->salida > 0) {
          $tratamiento->paciente->saldo += $movimiento->salida;
        }
        $tratamiento->paciente->save();

        $movimiento->delete();

     }

     // Borrar factura
     if ($tratamiento->factura) {
       $factura = Factura::find($tratamiento->factura->id);
       $factura->delete();
     }

     // Borrar presupuesto
     if ($tratamiento->presupuesto) {
       $presupuesto = Presupuesto::find($tratamiento->presupuesto->id);
       $presupuesto->delete();
     }

     // Borrar citas
     if ($tratamiento->programacion) {
       $programacion = TratamientoPacienteProgramacion::find($tratamiento->programacion->id);
       $programacion->delete();
     }

     // Cobrado y pendiente se suma al saldo
     $paciente = Paciente::find($tratamiento->paciente_id);
     $paciente->saldo += $tratamiento->pendiente + $tratamiento->cobrado;
     $paciente->save();

     $tratamiento->delete();
   }

    /*
    * Guardar línea individual. Se llama por ajax
    */
    public function crearLinea(Request $request, $id) {
        $tratamiento = TratamientoPaciente::find($id);

        $linea_id = $this->lineasController->guardarLinea($request, $tratamiento);

        return response()->json([
          'message'    => "Tratamiento añadido",
          'alert-type' => 'success',
          'linea_id'   => $linea_id
        ]);
    }

    public function eliminarLinea($tratamiento_id, $linea_id) {
      $this->lineasController->eliminarLinea($linea_id);

      $tratamiento = TratamientoPaciente::find($tratamiento_id);

      $tratamiento->numero_lineas -= 1;
      $tratamiento->save();

      return response()->json([
        'message'    => "Linea eliminada",
        'alert-type' => 'success'
      ]);
    }

    public function print(Request $request) {
      $id = $request->id_tratamiento;

      $tratamiento = TratamientoPaciente::find($id);

      $date = date('d-m-Y H:i:s');

      $view =  \View::make
      // return view
      ('templates.pdf.tratamiento',
        compact('tratamiento',
        'date'
        ))
        ->render()
        ;

      $pdf = \App::make('dompdf.wrapper');
      $pdf->loadHTML($view);
      return $pdf->stream('tratamiento');
    }

    public function sendMail(Request $request) {
      $id = $request->id_tratamiento;
      $tratamiento = TratamientoPaciente::find($id);
      $email = $tratamiento->paciente->email;
      $nombre = $tratamiento->paciente->nombre;

      if (!$email) {
        return response()->json([
          'message'    => "El paciente no tiene una dirección de email",
          'alert-type' => 'error'
        ]);
      }

      $info = ['info' => 'Tratamiento programado'];

      $pdf = \PDF::loadView('templates.pdf.tratamiento',
        compact('tratamiento','date')
      );

      \Mail::send('templates.mail.tratamiento', compact('tratamiento'), function($message) use($email, $nombre, $pdf) {
        $message->to($email, $nombre)->subject('Tratamiento solicitado')
          ->from('info@lapaz.myclinica.es', 'Información Clínica La Paz')
          ->attachData($pdf->output(), 'tratamiento.pdf');
      });

      return response()->json([
        'message'    => "Tratamiento enviado por email",
        'alert-type' => 'success'
      ]);
    }

    public function generatePresupuesto(Request $request, $id) {
      $tratamiento = TratamientoPaciente::find($id);
      $ejercicio = Ejercicio::find($request->ejercicio);
      $observaciones = $request->observacionesPresupuesto;
      $total = $tratamiento->total;
      $totalIva = $tratamiento->total_iva;
      $totalDescuento = $tratamiento->total_descuento;
      $nombre = $tratamiento->nombre;

      $presupuesto = $this->presupuestosController->makePresupuestoFromTratamiento($tratamiento, $ejercicio, $observaciones, $total, $totalDescuento, $nombre);

      // Asignar presupuesto al tratamiento
      $tratamiento->presupuestado = true;
      $tratamiento->presupuesto_id = $presupuesto->id;
      $tratamiento->save();

      return redirect()
          ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $id])
          ->with([
              'message'    => "Presupuesto " . $presupuesto->numero . " creado correctamente",
              'alert-type' => 'success',
          ]);
    }

    public function generateFactura(Request $request, $id) {

      $tratamiento = TratamientoPaciente::find($id);
      $ejercicio = Ejercicio::find($request->ejercicio);
      $observaciones = $request->observacionesPresupuesto;
      $total = $tratamiento->total;
      $totalIva = $tratamiento->total_iva;
      $totalDescuento = $tratamiento->total_descuento;
      $nombre = $tratamiento->nombre;

      $factura = $this->facturasController->makeFacturaFromTratamiento($tratamiento, $ejercicio, $observaciones, $total, $totalDescuento, $nombre);

      // Asignar factura al tratamiento
      $tratamiento->facturado = true;
      $tratamiento->factura_id = $factura->id;
      $tratamiento->save();

      return redirect()
          ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $id])
          ->with([
              'message'    => "Factura " . $factura->numero . " creada correctamente",
              'alert-type' => 'success',
          ]);
    }

    private function updateImportesTratamiento($tratamiento, $totalCobro) {
      $tratamiento->cobrado += $totalCobro;
      $tratamiento->pendiente -= $totalCobro;
      $tratamiento->save();

      // Actualizar saldo de usuario
      $tratamiento->paciente->saldo += $totalCobro;
      $tratamiento->paciente->save();

      return $tratamiento;
    }

    public function generateCobro(Request $request, $id) {
      $tratamiento = TratamientoPaciente::find($id);
      $tipoCobro = $request->tipoCobro;
      $totalCobro = $request->totalCobro;
      $concepto = $request->concepto;

      $movimiento = $this->movimientosCajaController->storeFromTratamiento($tratamiento->id,
        $tipoCobro,
        $totalCobro,
        $concepto);

      $tratamiento = $this->updateImportesTratamiento($tratamiento,
        $totalCobro);

      return redirect()
          ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $id])
          ->with([
              'message'    => "Cobro registrado",
              'alert-type' => 'success',
          ]);
    }

    public function ajaxGenerateCobro(Request $request, $id) {
      if (!$request->totalCobro || $request->totalCobro <= 0) {
        return response()->json([
          'alert-type' => 'error',
          'message'    => 'Debes indicar un importe mayor que 0'
        ]);
      }

      $tratamiento = TratamientoPaciente::find($id);
      $tipoCobro = $request->tipoCobro;
      $totalCobro = $request->totalCobro;
      $concepto = $request->concepto;

      // Buscar si hay un movimiento similar en el tratamiento
      $m_duplicado = MovimientosCaja::where('tratamiento_paciente_id', '=', $id)
      ->where('concepto', 'like', $concepto)
      ->where('entrada', '=', $totalCobro)
      ->get();

      $movimiento = $this->movimientosCajaController->storeFromTratamiento($tratamiento->id,
        $tipoCobro,
        $totalCobro,
        $concepto);

      $tratamiento = $this->updateImportesTratamiento($tratamiento,
        $totalCobro);

      if ($m_duplicado->isNotEmpty()) {
        return response()->json([
        'alert-type'  => 'warning',
        'message'     => 'El cobro registrado podría estar duplicado',
        'movimiento'  => $movimiento,
        'cobrado'     => $tratamiento->cobrado,
        'pendiente'   => $tratamiento->pendiente,
        'saldo'       => $tratamiento->paciente->saldo
        ]);
      }

      return response()->json([
        'alert-type'  => 'success',
        'message'     => 'Cobro registrado correctamente',
        'movimiento'  => $movimiento,
        'cobrado'     => $tratamiento->cobrado,
        'pendiente'   => $tratamiento->pendiente,
        'saldo'       => $tratamiento->paciente->saldo
      ]);
    }

    public function programarTratamiento(Request $request, $id) {
      $tratamiento = TratamientoPaciente::find($id);

      $programacion = new TratamientoPacienteProgramacion();
      $programacion->fecha = $this->stringToDatetime($request->fecha);
      $programacion->hora_inicio = $request->hora_inicio;
      $programacion->hora_fin = $request->hora_fin;
      $programacion->tiempo = $request->tiempo;
      $programacion->observaciones = $request->observaciones;
      $programacion->doctor_id = $request->doctor_id;
      $programacion->user_id = auth()->user()->id;

      $programacion->save();

      $programacion_conjunta = $request->programacion_conjunta;

      if ($programacion_conjunta) {
        $tratamiento->programacion_conjunta = true;
        $tratamiento->programacion_id = $programacion->id;
        $tratamiento->save();

      } else {
        $tratamiento->programacion_conjunta = false;
        $tratamiento->programacion_id = null;
        $tratamiento->save();

        $linea = TratamientoPacienteLinea::find($request->linea);
        $linea->programacion_id = $programacion->id;
        $linea->programado = true;
        $linea->save();
      }

      return redirect()
          ->route("voyager.tratamientos-paciente.edit", ['tratamiento' => $id])
          ->with([
              'message'    => "Programación añadida",
              'alert-type' => 'success',
          ]);
    }

    public function createFromPresupuesto($id) {
      $presupuesto = Presupuesto::find($id);
      $tratamiento = new TratamientoPaciente();
      $tratamiento->paciente_id = $presupuesto->paciente_id;
      $tratamiento->nombre  = $presupuesto->nombre;
      $tratamiento->numero  = $presupuesto->paciente->nextNumeroTratamiento();
      $tratamiento->presupuesto_id  = $presupuesto->id;
      $tratamiento->presupuestado = true;
      $tratamiento->numero_lineas = $presupuesto->lineas->count();
      $tratamiento->total = $presupuesto->total;
      $tratamiento->save();

      foreach ($presupuesto->lineas as $linea) {
        $lineaTratamiento = new TratamientoPacienteLinea();
        $lineaTratamiento->tratamiento_paciente_id = $tratamiento->id;
        $lineaTratamiento->linea = $linea->linea;
        $lineaTratamiento->descripcion = $linea->descripcion;
        $lineaTratamiento->pieza =  $linea->pieza;
        $lineaTratamiento->cantidad  = $linea->cantidad;
        $lineaTratamiento->importe_unitario  = $linea->importe_unitario;
        $lineaTratamiento->total = $linea->total;
        $lineaTratamiento->porcentaje_descuento  = $linea->porcentaje_descuento;
        $lineaTratamiento->save();
      }

      return redirect()
          ->route("voyager.pacientes.edit", ['paciente' => $presupuesto->paciente->id])
          ->with([
              'message'    => "Tratamiento añadido",
              'alert-type' => 'success',
          ]);
    }
}
