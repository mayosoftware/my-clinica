<?php

namespace App\Http\Controllers\Commons;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Carbon\Carbon;
use Debugbar;

use App\Http\Controllers\Controller;
use VoyagerBaseController;

use App\Models\Tratamiento;
use App\Models\Commons\PlanPredefinido;
use App\Models\Commons\PlanPredefinidoLinea;

class PlanesPredefinidosController extends VoyagerBaseController
{
    private $planesPredefinidosLineasController;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->planesPredefinidosLineasController = new PlanesPredefinidosLineasController();
    }

    function create(Request $request) {

      $tratamientos = Tratamiento::all();

      return $this->commonCreate($request, compact('tratamientos'), null);
    }

    function edit(Request $request, $id) {

      $tratamientos = Tratamiento::all();

      return $this->commonEdit($request, $id, compact('tratamientos'));
    }

    function store(Request $request) {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->addRows);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      // Si no hay lineas, se muestra un error
      if (!$request->descripcion_linea) {
        $message = [
                'message'    => "Se debe indicar al menos un tratamiento",
                'alert-type' => 'error',
            ];
        return redirect()->route("voyager.{$dataType->slug}.create")->with($message);
      }

      if (!$request->has('_validate')) {
          $this->makePlan($request);

          if ($request->ajax()) {
              return response()->json(['success' => true, 'data' => $data]);
          }

          return redirect()
              ->route("voyager.{$dataType->slug}.index")
              ->with([
                      'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                      'alert-type' => 'success',
                  ]);
      }
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        // Si no hay lineas, se muestra un error
        if (!$request->descripcion_linea) {
          $message = [
                  'message'    => "Se debe indicar al menos un tratamiento",
                  'alert-type' => 'error',
              ];

          return redirect()->route("voyager.planes-predefinidos.edit", ['presupuesto' => $id])->with($message);
        }

        if (!$request->ajax()) {
            $this->makePlan($request, $id);

            return redirect()
                ->route("voyager.planes-predefinidos.edit", ['plan' => $id])
                ->with([
                    'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                    'alert-type' => 'success',
                ]);
        }
    }

    private function makePlan(Request $request, $id = null) {
      $nombre = $request->nombre;
      $observaciones = $request->observaciones;
      $total = $request->total;

      if ($id == null) {
        $plan = new PlanPredefinido();
      } else {
        $plan = PlanPredefinido::find($id);
      }
      $plan->nombre = $nombre;
      $plan->observaciones = $observaciones;
      $plan->total = $total;
      $plan->save();

      if ($id == null) {
        $this->planesPredefinidosLineasController->guardarLineas($request, $plan);
      } else {
        $this->planesPredefinidosLineasController->actualizarLineas($request, $plan);
      }

    }

    public function ajaxFindPlanById($id) {
      \Debugbar::info($id);
      $plan = PlanPredefinido::find($id);

      if ($plan) {
        $event = $plan->toArray();
        \Debugbar::info($plan->nombre);
        $event["lineas"] = $plan->lineas->toArray();

        return response()->json([
          'event' => $event,
          'message' => "Plan '" . $plan->nombre . "' cargado correctamente",
          'alert-type' => 'success',
        ]);
      } else {
        return response()->json([
          'event' => null,
          'message' => 'El plan seleccionado no está correctamente configurado. Consulte con el administrador',
          'alert-type' => 'error',
        ]);
      }
    }
}
