<?php

namespace App\Http\Controllers\Commons;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Commons\PlanPredefinido;
use App\Models\Commons\PlanPredefinidoLinea;

class PlanesPredefinidosLineasController extends Controller
{
  public function guardarLineas(Request $request, $plan)
  {
      $numero_linea = 0;
      // Se recorren las líneas
      foreach ($request->descripcion_linea as $key => $value) {
          $numero_linea++;
          $linea = new PlanPredefinidoLinea();
          $linea->plan_predefinido_id = $plan->id;
          
          $linea->linea = $numero_linea;
          $linea->descripcion = $value;
          $linea->cantidad = $request->cantidad_linea[$key];
          $linea->precio = $request->precio_linea[$key];
          $linea->total = $request->total_linea[$key];
          $linea->save();
      }
      //$plan->numero_lineas = $numero_linea;
      $plan->save();
  }

  public function actualizarLineas(Request $request, $plan) {
    // Ver si hay lineas que borrar
    $lineasGuardar = $request->linea_id;
    $lineasBorrar = $plan->lineas->filter(function($item) use ($lineasGuardar) {
        return !in_array($item->id, $lineasGuardar);
    });

    foreach ($lineasBorrar as $value) {
      PlanPredefinidoLinea::destroy($value->id);
    }

    $numero_linea = 0;
    foreach ($request->descripcion_linea as $key => $value) {
        $numero_linea++;
        // Si existe la linea se modifica
        if ( array_key_exists($key, $request->linea_id) ) {
          $linea = PlanPredefinidoLinea::find($request->linea_id[$key]);

        // Si no existe se crea
        } else {
          $linea = new PlanPredefinidoLinea();
          $linea->plan_predefinido_id = $plan->id;
        }

        $linea->linea = $numero_linea;
        $linea->descripcion = $value;
        $linea->cantidad = $request->cantidad_linea[$key];
        $linea->precio = $request->precio_linea[$key];
        $linea->total = $request->total_linea[$key];

        $linea->save();
    }
    $plan->save();
  }
}
