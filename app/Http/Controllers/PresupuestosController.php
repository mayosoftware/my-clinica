<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Carbon\Carbon;

use App\Models\Paciente;
use App\Models\Tratamiento;
use App\Models\Contabilidad\Secuencia;
use App\Models\Contabilidad\TipoIva;
use App\Models\Contabilidad\Ejercicio;
use App\Models\Contabilidad\Presupuesto;
use App\Models\Contabilidad\LineaPresupuesto;
use App\Models\Commons\PlanPredefinido;

class PresupuestosController extends VoyagerBaseController
{

  private $lineasPresupuestoController;

  /**
   * Constructor
   */
  public function __construct()
  {
      $this->lineasPresupuestoController = new LineasPresupuestoController();
  }

  public function index(Request $request) {
    //dd($request);
    $slug = 'presupuestos';

    // GET THE DataType based on the slug
    $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

    // Check permission
    $this->authorize('browse', app($dataType->model_name));

    // Buscar
    $dataTypeContent = $this->applyFilters($request);

    $isServerSide = false;
    $isModelTranslatable = null;
    $search = null;
    $orderBy = null;
    $sortOrder = null;
    $searchable = null;

    $start = $request->start;
    $end = $request->end;

    $birthStart = $request->birthStart;
    $birthEnd = $request->birthEnd;

    // No mostrar los botones de acción
    $showActions = true;

    $view = 'voyager::presupuestos.browse';

    return Voyager::view($view, compact(
        'dataType',
        'dataTypeContent',
        'isModelTranslatable',
        'search',
        'orderBy',
        'sortOrder',
        'searchable',
        'isServerSide',
        'start',
        'end',
        'birthStart',
        'birthEnd',
        'showActions'
    ));

  }

  protected function applyFilters(Request $request) {
    $filter = $request->filter;
    $start = $request->start;
    $end = $request->end;

    $startDate = $start ? Carbon::createFromFormat('d/m/Y',  $start)->startOfDay() : null;
    $endDate = $end ? Carbon::createFromFormat('d/m/Y',  $end)->endOfDay() : null;

    $presupuestos = [];
    if ($startDate || $endDate) {
      $presupuestos = Presupuesto::whereBetween('fecha_presupuesto', [$startDate, $endDate])->get();
    } else {
      // Se cargan presupuestos de hoy
      $presupuestos = Presupuesto::whereDate('fecha_presupuesto', Carbon::today())->get();
    }

    return $presupuestos;
  }

  public function create(Request $request)
  {
      // Tipos iva
      $tiposIva = TipoIva::all();

      // Ejercicio
      $year = Carbon::now()->year;
      $ejercicio = Ejercicio::where([
        ['codigo', '=', $year],
        ['cerrado', false]
      ])->first();

      // TODO: Sino existe ejercicio para el año, se crea

      if ($ejercicio) {
        $secuencia = Secuencia::where([
          ['ejercicio_id', '=', $ejercicio->id],
          ['tipo_secuencia_id', '=', 1] // Secuencia de los presupuestos
        ])->first();
      }

      // Calcular número de factura
      $siguiente_numero_presupuesto = $this->calcularNumeroFactura($ejercicio, $secuencia, true);

      // Tratamientos
      $tratamientos = Tratamiento::all();

      // Planes predefinidos
      $planes = PlanPredefinido::all();

      $data = [
        'tiposIva' => $tiposIva,
        'tratamientos' => $tratamientos,
        'planes'  => $planes,
        'piezas' => $this->piezas,
        'ejercicio' => $ejercicio->id,
        'siguiente_numero_presupuesto' => $siguiente_numero_presupuesto
      ];

      return $this->commonCreate($request, $data, null);
  }

  public function edit(Request $request, $id)
  {
      // Paciente
      $presupuesto = Presupuesto::find($id);
      // Tipos iva
      $tiposIva = TipoIva::all();
      // Tratamientos
      $tratamientos = Tratamiento::all();
      // Ejercicios abiertos
      $ejercicios = Ejercicio::where('cerrado', '=', false)->get();

      $pacienteNombreHeader = $presupuesto->paciente->nombre;
      $pacienteCodigoHeader = $presupuesto->paciente->codigo;

      // Planes predefinidos
      $planes = PlanPredefinido::all();

      $data = [
        'tiposIva' => $tiposIva,
        'paciente' => $presupuesto->paciente,
        'tratamientos' => $tratamientos,
        'planes'  => $planes,
        'pacienteNombreHeader' => $pacienteNombreHeader,
        'pacienteCodigoHeader' => $pacienteCodigoHeader,
        'piezas' => $this->piezas,
        'ejercicios' => $ejercicios
      ];

      return $this->commonEdit($request, $id, $data);
  }

  public function store(Request $request)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Check permission
      $this->authorize('add', app($dataType->model_name));

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->addRows);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      $presupuestoDuplicated = Presupuesto::where('numero', '=', $request->numero)->get();

      if ($presupuestoDuplicated->count() > 0) {
        $message = [
                'message'    => "El número de presupuesto ya existe",
                'alert-type' => 'error',
            ];

        return redirect()->route("voyager.{$dataType->slug}.create")->with($message);
      }

      // Si no hay lineas, se muestra un error
      if (!$request->descripcion_linea) {
        $message = [
                'message'    => "Se debe indicar al menos un tratamiento",
                'alert-type' => 'error',
            ];
        return redirect()->route("voyager.{$dataType->slug}.create")->with($message);
      }

      if (!$request->has('_validate')) {
          $this->makePresupuesto($request);

          if ($request->ajax()) {
              return response()->json(['success' => true, 'data' => $data]);
          }

          return redirect()
              ->route("voyager.{$dataType->slug}.index")
              ->with([
                      'message'    => __('voyager::generic.successfully_added_new')." {$dataType->display_name_singular}",
                      'alert-type' => 'success',
                  ]);
      }
  }

  public function update(Request $request, $id)
  {
      $slug = $this->getSlug($request);

      $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

      // Compatibility with Model binding.
      $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

      $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

      // Check permission
      $this->authorize('edit', $data);

      // Validate fields with ajax
      $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->name, $id);

      if ($val->fails()) {
          return response()->json(['errors' => $val->messages()]);
      }

      $presupuestoDuplicated = Presupuesto::where([
            ['numero', '=', $request->numero],
            ['id', '!=', $id]
        ])->get();

      if ($presupuestoDuplicated->count() > 0) {
        $message = [
                'message'    => "El número de presupuesto ya existe",
                'alert-type' => 'error',
            ];
        // dd('duplicado');
        return redirect()->route("voyager.presupuestos.edit", ['presupuesto' => $id])->with($message);
      }

      // Si no hay lineas, se muestra un error
      if (!$request->descripcion_linea) {
        $message = [
                'message'    => "Se debe indicar al menos un tratamiento",
                'alert-type' => 'error',
            ];

        return redirect()->route("voyager.presupuestos.edit", ['presupuesto' => $id])->with($message);
      }

      if (!$request->ajax()) {
          $this->makePresupuesto($request, $id);

          return redirect()
              ->route("voyager.presupuestos.edit", ['presupuesto' => $id])
              ->with([
                  'message'    => __('voyager::generic.successfully_updated')." {$dataType->display_name_singular}",
                  'alert-type' => 'success',
              ]);
      }
  }

  /**
   * Función que parsea los datos para llamar a la función que almacena el presupuesto
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  private function makePresupuesto(Request $request, $id = null) {
    // Obtener el ejercicio seleccionado y comprobar que no está cerrado
    $ejercicio = Ejercicio::find($request->ejercicio);

    $tipoIva = 1; //TipoIva::find($request->tipoIva);

    $paciente = Paciente::find($request->paciente_id);

    $observaciones = $request->observaciones;
    $total = $request->total;
    $totalIva = 0; //$request->totalIvaPresupuesto;
    $totalDescuento = $request->total_descuento;
    $numero_presupuesto = $request->numero;
    $nombre = $request->nombre;

    return $this->storePresupuesto($request, $ejercicio, $tipoIva, $paciente, $observaciones,
      $total, $totalIva, $totalDescuento, $numero_presupuesto, $nombre, $id);
  }

  public function storePresupuesto($request, $ejercicio, $tipoIva, $paciente, $observaciones,
    $total, $totalIva, $totalDescuento, $numero_presupuesto = null, $nombre, $id) {
      // Obtener el ejercicio seleccionado y comprobar que no está cerrado
      if ($ejercicio->cerrado) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "El ejercicio " . $ejercicio->codigo . " está marcado como cerrado",
                'alert-type' => 'error',
            ]);
      }

      $secuencia = Secuencia::where([
        ['ejercicio_id', '=', $ejercicio->id],
        ['tipo_secuencia_id', '=', 1] // Secuencia de los presupuestos
      ])->first();

      // Calcular número de factura
      \Log::debug('numero: ' . $numero_presupuesto);
      if ($numero_presupuesto == null) {
        $numero_presupuesto = $this->calcularNumeroFactura($ejercicio, $secuencia, true);
      }

      if ($id == null) {
          $presupuesto = new Presupuesto();
          $presupuesto->indice = $paciente->nextIndicePresupuesto();
      } else {
        $presupuesto = Presupuesto::find($id);
      }

      $presupuesto->nombre = $nombre;
      $presupuesto->numero = $numero_presupuesto;
      $presupuesto->secuencia_id = $secuencia->id;
      $presupuesto->codigo_paciente = $paciente->codigo;
      $presupuesto->paciente_id = $paciente->id;
      $presupuesto->nombre_paciente = $paciente->nombre;
      $presupuesto->cif = $paciente->cif;
      $presupuesto->direccion = $paciente->direccion;
      $presupuesto->ciudad = $paciente->poblacion ? $paciente->poblacion->nombre : null;
      $presupuesto->provincia = $paciente->poblacion ? $paciente->poblacion->provincia->nombre : null;
      $presupuesto->pais = $paciente->poblacion ? $paciente->poblacion->provincia->comunidadAutonoma->pais->nombre : null;
      // $presupuesto->codigo_postal = $actuacion->paciente()->;
      $presupuesto->telefono = $paciente->telefono;
      $presupuesto->email = $paciente->email;
      $presupuesto->fecha_presupuesto = Carbon::now();
      $presupuesto->fecha_vencimiento = Carbon::now()->addMonths(1);
      $presupuesto->anulado = false;
      $presupuesto->observaciones = $observaciones;
      $presupuesto->total = $total;
      $presupuesto->total_iva = $totalIva;
      // $presupuesto->total_irpf = ;
      $presupuesto->total_descuento = $totalDescuento;
      $presupuesto->user_id = auth()->user()->id;

      $presupuesto->save();

      // Se guardan las lineas
      if ($id == null) {
        $this->lineasPresupuestoController->guardarLineas($request, $presupuesto);
        $secuencia->valor_siguiente += 1;
        $secuencia->save();
      } else {
        $this->lineasPresupuestoController->actualizarLineas($request, $presupuesto);
      }

      return $presupuesto;
  }

  /**
   * Función que parsea los datos para llamar a la función que almacena el presupuesto
   * @param  Request $request [description]
   * @return [type]           [description]
   */
  public function makePresupuestoFromTratamiento($tratamiento, $ejercicio, $observaciones, $total, $totalDescuento, $nombre) {
    $tipoIva = 1; //TipoIva::find($request->tipoIva);

    $paciente = Paciente::find($tratamiento->paciente_id);

    $totalIva = 0; //$request->totalIvaPresupuesto;

    return $this->storePresupuestoFromTratamiento($tratamiento, $ejercicio, $tipoIva, $paciente, $observaciones,
      $total, $totalIva, $totalDescuento, $nombre);
  }
  /**
   * Guardar presupuesto desde un tratamiento
   * @param  [type] $request            [description]
   * @param  [type] $ejercicio          [description]
   * @param  [type] $tipoIva            [description]
   * @param  [type] $paciente           [description]
   * @param  [type] $observaciones      [description]
   * @param  [type] $total              [description]
   * @param  [type] $totalIva           [description]
   * @param  [type] $totalDescuento     [description]
   * @param  [type] $numero_presupuesto [description]
   * @param  [type] $id                 [description]
   * @return [type]                     [description]
   */
  public function storePresupuestoFromTratamiento($tratamiento, $ejercicio, $tipoIva, $paciente, $observaciones,
    $total, $totalIva, $totalDescuento, $nombre) {
      // Obtener el ejercicio seleccionado y comprobar que no está cerrado
      if ($ejercicio->cerrado) {
        return redirect()
            ->route("voyager.agenda.index")
            ->with([
                'message'    => "El ejercicio " . $ejercicio->codigo . " está marcado como cerrado",
                'alert-type' => 'error',
            ]);
      }

      $secuencia = Secuencia::where([
        ['ejercicio_id', '=', $ejercicio->id],
        ['tipo_secuencia_id', '=', 1] // Secuencia de los presupuestos
      ])->first();

      $numero_presupuesto = $this->calcularNumeroFactura($ejercicio, $secuencia, true);

      $presupuesto = new Presupuesto();

      $presupuesto->numero = $numero_presupuesto;
      $presupuesto->secuencia_id = $secuencia->id;
      $presupuesto->codigo_paciente = $paciente->codigo;
      $presupuesto->paciente_id = $paciente->id;
      $presupuesto->nombre_paciente = $paciente->nombre;
      $presupuesto->cif = $paciente->cif;
      $presupuesto->direccion = $paciente->direccion;
      $presupuesto->ciudad = $paciente->poblacion ? $paciente->poblacion->nombre : null;
      $presupuesto->provincia = $paciente->poblacion ? $paciente->poblacion->provincia->nombre : null;
      $presupuesto->pais = $paciente->poblacion ? $paciente->poblacion->provincia->comunidadAutonoma->pais->nombre : null;
      // $presupuesto->codigo_postal = $actuacion->paciente()->;
      $presupuesto->telefono = $paciente->telefono;
      $presupuesto->email = $paciente->email;
      $presupuesto->fecha_presupuesto = Carbon::now();
      $presupuesto->fecha_vencimiento = Carbon::now()->addMonths(1);
      $presupuesto->anulado = false;
      $presupuesto->observaciones = $observaciones;
      $presupuesto->total = $total;
      $presupuesto->total_iva = $totalIva;
      // $presupuesto->total_irpf = ;
      $presupuesto->total_descuento = $totalDescuento;
      $presupuesto->user_id = auth()->user()->id;

      $presupuesto->nombre = $nombre;
      $presupuesto->indice = $paciente->nextIndicePresupuesto();

      $presupuesto->save();

      // Se guardan las lineas

      $this->lineasPresupuestoController->guardarLineasFromTratamiento($tratamiento, $presupuesto);
      $secuencia->valor_siguiente += 1;
      $secuencia->save();

      return $presupuesto;
  }

  public function print(Request $request) {
    $id = $request->id;

    $presupuesto = Presupuesto::find($id);

    $date = date('d-m-Y H:i:s');

    $view =  \View::make('templates.pdf.presupuesto',
      compact('presupuesto',
      'date'
      ))
      ->render();

    $pdf = \App::make('dompdf.wrapper');
    $pdf->loadHTML($view);
    return $pdf->stream('invoice');
  }
}
