<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contabilidad\LineaFactura;

use App\Models\TratamientoPaciente;

class LineasFacturaController extends Controller
{
  /**
   * Constructor
   */
  public function __construct()
  {

  }

  public function guardarLineas(Request $request, $factura)
  {
      $numero_linea = 0;
      // Se recorren las líneas
      foreach ($request->descripcion_linea as $key => $value) {
          $numero_linea++;
          $linea = new LineaFactura();

          $linea->factura_id = $factura->id;
          $linea->linea = $numero_linea;
          $linea->descripcion = $value;
          $linea->pieza = $request->pieza_linea[$key];
          $linea->tipo_iva_id = 1;
          $linea->cantidad = $request->cantidad_linea[$key];
          $linea->importe_unitario = $request->precio_linea[$key];
          $linea->total = $request->total_linea[$key];
          $linea->total_iva = 0;
          $linea->porcentaje_descuento = $request->descuento_linea[$key];
          $linea->save();
      }
      //$factura->numero_lineas = $numero_linea;
      $factura->save();
  }

  public function actualizarLineas(Request $request, $factura) {
    // Ver si hay lineas que borrar
    $lineasGuardar = $request->linea_id;
    $lineasBorrar = $factura->lineas->filter(function($item) use ($lineasGuardar) {
        return !in_array($item->id, $lineasGuardar);
    });

    foreach ($lineasBorrar as $value) {
      LineaFactura::destroy($value->id);
    }

    $numero_linea = 0;
    foreach ($request->descripcion_linea as $key => $value) {
        $numero_linea++;
        // Si existe la linea se modifica
        if ( array_key_exists($key, $request->linea_id) &&
          !empty($request->linea_id[$key]) ) {
          $linea = LineaFactura::find($request->linea_id[$key]);

        // Si no existe se crea
        } else {
          $linea = new LineaFactura();
          $linea->factura_id = $factura->id;
        }

        $linea->linea = $numero_linea;
        $linea->descripcion = $value;
        $linea->pieza = $request->pieza_linea[$key];
        $linea->tipo_iva_id = 1;
        $linea->cantidad = $request->cantidad_linea[$key];
        $linea->importe_unitario = $request->precio_linea[$key];
        $linea->total = $request->total_linea[$key];
        $linea->total_iva = 0;
        $linea->porcentaje_descuento = $request->descuento_linea[$key];

        $linea->save();
    }
    $factura->save();
  }

  public function guardarLineasFromTratamiento($tratamiento, $factura)
  {
      $numero_linea = 0;
      // Se recorren las líneas
      foreach ($tratamiento->lineas as $linea) {
          $numero_linea++;
          $lineaFactura = new LineaFactura();

          $lineaFactura->factura_id = $factura->id;
          $lineaFactura->linea = $numero_linea;
          $lineaFactura->descripcion = $linea->descripcion;
          $lineaFactura->pieza = $linea->pieza;
          $lineaFactura->tipo_iva_id = 1;
          $lineaFactura->cantidad = $linea->cantidad;
          $lineaFactura->importe_unitario = $linea->importe_unitario;
          $lineaFactura->total = $linea->total;
          $lineaFactura->total_iva = 0;
          $lineaFactura->porcentaje_descuento = $linea->porcentaje_descuento;
          $lineaFactura->save();
      }
      //$factura->numero_lineas = $numero_linea;
      $factura->save();
  }
}
