<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\TratamientoPacienteLinea;
use App\Models\TratamientoPacienteProgramacion;

use Carbon\Carbon;

class TratamientosPacienteLineasController extends Controller
{
  /**
   * Constructor
   */
  public function __construct()
  {

  }

  /*
  * Guardar línea individual. Se llama por ajax desde crearLinea
  */
  private function guardarLinea(Request $request, $tratamiento)
  {
      // TODO: Añadir validación de voyager
      // TODO: Añadir permisos usuario
      $numero_linea = $tratamiento->numero_lineas + 1;
      $linea = new TratamientoPacienteLinea();

      $linea->tratamiento_paciente_id = $tratamiento->id;
      $linea->linea = $numero_linea;
      $linea->descripcion = $request->descripcion_linea;
      $linea->pieza = $request->pieza_linea;
      $linea->zona = 0;
      $linea->tipo_iva_id = 1;
      $linea->cantidad = $request->cantidad_linea;
      $linea->importe_unitario = $request->precio_linea;
      $linea->total = $request->total_linea;
      $linea->total_iva = 0;
      $linea->porcentaje_descuento = $request->descuento_linea;
      $linea->cobrada = $request->cobrada;
      $linea->fecha_cobro = $request->fecha_cobro;
      $linea->save();

      $tratamiento->numero_lineas = $numero_linea;
      $tratamiento->save();

      return $linea->id;
  }

  public function guardarLineas(Request $request, $tratamiento)
  {
      $numero_linea = 0;
      // Se recorren las líneas
      foreach ($request->descripcion_linea as $key => $value) {
          $numero_linea++;
          $linea = new TratamientoPacienteLinea();

          $linea->tratamiento_paciente_id = $tratamiento->id;
          $linea->linea = $numero_linea;
          $linea->descripcion = $value;
          $linea->pieza = $request->pieza_linea[$key];
          $linea->zona = 0;
          $linea->tipo_iva_id = 1;
          $linea->cantidad = $request->cantidad_linea[$key];
          $linea->importe_unitario = $request->precio_linea[$key];
          $linea->total = $request->total_linea[$key];
          $linea->total_iva = 0;
          $linea->porcentaje_descuento = $request->descuento_linea[$key];
          $linea->cobrada = $request->cobrada[$key];
          $linea->fecha_cobro = $request->fecha_cobro[$key];
          $linea->save();
      }
      $tratamiento->numero_lineas = $numero_linea;
      $tratamiento->save();
  }

  public function actualizarLineas(Request $request, $tratamiento) {
    // Ver si hay lineas que borrar
    $lineasGuardar = $request->linea_id;
    $lineasBorrar = $tratamiento->lineas->filter(function($item) use ($lineasGuardar) {
        return !in_array($item->id, $lineasGuardar);
    });

    foreach ($lineasBorrar as $value) {
      TratamientoPacienteLinea::destroy($value->id);
    }

    $numero_linea = 0;
    foreach ($request->descripcion_linea as $key => $value) {
        $numero_linea++;
        // Si existe la linea se modifica
        if ( array_key_exists($key, $request->linea_id) &&
          !empty($request->linea_id[$key])) {

          $linea = TratamientoPacienteLinea::find($request->linea_id[$key]);

        // Si no existe se crea
        } else {
          $linea = new TratamientoPacienteLinea();
          $linea->tratamiento_paciente_id = $tratamiento->id;
        }

        $linea->linea = $numero_linea;
        $linea->descripcion = $value;
        $linea->pieza = $request->pieza_linea[$key];
        $linea->zona = 0;
        $linea->tipo_iva_id = 1;
        $linea->cantidad = $request->cantidad_linea[$key];
        $linea->importe_unitario = $request->precio_linea[$key];
        $linea->total = $request->total_linea[$key];
        $linea->total_iva = 0;
        $linea->porcentaje_descuento = $request->descuento_linea[$key];
        $linea->realizada = $request->lineaRealizada[$key] ?? false;
        \Log::debug('actualizar linea '.$linea->descripcion );
        if ($linea->realizada) {
          if ( ($request->lineaRealizada[$key] ?? false) && $linea->fecha_realizacion == null) {
              \Log::debug('se actualiza fecha linea '.$linea->descripcion );
              $linea->fecha_realizacion = Carbon::now();
          }
        } else {
          $linea->fecha_realizacion = null;
        }
        $linea->cobrada = $request->cobrada[$key] ?? false;
        $linea->fecha_cobro = $request->fecha_cobro[$key];

        $linea->save();
    }
    $tratamiento->numero_lineas = $numero_linea;
    $tratamiento->save();
  }

  public function eliminarLinea($linea_id) {
    $linea = TratamientoPacienteLinea::find($linea_id);
    // Si tiene programación se borra
    if ($linea->programacion) $linea->programacion->delete();
    $linea->delete();
  }

  public function buscarLineaPorProgramacion($programacion_id) {
    $linea = TratamientoPacienteLinea::where('programacion_id', '=', $programacion_id)->first();

    return $linea;
  }

}
