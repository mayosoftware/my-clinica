<?php

namespace App\Http\Controllers\Pusher;

use Illuminate\Http\Request;

use Pusher\Pusher;
use App\Events\AgendaUpdated;

class Pushercontroller {
  public $pusher;

  /**
   * Constructor
   */
  public function __construct()
  {
    $this->pusher = $this->connectPusher();
  }

  /**
   * Conectar cobn pusher e iniciar el objeto
   * @return [type] [description]
   */
  protected function connectPusher()
  {
      $appId = env('PUSHER_APP_ID', '');
      $appKey = env('PUSHER_APP_KEY', '');
      $appSecret = env('PUSHER_APP_SECRET', '');
      $appOptions = [
          'cluster' => env('PUSHER_APP_CLUSTER', ''),
          'encrypted' => true
      ];

      $pusher = new Pusher($appKey, $appSecret, $appId, $appOptions);

      return $pusher;
  }

  // TODO: Se realiza de esta forma porque el evento o broadcast directamente no funciona
  /**
   * Se envía un evento para notificar que se ha producido un cambio en el
   * tratamiento
   *
   * @param  [type] $tratamiento_id id del tratamiento que se ha modificado
   * @param  [type] $event_id       id del evento en la agenda
   * @return [type]                 [description]
   */
  public function enviarPusherAgenda($event, $programacion_id, $socket_id, $tratamiento_id = null) {
    /* Enable pusher logging - I used an anonymous class and the Monolog */
    $this->pusher->set_logger(new class {
        public function log($msg)
        {
            \Log::info($msg);
        }
    });

    // Enviar evento de actualizacion de agenda
    // broadcast(new AgendaUpdated($programacion_id, $event_id))->toOthers();

    $data = [
      'programacion_id' => $programacion_id,
      'tratamiento_id' => $tratamiento_id,
      'user_id' => auth()->user()->id
    ];

    $this->pusher->trigger( 'agenda', $event, $data, $socket_id);
  }

  public function ajaxEnviarPusherAgenda(Request $request) {
    \Log::debug($request);
    $event = $request->event;
    $programacion_id = $request->programacion_id;
    $socket_id = $request->socket_id;

    $this->enviarPusherAgenda($event, $programacion_id, $socket_id, null);

  }
}
