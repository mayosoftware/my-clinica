<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNombreYNumeroPresupuestos extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('contabilidad.presupuestos', function (Blueprint $table) {
        $table->string('nombre')->default('Presupuesto')->after('id');
        $table->integer('indice')->nullable()->after('nombre');
    });
    Schema::table('contabilidad.facturas', function (Blueprint $table) {
        $table->string('nombre')->default('Presupuesto')->after('id');
        $table->integer('indice')->nullable()->after('nombre');
    });

    //Actualizar presupuestos
    DB::update("update contabilidad.presupuestos p
      set indice = q.n, nombre = 'Presupuesto ' || q.n::text
      FROM (
      SELECT
      	pre.id,
      	pre.paciente_id,
      	row_number() over (partition by pre.paciente_id ORDER by pre.id) AS n
      FROM contabilidad.presupuestos pre
      ORDER by 2
      ) q
      WHERE p.id = q.id;");

    //Actualizar facturas
    DB::update("update contabilidad.facturas p
      set indice = q.n, nombre = 'Factura ' || q.n::text
      FROM (
      SELECT
      	pre.id,
      	pre.paciente_id,
      	row_number() over (partition by pre.paciente_id ORDER by pre.id) AS n
      FROM contabilidad.facturas pre
      ORDER by 2
      ) q
      WHERE p.id = q.id;");
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('contabilidad.presupuestos', function (Blueprint $table) {
        $table->dropColumn('nombre');
        $table->dropColumn('indice');
    });
    Schema::table('contabilidad.facturas', function (Blueprint $table) {
        $table->dropColumn('nombre');
        $table->dropColumn('indice');
    });
  }
}
