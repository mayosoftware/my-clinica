<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MovimientoCajaConceptoNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('movimientos_caja', function (Blueprint $table) {
          $table->text('concepto')->nullable()->change();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('movimientos_caja', function (Blueprint $table) {
          $table->text('concepto')->nullable(false)->change();
      });
    }
}
