<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanesTratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
      Schema::create('commons.planes_predefinidos', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nombre');
          $table->string('observaciones');
          $table->decimal('total', 8, 2);
          $table->timestampsTz();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commons.planes_predefinidos');
    }
}
