<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTratamientosLineasCobros extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('tratamientos_paciente_lineas', function (Blueprint $table) {
          $table->boolean('cobrada')->default(false)->after('realizada_programacion_id');
          $table->timestamp('fecha_cobro')->nullable()->after('cobrada');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('tratamientos_paciente_lineas', function (Blueprint $table) {
          $table->dropColumn('cobrada');
          $table->dropColumn('fecha_cobro');
      });
    }
}
