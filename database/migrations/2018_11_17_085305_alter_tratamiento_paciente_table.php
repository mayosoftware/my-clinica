<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTratamientoPacienteTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::table('tratamientos_paciente', function (Blueprint $table) {
        $table->string('nombre')->default('Plan de tratamiento')->after('id');
        $table->integer('numero')->nullable()->after('nombre');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::table('tratamientos_paciente', function (Blueprint $table) {
        $table->dropColumn('nombre');
        $table->dropColumn('numero');
    });
  }
}
