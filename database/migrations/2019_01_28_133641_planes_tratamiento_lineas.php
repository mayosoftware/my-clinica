<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PlanesTratamientoLineas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('commons.planes_predefinidos_lineas', function (Blueprint $table) {
          $table->increments('id');
          $table->unsignedInteger('plan_predefinido_id');
          $table->foreign('plan_predefinido_id')->references('id')->on('commons.planes_predefinidos');
          $table->integer('linea');
          $table->string('descripcion');
          $table->integer('cantidad')->default(1);
          $table->decimal('precio', 8, 2)->default(0);
          $table->decimal('total', 8, 2);
          $table->timestampsTz();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commons.planes_predefinidos_lineas');
    }
}
