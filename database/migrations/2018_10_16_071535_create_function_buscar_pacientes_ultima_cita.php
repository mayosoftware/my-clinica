<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFunctionBuscarPacientesUltimaCita extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('
          CREATE or replace FUNCTiON pacientes_ultima_cita_entre(_start date, _end date, _start_birthdate date, _end_birthdate date)
          RETURNS TABLE (
          	id bigint,
            tipo_paciente_id SMALLINT,
            mutua VARCHAR(255),
            nif VARCHAR(20),
            nombre VARCHAR(100),
            codigo VARCHAR(50),
            telefono VARCHAR(20),
            movil VARCHAR(20),
            fax VARCHAR(20),
            email VARCHAR(100),
            direccion VARCHAR(100),
            poblacion varchar(50),
            provincia varchar(50),
            fecha_nacimiento DATE,
            notas TEXT,
            motivo_consulta TEXT,
            sintomas TEXT,
            created_at TIMESTAMP(0) WITH TIME ZONE,
            updated_at TIMESTAMP(0) WITH TIME ZONE,
            deleted_at TIMESTAMP(0) WITH TIME ZONE,
            created_by INTEGER,
            updated_by INTEGER,
            saldo NUMERIC(10,2),
            cuenta_contabilidad VARCHAR(50),
            numero_poliza VARCHAR(100),
            codigo_postal VARCHAR(8),
            fecha_ultima_cita date
          )
          AS
          $BODY$

          select
          	id,
              tipo_paciente_id,
              mutua,
              nif,
              nombre,
              codigo,
              telefono,
              movil,
              fax,
              email,
              direccion,
              poblacion,
              provincia,
              fecha_nacimiento,
              notas,
              motivo_consulta,
              sintomas,
              created_at,
              updated_at,
              deleted_at,
              created_by,
              updated_by,
              saldo,
              cuenta_contabilidad,
              numero_poliza,
              codigo_postal,
              fecha
          from (
              select DISTINCT on (p.id)
                  p.*,
                  pro.fecha,
                  plo.nombre as poblacion,
                  pca.nombre as provincia
              from tratamientos_paciente t
              join pacientes p
              on t.paciente_id = p.id
              left join poblaciones plo
              on p.poblacion_id = plo.id
              left join provincias pca
              on plo.provincia_id = pca.id
              join tratamientos_paciente_lineas l
              on t.id = l.tratamiento_paciente_id
              join tratamientos_paciente_programaciones pro
              on l.programacion_id = pro.id
              order by p.id, pro.fecha DESC, pro.hora_inicio desc, pro.hora_fin desc
          ) q
          where
          	(_start is null or q.fecha >= _start) and
              (_end is null or q.fecha <= _end) and
              (_start_birthdate is null or q.fecha_nacimiento >= _start_birthdate) and
              (_end_birthdate is null or q.fecha_nacimiento <= _end_birthdate)

          $BODY$
          LANGUAGE sql;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP FUNCTiON pacientes_ultima_cita_entre(_start date, _end date, _start_birthdate date, _end_birthdate date)');
    }
}
