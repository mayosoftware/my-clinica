<?php

use Illuminate\Database\Seeder;
use App\Models\Commons\PlanPredefinido;
use App\Models\Commons\PlanPredefinidoLinea;

class PlanPredefinidoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (PlanPredefinido::count() == 0) {
          PlanPredefinido::create([
            'nombre' => 'Plan predefinido 1',
            'observaciones' => 'Lorem ipsum dolor',
            'total' => 100.0
          ]);

          PlanPredefinido::create([
            'nombre' => 'Plan predefinido 2',
            'observaciones' => 'Lorem ipsum dolor',
            'total' => 220.0
          ]);

          // Plan 1
          PlanPredefinidoLinea::create([
            'plan_predefinido_id' => 1,
            'linea' => 1,
            'descripcion' => 'Lorem ipsum dolor',
            'cantidad' => 1,
            'precio' => 20,
            'total' => 20
          ]);
          PlanPredefinidoLinea::create([
            'plan_predefinido_id' => 1,
            'linea' => 2,
            'descripcion' => 'Lorem ipsum dolor',
            'cantidad' => 4,
            'precio' => 20,
            'total' => 80
          ]);

          // Plan 2
          PlanPredefinidoLinea::create([
            'plan_predefinido_id' => 2,
            'linea' => 1,
            'descripcion' => 'Lorem ipsum dolor',
            'cantidad' => 5,
            'precio' => 20,
            'total' => 100
          ]);
          PlanPredefinidoLinea::create([
            'plan_predefinido_id' => 2,
            'linea' => 2,
            'descripcion' => 'Lorem ipsum dolor',
            'cantidad' => 1,
            'precio' => 100,
            'total' => 100
          ]);
          PlanPredefinidoLinea::create([
            'plan_predefinido_id' => 2,
            'linea' => 3,
            'descripcion' => 'Lorem ipsum dolor',
            'cantidad' => 2,
            'precio' => 10,
            'total' => 20
          ]);
        }
    }
}
