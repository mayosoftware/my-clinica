<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/login');
});

Route::get('tests', 'Chat\MessageController@tests');

Route::group(['prefix'=>'chat'], function() {
  Route::get('home', 'Chat\HomeController@index');
  Route::get('message/{id}', 'Chat\MessageController@chatHistory')->name('message.read');

  Route::group(['prefix'=>'ajax', 'as'=>'ajax::'], function() {
     Route::post('message/send', 'Chat\MessageController@ajaxSendMessage')->name('message.new');
     Route::delete('message/delete/{id}', 'Chat\MessageController@ajaxDeleteMessage')->name('message.delete');
     Route::post('messages/allNotSeen', 'Chat\MessageController@ajaxMessagesNotSeen')->name('messages.allNotSeen');
     Route::post('messages/markAllAsSeen', 'Chat\MessageController@ajaxMarkAllMessagesAsSeen')->name('messages.markAllAsSeen');
  });
});

Route::get('/videotutorial', 'VideotutorialesController@index')->name('videotutorial');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::get('/', 'DashboardController@index')->name('voyager.dashboard');
    Route::get('dashboard/searchPacienteFreeText', 'DashboardController@searchPacienteFreeText')->name('searchPaciente');

    // Tratamientos
    Route::get('paciente/{id}/tratamientos-paciente/create', 'TratamientosPacienteController@createWithPaciente')->name('admin.tratamiento-paciente.create');
    Route::get('paciente/{id_paciente}/tratamiento/{id_tratamiento}/edit', 'TratamientosPacienteController@createWithPaciente')->name('admin.tratamientoPaciente.edit');
    Route::get('tratamiento-paciente/{id_tratamiento}/print', 'TratamientosPacienteController@print')->name('admin.tratamientoPaciente.print');
    Route::post('tratamiento-paciente/{id_tratamiento}/sendMail', 'TratamientosPacienteController@sendMail')->name('admin.tratamientoPaciente.sendMail');
    Route::get('tratamiento-paciente/{id_tratamiento}/generatePresupuesto', 'TratamientosPacienteController@generatePresupuesto')->name('admin.tratamientoPaciente.generatePresupuesto');
    Route::get('tratamiento-paciente/{id_tratamiento}/generateFactura', 'TratamientosPacienteController@generateFactura')->name('admin.tratamientoPaciente.generateFactura');
    Route::get('tratamiento-paciente/{id_tratamiento}/generateCobro', 'TratamientosPacienteController@generateCobro')->name('admin.tratamientoPaciente.generateCobro');
    Route::post('tratamiento-paciente/{id_tratamiento}/ajaxGenerateCobro', 'TratamientosPacienteController@ajaxGenerateCobro')->name('admin.tratamientoPaciente.ajaxGenerateCobro');
    Route::post('tratamiento-paciente/{id_tratamiento}/programarTratamiento', 'TratamientosPacienteController@programarTratamiento')->name('admin.tratamientoPaciente.programarTratamiento');
    Route::post('tratamiento-paciente/{id_linea}/actualizarEstadoLinea', 'TratamientosPacienteController@actualizarEstadoLinea')->name('admin.tratamientoPaciente.actualizarEstadoLinea');
    Route::post('tratamiento-paciente/{id_tratamiento}/crearLinea', 'TratamientosPacienteController@crearLinea')->name('admin.tratamientoPaciente.crearLinea');
    Route::post('tratamiento-paciente/{id_tratamiento}/eliminarLinea/{id_linea}', 'TratamientosPacienteController@eliminarLinea')->name('admin.tratamientoPaciente.eliminarLinea');
    Route::put('tratamiento-paciente/{id_linea}/actualizarPieza', 'TratamientosPacienteController@actualizarPieza')->name('admin.tratamientoPaciente.actualizarPieza');
    Route::put('tratamiento-paciente/{id_linea}/actualizarDescripcion', 'TratamientosPacienteController@actualizarDescripcion')->name('admin.tratamientoPaciente.actualizarDescripcion');
    Route::put('tratamiento-paciente/{id_linea}/actualizarImportes', 'TratamientosPacienteController@actualizarImportes')->name('admin.tratamientoPaciente.actualizarImportes');
    Route::delete('tratamiento-paciente/{tratamiento}', 'TratamientosPacienteController@destroyAll')->name('admin.tratamientoPaciente.destroyAll');
    Route::post('tratamiento-paciente/createFromPresupuesto/{presupuesto}', 'TratamientosPacienteController@createFromPresupuesto')->name('admin.tratamientoPaciente.createFromTratamiento');


    // Programaciones
    Route::post('programaciones/{fecha}/getProgramacionesDesde', 'TratamientosPacienteProgramacionesController@getProgramacionesDesde')->name('admin.programaciones.getProgramacionesDesde');
    Route::post('programaciones/{fecha}/getProgramaciones', 'TratamientosPacienteProgramacionesController@getProgramaciones')->name('admin.programaciones.getProgramaciones');
    Route::post('programaciones/{id}/getProgramacion', 'TratamientosPacienteProgramacionesController@getProgramacion')->name('admin.programaciones.getProgramacion');
    Route::get('programaciones/siguienteProgramacionPaciente/{id}', 'TratamientosPacienteProgramacionesController@getSiguienteProgramacionPaciente')->name('admin.programaciones.getSiguienteProgramacionPaciente');
    Route::delete('programaciones/{id}/destroy', 'TratamientosPacienteProgramacionesController@destroy')->name('admin.programaciones.destroy');
    Route::post('programaciones/storeFromModal/', 'TratamientosPacienteProgramacionesController@storeFromModal');

    Route::post('programaciones/ajaxCrearProgramacionLinea/{id}', 'TratamientosPacienteProgramacionesController@ajaxCrearProgramacionLinea')->name('admin.programaciones.ajaxCrearProgramacionLinea');
    Route::delete('programaciones/ajaxEliminarProgramacionLinea/{id}', 'TratamientosPacienteProgramacionesController@ajaxEliminarProgramacionLinea')->name('admin.programaciones.ajaxEliminarProgramacionLinea');

    // Obtener programación por id
    Route::get('programaciones/ajaxLoadProgramacion/{id}', 'TratamientosPacienteProgramacionesController@ajaxLoadProgramacion')->name('admin.programaciones.ajaxLoadProgramacion');

    // Agenda
    Route::post('programaciones/ajaxProgramacionesDoctor/{id}', 'TratamientosPacienteProgramacionesController@ajaxGetProgramacionesDoctor')->name('admin.programaciones.ajaxProgramacionesDoctor');
    Route::post('programaciones/ajaxProgramacion/{id}', 'TratamientosPacienteProgramacionesController@ajaxGetProgramacion')->name('admin.programaciones.ajaxProgramacion');
    Route::post('programaciones/ajaxProgramacionEntreFechas', 'TratamientosPacienteProgramacionesController@ajaxGetProgramacionesEntreFechas')->name('admin.programaciones.ajaxProgramacionEntreFechas');

    // Programaciones desde modal de agenda
    Route::post('programaciones/modalUpdate/{id}', 'TratamientosPacienteProgramacionesController@updateFromModal')->name('admin.programaciones.modalUpdate');
    Route::post('programaciones/modalStore/', 'TratamientosPacienteProgramacionesController@storeFromModal')->name('admin.programaciones.modalStore');
    Route::delete('programaciones/{id}/ajaxDestroy', 'TratamientosPacienteProgramacionesController@ajaxDestroy')->name('admin.programaciones.ajaxDestroy');

    Route::get('ajax/buscarTratamientos', 'TratamientosPacienteProgramacionesController@buscarTratamientos')->name('buscarTratamientos');

    // Consultar eventos por mes
    Route::post('programaciones/eventsByMonth/{month}', 'TratamientosPacienteProgramacionesController@ajaxGetEventsByMonth')->name('admin.programaciones.eventsByMonth');


    Route::post('agenda/actuacionesDoctor/{id}', 'ActuacionesPacienteController@getActuacionesDoctor');
    Route::put('agenda/{id}/cobrar', 'ActuacionesPacienteController@cobrarActuacion')->name('admin.agenda.cobrar');
    Route::put('agenda/{id}/facturar', 'ActuacionesPacienteController@facturarActuacion')->name('admin.agenda.facturar');

    // Etiquetas
    Route::get('etiquetas', 'EtiquetasController@index')->name('admin.etiquetas.index');
    Route::post('etiquetas/filter', 'EtiquetasController@filter')->name('admin.etiquetas.filter');
    Route::get('etiquetas/print', 'EtiquetasController@print')->name('admin.etiquetas.print');

    // Rutas relacionadas con Pacientes
    Route::get('pacientes/{id}/actuaciones', 'PacientesController@getActuaciones')->name('admin.paciente.actuaciones');
    Route::post('pacientes/ajaxBuscarPacientePotencial', 'PacientesController@ajaxBuscarPacientePotencial')->name('admin.paciente.ajaxBuscarPacientePotencial');
    Route::post('pacientes/ajaxGuardarPacientePotencial', 'PacientesController@ajaxGuardarPacientePotencial')->name('admin.paciente.ajaxGuardarPacientePotencial');

    // Tratamiento de ficheros
    Route::post('media/get_files', 'UploadFilesController@files');
    Route::post('media/upload_file', 'UploadFilesController@upload');
    Route::post('media/delete_file', 'UploadFilesController@delete');

    // Movimientos caja
    Route::get('movimientes-caja/filter', 'MovimientosCajaController@filter')->name('admin.movimientos-caja.filter');
    Route::get('movimientes-caja/print', 'MovimientosCajaController@imprimirMovimientos')->name('admin.movimientos-caja.print');
    Route::get('movimientes-caja/tratamiento/{id}', 'MovimientosCajaController@ajaxMovimientosCajaTratamiento')->name('admin.movimientos-caja.tratamiento');
    
    // Facturas
    Route::get('facturas/print/{id}', 'FacturasController@print')->name('admin.facturas.print');
    Route::post('facturas/printBetweenDates', 'FacturasController@printBetweenDates')->name('admin.facturas.printBetweenDates');

    // Presupuestos
    Route::get('presupuestos/print/{id}', 'PresupuestosController@print')->name('admin.presupuestos.print');

    // Localización
    Route::get('pais/{pais}/comunidadesAutonomas', 'PaisesController@getComunidadesAutonomas');
    Route::get('comunidadAutonoma/{comunidadAutonoma}/provincias', 'ComunidadesAutonomasController@getProvincias');
    Route::get('provincia/{provincia}/poblaciones', 'ProvinciasController@getPoblaciones');

    // Pusher
    Route::post('sendPusher/agenda', 'Pusher\Pushercontroller@ajaxEnviarPusherAgenda')->name('sendPusher.agenda');

    // Planes predefinidos
    Route::get('planesPredefinidos/find/{plan}', 'Commons\PlanesPredefinidosController@ajaxFindPlanById')->name('planesPredefinidos.find');
});
