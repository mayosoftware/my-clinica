var tooltips = {
    today: 'Dia de hoy',
    clear: 'Borrar selección',
    close: 'Cerrar el selector',
    selectMonth: 'Seleccionar mes',
    prevMonth: 'Mes anterior',
    nextMonth: 'Mes siguiente',
    selectYear: 'Seleccionar año',
    prevYear: 'Año anterior',
    nextYear: 'Año siguiente',
    incrementHour: 'Incrementar hora',
    pickHour: 'Seleccionar hora',
    decrementHour: 'Decrementar hora',
    incrementMinute: 'Incrementar minuto',
    pickMinute: 'Elegir minuto',
    decrementMinute: 'Decrementar minuto',
    incrementSecond: 'Incrementar segundo',
    pickSecond: 'Elegir segundo',
    decrementSecond: 'Decrementar segundo'
};

function configDatePicker(id) {
    $(id).datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        calendarWeeks: true,
        toolbarPlacement: 'bottom',
        showTodayButton: true,
        showClear: true,
        showClose: true,
        tooltips: tooltips
    });
}

function configDatePickerInline(id) {
    $(id).datetimepicker({
        locale: 'es',
        format: 'DD/MM/YYYY',
        inline: true,
        sideBySide: false,
        calendarWeeks: true,
        toolbarPlacement: 'bottom',
        showTodayButton: false,
        tooltips: tooltips
    });
}

function configTimePicker(id) {
    $(id).datetimepicker({
        locale: 'es',
        format: 'HH:mm',
        toolbarPlacement: 'bottom',
        showClear: true,
        showClose: true,
        tooltips: tooltips
    });
}

function getAgeInYears(date) {
    var secondDate = moment();
    var duration = moment.duration(secondDate.diff(date));
    var years = duration.asYears();
    //console.log(years)
    //console.log(Math.floor(years))

    return Math.floor(years);
}
