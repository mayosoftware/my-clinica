var routeCreate = null;
var routeDelete = null;
var routeEvents = null;
var $form_citas = null;
var routePusher = null;

function setupCitas(_routeCreate, _routeDelete, _routeEvents, _routePusher) {
  routeCreate = _routeCreate;
  routeDelete = _routeDelete;
  routeEvents = _routeEvents;
  routePusher = _routePusher;

  $form_citas = $('.programar-cita-form');

  var calendar = $('#agenda').fullCalendar({
    header: {
      left: 'prev, next today',
      center: 'title',
      right: 'agendaWeek, agendaDay'
    },
    minTime: '8:00:00',
    eventAfterRender: function(event, element, view) {
      // Se añaden los iconos
      $(element).find("div.fc-title").empty();

      var alerts = '';

      // Si tiene historial clínico
      if (event.historialClinico > 0) {
        alerts = ' <i class="fa fa-2x fa-notes-medical" aria-hidden="true" style="color:yellow;"></i> ';
      }

      // Si tiene notas
      if (event.notas) {
        alerts += ' <i class="fa fa-2x fa-comment-alt" aria-hidden="true"></i> ';
      }

      if (alerts) {
        $(element).find("div.fc-title").append(alerts);
      }

      $(element).find("div.fc-title").append(event.title);
    },
    defaultDate: moment(),
    height: 350,
    editable: false,
    slotDuration: '00:30:00',
    scrollTime: '08:00:00',
    weekends: true,
    defaultView: 'agendaDay',
    draggable: false,
    locale: 'es',
    buttonIcons: true,
    dayClick: function(date, jsEvent, view) {
      $form_citas.find( 'input[name="hora_inicio"], input[name="hora_fin"]' ).off('dp.change');
      fillProgramacion(date);
    },
    viewRender: function(view, element){
      // Cargar las citas del día cuando se actualiza la vista
      var viewDate = view.intervalStart;
      loadEvents(viewDate);
    }
  });

  configDatePicker($form_citas.find('input[name="fecha"]'));
  configTimePicker($form_citas.find('input[name="hora_inicio"]'));
  configTimePicker($form_citas.find('input[name="hora_fin"]'));

  refreshActions();
  setupEventos();

  $form_citas.append('<div class="div-disabled"></div>');
}

function resetCitas() {
  $('#agenda').fullCalendar('gotoDate', moment());
  changeProgramacionState(false);
}

function setupEventos() {
  // Cargar eventos al cambiar la pestaña
   $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
     var target = $(e.target).attr("href") // activated tab
     if (target == '#citas') {
       $('#agenda').fullCalendar('render');
       loadEvents();
     }
   });

  // Cambiar fecha agenda al elegir fecha en el datepicker
   $form_citas.find('input[name="fecha"]').on("dp.change", function (e) {
     if (e.date) {
         $('#agenda').fullCalendar('gotoDate', e.date);
     }
   });

   $form_citas.find('input[name="hora_inicio"], input[name="hora_fin"]').on('dp.change', cambiarFechas);
   // Controlar cambios en el campo Tiempo cuando se
   $form_citas.find('input[name="tiempo"]').keyup(cambiarTiempo);
   // Controlar cambios en el campo Tiempo cuando se utiliza el selector
   $form_citas.find('input[name="tiempo"]').change(cambiarTiempo);
}

function activarCitas() {
  $('.alert-citas-group').addClass('hidden');
  $('.citas-tratamiento-group').removeClass('hidden');
}

function desactivarCitas() {
  $('.alert-citas-group').removeClass('hidden');
  $('.citas-tratamiento-group').addClass('hidden');
}

function refreshActions() {
  $('.crear-cita').unbind().click( crearCita );
  $('.borrar-cita').unbind().click( borrarCita )
}

function loadCitasFromLineas(lineas) {
  $('#citasTable tbody').empty();
  if (lineas) {
    $.each(lineas, function( index, linea ) {

      var row =
      ' <tr>' +
      '   <td><span name="descripcion['+ linea.id +']">'+ linea.descripcion +'</span></td>' +
      '   <td>'+ linea.total +'</td>' +
      '   <td>';

      if (linea.realizada == true) {
          row = row + '     <span class="label label-success">SI</span>';
      } else {
          row = row + '     <span class="label label-default">NO</span>';
      }

      row = row +
      '   </td>' +
      '   <td>' +
      '     <div class="actions" name="actions['+ linea.id +']">';

      if (linea.realizada == true) {
          row = row + linea.fecha_realizacion_format;
      } else if (linea.programacion) {
          row = row +
          '         <span class="fecha-cita" name="fecha-cita['+ linea.id +']">'+ linea.fecha_inicio_completa +'</span>' +
          '         <a class="btn-sm btn-danger borrar-cita" name="borrar-cita['+ linea.id +']" role="button"' +
          '           data-id="'+ linea.programacion_id +'"><i class="fas fa-trash"></i></a>';
      } else {
          row = row +
          '         <a class="btn-sm btn-info crear-cita" name="crear-cita['+ linea.id +']"' +
          '           data-id="' +linea.id+ '"><i class="fa fa-calendar-alt"></i></a>';
      }

      row = row +
      '     </div>' +
      '   </td>' +
      ' </tr>';

      $('#citasTable tbody').append(row);

      refreshActions();

    });
  }
}

function changeProgramacionState(enabled, message = "Seleccione un tratamiento para crear o modificar una cita") {
    if (enabled == true) {
      $('.programacion-cita').removeClass('dimmed');
      $('.alert-tratamiento').removeClass('alert-warning');
      $('.alert-tratamiento').addClass('alert-info');
      $('.alert-tratamiento').text(message);
    } else {
      $('.programacion-cita').addClass('dimmed');
      $('.alert-tratamiento').addClass('alert-warning');
      $('.alert-tratamiento').removeClass('alert-info');
      $('.alert-tratamiento').text(message);
    }
}

function loadEvents(fecha = null) {
  // Obtener los datos de la agenda
  var url = routeEvents;
  if (fecha == null) {
    url = url.replace("__fecha", moment(new Date).format('YYYY-MM-DD'));
  } else {
    url = url.replace("__fecha", fecha);
  }

  ajaxRequest(url, null, 'POST', function(json) {
    if (json.data) {
      console.log(json.data);
      // console.log(data);

      var events = json.data.events;
      // Añadir evento actualizado
      $('#agenda').fullCalendar('removeEvents');
      $('#agenda').fullCalendar('renderEvents', events, true);
    }
  });
}

function fillProgramacion(date) {
  var $form_citas = $('.programar-cita-form');
  $form_citas.find('input[name="fecha"]').data("DateTimePicker").date(moment(date, "DD/MM/YYYY"));
  $form_citas.find('input[name="hora_inicio"]').data("DateTimePicker").date(moment(date, "HH:MM:SS"));
  $form_citas.find('input[name="hora_fin"]').data("DateTimePicker").date(moment(date.add(1, 'hour'), "HH:MM:SS"));
  $form_citas.find('input[name="tiempo"]').val('60');
}

function clearProgramacion() {
  var $form_citas = $( '.programar-cita-form' );
  $form_citas.find("input[name='tiempo']").val('');
  $form_citas.find("input[name='fecha']").data("DateTimePicker").clear();
  $form_citas.find("input[name='hora_inicio']").data("DateTimePicker").clear();
  $form_citas.find("input[name='hora_fin']").data("DateTimePicker").clear();
}

function cambiarFechas(e) {
  if (e.date) {
    var fin = $form_citas.find('input[name="hora_fin"]').data("DateTimePicker").date();
    var ini = $form_citas.find('input[name="hora_inicio"]').data("DateTimePicker").date();

    if (fin && ini) {
      var timefin = fin.format('HH:mm');
      var timeini = ini.format('HH:mm');

      var tiempo = moment(timefin, "HH:mm").diff(moment(timeini, "HH:mm"), 'minutes');

      // Se controla que la hora de inicio sea menor que la de fin
      if (tiempo < 0) {
          $form_citas.find('input[name="hora_fin"]').data("DateTimePicker").date(ini.add(1, 'minutes'));
          toastr.error("La hora de inicio no puede ser anterior");
      } else {
          $form_citas.find('input[name="tiempo"]').val(tiempo);
      }
    }
  }
}

function cambiarTiempo() {
  var tiempo = $(this).val() ? $(this).val() : 0,
    ini = $form_citas.find('input[name="hora_inicio"]').data("DateTimePicker").date(),
    fin = $form_citas.find('input[name="hora_fin"]').data("DateTimePicker").date();

  if (tiempo < 0 || !$.isNumeric(tiempo)) {
    toastr.error("El tiempo debe ser numérico y mayor que 0");
    tiempo = 1;
    $(this).val(1);
  }

  if (ini) {
    $form_citas.find('input[name="hora_fin"]').data("DateTimePicker").date(ini.add(tiempo, 'minutes'));
  } else if (fin) {
    $form_citas.find('input[name="hora_inicio"]').data("DateTimePicker").date(fin.add(-tiempo, 'minutes'));
  }
}

// Click sobre crear cita para linea
function crearCita() {
    var id = $(this).attr('data-id');

    // Cambiar action del formulario
    var action = routeCreate + "/" + id;
    $('.programar-cita-form').attr('action', action);

    refreshActions();
    //Modificar aviso de selección de tratamiento
    var desc = $('span[name="descripcion[' + id + ']"]').text();
    changeProgramacionState(true, desc.toUpperCase());
}

// Click sobre borrar cita de linea
function borrarCita() {
    var id = $(this).attr('data-id');

    var url = routeDelete + "/" + id;

    ajaxRequest(url, null, 'DELETE', function(json) {
      if (json.data) {
        var linea_id = json.data.linea_id;
        var programacion_id = json.data.programacion_deleted;
        // Actualizar las acciones
        $('span[name="fecha-cita[' + linea_id + ']"]').remove();
        $('a[name="borrar-cita[' + linea_id + ']"]').remove();

        // Añadir botón de creación de cita
        $('div[name="actions[' + linea_id + ']"]').append(
          ' <a class="btn-sm btn-info crear-cita" name="crear-cita['+ linea_id +']" data-id="'+ linea_id +'">' +
          '   <i class="fa fa-calendar-alt"></i>' +
          ' </a>'
        );

        refreshActions();
        loadEvents();
        clearProgramacion();
        changeProgramacionState(false);

        // Send pusher
        var dataPusher = {
          event: 'agenda.deleted',
          programacion_id: programacion_id,
          socket_id: $('.socketId').val()
        };

        ajaxRequest(routePusher, dataPusher, 'POST', function(json) {
          console.log(json);
        });
      }
    });

    return false;
}

// Submit del formulario
$( ".programar-cita-form" ).submit(function( event ) {
  event.preventDefault();

  var $form_citas = $( '.programar-cita-form' ),
    url = $form_citas.attr( "action" ),
    data = $form_citas.serialize();

  ajaxRequest(url, data, 'POST', function(json) {
    if (json.data) {
      var linea_id = json.data.linea_id,
        programacion_id = json.data.programacion_id,
        fecha_inicio = json.data.fecha_inicio;

      // Actualizar fecha seleccionada
      $('div[name="actions[' + linea_id + ']"]').append(
        ' <span class="fecha-cita" name="fecha-cita['+ linea_id +']">'+ fecha_inicio +'</span> ' +
        ' <a class="btn-sm btn-danger borrar-cita" name="borrar-cita['+ linea_id +']" role="button" ' +
        '   data-id="'+ programacion_id +'"><i class="fas fa-trash"></i> ' +
        ' </a> '
      );

      $('a[name="crear-cita[' + linea_id + ']"]').remove();

      refreshActions();
      loadEvents();
      clearProgramacion();
      changeProgramacionState(false);

      // Send pusher
      var dataPusher = {
        event: 'agenda.updated',
        programacion_id: programacion_id,
        socket_id: $('.socketId').val()
      };

      ajaxRequest(routePusher, dataPusher, 'POST', function(json) {
        console.log(json);
      });
    }
  });

});
