  var idTratamiento = null;
  var rows = 0;
  var routeProgramacion = null;
  var routeDestroy = null;

  function setupTratamiento(id, route1, route2) {
    idTratamiento = id;
    routeTratamiento = route1;
    routeDestroy = route2;
    console.log(idTratamiento);
    console.log(routeTratamiento);
  }

  $( document ).ready(function() {

    // Botón envío de email
    $( '#send_mail' ).click(function() {
      $('#voyager-loader').show();
      var url = '/admin/tratamiento-paciente/' + idTratamiento + '/sendMail';
      var posting = $.post( url );

      posting.done(function( data ) {
        $('#voyager-loader').hide();
        if (data["alert-type"] == "success") {
          toastr.success(data.message);
        } else {
          toastr.error(data.message);
        }
      });
      posting.fail(function() {
        $('#voyager-loader').hide();
        toastr.error("Se ha producido un error");
      })
    });

    // Botón crear presupuesto
    $(document).on('click', '.crear-presupuesto', function(event) {
      if (cambiosEnImportes) {
        alert("Antes de crear un presupuesto, tienes que guardar los cambios del tratamiento haciendo click en 'Guardar'");
        return;
      }

      $('#presupuestoModal').modal('show');
    });

    // Botón crear factura
    $(document).on('click', '.crear-factura', function(event) {
      if (cambiosEnImportes) {
        alert("Antes de crear una factura, tienes que guardar los cambios del tratamiento haciendo click en 'Guardar'");
        return;
      }

      $('#facturarModal').modal('show');
    });

    // Crear cobro
    $(document).on('click', '.create-cobro', function(event){
      if (cambiosEnImportes) {
        alert("Antes de registrar una cobro, tienes que guardar los cambios del tratamiento haciendo click en 'Guardar'");
        return;
      }

      $('#cobrarModal').modal('show');
    });

    // // Crear programación
    // $(document).on('click', '.create-programacion', function(event){
    //   if (cambiosEnImportes) {
    //     alert("Antes de registrar una programación, tienes que guardar los cambios del tratamiento haciendo click en 'Guardar'");
    //     return;
    //   }
    //   var $form = $( '#programacionForm' );
    //   var action = routeTratamiento;
    //   $form.attr('action', action);
    //   $form.find('input[name="_name"]').remove();
    //   $form.find('input[name="id"]').remove();
    //   // $form.find("select[name='doctor_id']").val('');
    //
    //   // Por defecto se marca programacion conjunta
    //   $form.find("input[name='programacion_conjunta']").prop('checked', 'true');
    //   $form.find("input[name='programacion_conjunta']").removeAttr('disabled');
    //   $form.find('div[name="lineasDiv"]').addClass('hidden');
    //   $form.find("select[name='linea']").removeAttr('disabled');
    //   $form.find("select[name='linea']").val('');
    //   // Abrir calendario en el día de hoy
    //   $('#agenda').fullCalendar('gotoDate', moment(new Date));
    //
    //   $('#programarModal').modal('show');
    // });

    // // Editar programación conjunta
    // $(document).on('click', '.edit', function(event){
    //   var id = event.target.id;
    //   var name = event.target.name;
    //   var url = '{{ route('admin.programaciones.getProgramacion', ['id' => '__id']) }}'.replace('__id', id);
    //   var posting = $.post( url );
    //   posting.done(function( data ) {
    //     if (!data.errors) {
    //       console.log(data);
    //       var programacion = data.programacion;
    //       var $form = $( '#programacionForm' );
    //       $form.find("input[name='tiempo']").val(programacion.tiempo);
    //       $form.find("input[name='fecha']").data("DateTimePicker").date(moment(programacion.fecha, "YYYY-MM-DD HH:mm:ss.sssZZ"));
    //       $form.find("input[name='hora_inicio']").data("DateTimePicker").date(moment(programacion.hora_inicio, "HH:mm:ss"));
    //       $form.find("input[name='hora_fin']").data("DateTimePicker").date(moment(programacion.hora_fin, "HH:mm:ss"));
    //       $form.find("select[name='doctor_id']").val(programacion.doctor_id);
    //
    //       if (name == 'editarProgramacion') {
    //         // Al ser programación de conjunta se activa el checkbox 'conjunta'
    //         $form.find("input[name='programacion_conjunta']").prop('checked', 'true');
    //         $form.find("input[name='programacion_conjunta']").prop('disabled', 'true');
    //
    //         // Se oculta el select de lineas disabled con la linea
    //         $form.find("select[name='linea']").val(null);
    //         $form.find('div[name="lineasDiv"]').addClass('hidden');
    //
    //       } else if (name == 'editarProgramacionLinea') {
    //         var linea = data.linea;
    //         // Al ser programación de linea se desactiva el checkbox 'conjunta'
    //         $form.find("input[name='programacion_conjunta']").removeAttr('checked');
    //         $form.find("input[name='programacion_conjunta']").prop('disabled', 'true');
    //
    //         // Se muestra el select de lineas disabled con la linea
    //         $form.find('div[name="lineasDiv"]').removeClass('hidden');
    //         $form.find("select[name='linea']").prop('disabled', 'true');
    //         $form.find("select[name='linea']").val(linea.id);
    //       }
    //
    //       // Se calcula la fecha y hora de inicio de la actuación
    //       let dateStr = moment(programacion.fecha, "YYYY-MM-DD HH:mm:ss.sssZZ").format("YYYY-MM-DD"),
    //           timeStr = programacion.hora_inicio,
    //           date    = moment(dateStr + ' ' + timeStr, "YYYY-MM-DD HH:mm:ss");
    //
    //       // Navegar a la fecha del evento
    //       $('#agenda').fullCalendar('gotoDate', date);
    //       // $('#agenda').fullCalendar('render');
    //
    //       // Se cambia el action del form para editar el evento seleccionado
    //       var action = '{{ route('voyager.programaciones.update', ['id' => '__id']) }}'.replace('__id', programacion.id);
    //       $form.attr('action', action);
    //
    //       // Al ser una acción de update, se añade el método PUT
    //       $form.append('<input type="hidden" name="_method" value="PUT">');
    //
    //     } else {
    //       toastr.error("No se han podido obtener los datos");
    //     }
    //   });
    //   posting.fail(function() {
    //     alert( "error" );
    //   });
    //
    // });

    // Eliminar programación tratamiento
    $(document).on('click', '.delete', function(event){
      $('.delete_form').attr('action', routeDestroy);
      $('#delete_modal').modal('show');
    });
});
