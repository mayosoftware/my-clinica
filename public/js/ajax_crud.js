function ajaxRequest(url, data, type, cb) {

  // Se añade token en estas operaciones (CSRF Protection)
  if (type == 'POST' ||
      type == 'PUT' ||
      type == 'DELETE') {

      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });
  }

  var response = {
      error: false,
      message: null
  };

  $.ajax({
    url: url,
    type: type,
    data: data,
    success: function(data) {
      if (data["alert-type"] == "success") {
        if (data.message) {
          toastr.success(data.message);
        }
        response["error"] = false;
        response["message"] = null;
        response["data"] = data;
      } else if (data["alert-type"] == "warning") {
        if (data.message) {
          toastr.warning(data.message);
        }
        response["error"] = false;
        response["message"] = null;
        response["data"] = data;
      } else {
        if (data.message) {
          toastr.error(data.message);
        }
        response["error"] = true;
        response["message"] = data.message;
        response["data"] = data;
      }
      cb(response);
    },
    error: function() {
      toastr.error("Se ha producido un error");
      response["error"] = true;
      if (data && data.message) {
        response["message"] = data.message;
      }
      cb(response);
    }
  });
}
